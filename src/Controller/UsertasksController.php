<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */



namespace App\Controller;
use Cake\ORM\TableRegistry;
use Cake\ORM\Table;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Event\Event;
use Cake\I18n\Time;



/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */

class UsertasksController extends AppController {

    /**
     * Displays a view
     *
     * @return void|\Cake\Network\Response
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
  	public function beforeFilter(Event $event){
	    parent::beforeFilter($event);

	    $this->Auth->allow([]);
	}
		
	public function initialize()
	{
		parent::initialize();
		$this->loadComponent('Stripe');
	}

    public function taskview() {
    	$this->viewBuilder()->layout('admin');
    	$listing = array();
    	
    	$limit = 5;
    	$select = [];
		$conditions = [];
		$contain = ['Services','Subservices','offered_service','offered_subservice',
			'Users'=> function ($q) {
    			return $q
    			->select(['Users.id','Users.firstname','Users.lastname']);
			},
			'Userwith'=> function ($q) {
				return $q
				->select(['Userwith.id','Userwith.firstname','Userwith.lastname']);
			}
		];
        $requestData=$this->request->query;

        if (isset($requestData['q']) && !empty($requestData['q'])) {
            $conditions['Usertasks.id'] = trim($requestData['q']);
        }
        if (isset($requestData['limit']) && !empty($requestData['limit'])) {
            $limit = $requestData['limit'];
        }
        
        $this->paginate = [
            'limit' => $limit,
            'select' => $select,
            'conditions' => $conditions,
            'contain' => $contain
        ];
        $data = $this->paginate();
		$response = $this->Usertasks->getDataForUsertaskView($data);
		
		$this->set('response',$response);
        $this->set('action','taskview');
        $this->set('csv_export',1);
    }
    
    public function tasklist() {
        $this->viewBuilder()->layout('admin');
        $listing = array();
        $limit = 5;
        $select = [];
        $conditions = [];
        $contain = ['Services','Subservices','offered_service','offered_subservice',
            'Users'=> function ($q) {
                return $q
                ->select(['Users.id','Users.firstname','Users.lastname']);
            },
            'Userwith'=> function ($q) {
                return $q
                ->select(['Userwith.id','Userwith.firstname','Userwith.lastname']);
            }
        ];
        $requestData=$this->request->query;

        if (isset($requestData['q']) && !empty($requestData['q'])) {
            $conditions['Usertasks.id'] = trim($requestData['q']);
        }
        if (isset($requestData['limit']) && !empty($requestData['limit'])) {
            $limit = $requestData['limit'];
        }
        $this->paginate = [
            'limit' => $limit,
            'select' => $select,
            'conditions' => $conditions,
            'contain' => $contain
        ];
        $data = $this->paginate();
        $response = $this->Usertasks->getDataForUsertaskList($data);
        $this->set(compact('response'));
        $this->set('action','tasklist');
        $this->set('csv_export',1);
    }
        
    public function tasksummary() {
    	$this->viewBuilder()->layout('admin');
    	$listing = array();

    	$limit = 10;
    	$select = [];
		$conditions = [];
		$contain = ['Services','Usertasks'];
        $requestData=$this->request->query;

        if (isset($requestData['q']) && !empty($requestData['q'])) {
            $conditions['Subservices.subservice_name like'] = "%" . trim($requestData['q']) . "%";
        }
        if (isset($requestData['limit']) && !empty($requestData['limit'])) {
            $limit = $requestData['limit'];
        }
        $Subservices = TableRegistry::get('Subservices');
        $this->paginate = [
            'limit' => $limit,
            'select' => $select,
            'conditions' => $conditions,
            'contain' => $contain
        ];
        $data = $this->paginate('Subservices');

    	$listing = $Subservices->get_tasksummary($data);
    	$this->set(compact('listing'));
    	$this->set('action','tasksummary');
        $this->set('csv_export',1);
    }
    
    /*
     * user management - user detail list view at transaction level
     */
    public function userlistview()
    {
    	$this->viewBuilder()->layout('admin');
    	$listing = array();

    	$limit = 10;
    	$contain 	= ['Users','Subservices','offered_subservice'];
        $requestData=$this->request->query;

        if (isset($requestData['q']) && !empty($requestData['q'])) {
            $conditions['Usertasks.id'] = trim($requestData['q']);
        }
        
        $this->paginate = [
            'limit' => $limit,
            'contain' => $contain,
            'order' => ['Usertasks.id'=>'DESC']
        ];
        $data = $this->paginate();
    	$response = $this->Usertasks->getDataForUserlistview($data);
    	$this->set('response',$response);
        $this->set('action','userlistview');
        $this->set('csv_export',1);
    }
    
    public function getinvoicedata($id=null)
    {
        $this->viewBuilder()->layout('ajax');
    	if($id)
    	{
    		$select = [];
    		$condition = ['Usertasks.id' => $id];
    		$contain = ['Services','Subservices','offered_service','offered_subservice',
	    		'Users'=> function ($q) {
	    			return $q
	    			->select(['Users.id','Users.firstname','Users.lastname','Users.postcode','Users.suburb','Users.city','Users.state','Users.country','Users.email']);
	    		},
	    		'Userwith'=> function ($q) {
	    			return $q
	    			->select(['Userwith.id','Userwith.firstname','Userwith.lastname','Userwith.postcode','Userwith.suburb','Userwith.city','Userwith.state','Userwith.country','Userwith.email']);
	    		}
    		];
    		$result = $this->Usertasks->getinvoicedtransaction($select,$condition,$contain);
    	}
    	$select = ['fee_percent','gst_tax','from_address'];
    	$this->loadModel('Adminsettings');
    	$adminsetting = $this->Adminsettings->getsettings($select);
    	
    	$data = array();
    	$data['id'] 				= $result['id'];
    	$time = new Time();
    	$date = $time::now();
    	$data['date'] 				= date_format($date,'Y-m-d');
    	$data['name'] 				= $result['user']['firstname']." ".$result['user']['lastname'];
    	$data['postcode'] 			= $result['user']['postcode'];
    	$data['suburb'] 			= $result['user']['suburb'];
    	$data['city'] 				= $result['user']['city'];
    	$data['state'] 				= $result['user']['state'];
    	$data['country'] 			= $result['user']['country'];
    	$data['email'] 				= $result['user']['email'];
    	$data['invoice_no'] 		= $result['id'];
    	$data['account_no'] 		= '********';
    	$data['item'] 				= 1;
    	$data['service_id'] 		= $result['offer_serviceid'];
    	$data['service_provider_name'] = $result['userwith']['firstname']." ".$result['userwith']['lastname'];
    	$data['status'] 			= ($result['status'] == '0') ? 0 : (($result['status'] == '1') ? 1 : 2);
    	$data['description'] 		= $result['offer_note'];
    	$data['completion_date'] 	= date_format($result['offer_completion_datetime'],'Y-m-d');
    	$data['location'] 			= $result['offer_location'];
    	$data['total'] 				= $result['amount'];
    	$data['gst_tax'] 			= $adminsetting['gst_tax'];
    	$data['tax_amount'] 		= $result['tax'];
    	$data['fee_percent'] 		= $adminsetting['fee_percent'];
    	$data['fee_amount'] 		= $result['fee'];
    	$data['grand_total'] 		= $result['amount'] + $result['tax'] + $result['fee'];
    	$data['admin_address'] 		= $adminsetting['from_address'];
    	$data['payment_status'] 	= ($result['payment_status'] == '0') ? 0 : 1;
    	$this->set(compact('data'));
    }
    
    public function invoicelist(){
    	$this->viewBuilder()->layout('admin');
    	$listing = array();

    	$limit = 10;
        $select = ['id','status','amount','need_serviceid','offer_serviceid','need_note','offer_note','need_location','offer_location','fee','tax','createdAt'];
    	$conditions["status"] = '1';
    	$contain = [
	    	'Users'=> function ($q) {
	    		return $q
	    		->select(['Users.id','Users.firstname','Users.lastname','Users.postcode','Users.suburb','Users.city','Users.state','Users.country']);
	    	},
	    	'Userwith'=> function ($q) {
	    		return $q
	    		->select(['Userwith.id','Userwith.firstname','Userwith.lastname','Userwith.postcode','Userwith.suburb','Userwith.city','Userwith.state','Userwith.country']);
	    	}
    	];
        $requestData=$this->request->query;

        if (isset($requestData['q']) && !empty($requestData['q'])) {
            $conditions['email like'] = "%" . trim($requestData['q']) . "%";
        }
        if (isset($requestData['limit']) && !empty($requestData['limit'])) {
            $limit = $requestData['limit'];
        }
        
        $this->paginate = [
            'limit' => $limit,
            'select' => $select,
            'conditions' => $conditions,
            'contain' => $contain
        ];
        $data = $this->paginate();
        $listing = $this->Usertasks->getalltransaction($data);
        $this->set('listing',$listing);
        $this->set('action','invoicelist');
        $this->set('csv_export',1);
    }

    public function currentoffer(){
    	$this->viewBuilder()->layout('admin');
    	$listing = array();

    	$limit = 10;
        $select = ['id','status','amount','need_serviceid','offer_serviceid','need_note','offer_note','need_location','offer_location','fee','tax','createdAt'];
    	$conditions["status"] = '0';
    	$conditions['payment_status'] = '0';
    	$contain = [
	    	'Users'=> function ($q) {
	    		return $q
	    		->select(['Users.id','Users.firstname','Users.lastname','Users.postcode','Users.suburb','Users.city','Users.state','Users.country']);
	    	},
	    	'Userwith'=> function ($q) {
	    		return $q
	    		->select(['Userwith.id','Userwith.firstname','Userwith.lastname','Userwith.postcode','Userwith.suburb','Userwith.city','Userwith.state','Userwith.country']);
	    	}
    	];
        $requestData=$this->request->query;

        if (isset($requestData['q']) && !empty($requestData['q'])) {
            $conditions['email like'] = "%" . trim($requestData['q']) . "%";
        }
        if (isset($requestData['limit']) && !empty($requestData['limit'])) {
            $limit = $requestData['limit'];
        }
        
        $this->paginate = [
            'limit' => $limit,
            'select' => $select,
            'conditions' => $conditions,
            'contain' => $contain
        ];
        $data = $this->paginate();
        $listing = $this->Usertasks->getalltransaction($data);
        $this->set('listing',$listing);
        $this->set('action','currentoffer');
        $this->set('csv_export',1);
    }
    
    public function payByStrip(){
        $this->viewBuilder()->layout('ajax');
		$response = '';
    	if(!empty($this->request->query['offer_id'])){
    		$offer_id = $this->request->query['offer_id'];
    		$this->loadModel('Usertasks');
        	$select = $condition = $contain = array();
        	$condition = array('Usertasks.id' => $offer_id);
            $contain = array('Users','Userwith');
            $result = $this->Usertasks->getRecordstoController($select,$condition,$contain);
	        if($result){
    	        $result = $result[0];
		    	if($result['payment_status'] == 0 && ($result['offer_serviceid'] == 8 || $result['need_serviceid'] == 8)){
	            	$response = $this->initiate_payment($result);
					if($response == 1){
	            		$datatoSave['payment_status'] = 1;
	            		$datatoSave['payment_date'] = date('Y-m-d');
	            		$datatoSave['status'] = '1';
	            		$this->Usertasks->updatedata($offer_id, $datatoSave);
	            	}
		    	}
    	    }
    	}
		echo json_encode($response);
        exit();
    }
}
