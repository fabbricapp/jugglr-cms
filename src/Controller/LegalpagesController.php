<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */



namespace App\Controller;
use Cake\ORM\TableRegistry;
use Cake\ORM\Table;



use Cake\Core\Configure;

use Cake\Network\Exception\NotFoundException;

use Cake\View\Exception\MissingTemplateException;

use Cake\Event\Event;



/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */

class LegalpagesController extends AppController {

    /**
     * Displays a view
     *
     * @return void|\Cake\Network\Response
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function beforeFilter(Event $event){
	    parent::beforeFilter($event);
	
	    $this->Auth->allow(['add']);		
	}

    public function add() {
        $this->viewBuilder()->layout('admin');
	    if($this->request->data){
    		
    	}
    	$this->loadModel('Legalpages');
    	$users = TableRegistry::get('Legalpages');
    	$article = $users->find('all')->toArray();
    	if($article){
    		foreach ($article as $key=>$value){
    			if($value['page_name'] == 'terms_conditions'){
	    			$response['TC']['id'] = $value['id'];
	    			$response['TC']['page_name'] = $value['page_name'];
	    			$response['TC']['page_content'] = $value['page_content'];
    			}elseif ($value['page_name'] == 'policies'){
	    			$response['PP']['id'] = $value['id'];
	    			$response['PP']['page_name'] = $value['page_name'];
	    			$response['PP']['page_content'] = $value['page_content'];
    			}elseif ($value['page_name'] == 'FAQ'){
	    			$response['FQ']['id'] = $value['id'];
	    			$response['FQ']['page_name'] = $value['page_name'];
	    			$response['FQ']['page_content'] = $value['page_content'];
    			}
    		}
    	}
    	$this->set(compact('response'));
    }
    
    public function setting(){
        $this->viewBuilder()->layout('admin');
    	$this->loadModel('Adminsettings');
    	if($this->request->data){
    		foreach ($this->request->data as $key => $value) {
    			$data[$key] 	= $value;
    		}
    		$settings = $this->Adminsettings->addsetting($data);
            if($settings){
                $this->Flash->success('Settings updated!');
            }
    	}
    	$settings = $this->Adminsettings->getsettings();
    	
    	$this->set(compact('settings'));
    }
}
