<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */



namespace App\Controller;
use Cake\ORM\TableRegistry;
use Cake\ORM\Table;



use Cake\Core\Configure;

use Cake\Network\Exception\NotFoundException;

use Cake\View\Exception\MissingTemplateException;

use Cake\Event\Event;



/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */

class PromotionsController extends AppController {


    /**
     * Displays a view
     *
     * @return void|\Cake\Network\Response
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function add()
    {
        $this->viewBuilder()->layout('admin');
        if($this->request->data){
            if($this->request->data['promotiondate'] != '' && $this->request->data['accessibility'] != '' && $this->request->data['type'] != '')
            {
                $data['promotion_type'] = 1;
                $date = explode('-',$this->request->data['promotiondate']);
                $data['startdate'] = str_replace('/','-',trim($date[0]));
                $data['enddate'] = str_replace('/','-',trim($date[1]));
                $data['accessibility'] = $this->request->data['accessibility'];
                $data['type'] = $this->request->data['type'];
                $data['title'] = $this->request->data['title'];
                $data['description'] = $this->request->data['description'];
                $data['usage_cnt'] = $this->request->data['usage_cnt'];
                $data['url'] = $this->request->data['url'];
                if(isset($this->request->data['conditionaltype'])){
                    $data['conditionaltype'] = $this->request->data['conditionaltype'];
                }
                
                if($this->request->data['type'] == 1 && $this->request->data['percent'] == ''){
                    $this->request->data['message'] = 'Percentage not set.';
                }
                if($this->request->data['type'] == 2 && $this->request->data['amount'] == ''){
                    $this->request->data['message'] = 'Amount not set.';
                }
                if($this->request->data['type'] == 3 && $this->request->data['offeramount'] == ''){
                    $this->request->data['message'] = 'Offeramount not set.';
                }
                if($this->request->data['accessibility'] == 2 && $this->request->data['users'] == ''){
                    $this->request->data['message'] = 'Users IDs not set.';
                }
                $data['percentage'] = $this->request->data['percent'];
                $data['amount'] = $this->request->data['amount'];
                $data['users'] = $this->request->data['users'];
                $data['offeramount'] = $this->request->data['offeramount'];
                $data['usage_type'] = $this->request->data['usage_type'];
                if(isset($this->request->data['message'])){
                    $this->Flash->error(__($this->request->data['message']));
                    $promotion = $this->request->data;
                }else {
                    $this->loadModel('Promotions');
                    $id = $this->Promotions->addPromotion($data);
                    if($id && $this->request->data['image'] != ''){
                        $imagedata = array();
                        $imagedata[0]['key']      = 'promotionimages';
                        $imagedata[0]['file']     = $_FILES['image'];
                        $this->Userimages = TableRegistry::get('Userimages');
                        $path = $this->Userimages->uploadimage($imagedata);
                        if(isset($path[0])){
                            $data = array();
                            $data['image'] = BASE_URL."/img/promotionimages/".$path[0];
                            $this->Promotions->updatedata($id, $data);
                        }
                    }

                    $promotion['promotiondate'] = $promotion['type'] = $promotion['accessibility'] = $promotion['amount'] = $promotion['percent'] = $promotion['users'] = $promotion['offeramount'] = $promotion['title'] = $promotion['usage_cnt'] = $promotion['description'] = $promotion['url'] = '';
                    $this->Flash->success('Promotion added successfully!');
                }
            }else {
                $this->Flash->error(__('Please fill needed information for promotion.'));
            }
            return $this->redirect(['controller'=>'promotions','action'=>'view']);
        }else {
            $promotion['promotiondate'] = $promotion['type'] = $promotion['accessibility'] = $promotion['amount'] = $promotion['percent'] = $promotion['users'] = $promotion['offeramount'] = $promotion['title'] = $promotion['usage_cnt'] = $promotion['description'] = $promotion['url'] = '';
        }
        $this->set(compact('promotion'));
    }

    public function informationalimage()
    {
        $this->viewBuilder()->layout('admin');
        if($this->request->data){
            $data['promotion_type'] = 2;
            $data['url'] = $this->request->data['url'];
            
            $this->loadModel('Promotions');
            $id = $this->Promotions->addPromotion($data);
            if($id && $this->request->data['image'] != ''){
                $imagedata = array();
                $imagedata[0]['key']      = 'promotionimages';
                $imagedata[0]['file']     = $_FILES['image'];
                $this->Userimages = TableRegistry::get('Userimages');
                $path = $this->Userimages->uploadimage($imagedata);
                if(isset($path[0])){
                    $data = array();
                    $data['image'] = BASE_URL."/img/promotionimages/".$path[0];
                    $this->Promotions->updatedata($id, $data);
                }
            }

            $promotion['url'] = '';
            $this->Flash->success('Informational Promotion added successfully!');
            return $this->redirect(['controller'=>'promotions','action'=>'informationalview']);
        }else {
            $promotion['url'] = '';
        }
        $this->set(compact('promotion'));
    }

    public function view()
    {
        $this->viewBuilder()->layout('admin');
        $select = $contain = [];
        $condition = ['promotion_type' => 1];
        $listing = $this->Promotions->getAllPromotions($select, $condition, $contain);
        //pr($listing);die();
        $this->set(compact('listing'));
    }

    public function informationalview()
    {
        $this->viewBuilder()->layout('admin');
        $select = $contain = [];
        $condition = ['promotion_type' => 2];
        $listing = $this->Promotions->getAllPromotions($select, $condition, $contain);
        $this->set(compact('listing'));
    }

    public function edit()
    {
        $this->viewBuilder()->layout('admin');
        $listing = array();
        if(isset($this->request->query['id']))
        {
            $select = $contain = [];
            $condition = ['id' => $this->request->query['id']];
            $promotion = $this->Promotions->getAllPromotions($select, $condition, $contain);
            $promotion = $promotion[0]->toArray();
            $promotion['startdate'] = date_format($promotion['startdate'],'Y/m/d');
            $promotion['enddate'] = date_format($promotion['enddate'],'Y/m/d');
        }
        $this->set(compact('promotion'));
        
        if(!empty($this->request->data)){
            if($this->request->data['promotiondate'] != '' && $this->request->data['accessibility'] != '' && $this->request->data['type'] != '')
            {
                $date = explode('-',$this->request->data['promotiondate']);
                $data['startdate'] = str_replace('/','-',trim($date[0]));
                $data['enddate'] = str_replace('/','-',trim($date[1]));
                $data['accessibility'] = $this->request->data['accessibility'];
                $data['id'] = $this->request->data['id'];
                $data['type'] = $this->request->data['type'];
                $data['title'] = $this->request->data['title'];
                $data['description'] = $this->request->data['description'];
                $data['usage_cnt'] = $this->request->data['usage_cnt'];
                $data['url'] = $this->request->data['url'];
                if(isset($this->request->data['conditionaltype'])){
                    $data['conditionaltype'] = $this->request->data['conditionaltype'];
                }
                
                if($this->request->data['type'] == 1 && $this->request->data['percent'] == ''){
                    $this->request->data['message'] = 'Percentage not set.';
                }
                if($this->request->data['type'] == 2 && $this->request->data['amount'] == ''){
                    $this->request->data['message'] = 'Amount not set.';
                }
                if($this->request->data['type'] == 3 && $this->request->data['offeramount'] == ''){
                    $this->request->data['message'] = 'Offeramount not set.';
                }
                if($this->request->data['accessibility'] == 2 && $this->request->data['users'] == ''){
                    $this->request->data['message'] = 'Users IDs not set.';
                }
                $data['percentage'] = $this->request->data['percent'];
                $data['amount'] = (isset($this->request->data['amount'])) ? $this->request->data['amount'] : 0.00;
                $data['users'] = (isset($this->request->data['users'])) ? $this->request->data['users'] : '';
                $data['offeramount'] = $this->request->data['offeramount'];
                $data['usage_type'] = $this->request->data['usage_type'];
                if(isset($this->request->data['message'])){
                    $this->Flash->error(__($this->request->data['message']));
                    $promotion = $this->request->data;
                }else {
                    $this->Promotions = TableRegistry::get('Promotions');
                    if($this->request->data['image'] != ''){
                        $imagedata = array();
                        $imagedata[0]['key']      = 'promotionimages';
                        $imagedata[0]['file']     = $_FILES['image'];
                        $this->Userimages = TableRegistry::get('Userimages');
                        $path = $this->Userimages->uploadimage($imagedata);
                        if(isset($path[0])){
                            $data['image'] = BASE_URL."/img/promotionimages/".$path[0];
                        }
                    }
                    $id = $this->Promotions->updatedata($data['id'],$data);

                    $promotion['promotiondate'] = $promotion['type'] = $promotion['accessibility'] = $promotion['amount'] = $promotion['percent'] = $promotion['users'] = $promotion['offeramount'] = $promotion['title'] = $promotion['usage_cnt'] = $promotion['description'] = $promotion['url'] = '';
                    $this->Flash->success('Promotion added successfully!');
                }
                return $this->redirect(['controller'=>'promotions','action'=>'view']);
            }else {
                $this->Flash->error(__('Please fill needed information for promotion.'));
            }
        } 
    }

    public function editinformationalimage()
    {
        $this->viewBuilder()->layout('admin');
        $listing = array();
        if(isset($this->request->query['id']))
        {
            $select = $contain = [];
            $condition = ['id' => $this->request->query['id']];
            $promotion = $this->Promotions->getAllPromotions($select, $condition, $contain);
            $promotion = $promotion[0]->toArray();
        }
        $this->set(compact('promotion'));
        
        if($this->request->data){
            $data['url'] = $this->request->data['url'];
            
            if($this->request->data['image'] != ''){
                $imagedata = array();
                $imagedata[0]['key']      = 'promotionimages';
                $imagedata[0]['file']     = $_FILES['image'];
                $this->Userimages = TableRegistry::get('Userimages');
                $path = $this->Userimages->uploadimage($imagedata);
                if(isset($path[0])){
                    $data = array();
                    $data['image'] = BASE_URL."/img/promotionimages/".$path[0];
                }
            }
            $this->Promotions->updatedata($this->request->data['id'], $data);

            $promotion['url'] = '';
            $this->Flash->success('Informational Promotion added successfully!');
            return $this->redirect(['controller'=>'promotions','action'=>'informationalview']);
        }else {
            $promotion['url'] = '';
        }
    }

    public function updatepromotionstatus()
    {
        $this->viewBuilder()->layout('ajax');
        if($this->request->data)
        {
            $data = $this->Promotions->get($this->request->data['promotion_id']);
            if($data->is_active == 0){
                $data->is_active = 1;
            }else {
                $data->is_active = 0;    
            }
            $result = $this->Promotions->save($data);
            echo true;
            die();
        }
    }
}

 

  





