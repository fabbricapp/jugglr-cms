<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */



namespace App\Controller;
use Cake\ORM\TableRegistry;
use Cake\ORM\Table;



use Cake\Core\Configure;

use Cake\Network\Exception\NotFoundException;

use Cake\View\Exception\MissingTemplateException;

use Cake\Event\Event;



/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */

class AdminsController extends AppController {


    /**
     * Displays a view
     *
     * @return void|\Cake\Network\Response
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */

    public function beforeFilter(Event $event)
	{
	    parent::beforeFilter($event);
	    // Allow users to register and logout.
	    // You should not add the "login" action to allow list. Doing so would
	    // cause problems with normal functioning of AuthComponent.
	    $this->Auth->allow(['login', 'logout','forgotpassword','resetpassword','setPassword','cleanDatabase','stat']);
        $this->loadComponent('Mailer');
        $this->loadComponent('Mailcontent');
	}

	public function login()
	{    
	    //$this->layout = 'login';
        $this->viewBuilder()->layout('login');
	    if ($this->request->is('post')) {
	       $user = $this->Auth->identify();
	        if ($user) {
	            $this->Auth->setUser($user);
	            $this->redirect('/users/index');
	            //$this->redirect('https://jugglr-test-ws.jugglrapp.net/users/index');
	            return $this->redirect($this->Auth->redirectUrl());
	        }
	        $this->Flash->error(__('Invalid username or password, try again'));
	    }
	}
	
	public function logout()
	{
	    $this->Flash->success('You are now logged out.');
        $this->redirect('/admins/login');
        //$this->redirect('https://jugglr-test-ws.jugglrapp.net/admins/login');
	    return $this->redirect($this->Auth->logout());
	}
	
	
	public function forgotpw(){
    	$this->layout = false;
   	}

   	/*
    *	Reset Password
    */
    public function resetpassword()
    {
    	$this->viewBuilder()->layout('login');
    	$requestdata = $_REQUEST;
    	$token = '';
    	if(!empty($requestdata['token']))
    	{
    		$token = $requestdata['token'];
    	}elseif(!empty($requestdata['username'])){
    		$adminObj = TableRegistry::get('Admins');
            $mailData['to'] = $requestdata['username'];
            $result = $adminObj->find()->select(['id'])->where(['username'=>$requestdata['username']])->first();
            $this->Users = TableRegistry::get('Users');
            $savetoData['password_token'] = $this->Users->__getGUID();
            $mailData['url'] = BASE_URL.'/admins/setpassword?token='.$savetoData['password_token'];
            $adminObj->updatedata($result['id'],$savetoData);

            $result = $this->Mailcontent->forgotPassword($mailData);
            if($result){
                $this->Flash->success('Email has been sent to your email address.');
            }else {
                $this->Flash->success('Email could not been sent to your email address.');
            }
    	}
    	$this->set('token',$token);
    }

    public function account()
    {
        $this->viewBuilder()->layout('admin');
        if ($this->request->is('post')) {
            if($this->request->data['image'] != ''){
                $imagedata = array();
                $imagedata[0]['key']      = 'adminimages';
                $imagedata[0]['file']     = $_FILES['image'];
                $this->Userimages = TableRegistry::get('Userimages');
                $path = $this->Userimages->uploadimage($imagedata);
                if(isset($path[0])){
                    $data['image'] = BASE_URL."/img/adminimages/".$path[0];
                }
            }
            if($this->request->data['name'] != ''){
                $data['name'] = $this->request->data['name'];
            }
            $this->Admins = TableRegistry::get('Admins');
            $this->Admins->addData($data,$this->Auth->user('id'));
            $this->Flash->success('Account setting updated!.');
        }
        $this->Admins = TableRegistry::get('Admins');
        $account = $this->Admins->getData($this->Auth->user('id'));
        $this->set(compact('account'));
    }
    public function stat()
    {
        $this->viewBuilder()->layout('admin');
        if ($this->request->is('post')) {
            if($this->request->data['image'] != ''){
                $imagedata = array();
                $imagedata[0]['key']      = 'adminimages';
                $imagedata[0]['file']     = $_FILES['image'];
                $this->Userimages = TableRegistry::get('Userimages');
                $path = $this->Userimages->uploadimage($imagedata);
                if(isset($path[0])){
                    $data['image'] = BASE_URL."/img/adminimages/".$path[0];
                }
            }
            if($this->request->data['name'] != ''){
                $data['name'] = $this->request->data['name'];
            }
            $this->Admins = TableRegistry::get('Admins');
            $this->Admins->addData($data,$this->Auth->user('id'));
            $this->Flash->success('Account setting updated!.');
        }
        $this->Admins = TableRegistry::get('Admins');
        $account = $this->Admins->getData($this->Auth->user('id'));
        $this->set(compact('account'));
    }

    public function setpassword()
    {
        $this->viewBuilder()->layout('login');
        if($this->request->query['token']){
            $adminObj = TableRegistry::get('Admins');
            $result = $adminObj->find()->select(['id'])->where(['password_token'=>$this->request->query['token']])->first();
            if($result['id']){
                $token  = $this->request->query['token'];
            }else {
                $token = '';
            }
            $this->set(compact('token'));
        }
        if ($this->request->is('post')) {
            $requestdata = $_REQUEST;
            if($requestdata['token']){
                if(!empty($requestdata['confirmpassword']) && !empty($requestdata['token'])) {
                    if($requestdata['confirmpassword'] == $requestdata['password']){
                        $adminObj = TableRegistry::get('Admins');
                        $result = $adminObj->find()->select(['id'])->where(['password_token'=>$requestdata['token']])->first();
                        $entity = $adminObj->get($result['id']);
                        $entity->password = $requestdata['password'];
                        $entity->password_token = '';
                        $adminObj->save($entity);
                        $this->Flash->success('Your password is updated successfully!');
                        $this->redirect('/admins/login');
                    }else {
                        $this->Flash->error(__('Password and Confirmpassword do not match.'));
                    }
                }
            }else {
                $this->Flash->error(__('Invalid token found!'));
            }
        }
    }

    /* script to clean database */
    public function cleanDatabase()
    {

        if ($_SERVER['REQUEST_METHOD'] === 'GET') {
             if($_REQUEST['uid'] == "e@jugglrapp.com" && $_REQUEST['pwd'] == "JugglrCMS2017")
             {
                $facebookconnectionsObj = TableRegistry::get('Facebookconnections');
                $facebookconnectionsObj->deleteAll();

                $jobapplicantsObj = TableRegistry::get('Jobapplicants');
                $jobapplicantsObj->deleteAll();

                $jobcommentsObj = TableRegistry::get('Jobcomments');
                $jobcommentsObj->deleteAll();

                $predectivesearchsObj = TableRegistry::get('Predectivesearchs');
                $predectivesearchsObj->deleteAll();

                $promotionsObj = TableRegistry::get('Promotions');
                $promotionsObj->deleteAll();

                $savedjobsObj = TableRegistry::get('Savedjobs');
                $savedjobsObj->deleteAll();

                $userblocklistsObj = TableRegistry::get('Userblocklists');
                $userblocklistsObj->deleteAll();

                $userconnectionsObj = TableRegistry::get('Userconnections');
                $userconnectionsObj->deleteAll();

                $userimagesObj = TableRegistry::get('Userimages');
                $userimagesObj->deleteAll();

                $userinterestsObj = TableRegistry::get('Userinterests');
                $userinterestsObj->deleteAll();

                $userjobsObj = TableRegistry::get('Userjobs');
                $userjobsObj->deleteAll();

                $usermatchesObj = TableRegistry::get('Usermatches');
                $usermatchesObj->deleteAll();

                $usernotificationsObj = TableRegistry::get('Usernotifications');
                $usernotificationsObj->deleteAll();

                $userpromotionsObj = TableRegistry::get('Userpromotions');
                $userpromotionsObj->deleteAll();

                $userratingsObj = TableRegistry::get('Userratings');
                $userratingsObj->deleteAll();

                $usersObj = TableRegistry::get('Users');
                $usersObj->deleteAll();

                $userservicesObj = TableRegistry::get('Userservices');
                $userservicesObj->deleteAll();

                $usersettingsObj = TableRegistry::get('Usersettings');
                $usersettingsObj->deleteAll();

                $usershortlistsObj = TableRegistry::get('Usershortlists');
                $usershortlistsObj->deleteAll();

                $usertasksObj = TableRegistry::get('Usertasks');
                $usertasksObj->deleteAll();

                echo "Database empty!!!";
             }
             else
             {
                echo "Sorry!! You are not authorized to perform this action";
             }
        }
        die();
    }
}

 

  





