<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */



namespace App\Controller;
use Cake\ORM\TableRegistry;
use Cake\ORM\Table;



use Cake\Core\Configure;

use Cake\Network\Exception\NotFoundException;
use Cake\Network\Exception\InternalErrorException;
use Cake\View\Exception\MissingTemplateException;

use Cake\Event\Event;
use Cake\Utility\Hash;



/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
//this is a test comment from muneeb   
class UsersController extends AppController {


    /**
     * Displays a view
     *
     * @return void|\Cake\Network\Response
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */

    public function beforeFilter(Event $event)
	{
	    parent::beforeFilter($event);
            $this->loadComponent("Stripe");
            $this->loadComponent('Facebook');
            $this->loadComponent('Mailer');
            $this->loadComponent('Mailcontent');
            $this->loadComponent('Facebook');
            $this->loadComponent('Pushnotification');
	    // Allow users to register and logout.
	    // You should not add the "login" action to allow list. Doing so would
	    // cause problems with normal functioning of AuthComponent.
	    $this->Auth->allow(['add', 'logout','forgotpw','defaultJobs','stripeWebhook','everyHourCronJob','everyDayCronJob','everyDayFBCronJob', 'targetedNotifications','setpassword']);
	}

	public function login()
	{    
	  //$this->layout = 'login';
          $this->viewBuilder()->layout('login');
	  if ($this->request->is('post')) {
	       $user = $this->Auth->identify();
	        if ($user) {
	            $this->Auth->setUser($user);
	            $this->redirect(array('action' => 'index'));
	            return $this->redirect($this->Auth->redirectUrl());
	        }
	        $this->Flash->error(__('Invalid username or password, try again'));
	    }
	}
	
	public function logout()
	{
	  $this->Flash->success('You are now logged out.');
	  return $this->redirect($this->Auth->logout());
	}
	
	public function index() 
	{    
        $this->viewBuilder()->layout('admin');
		$select = ['id','is_active','verified'];
		$condition = [];
		$contain = [];
		$user_data = $this->Users->getRecordstoController($select,$condition,$contain);
		$response['active_user'] = $response['inactive_user'] = $response['pending_user'] = 0;
		if(!empty($user_data))
		{
			foreach ($user_data as $key=>$value)
			{
				if($value['is_active'] == '0'){
					$response['inactive_user']++;
				}
				if($value['verified'] == '0' && $value['is_active'] == '1'){
					$response['pending_user']++;
				}
				if($value['is_active'] == '1' && $value['verified'] == '1'){
					$response['active_user']++;
				}
			}
		}
		/*
		 * Get Services
		 */
		$serviceObj = TableRegistry::get('Services');
		$result = $serviceObj->find()->select(['id'])->all();
		if($result)
		{
			$result = $result->toArray();
			foreach ($result as $key => $value) {
				$serviceData[$value['id']] = "'Service-".$value['id']."'";
			}
			$serviceKey = array_keys($serviceData);
			$serviceValue = array_values($serviceData);
			$this->set(compact('serviceKey'));
			$this->set(compact('serviceValue'));	
		}

		$this->loadModel('Usertasks');
		$select = ['id','status','amount','createdAt','need_serviceid','offer_serviceid'];
		$condition = [];
		$contain = [];
		$task_data = $this->Usertasks->getRecordstoController($select,$condition,$contain);
		$response['complete_task'] = $response['current_task'] = $response['cancelled_task'] = 0;
		$month_arr = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
		$service = array();
		foreach ($month_arr as $key=>$value)
		{
			$response['month_sales'][$value] = 0;
			$service[$value] = array();
			for($i=1;$i<=count($serviceKey);$i++){
				$service[$value][$i] = 0;
			}
		}
		
		if(!empty($task_data))
		{
			foreach ($task_data as $key=>$value)
			{
				if($value['status'] == 0){
					$response['current_task']++;
				}elseif($value['status'] == 1){
					$response['complete_task']++;
				}elseif($value['status'] == 2){
					$response['cancelled_task']++;
				}
				$month = date_format($value['createdAt'],'M');
				$response['month_sales'][$month]++;
				if(isset($response['service_sales'][$value['need_serviceid']])){
					$service[$month][$value['need_serviceid']]++;
				}else {
					$service[$month][$value['need_serviceid']] = 1;
				}
				if(isset($response['service_sales'][$value['offer_serviceid']])){
					$service[$month][$value['offer_serviceid']]++;
				}else {
					$service[$month][$value['offer_serviceid']] = 1;
				}
			}
		}
		$finalservice = array();
		if(!empty($service)){
			foreach ($month_arr as $value) {
				$temp = array();
				$temp['y'] = $value;
				foreach ($service[$value] as $key => $val) {
					$temp[$key] = $val;	
				}
				$temp = json_encode($temp);
				$finalservice[] = $temp;
			}
		}
		$this->set(compact('finalservice'));
		$this->set(compact('response'));
		/*
		 * Recent Offer
		 */
		$offer_data = array();
		$select = ['id','user_id','user_with','need_serviceid','need_subserviceid','offer_serviceid','offer_subserviceid'];           
        $condition['is_accepted'] = '0';
        $contain = [
            'Userwith'=> function ($q) {
                return $q
                ->select(['Userwith.firstname']);
            },
            'Users'=> function ($q) {
                return $q
                ->select(['Users.firstname']);
            },
            'Subservices'=> function ($q) {
                return $q
                ->select(['Subservices.subservice_name']);
            },
            'offered_subservice'=> function ($q) {
                return $q
                ->select(['offered_subservice.subservice_name']);
            }
        ];
        $result = $this->Usertasks->find()
                ->select($select)
                ->where($condition)
                ->contain($contain)
                ->limit(4)
                ->order('Usertasks.createdAt DESC')
                ->all();
        if($result){     
        	$result = $result->toArray();
        	foreach ($result as $key => $value) {
        		$offer_data[$key]['id'] = $value['id'];
        		$offer_data[$key]['language'] = $value['userwith']['firstname']." is helping ".$value['user']['firstname']." with ".$value['subservice']['subservice_name']." in exchange of ".$value['offered_subservice']['subservice_name'];
        		$offer_data[$key]['status'] = 'Review Pending';
        	}
        }
        $this->set(compact('offer_data'));
	}
	
	public function usersbyvolume()
	{
		$this->viewBuilder()->layout('admin');
		$this->paginate = [
            'limit' => 10,
            'fields' => ['state','count' => 'count(id)'],
            'conditions' => ['is_active' => '1'],
            'group' => ['state']
        ];
        $data = $this->paginate('Users');
        $this->set('data',$data);
        $data = $this->Users->find('all')
	            ->where(['is_active' => '1', 'device_type' => 'ios'])
	            ->count();
	    $this->set('ios_cnt',$data);
	    $data = $this->Users->find('all')
	            ->where(['is_active' => '1', 'device_type !=' => 'ios'])
	            ->count();
	    $this->set('android_cnt',$data);
	}
	
	public function summary()
	{
		$this->viewBuilder()->layout('admin');
	    	$listing = array();

	    	$limit = 10;
	        $select 	= [];
		//$condition 	= ['Users.is_active'=>'1','Users.verified'=>'1'];
		$conditions 	= [];
		$contain 	= ['Usertasks','Userservices'];
	        $requestData=$this->request->query;

	        if (isset($requestData['q']) && !empty($requestData['q'])) {
	            $conditions['OR']['email like'] = "%" . trim($requestData['q']) . "%";
	            $conditions['OR']['lastname like'] = "%" . trim($requestData['q']) . "%";
	            $conditions['OR']['firstname like'] = "%" . trim($requestData['q']) . "%";
	            $conditions['OR']['suburb like'] = "%" . trim($requestData['q']) . "%";
	            $conditions['OR']['postcode like'] = "%" . trim($requestData['q']) . "%";
	            if($requestData['q'] == 'ios' || $requestData['q'] == 'android'){
	            	$conditions['OR']['device_type'] = trim($requestData['q']);
	            }
	        }
	        if (isset($requestData['limit']) && !empty($requestData['limit'])) {
	            $limit = $requestData['limit'];
	        }
        
	        $this->paginate = [
	            'limit' => $limit,
	            'conditions' => $conditions,
	            'contain' => $contain,
	            'order' => ['Users.id'=>'DESC']
	        ];
	        $users = $this->paginate();
			if($this->request->is('ajax')){
	        	$this->exportcsvs('summary', $requestData);
	        	die();
	        }else{
			$response 	= array(); 
			if(!empty($users))
			{	
				$i = 0;
				foreach ($users as $datavalue)
				{
					$response[$i]['id']							= $datavalue['id'];
					$response[$i]['os']							= $datavalue['device_type'];
					$response[$i]['firstname']					= $datavalue['firstname'];
					$response[$i]['lastname']					= $datavalue['lastname'];
					$response[$i]['email']						= $datavalue['email'];
					$response[$i]['postcode']					= $datavalue['postcode'];
					$response[$i]['suburb']						= $datavalue['suburb'];
					$response[$i]['city']						= $datavalue['city'];
					$response[$i]['state']						= $datavalue['state'];
					$response[$i]['country']					= $datavalue['country'];
					$response[$i]['createdAt']					= (!empty($datavalue['createdAt'])) ? date_format($datavalue['createdAt'],'Y-m-d') : "";
					$response[$i]['verified_at']				= (!empty($datavalue['verified_at'])) ? date_format($datavalue['verified_at'],'Y-m-d H:i:s') : "";
					$response[$i]['total_transaction']			= 0;
					$response[$i]['complete_transaction']		= 0;
					$response[$i]['cancelled_transaction']		= 0;
					$response[$i]['current_transaction']		= 0;
					$response[$i]['paid_transaction']			= 0;
					$response[$i]['paid_value_transaction']		= 0;
					$response[$i]['total_fee']					= 0;
					$response[$i]['average_time_completion']	= 0;
					$response[$i]['average_time_payment']		= 0;
					$response[$i]['positive_rating']			= 0;
					$response[$i]['neutral_rating']				= 0;
	                		$response[$i]['verified_by']				= $datavalue['verified_by'];
					if($datavalue['verified'] == '1' && $datavalue['is_active'] == '1')
					{
						$response[$i]['status']					= 'verified';
					}elseif ($datavalue['verified'] == '0' && $datavalue['is_active'] == '1'){
						$response[$i]['status']					= 'verification Pending';
					}elseif ($datavalue['is_active'] == '0'){
						$response[$i]['status']					= 'inactive';
					}
					if(!empty($datavalue['userservices']))
					{
						foreach ($datavalue['userservices'] as $key=>$value)
						{
							$response[$i]['positive_rating'] 	+= $value['positive_rating'];
							$response[$i]['neutral_rating'] 	+= $value['neutral_rating'];
						}
					}
					if(!empty($datavalue['usertasks']))
					{
						foreach ($datavalue['usertasks'] as $key=>$value)
						{
							$response[$i]['total_transaction']++;
							if($value['status'] == '1')
							{
								$response[$i]['complete_transaction']++;
							}
							if($value['status'] == '2')
							{
								$response[$i]['cancelled_transaction']++;
							}
							if($value['status'] == '0')
							{
								$response[$i]['current_transaction']++;
							}
							if($value['payment_status'] == '1')
							{
								$response[$i]['paid_transaction']++;
								$response[$i]['total_fee'] 				+= $value['fee'];
								$response[$i]['paid_value_transaction']	+= $value['amount'];
							}
							
						}
					}
					$i++;
				}
			}
	        $this->set('users',$response);
	        $this->set('action','summary');
	        $this->set('controller','users');
	        $csv_export = 1;
	        $this->set('csv_export',$csv_export);
	    }
	}
	
    public function activeuser() 
    {
    	$this->viewBuilder()->layout('admin');
    	$listing = array();

    	$limit = 10;
        $conditions = array();
        $conditions['type'] = 1;
        $conditions['verified']='1';
        $conditions['is_active']='1';
        $requestData=$this->request->query;

        if (isset($requestData['q']) && !empty($requestData['q'])) {
            $conditions['OR']['email like'] = "%" . trim($requestData['q']) . "%";
            $conditions['OR']['lastname like'] = "%" . trim($requestData['q']) . "%";
            $conditions['OR']['firstname like'] = "%" . trim($requestData['q']) . "%";
            $conditions['OR']['suburb like'] = "%" . trim($requestData['q']) . "%";
            $conditions['OR']['postcode like'] = "%" . trim($requestData['q']) . "%";
        }
        if (isset($requestData['limit']) && !empty($requestData['limit'])) {
            $limit = $requestData['limit'];
        }
        
        $this->paginate = [
            'limit' => $limit,
            'conditions' => $conditions,
            'order' => ['Users.id'=>'DESC']
        ];
        $users = $this->paginate();
        if($this->request->is('ajax')){
        	$this->exportcsvs('activeuser', $requestData);
        	die();
        }else{
	        $this->set('users',$users);
	        $this->set('action','activeuser');
	        $this->set('controller','users');
	        $csv_export = 1;
	        $this->set('csv_export',$csv_export);
	    }
    }
    
    public function pendinguser() 
    {
    	$this->viewBuilder()->layout('admin');
    	$listing = array();

    	$limit = 10;
        $conditions = array();
        $conditions['type'] = 1;
        $conditions['verified']='0';
        $conditions['is_active']='1';
        $requestData=$this->request->query;

        if (isset($requestData['q']) && !empty($requestData['q'])) {
            $conditions['OR']['email like'] = "%" . trim($requestData['q']) . "%";
            $conditions['OR']['lastname like'] = "%" . trim($requestData['q']) . "%";
            $conditions['OR']['firstname like'] = "%" . trim($requestData['q']) . "%";
            $conditions['OR']['suburb like'] = "%" . trim($requestData['q']) . "%";
            $conditions['OR']['postcode like'] = "%" . trim($requestData['q']) . "%";
        }
        if (isset($requestData['limit']) && !empty($requestData['limit'])) {
            $limit = $requestData['limit'];
        }
        
        $this->paginate = [
            'limit' => $limit,
            'conditions' => $conditions,
            'order' => ['Users.id'=>'DESC']
        ];
	    $users = $this->paginate();
        if($this->request->is('ajax')){
        	$this->exportcsvs('pendinguser', $requestData);
        	die();
        }else{
	        $csv_export = 1;
	        $this->set('users',$users);
	        $this->set('action','pendinguser');
	        $this->set('controller','users');
	        $this->set('csv_export',$csv_export);
	    }
    }
    
    public function inactiveuser() 
    {
    	$this->viewBuilder()->layout('admin');
    	$listing = array();

    	$limit = 10;
        $conditions = array();
        $conditions['type'] = 1;
        $conditions['is_active']='0';
        $requestData=$this->request->query;

        if (isset($requestData['q']) && !empty($requestData['q'])) {
            $conditions['OR']['email like'] = "%" . trim($requestData['q']) . "%";
            $conditions['OR']['lastname like'] = "%" . trim($requestData['q']) . "%";
            $conditions['OR']['firstname like'] = "%" . trim($requestData['q']) . "%";
            $conditions['OR']['suburb like'] = "%" . trim($requestData['q']) . "%";
            $conditions['OR']['postcode like'] = "%" . trim($requestData['q']) . "%";
        }
        if (isset($requestData['limit']) && !empty($requestData['limit'])) {
            $limit = $requestData['limit'];
        }
        
        $this->paginate = [
            'limit' => $limit,
            'conditions' => $conditions,
            'order' => ['Users.id'=>'DESC']
        ];
        $users = $this->paginate();
        if($this->request->is('ajax')){
        	$this->exportcsvs('inactiveuser', $requestData);
        	die();
        }else{
	        $this->set('users',$users);
	        $this->set('action','inactiveuser');
	        $this->set('controller','users');
	        $csv_export = 1;
	        $this->set('csv_export',$csv_export);
	    }
    }
    
    public function updateuserstatus()
    {
        $this->viewBuilder()->layout('ajax');
    	if($this->request->data)
    	{
    		$datatosave = array();
    		foreach ($this->request->data as $key=>$value)
    		{
    			if($key != 'user_id')
    			{
    				$datatosave[$key] = $value;
    			}
    			$datatosave['verified_at'] = date('Y:m:d H:i:s');
			if($key == 'verified'){
    				$params['user_with'] = $this->request->data['user_id'];
    				$data   = $this->Pushnotification->getnotificationMessage('NOTIFICATIONTOVERIFIEDUSER',$params);
    			}
    			if($key == 'is_active' && $value == '0'){
    				$data   = $this->Pushnotification->getnotificationMessage('INACTIVEFROMCMS',$this->request->data['user_id']);
    			}
    		}
    		$result = $this->Users->updatedata($this->request->data['user_id'],$datatosave);
    		echo json_encode($result);
    		die();
    	}
    }
    
    public function view() 
    {
        $this->viewBuilder()->layout('admin');
    	if($this->request->data)
    	{
    		$param = array();
    	}
    	if($this->request->query['id'])
    	{
    		$listing = array();
    		$select = ['id','firstname','lastname','about_me','email','postcode','suburb','city','state','country','verified','verified_by','profile_image_thumb','dob','gender','is_active','createdAt','shortlist_cnt','match_cnt','positive_rating','neutral_rating'];
    		$contain = ['Userservices.Subservices','Userinterests','Usertasks'];
    		$condition = ['id'=>$this->request->query['id']];
    		//$this->loadModel('Userservices');
    		$listing = $this->Users->getuser_dashboard($select,$contain,$condition);
    		//$result  = $this->Userservices->getservicesforrating($this->request->query['id']);
    		//$listing['average_rating'] = isset($result['average_rating']) ? $result['average_rating'] : 0;
    		$this->set('data',$listing);
    	}	
    }
    
	public function forgotpw(){
    	$this->layout = false;
   	}

	public function delete_users($id = null)
	{
	    $this->loadModel('Users');
	    $users = TableRegistry::get('Users');
	    $this->loadModel('UserDetails');
	    $entity = $this->Users->get($id,[
	    'contain' => [
	        'Userdetails',
	    ]]);
	    if($users->delete($entity,['associated' => ['Userdetails']])){
	       $this->Flash->success('The user has been deleted.');           
	    }
	    else{
	       $this->Flash->error('The user could not be deleted. Please, try again.');
	    }
	    $this->redirect(['action' => 'index']);
	}

    public function deleteUser(){
    	$user_id = $this->request->query['id'];
        if($this->Users->deleteUserData($user_id)){
        	echo 1;
        }else {
        	echo 0;
        }
        die();
    }

    public function ratingsummary() {
    	$this->viewBuilder()->layout('admin');
    	$listing = array();
    	$limit = 10;
    	$select = ['Users.id','Users.firstname','Users.lastname','Users.profile_image_thumb','positive_rating','neutral_rating'];
    	$conditions = [];
    	$contain = [
    		'Usertasks'=> function ($q) {
    			return $q
    			->select(['Usertasks.id','Usertasks.user_id','Usertasks.user_with','Usertasks.need_serviceid','Usertasks.need_subserviceid','Usertasks.offer_serviceid','Usertasks.offer_subserviceid','Usertasks.amount','Usertasks.fee'])
    			->where(['Usertasks.payment_status'=>'1']);
			}
    	];
    	if (isset($requestData['q']) && !empty($requestData['q'])) {
            $conditions['OR'][]['Users.lastname like'] = "%" . trim($requestData['q']) . "%";
            $conditions['OR'][]['Users.firstname like'] = "%" . trim($requestData['q']) . "%";
        }
        if (isset($requestData['limit']) && !empty($requestData['limit'])) {
            $limit = $requestData['limit'];
        }
    	$this->paginate = [
            'limit' => $limit,
            'fields' => $select,
            'conditions' => $conditions,
            'contain' => $contain
        ];
        $users = $this->paginate();
    	$listing = $this->Users->getratingsummary($users);
    	$this->set(compact('listing'));
    	$this->set('action','ratingsummary');
        $this->set('csv_export',1);
    }
    /*
     *   Function for Cron jobs start from here.
     */
    public function everyDayCronJob()
    {
    	ini_set('max_execution_time', -1);
    	$date = date('Y-m-d');
    	$monthago =  date('Y-m-d', strtotime('-1 months'));
    	/*
    	 * Share Jugglr Notification, monthly
    	 */
    	$user_ids = array();
    	$userObj = TableRegistry::get('Users');
    	$result = $userObj->find('all')
    	->select(['id'])
    	->where(['verified' => '1','is_active' => '1','modifiedAt' => $monthago])
    	->all();
    	if(!empty($result)){
    		$user_ids = Hash::extract($result->toArray(), '{n}.id');
    	}
    	if(count($user_ids) > 0){
    		$this->Pushnotification->getnotificationMessage('SHAREJUGGLR',$user_ids);
    		$share_userids = $user_ids;
    	}else {
    		$share_userids = array();
    	}
    	/*
    	 * Invite Jugglr Notification, monthly
    	 */
    	$result = $userObj->find('all')
    	->select(['Users.id'])
    	->where(['Users.verified' => '1','Users.is_active' => '1','Users.modifiedAt' => $monthago,'Usersettings.shareJugglr_cnt >' => 5,'Usersettings.shareJugglr_cnt <' => 18])
    	->contain(['Usersettings'])
    	->all();
    	if(!empty($result)){
    		$user_ids = Hash::extract($result->toArray(), '{n}.id');
    	}else {
    		$user_ids = array();
    	}
    	$usersetting = TableRegistry::get('Usersettings');
    	if(!empty($user_ids)){
    		$this->Pushnotification->getnotificationMessage('INVITEJUGGLR',$user_ids);
    		foreach ($user_ids as $key => $value) {
    			$query = $usersetting->query();
    			$query->update()
    			->set($query->newExpr('shareJugglr_cnt = shareJugglr_cnt + 1'))
    			->where(['user_id' => $value])
    			->execute();
    		}
    	}
    	
    	$userids = array_unique(array_merge(array_values($user_ids),array_values($share_userids)));
    	if(!empty($userids))
    	{
    		foreach ($userids as $key => $value) {
    			$query = $userObj->query();
    			$query->update()
    			->set(['modifiedAt' => date('Y-m-d')])
    			->where(['id' => $value])
    			->execute();
    		}
    	}
    	
    	/*
    	 * Invite Jugglr Notification, weekly
    	 */
    	$weekagodate = date('Y-m-d', time() + (60 * 60 * 24 * -7) );
    	$result = $userObj->find('all')
    	->select(['Users.id'])
    	->where(['Users.verified' => '1','Users.is_active' => '1','Users.modifiedAt' => $weekagodate,'Usersettings.shareJugglr_cnt <=' => 5])
    	->contain(['Usersettings'])
    	->all();
    	if(!empty($result)){
    		$user_ids = Hash::extract($result->toArray(), '{n}.id');
    	}else {
    		$user_ids = array();
    	}
    	if(!empty($user_ids)){
    		$this->Pushnotification->getnotificationMessage('INVITEJUGGLR',$user_ids);
    		$usersetting = TableRegistry::get('Usersettings');
    		foreach ($user_ids as $key => $value) {
    			$query = $usersetting->query();
    			$query->update()
    			->set($query->newExpr('shareJugglr_cnt = shareJugglr_cnt + 1'))
    			->where(['user_id' => $value])
    			->execute();
    			
    			$query = $userObj->query();
    			$query->update()
    			->set(['modifiedAt' => date('Y-m-d')])
    			->where(['id' => $value])
    			->execute();
    		}
    	}
    	
    	/*
    	 * FB friends synch, weekly
    	 */
    	/*$weekday = date('d');
    	 $weekday = 1;
    	 if(in_array($weekday,array(1,15))){
    	 $userObj = TableRegistry::get('Users');
    	 $result = $userObj->find('all')
    	 ->select(['Users.id','Users.facebook_token'])
    	 ->where(['is_active' => '1','Users.facebook_token !=' => ''])*/
    	/*->contain(['Facebookconnections'=> function ($q) {
    	 return $q
    	 ->select(['Facebookconnections.connection_fb_id','Facebookconnections.user_id']);
    	 }])*/
    	/*->all();
    	 $FacebookconnectionsObj = TableRegistry::get('Facebookconnections');
    	 if(!empty($result)){
    	 $result = $result->toArray();
    	 foreach ($result as $key => $value) {
    	 //$presentFBData = Hash::extract($value['facebookconnections'], '{n}.connection_fb_id');
    	 $FBfriendsData = $this->Facebook->getmutualFriends($value['facebook_token'], $value['id']);
    	 if(!empty($FBfriendsData)){
    	 $result = $FacebookconnectionsObj->deleteAll(['user_id'=>$value['id']]);
    	 $FacebookconnectionsObj->addFbConnections($value['id'],$FBfriendsData);
    	 }
    	 }
    	 }
    	 }*/
    	echo "success";
    	die();
    }
    /*
    *   Function for Cron jobs start from here.
    */
    public function targetedNotifications($user_id, $not_msg)
    {

        
    	$this->Pushnotification->postTargetMessage($user_id, $not_msg);
    
        echo "success";
        die();
    }

    public function everyDayFBCronJob()
    {
    	/*
        * FB friends synch, weekly
        */
        $weekday = date('d');
        $weekday = 1;
        if(in_array($weekday,array(1,15))){
            $userObj = TableRegistry::get('Users');
            $result = $userObj->find('all')
                ->select(['Users.id','Users.facebook_token'])
                ->where(['is_active' => '1','Users.facebook_token !=' => ''])
                /*->contain(['Facebookconnections'=> function ($q) {
                                return $q
                                ->select(['Facebookconnections.connection_fb_id','Facebookconnections.user_id']);
                            }])*/
                ->all();
            $FacebookconnectionsObj = TableRegistry::get('Facebookconnections');
            if(!empty($result)){
                $result = $result->toArray();
                foreach ($result as $key => $value) {
                    //$presentFBData = Hash::extract($value['facebookconnections'], '{n}.connection_fb_id');
                    $FBfriendsData = $this->Facebook->getmutualFriends($value['facebook_token'], $value['id']);
                    if(!empty($FBfriendsData)){
                        $result = $FacebookconnectionsObj->deleteAll(['user_id'=>$value['id']]);
                        $FacebookconnectionsObj->addFbConnections($value['id'],$FBfriendsData);
                    }
                }
            }
        }
        echo "success";
        die();
    }

    public function everyHourCronJob()
    {
    	$userObj = TableRegistry::get('Usertasks');
    	$condition['status'] = '2';
    	$condition['payment_status'] = '0';
    	$condition['OR'][]['offer_serviceid'] = 8;
    	$condition['OR'][]['need_serviceid'] = 8;
        $result = $userObj->find('all')
            ->where($condition)
            ->contain(['Users'=> function ($q) {
                            return $q
                            ->select(['stripe_acct_id','stripe_cust_id']);
                        },
                        'Userwith'=> function ($q) {
                            return $q
                            ->select(['stripe_acct_id','stripe_cust_id']);
                        }])
            ->all();
        if(!empty($result))
        {
	        $Usersettings = TableRegistry::get('Usersettings');
	        foreach ($result as $key => $value) {	
	        	if($value['need_serviceid'] == 8){
	        		$conditions = ['user_id' => $value['user_id']];
	        	}elseif ($value['offer_serviceid'] == 8) {
	        		$conditions = ['user_id' => $value['user_with']];
	        	}
		        $user = $Usersettings->find('all')
		        	->select(['stripe_bank_flag','stripe_bank_verified'])
		            ->where($conditions)
		            ->first();
		        $data = array();
		        if($user['stripe_bank_flag'] == '1'){
		        	$data['id'] = $value['id'];
		        	$data['amount'] = $value['amount'];
		        	$data['tax'] = $value['tax'];
		        	$data['fee'] = $value['fee'];
		        	$data['discount'] = $value['discount'];
		        	$data['stripe_cust_id'] = ($value['need_serviceid'] == 8) ? base64_decode($value['userwith']['stripe_cust_id']) : base64_decode($value['user']['stripe_cust_id']);
		        	$data['stripe_acct_id'] = ($value['need_serviceid'] == 8) ? base64_decode($value['user']['stripe_acct_id']) : base64_decode($value['userwith']['stripe_acct_id']);
		        	if($data['stripe_cust_id'] != '' && $data['stripe_acct_id'] != '')
		        	{
		        		$response = $this->Stripe->createTranfer($data);
		        		if($response['status'] == 1){
			        		$datatosave = array();
			        		$datatosave['payment_status'] = '1';
			        		$userObj->updatedata($value['id'], $datatosave);
			        	}
		        	}
		        }
	        }
	    }
        echo "success";
        die();
    }

	public function add()
	{
		$this->viewBuilder()->layout('admin');
		if($this->request->data && !empty($this->request->data['subservice'])){
			$result = $this->Users->addBusinessUser($this->request->data);
			if($result['status']){
				$user_id = $result['data']['id'];
				$servicesObj = TableRegistry::get('Userservices');
				$servicesObj->addBusinessSubservices($user_id,$this->request->data['subservice']);
				/*
	             * Adding user default Setting
	             */
	            //$this->Usersettings = TableRegistry::get('Usersettings');
	            //$this->Usersettings->adduser_settings($user_id);
	            /*
	             * Adding user cover images
	             */
	            //$Userimages = TableRegistry::get('Userimages');
        		//$Userimages->adddefaultuserimages($user_id);
				$this->Mailcontent->registeredBusinessUser($result['data']);
				
				//add data to firebase
				if($user_id){
					$this->updateUserServiceToFirebase($user_id, 0);
				}

				$this->Flash->success('The user has been added successfully!');
			}else {
				$this->Flash->error(__($result['message']));
			}
		}
		$tableObj = TableRegistry::get('Services');
		$services = $tableObj->getBusinessServicess();
		$this->set('services',$services);
	}

	public function businessuser()
	{
		$this->viewBuilder()->layout('admin');
    	$listing = array();

    	$limit = 10;
        $conditions = array();
        $conditions['type'] = 2;
        $requestData=$this->request->query;

        if (isset($requestData['q']) && !empty($requestData['q'])) {
            $conditions['OR']['email like'] = "%" . trim($requestData['q']) . "%";
            $conditions['OR']['lastname like'] = "%" . trim($requestData['q']) . "%";
            $conditions['OR']['firstname like'] = "%" . trim($requestData['q']) . "%";
            $conditions['OR']['suburb like'] = "%" . trim($requestData['q']) . "%";
            $conditions['OR']['postcode like'] = "%" . trim($requestData['q']) . "%";
        }
        if (isset($requestData['limit']) && !empty($requestData['limit'])) {
            $limit = $requestData['limit'];
        }
        
        $this->paginate = [
            'limit' => $limit,
            'conditions' => $conditions,
            'order' => ['Users.id'=>'DESC']
        ];
        $users = $this->paginate();
        
        $this->set('users',$users);
        $this->set('action','businessuser');
        $this->set('controller','users');
	    $this->set('csv_export',0);
	}

	public function editbusinessuser()
	{
		$this->viewBuilder()->layout('admin');
		if($this->request->is('GET'))
		{
			$user_id = $this->request->query['id'];
			$userObj = TableRegistry::get('Users');
			$result = $userObj->get($user_id);
			$this->set('user',$result);
			
			$userservicesObj = TableRegistry::get('Userservices');
			$select = ['user_id','service_id','subservice_id','subservicetag_id'];
			$contain = [];
			$condition = ['user_id' => $user_id];
			$service = $userservicesObj->dataToController($select, $condition, $contain);
			if(!empty($service))
			{
				$subservice_ids = Hash::extract($service, '{n}.subservice_id');
			}
			$this->set('subservice_ids',$subservice_ids);
			$this->set('user',$result);

			$tableObj = TableRegistry::get('Services');
			$services = $tableObj->getBusinessServicess();
			$this->set('services',$services);
		}
		if($this->request->is('POST')){
			$result = $this->Users->editBusinessUser($this->request->data);
			if($result['status']){
				$user_id = $this->request->data['id'];
				$servicesObj = TableRegistry::get('Userservices');
				$result = $servicesObj->deleteAll(['user_id'=>$user_id]);
				$result = $servicesObj->addBusinessSubservices($user_id,$this->request->data['subservice']);

				//update data to firebase
				if($user_id){
					$this->updateUserServiceToFirebase($user_id, 2);
				}

				$this->Flash->success('The user has been updated successfully!');
			}else {
				$this->Flash->error(__($result['message']));
			}
			$this->redirect(array('action' => 'businessuser'));
		}
	}

	public function updatebusinessuserstatus()
    {
        $this->viewBuilder()->layout('ajax');
    	if($this->request->data)
    	{
    		$userData = $this->Users->get($this->request->data['user_id']);
			$datatosave = array();
			$datatosave['is_active'] = ($userData['is_active'] == '0') ? '1' : '0';
    		$result = $this->Users->updatedata($this->request->data['user_id'],$datatosave);
    		echo json_encode($result);
    		die();
    	}
    }

    /*
    *	Set Password
    */
    public function setpassword()
    {
    	$this->viewBuilder()->layout('login');
    	$requestdata = $_REQUEST;
    	$data['token'] = '';
    	$tableObj = TableRegistry::get('Users');
    	if($this->request->is('post'))
    	{
            if($requestdata['password'] === $requestdata['confirmpassword']){
            	$savetoData['password_token'] = '';
            	$savetoData['password'] = md5($requestdata['password']);
            	$result = $tableObj->updatedata($requestdata['id'],$savetoData);
            	$data['token'] = 1;
            }else {
            	$result = '';
            }
            if($result){
                $this->Flash->success('Password successfully set');
            }else {
                $this->Flash->error(__('This URL is invalid'));
            }
    	}elseif($this->request->is('get')){
    		if($requestdata['token'] != ''){
    			$result = $tableObj->find()->select(['id'])->where(['password_token'=>$requestdata['token']])->first();
    			if(!empty($result)){
    				$data['token'] = $requestdata['token'];
    				$data['id'] = $result['id'];
    			}else {
    				$data['token'] = '';
    			}
    		}
    	}
    	$this->set('data',$data);
    }

    public function updateUserServiceToFirebase($user_id, $type)
    {
    	if($type == 1 || $type == 0)
    	{
    		/*
			 * Adding to firebase
			 */
    		$tableObj = TableRegistry::get('Users');
	        $userDate = $tableObj->find()->where(['id' => $user_id])->select(['firstname','id'])->first();    
			$user_id = $result['data']['id'];
			$data[$user_id]['name'] = $userDate['firstname'];
			$data[$user_id]['is_business_user'] = 1;
			$data[$user_id]['profile-image'] = BASE_URL.'/img/default_image.png';
			$this->Firebase->dataPush($data,'DEFAULT_BUSINESS_USER_PATH');
    	}
    	elseif ($type == 2 || $type == 0)
    	{
	    	/* add userservices to service node in Firebase */
	        $userserviceObj = TableRegistry::get('Userservices');
	        $subserviceDate = $userserviceObj->find()->where(['user_id' => $user_id])->select(['subservice_id','subservicetag_id','type'])->all();    
	        if(!empty($subserviceDate)){
	        	$subservice = Hash::extract($subserviceDate->toArray(), '{n}.subservice_id');
		        $subservicetag = Hash::extract($subserviceDate->toArray(), '{n}.subservicetag_id');
		        foreach ($subservice as $key => $value) {
		            $data = $userserviceObj->find()->where(['subservice_id' => $value])->select(['user_id','type'])->all();    
		            /*$need_tmp = Hash::extract($data->toArray(), '{n}[type=0].user_id');
		            if($need_tmp){
		                $response['need']['subservice'][$value] = implode(',', $need_tmp);
		            }*/
		            $offer_tmp = Hash::extract($data->toArray(), '{n}[type=1].user_id');
		            if($offer_tmp){
		                $response['offer']['subservice'][$value] = implode(',', $offer_tmp);
		            }
		        }
		        /*if(isset($response['need']['subservice'])){
		        	$this->Firebase->dataPush($response['need']['subservice'],'DEFAULT_SERVICE_NEED_SUBSERVICE_PATH');
		        }*/
		        if(isset($response['offer']['subservice'])){
		        	$this->Firebase->dataPush($response['offer']['subservice'],'DEFAULT_SERVICE_OFFER_SUBSERVICE_PATH');
		        }

		        foreach ($subservicetag as $key => $value) {
		            $data = $userserviceObj->find()->where(['subservicetag_id' => $value])->select(['user_id','type'])->all();    
		            /*$need_tmp = Hash::extract($data->toArray(), '{n}[type=0].user_id');
		            if($need_tmp){
		                $response['need']['subservicetag'][$value] = implode(',', $need_tmp);
		            }*/
		            $offer_tmp = Hash::extract($data->toArray(), '{n}[type=1].user_id');
		            if($offer_tmp){
		                $response['offer']['subservicetag'][$value] = implode(',', $offer_tmp);
		            }
		        }
		        /*if(isset($response['need']['subservicetag'])){
		        	$this->Firebase->dataPush($response['need']['subservicetag'],'DEFAULT_SERVICE_NEED_SUBSERVICETAG_PATH');
		        }*/
		        if(isset($response['offer']['subservicetag'])){
		        	$this->Firebase->dataPush($response['offer']['subservicetag'],'DEFAULT_SERVICE_OFFER_SUBSERVICETAG_PATH');
		        }
		    }
		}
		return true;
    }
}
