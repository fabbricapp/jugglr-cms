<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */



namespace App\Controller;
use Cake\ORM\TableRegistry;
use Cake\ORM\Table;



use Cake\Core\Configure;

use Cake\Network\Exception\NotFoundException;

use Cake\View\Exception\MissingTemplateException;

use Cake\Event\Event;



/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */

class ServicesController extends AppController {

    /**
     * Displays a view
     *
     * @return void|\Cake\Network\Response
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
      public function beforeFilter(Event $event){
		    parent::beforeFilter($event);
		
		    // Allow users to register and logout.
		
		    // You should not add the "login" action to allow list. Doing so would
		
		    // cause problems with normal functioning of AuthComponent.
		
		    $this->Auth->allow([]);		
		}

	    public function add() {
	    	$this->viewBuilder()->layout('admin');
	    	if($this->request->data){
		    	$request_data = $this->request->data['service_name'];
                if($request_data[0] != '')
                {
                    $datatosave = array();
                    foreach ($request_data as $key=>$value){
                            if($value != ''){
                                    if($this->request->data['service_type'] == 'services'){
                                            $datatosave[$key]['service_name'] = $value;
                                            if(isset($this->request->data['type'])){
                                            	$datatosave[$key]['type'] = 2;
                                            }
                                    }elseif($this->request->data['service_type'] == 'subservices'){
                                            $datatosave[$key]['service_id'] = $this->request->data['service_id'];
                                            $datatosave[$key]['subservice_name'] = $value;
                                    }else {
                                            $datatosave[$key]['subservice_id'] = $this->request->data['subservice_id'];
                                            $datatosave[$key]['subservicetag_name'] = $value;
                                    }
                            }
                    }
                    if($this->request->data['service_type'] == 'services'){
                        $services = TableRegistry::get('Services');
                        $contain = [];
                    }elseif($this->request->data['service_type'] == 'subservices'){
                        $services = TableRegistry::get('Subservices');
                        $contain = ['Services'];
                    }else {
                        $services = TableRegistry::get('Subservicetags');
                        $contain = ['Subservices.Services'];
                    }
                    /*$entities = $services->newEntities($datatosave);
                    foreach ($entities as $entity) {
                            $services->save($entity);
                    }*/
                    $savedId = array();
                    foreach ($datatosave as $entity) {
                        $id = $services->savedata($entity);
                        $savedId[] = $id;
                    }
                    
                    /*
					 * Adding to firebase
					 */
                    if($this->request->data['service_type'] == 'subservices' && !empty($savedId)){   
                    	foreach ($savedId as $key => $value) {
                    		$data = array();
							$data[$value] = "0";
							$this->Firebase->dataPush($data,'DEFAULT_SERVICE_NEED_SUBSERVICE_PATH');
							$this->Firebase->dataPush($data,'DEFAULT_SERVICE_OFFER_SUBSERVICE_PATH');
						}
                    }elseif($this->request->data['service_type'] == 'subservicetags' && !empty($savedId)){
                        foreach ($savedId as $key => $value) {
                    		$data = array();
							$data[$value] = "0";
							$this->Firebase->dataPush($data,'DEFAULT_SERVICE_NEED_SUBSERVICETAG_PATH');
							$this->Firebase->dataPush($data,'DEFAULT_SERVICE_OFFER_SUBSERVICETAG_PATH');
						}
                    }

                    $this->Flash->success('Services added!');
                }else {
                    $this->Flash->error(__('Services not found!'));
                } 
	    	}
	    	$this->loadModel('Services');
	    	$select 	= ['id','service_name'];
	    	$condition 	= ['is_active' => '1'];
	    	$services 	= $this->Services->get_services($select,$condition);
	    	$this->set(compact('services'));
	    	$subservices = $this->Services->get_subservices();
	    	$this->set(compact('subservices'));
	    }
	    
	    public function edit() {
	    	$this->viewBuilder()->layout('admin');
	    	$this->loadModel('Services');
	    	$select = ['id','service_name','is_active'];
	    	$services = $this->Services->get_services($select);
	    	$this->set(compact('services'));
	    	$subservices = $this->Services->get_subservices();
	    	$this->set(compact('subservices'));
	    }
	    
	    public function getsubservicesandtags() {
	    	$gettype = $this->request->query['gettype'];
	    	if($gettype == 'subservice'){
	    		$service_id = $this->request->query['service_id'];
	    		$this->loadModel('Subservices');
	    		$select = [];
	    		$condition = [];
	    		$response = $this->Subservices->get_subservicewithid($service_id,$select,$condition);
	    	}else {
	    		$subservice_id = $this->request->query['subservice_id'];
	    		$this->loadModel('Subservicetags');
	    		$select = [];
	    		$condition = [];
	    		$response = $this->Subservicetags->get_subservicetagwithid($subservice_id,$select,$condition);
	    	}
	    	echo json_encode($response);
	    	die();
	    }
	    
	    public function view(){
	    	$this->viewBuilder()->layout('admin');
	    	$this->loadModel('Services');
    		$services = $this->Services->getall_services(array('Subservices.Subservicetags'));
    		$this->set(compact('services'));
	    }
	    
	    public function updateservice(){
	    	$this->viewBuilder()->layout('ajax');
	    	if($this->request->data)
	    	{
	    		$datatosave = array();
	    		foreach ($this->request->data as $key=>$value)
	    		{
	    			if($key != 'service_id')
	    			{
	    				$datatosave[$key] = $value;
	    			}
	    		}
	    		$result = $this->Services->updatedata($this->request->data['service_id'],$datatosave);
	    		echo json_encode($result);
	    		die();
	    	}
	    }
	    
	    public function updatesubservice(){
	    	$this->viewBuilder()->layout('ajax');
	    	if($this->request->data)
	    	{
	    		$datatosave = array();
	    		foreach ($this->request->data as $key=>$value)
	    		{
	    			if($key != 'subservice_id')
	    			{
	    				$datatosave[$key] = $value;
	    			}
	    		}
	    		$this->loadModel('Subservices');
	    		$result = $this->Subservices->updatedata($this->request->data['subservice_id'],$datatosave);
	    		echo json_encode($result);
	    		die();
	    	}
	    }
	    
	    public function updatesubservicetag(){
	    	$this->viewBuilder()->layout('ajax');
	    	if($this->request->data)
	    	{
	    		$datatosave = array();
	    		foreach ($this->request->data as $key=>$value)
	    		{
	    			if($key != 'subservicetag_id')
	    			{
	    				$datatosave[$key] = $value;
	    			}
	    		}
	    		$this->loadModel('Subservicetags');
	    		$result = $this->Subservicetags->updatedata($this->request->data['subservicetag_id'],$datatosave);
	    		echo json_encode($result);
	    		die();
	    	}
	    }
}