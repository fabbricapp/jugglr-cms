<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */



namespace App\Controller;
use Cake\ORM\TableRegistry;
use Cake\ORM\Table;



use Cake\Core\Configure;

use Cake\Network\Exception\NotFoundException;

use Cake\View\Exception\MissingTemplateException;

use Cake\Event\Event;



/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */

class UserratingsController extends AppController {

    /**
     * Displays a view
     *
     * @return void|\Cake\Network\Response
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
      public function beforeFilter(Event $event){
		    parent::beforeFilter($event);
		
		    // Allow users to register and logout.
		
		    // You should not add the "login" action to allow list. Doing so would
		
		    // cause problems with normal functioning of AuthComponent.
		
		    $this->Auth->allow(['view','edit']);		
		}
	    
	    public function edit() {
	    	$this->viewBuilder()->layout('admin');
	    	if($this->request->data){
	    		$param = array();
	    	}
	    	if($this->request->query['id']){
	    		$listing = array();
	    		$listing['id'] = 1;
	    		$listing['name'] = 'Ashwin Y';
	    		$listing['subservice'][1]['name'] = 'Subservice-1';
	    		$listing['subservice'][1]['rating'] = 85;
	    		$listing['subservice'][2]['name'] = 'Subservice-2';
	    		$listing['subservice'][2]['rating'] = 77;
	    		$listing['subservice'][3]['name'] = 'Subservice-3';
	    		$listing['subservice'][3]['rating'] = 95;
	    		$listing['subservice'][4]['name'] = 'Subservice-4';
	    		$listing['subservice'][4]['rating'] = 80;
	    		$listing['subservice'][5]['name'] = 'Subservice-5';
	    		$listing['subservice'][5]['rating'] = 65;
	    		$this->set('data',$listing);
	    	}
	    }
}