<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\ORM\Table;
use ZipArchive;
use Cake\Utility\Hash;
/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * @return void
     */
    public function beforeFilter(Event $event)
    {
        $this->Auth->allow(['display']);
        $this->set('current_page',$this->request->params['controller']."-".$this->request->params['action']);
        $this->Admins = TableRegistry::get('Admins');
        $userData = $this->Admins->getData($this->Auth->user('id'));
        $this->set('authUser', $userData);
    }


    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Flash');
        $this->loadComponent("Stripe");
        $this->loadComponent("Firebase");
        $this->loadComponent("Pushnotification");
        $this->loadComponent('Auth', [
           // 'authorize'=> 'Controller',//added this line
            'authenticate' => [
                'Form' => [
                    'userModel' => 'Admins',
                    'fields' => [
                        'username' => 'username',
                        'password' => 'password'
                    ]
                ]
            ],
            'loginAction' => [
                'controller' => 'Admins',
                'action' => 'login'
            ]
        ]);
    
        //... 
        $this->Auth->allow(['display','sendCurlRequest']);
    }
    
    public function encryptdecryptstring($string,$type)
    {
        $secret_key = "bcb04b7e103a0cd8b54763051cef08bc55abe029fdebae5e1d417e2ffb2a00a3";
        
        // Create the initialization vector for added security.
        $iv = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND);
        if($type == 'encrypt')
        {
            //Encrypt $string
            $modified_string = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $secret_key, $string, MCRYPT_MODE_CBC, $iv);
        }
        if($type == 'decrypt')
        {
            //Decrypt $string
            $modified_string = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $secret_key, $encrypted_string, MCRYPT_MODE_CBC, $iv);
        }
        return $modified_string;
    }

    public function getAmountData($amount)
    {
        $Adminsettings = TableRegistry::get('Adminsettings');
        $adminData = $Adminsettings->getsettings();
        if($amount > 0){
            $data['fee']  = number_format(($amount * $adminData['fee_percent'])/100,2);
            $data['tax']  = number_format(($data['fee'] * $adminData['gst_tax'])/100,2);
        }else {
            $data['fee']  = 0.00;
            $data['tax']  = 0.00;
        }
        return $data;
    }

    /*
    *   Function to initiate stripe payment
    */
    public function initiate_payment($offer_id)
    {
        $select = array();
        $condition = array('Usertasks.id' => $offer_id);
        $contain = array('Users','Userwith');
        $this->Usertasks = TableRegistry::get('Usertasks');
        $data = $this->Usertasks->getRecordstoController($select,$condition,$contain);
        $data = $data[0];
        if($data['amount'] > 0)
        {
            $userData['amount']         = ($data['amount']) ? ($data['amount']) : 0.00;
            $userData['discount']       = ($data['discount']) ? ($data['discount']) : 0.00;
            $userData['fee']            = ($data['fee']) ? ($data['fee']) : 0.00;
            $userData['tax']            = ($data['tax']) ? ($data['tax']) : 0.00;
            if($data['need_serviceid'] == 8){
                $userData['stripe_cust_id']    = base64_decode($data['userwith']['stripe_cust_id']);    
                $userData['stripe_acct_id']    = base64_decode($data['user']['stripe_acct_id']);
            }elseif($data['offer_serviceid'] == 8) {
                $userData['stripe_cust_id']    = base64_decode($data['user']['stripe_cust_id']);    
                $userData['stripe_acct_id']    = base64_decode($data['userwith']['stripe_acct_id']);
            }
            if($userData['stripe_acct_id'] != '' && $userData['stripe_cust_id'] != '')
            {
                /*$output = print_r($userData,true);
                file_put_contents(TMP."Stripejob.txt", print_r($output,true),FILE_APPEND);*/
                $detail = $this->Stripe->createTranfer($userData);
            }else {
                return 'BANK_ACCOUNT_OR_CC_DETAIL_NOT_AVAILABLE';
            }
            if($detail['status'] == 1){
                $datatosave = array();
                $datatosave['payment_date']     = date('Y-m-d H:i:s');
                $datatosave['payment_status']   = '1';
                $datatosave['payment_stripetransactionid'] = $detail['charge_id'];
                $this->loadModel('Usertasks');
                $temp = $this->Usertasks->updatetransaction($data['id'],$datatosave);
                return 1;
            }else {
                return 0;
            }
        }   
    }

    public function sendCurlRequest()
    {
        $params = json_decode($this->request->params['pass'][0], true);
        /*$output = print_r($params,true);
        file_put_contents(TMP."Notification-ashwin.txt", print_r($output,true),FILE_APPEND);*/
        switch ($params['url']) {
            case 'notification':
                if($params['redirect'] == 'ADDOFFER' && isset($params['promotion_id'])){
                    $this->Userpromotions = TableRegistry::get('Userpromotions');
                    $datatosave['user_id'] = $params['data']['user_id'];
                    $datatosave['promotion_id'] = $params['promotion_id'];
                    $this->Userpromotions->addUserPromotion($datatosave);
                }elseif($params['redirect'] == 'CANCELLOFFER'){
                    $this->Userpromotions = TableRegistry::get('Userpromotions');
                    $entity = $this->Userpromotions->find()->where(['offer_id'=>$params['data']['offer_id'],'user_id'=>$params['data']['user_id']])->first();
                    $this->Userpromotions->delete($entity);
                }
                $this->Pushnotification->getnotificationMessage($params['redirect'], $params['data']);
                break;
            case 'payment':
                $this->initiate_payment($params['offer_id']);
                break;
            case 'usertableservices':
                $Users = TableRegistry::get('Users');
                $result = $Users->updateuserservicedata($params['data']);
                break;
            case 'updatefirebase':
                $this->updateFirebaseService($params['data']);
                break;
            case 'acceptjoboffer':
                if($params['data']['action'] == 'ASSIGN'){
                    $tableObj = TableRegistry::get('Userjobs');
                    $tableObj->convertJobToOffer($params['data']);
                    $result = $tableObj->getRecordstoController(array('user_id','type'),array('id' => $params['data']['job_id']));
                    $params['data']['job_user'] = $result[0]['user_id'];
                    $params['data']['type'] = $result[0]['type'];
                    $tableObj = TableRegistry::get('Jobapplicants');
                    $result = $tableObj->getRecordstoController(array('user_id'),array('id' => $params['data']['offer_id'],'user_id !=' => $params['data']['applicant_id']));
                    $params['data']['user_arr'] = Hash::extract($result, '{n}.user_id');
                    //$this->Pushnotification->getnotificationMessage('REJECTJOBOFFER', $params['data']);
                    $this->Pushnotification->getnotificationMessage('ACCEPTJOBOFFER', $params['data']);
                }
                break;
            case 'NewJobNotification':
                $user = TableRegistry::get('Users');
                $result = $user->getRecordstoController(array('id'),array('postcode' => $params['data']['postcode'], 'is_active' => '1', 'verified' => '1','id !='=>$params['data']['user_id']));
                $params['data']['user_arr'] = Hash::extract($result, '{n}.id');
                $tableObj = TableRegistry::get('Userjobs');
                $result = $tableObj->getRecordstoController(array('user_id','type'),array('id' => $params['data']['job_id']));
                $params['data']['job_user'] = $result[0]['user_id'];
                $params['data']['type'] = $result[0]['type'];
                $this->Pushnotification->getnotificationMessage('NEWJOB', $params['data']);
                break;
            case 'NewJobCommentNotification':
                $tableObj = TableRegistry::get('Jobcomments');
                $result = $tableObj->getRecordstoController(array('user_id','job_id'),array('job_id' => $params['data']['job_id'],'user_id !='=>$params['data']['user_id']));
                $params['data']['user_arr'] = Hash::extract($result, '{n}.user_id');
                $params['data']['user_arr'] = array_unique($params['data']['user_arr']);
                $tableObj = TableRegistry::get('Userjobs');
                $result = $tableObj->getRecordstoController(array('user_id','type'),array('id' => $params['data']['job_id']));
                $params['data']['job_user'] = $result[0]['user_id'];
                $params['data']['type'] = $result[0]['type'];
                if($params['data']['job_user'] != $params['data']['user_id']){
                    $this->Pushnotification->getnotificationMessage('NEWJOBCOMMENTTOUSER', $params['data']);
                    if(in_array($params['data']['user_id'], $params['data']['user_arr'])){
                        $key = array_search($params['data']['user_id'], $params['data']['user_arr']);
                        unset($params['data']['user_arr'][$key]);
                    }
                }
                $this->Pushnotification->getnotificationMessage('NEWJOBCOMMENTTOOTHER', $params['data']);
                break;
            case 'OfferToJobNotification':
                $tableObj = TableRegistry::get('Userjobs');
                $result = $tableObj->getRecordstoController(array('user_id','type'),array('id' => $params['data']['job_id']));
                if(isset($result[0]['user_id'])){
                    $params['data']['job_user'] = $result[0]['user_id'];
                    $params['data']['type'] = $result[0]['type'];
                    $this->Pushnotification->getnotificationMessage('MAKEOFFERTOJOB', $params['data']);
                }
                break;
            case 'CancellJobNotification':
                $tableObj = TableRegistry::get('Jobcomments');
                $result = $tableObj->getRecordstoController(array('user_id','job_id'),array('job_id' => $params['data']['job_id'],'user_id !='=>$params['data']['user_id']));
                $params['data']['user_arr'] = Hash::extract($result, '{n}.user_id');
                $params['data']['user_arr'] = array_unique($params['data']['user_arr']);
                $this->Pushnotification->getnotificationMessage('CANCELLJOB', $params['data']);
                break;
            default:
                break;
        }
        die();
    }
    
    public function exportcsvs(){
        $action = $this->request->query['action'];
        $requestData = $this->request->query['get_data'];
        
        switch ($action) {
            case 'activeuser':
                $conditions = array();
                $conditions['verified']='1';
                $conditions['is_active']='1';
                if (isset($requestData) && !empty($requestData)) {
                    $conditions['OR']['email like'] = "%" . trim($requestData) . "%";
                    $conditions['OR']['lastname like'] = "%" . trim($requestData) . "%";
                    $conditions['OR']['firstname like'] = "%" . trim($requestData) . "%";
                    $conditions['OR']['suburb like'] = "%" . trim($requestData) . "%";
                    $conditions['OR']['postcode like'] = "%" . trim($requestData) . "%";
                }
                $count = $this->Users->find('all')
                            ->where($conditions)
                            ->count();
                $columns = ['User ID','Firstname','Lastname','Email','Postcode','Suburb','City','State','Country'];
                $fields = ['id','firstname','lastname','email','postcode','suburb','city','state','country'];
                $file_name = 'active_user';
                break;
            case 'pendinguser':
                $conditions = array();
                $conditions['verified']='0';
                $conditions['is_active']='1';
                if (isset($requestData) && !empty($requestData)) {
                    $conditions['OR']['email like'] = "%" . trim($requestData) . "%";
                    $conditions['OR']['lastname like'] = "%" . trim($requestData) . "%";
                    $conditions['OR']['firstname like'] = "%" . trim($requestData) . "%";
                    $conditions['OR']['suburb like'] = "%" . trim($requestData) . "%";
                    $conditions['OR']['postcode like'] = "%" . trim($requestData) . "%";
                }
                $count = $this->Users->find('all')
                            ->where($conditions)
                            ->count();
                $columns = ['User ID','Firstname','Lastname','Email','Postcode','Suburb','City','State','Country'];
                $fields = ['id','firstname','lastname','email','postcode','suburb','city','state','country'];
                $file_name = 'pending_user';
                break;
            case 'inactiveuser':
                $conditions = array();
                $conditions['is_active']='0';
                if (isset($requestData) && !empty($requestData)) {
                    $conditions['OR']['email like'] = "%" . trim($requestData) . "%";
                    $conditions['OR']['lastname like'] = "%" . trim($requestData) . "%";
                    $conditions['OR']['firstname like'] = "%" . trim($requestData) . "%";
                    $conditions['OR']['suburb like'] = "%" . trim($requestData) . "%";
                    $conditions['OR']['postcode like'] = "%" . trim($requestData) . "%";
                }
                $count = $this->Users->find('all')
                            ->where($conditions)
                            ->count();
                $columns = ['User ID','Firstname','Lastname','Email','Postcode','Suburb','City','State','Country'];
                $fields = ['id','firstname','lastname','email','postcode','suburb','city','state','country'];
                $file_name = 'inactive_users';
                break;
            case 'summary':
                $select = $conditions = [];
                $contain    = ['Usertasks','Userservices'];
                if (isset($requestData) && !empty($requestData)) {
                    $conditions['OR']['email like'] = "%" . trim($requestData) . "%";
                    $conditions['OR']['lastname like'] = "%" . trim($requestData) . "%";
                    $conditions['OR']['firstname like'] = "%" . trim($requestData) . "%";
                    $conditions['OR']['suburb like'] = "%" . trim($requestData) . "%";
                    $conditions['OR']['postcode like'] = "%" . trim($requestData) . "%";
                    if($requestData == 'ios' || $requestData == 'android'){
                        $conditions['OR']['device_type'] = trim($requestData);
                    }
                }
                $count = $this->Users->find('all')
                            ->where($conditions)
                            ->contain($contain)
                            ->count();
                $columns = ['User ID','OS','Firstname','Lastname','Email','Postcode','Suburb','City','State','Country','RegisteredAt','Status','Verified By','Verified At','Total Transaction','Complete Transaction','Cancelled Transaction','Current Transaction','Paid Transaction','Paid Value','Total Fee','Positive Rating','Neutral Rating'];
                $fields = ['id','os','firstname','lastname','email','postcode','suburb','city','state','country','createdAt','status','verified_by','verified_at','total_transaction','complete_transaction','cancelled_transaction','current_transaction','paid_transaction','paid_value_transaction','total_fee','positive_rating','neutral_rating'];
                $file_name = 'user_summary';
                break;
            case 'tasksummary':
                $select = $conditions = [];
                $contain = ['Services','Usertasks'];
                if (isset($requestData) && !empty($requestData)) {
                    $conditions['Subservices.subservice_name like'] = "%" . trim($requestData) . "%";
                }
                $Subservices = TableRegistry::get('Subservices');
                $count = $Subservices->find('all')
                            ->where($conditions)
                            ->contain($contain)
                            ->count();
                $columns = ['Subservice','Service','Total Volume','Total Value','Total Fee','Total Tax','Total Invoiced','Total Completed','Total Current','Total Cancelled'];
                $fields = ['subservice_name','service_name','volume','amount','fee','tax','invoiced','complete_cnt','current_cnt','cancelled_cnt'];
                $file_name = 'task_summary';
                break;
            case 'taskview':
                $select = $conditions = [];
                $contain = ['Services','Subservices','offered_service','offered_subservice',
                    'Users'=> function ($q) {
                        return $q
                        ->select(['Users.id','Users.firstname','Users.lastname']);
                    },
                    'Userwith'=> function ($q) {
                        return $q
                        ->select(['Userwith.id','Userwith.firstname','Userwith.lastname']);
                    }
                ];
                if (isset($requestData) && !empty($requestData)) {
                    $conditions['Usertasks.id'] = trim($requestData);
                }
                $Usertasks = TableRegistry::get('Usertasks');
                $count = $Usertasks->find('all')
                            ->where($conditions)
                            ->contain($contain)
                            ->count();
                $columns = ['Trans ID','Provider','FeedbackBy','Initiator','Requested Date','Completion Date','Rating','Rating Date','Service','Subservice','Location','Note','Trans Value','Fee','Tax'];
                $fields = ['id','requestor','feedbackby','request_date','completion_date','rating','rating_date','service','subservice','location','note','transaction_value','transaction_fee','transaction_tax','initiator'];
                $file_name = 'task_view';
                break;
            case 'ratingsummary':
                $select = $conditions = [];
                $contain = [
                    'Usertasks'=> function ($q) {
                        return $q
                        ->select(['Usertasks.id','Usertasks.user_id','Usertasks.user_with','Usertasks.need_serviceid','Usertasks.need_subserviceid','Usertasks.offer_serviceid','Usertasks.offer_subserviceid','Usertasks.amount','Usertasks.fee'])
                        ->where(['Usertasks.payment_status'=>'1']);
                    }
                ];
                $count = $this->Users->find('all')
                            ->where($conditions)
                            ->contain($contain)
                            ->count();
                $columns = ['User ID','Name','Average Rating','Total Ratings','Positive Rating','Neutral Rating','Total Value','Total Fee'];
                $fields = ['id','name','total_value','total_fee','total_positive_rating','total_neutral_rating','average_rating','total_rating'];
                $file_name = 'rating_summary';
                break;
            case 'tasklist':
                $conditions = [];
                $contain = ['Services','Subservices','offered_service','offered_subservice',
                    'Users'=> function ($q) {
                        return $q
                        ->select(['Users.id','Users.firstname','Users.lastname']);
                    },
                    'Userwith'=> function ($q) {
                        return $q
                        ->select(['Userwith.id','Userwith.firstname','Userwith.lastname']);
                    }
                ];
                $Usertasks = TableRegistry::get('Usertasks');
                $count = $Usertasks->find('all')
                            ->where($conditions)
                            ->contain($contain)
                            ->count();
                $columns = ['Trans ID','Requestor','Service Provider','Transaction Status','Accept Date','Initiator','Service','Subservice','Service Date','Location','Note','Transaction Value','Transaction Fee','Transaction Tax','Invoice Value'];
                $fields = ['id','requestor','serviceprovider','accept_date','status','service','subservice','date','location','note','transaction_value','transaction_fee','transaction_tax','invoice_value','initiator'];
                $file_name = 'task_list';
                break;
            case 'userlistview':
                $conditions = [];
                $contain    = ['Users','Subservices','offered_subservice'];
                $Usertasks = TableRegistry::get('Usertasks');
                $count = $Usertasks->find('all')
                            ->where($conditions)
                            ->contain($contain)
                            ->count();
                $columns = ['Trans ID','FirstName','LastName','Email','Postcode','Suburb','City','State','Country','Status','Subservice Needed','Subservice Offered','Transaction Amount','Transaction Fee','Date Requested','Date Completed','Date Paid'];
                $fields = ['transaction_id','firstname','lastname','email','postcode','suburb','city','state','country','transaction_status','subservice_needed','subservice_offered','transaction_amount','transaction_fee','date_requested','date_completed','date_paid'];
                $file_name = 'user_list_view';
                break;
            default:
                # code...
                break;
        }

        $i=0;
        $page = 0;
        $file_counter =0;
        $Final = [];
        while ( $i < $count ) {
            $page++;
            $users = $this->paginate_export($page, $action);
            $Final[] = $users;
            $i += count($users);
            $file_counter++;
        }
        /* If single CSV, simply allow to download */
        if($file_counter == 1){
            header('Content-Type: text/csv; charset=utf-8');
            header('Content-Disposition: attachment; filename=csv_export_'.$file_name.'.csv');
            $output = fopen('php://output', 'w');
            fputcsv($output, $columns);

            foreach ($Final[0] as $key => $value) {
                if(!is_array($value)){
                    $value = $value->toArray();
                }
                $data = [];
                if(is_array($value)){
                    foreach ($fields as $field) {
                        if(is_array($value[$field])){
                            
                        }else {
                            $data[] = $value[$field];
                        }        
                    }
                }else {
                    $data[] = $value[$field];
                }
                fputcsv($output, $data);
            }
            fclose($output);
        }else{
            /*Create folder and file*/
            $zipname = $file_name.'.zip';
            if(file_exists($zipname)){
                unlink($zipname);
            }
            $dirname = dirname($zipname);
            if (!is_dir($dirname))
            {
                mkdir($dirname, 0777, true);
            }
            $zip = new ZipArchive;
            $zip->open($zipname, ZipArchive::CREATE);
            $i=1;
            foreach ($Final as $key => $filedata){

                // create a temporary file
                $output = fopen('php://temp/maxmemory:1048576', 'w');
                if (false === $output) {
                    die('Failed to create temporary file.');
                }

                // write the data to csv                
                fputcsv($output, $columns);
                foreach($filedata as $value) {
                    $data = [];
                    foreach ($fields as $key => $field) {
                        $data[] = $value[$field];
                    }
                    fputcsv($output, $data);
                }

                // return to the start of the stream
                rewind($output);

                // add the in-memory file to the archive, giving a name
                $zip->addFromString($file_name.'-'.$i.'.csv', stream_get_contents($output) );
                $i++;
                //close the file
                fclose($output);
            }
            // close the archive
            $zip->close();
            
            header('Content-Type: application/zip');
            header('Content-disposition: attachment; filename='.$zipname);
            header('Content-Length: ' . filesize($zipname));
            readfile($zipname);

            // remove the zip archive
            // you could also use the temp file method above for this.
            unlink($zipname);
        }
        die();
    }

    protected function paginate_export($page, $action){
        switch ($action) {
            case 'activeuser':
                $conditions = array();
                $conditions['verified']='1';
                $conditions['is_active']='1';
                $this->paginate =[
                    'conditions' => [$conditions],
                    'fields' => ['Users.id','Users.firstname','Users.email','Users.lastname','Users.postcode','Users.suburb','Users.city','Users.state','Users.country'],
                    'limit' => 10000,
                    'page'=>$page
                ];
                $data = $this->paginate('Users');
                $data = $data->toArray();
                break;
            case 'pendinguser':
                $conditions = array();
                $conditions['verified']='0';
                $conditions['is_active']='1';
                $this->paginate =[
                    'conditions' => [$conditions],
                    'fields' => ['Users.id','Users.firstname','Users.email','Users.lastname','Users.postcode','Users.suburb','Users.city','Users.state','Users.country'],
                    'limit' => 10000,
                    'page'=>$page
                ];
                $data = $this->paginate('Users');
                $data = $data->toArray();
                break;
            case 'inactiveuser':
                $conditions = array();
                $conditions['is_active']='0';
                $this->paginate =[
                    'conditions' => [$conditions],
                    'fields' => ['Users.id','Users.firstname','Users.email','Users.lastname','Users.postcode','Users.suburb','Users.city','Users.state','Users.country'],
                    'limit' => 10000,
                    'page'=>$page
                ];
                $data = $this->paginate('Users');
                $data = $data->toArray();
                break;
            case 'summary':
                $conditions = [];
                $this->paginate =[
                    'conditions' => [$conditions],
                    'contain' => ['Usertasks','Userservices'],
                    'fields' => ['Users.id','Users.firstname','Users.email','Users.lastname','Users.postcode','Users.suburb','Users.city','Users.state','Users.country','Users.is_active','Users.verified_at','Users.verified_by','Users.createdAt','Users.verified'],
                    'limit' => 500,
                    'page'=>$page
                ];
                $users = $this->paginate('Users');
                $data   = array(); 
                if(!empty($users))
                {   
                    $i = 0;
                    foreach ($users as $datavalue)
                    {
                        $data[$i]['id']                         = $datavalue['id'];
                        $data[$i]['os']                         = $datavalue['device_type'];
                        $data[$i]['firstname']                  = $datavalue['firstname'];
                        $data[$i]['lastname']                   = $datavalue['lastname'];
                        $data[$i]['email']                      = $datavalue['email'];
                        $data[$i]['postcode']                   = $datavalue['postcode'];
                        $data[$i]['suburb']                     = $datavalue['suburb'];
                        $data[$i]['city']                       = $datavalue['city'];
                        $data[$i]['state']                      = $datavalue['state'];
                        $data[$i]['country']                    = $datavalue['country'];
                        $data[$i]['createdAt']                  = (!empty($datavalue['createdAt'])) ? date_format($datavalue['createdAt'],'Y-m-d') : "";
                        $data[$i]['verified_at']                = (!empty($datavalue['verified_at'])) ? date_format($datavalue['verified_at'],'Y-m-d H:i:s') : "";
                        $data[$i]['total_transaction']          = 0;
                        $data[$i]['complete_transaction']       = 0;
                        $data[$i]['cancelled_transaction']      = 0;
                        $data[$i]['current_transaction']        = 0;
                        $data[$i]['paid_transaction']           = 0;
                        $data[$i]['paid_value_transaction']     = 0;
                        $data[$i]['total_fee']                  = 0;
                        $data[$i]['average_time_completion']    = 0;
                        $data[$i]['average_time_payment']       = 0;
                        $data[$i]['positive_rating']            = 0;
                        $data[$i]['neutral_rating']             = 0;
                        $data[$i]['verified_by']                = $datavalue['verified_by'];
                        if($datavalue['verified'] == '1' && $datavalue['is_active'] == '1')
                        {
                            $data[$i]['status']                 = 'verified';
                        }elseif ($datavalue['verified'] == '0' && $datavalue['is_active'] == '1'){
                            $data[$i]['status']                 = 'verification Pending';
                        }elseif ($datavalue['is_active'] == '0'){
                            $data[$i]['status']                 = 'inactive';
                        }
                        if(!empty($datavalue['userservices']))
                        {
                            foreach ($datavalue['userservices'] as $key=>$value)
                            {
                                $data[$i]['positive_rating']    += $value['positive_rating'];
                                $data[$i]['neutral_rating']     += $value['neutral_rating'];
                            }
                        }
                        if(!empty($datavalue['usertasks']))
                        {
                            foreach ($datavalue['usertasks'] as $key=>$value)
                            {
                                $data[$i]['total_transaction']++;
                                if($value['status'] == '1')
                                {
                                    $data[$i]['complete_transaction']++;
                                }
                                if($value['status'] == '2')
                                {
                                    $data[$i]['cancelled_transaction']++;
                                }
                                if($value['status'] == '0')
                                {
                                    $data[$i]['current_transaction']++;
                                }
                                if($value['payment_status'] == '1')
                                {
                                    $data[$i]['paid_transaction']++;
                                    $data[$i]['total_fee']              += $value['fee'];
                                    $data[$i]['paid_value_transaction'] += $value['amount'];
                                }
                            }
                        }
                        $i++;
                    }
                }
                break;
            case 'tasksummary':
                $conditions = array();
                $contain = ['Services','Usertasks'];
                $this->paginate =[
                    'conditions' => $conditions,
                    'contain' => $contain,
                    'limit' => 10000,
                    'page'=>$page
                ];
                $data = $this->paginate('Subservices');
                $data = $this->Subservices->get_tasksummary($data);
                break;
            case 'taskview':
                $conditions = array();
                $contain = ['Services','Subservices','offered_service','offered_subservice',
                    'Users'=> function ($q) {
                        return $q
                        ->select(['Users.id','Users.firstname','Users.lastname']);
                    },
                    'Userwith'=> function ($q) {
                        return $q
                        ->select(['Userwith.id','Userwith.firstname','Userwith.lastname']);
                    }
                ];
                $this->paginate =[
                    'conditions' => $conditions,
                    'contain' => $contain,
                    'limit' => 10000,
                    'page'=>$page
                ];
                $data = $this->paginate('Usertasks');
                $data = $this->Usertasks->getDataForUsertaskView($data);
                break;
            case 'ratingsummary':
                $select = ['Users.id','Users.firstname','Users.lastname','Users.profile_image_thumb','positive_rating','neutral_rating'];
                $conditions = [];
                $contain = [
                    'Usertasks'=> function ($q) {
                        return $q
                        ->select(['Usertasks.id','Usertasks.user_id','Usertasks.user_with','Usertasks.need_serviceid','Usertasks.need_subserviceid','Usertasks.offer_serviceid','Usertasks.offer_subserviceid','Usertasks.amount','Usertasks.fee'])
                        ->where(['Usertasks.payment_status'=>'1']);
                    }
                ];
                $this->paginate = [
                    'fields' => $select,
                    'conditions' => $conditions,
                    'contain' => $contain,
                    'limit' => 10000,
                    'page' => $page
                ];
                $data = $this->paginate('Users');
                $data = $this->Users->getratingsummary($data);
                break;
            case 'tasklist':
                $select = $conditions = [];
                $contain = ['Services','Subservices','offered_service','offered_subservice',
                    'Users'=> function ($q) {
                        return $q
                        ->select(['Users.id','Users.firstname','Users.lastname']);
                    },
                    'Userwith'=> function ($q) {
                        return $q
                        ->select(['Userwith.id','Userwith.firstname','Userwith.lastname']);
                    }
                ];
                $this->paginate = [
                    'fields' => $select,
                    'conditions' => $conditions,
                    'contain' => $contain,
                    'limit' => 10000,
                    'page' => $page
                ];
                $data = $this->paginate('Usertasks');
                $data = $this->Usertasks->getDataForUsertaskList($data);
                break;
            case 'userlistview':
                $select = $conditions = [];
                $contain    = ['Users','Subservices','offered_subservice'];
                $this->paginate = [
                    'fields' => $select,
                    'conditions' => $conditions,
                    'contain' => $contain,
                    'limit' => 10000,
                    'page' => $page
                ];
                $data = $this->paginate('Usertasks');
                $data = $this->Usertasks->getDataForUserlistview($data);
                break;
            default:
                $data = array();
                break;
        }
        return $data;
    }


    public function updateFirebaseService($data)
    {
        if($data['service'] == 'subservice') {
            $type = ($data['type'] == 'need') ? '1' : '2';
            $path = '/service/'.$data['type'].'/'.$data['service'].'/';
            if(!empty($data['checked_subservice_ids'])){
                $checked_subservice_ids = explode(',', trim($data['checked_subservice_ids'],'"'));
                foreach ($checked_subservice_ids as $key => $value) {
                   $result =  $this->Firebase->dataGet('SUBSERVICE-'.$value, $type);    
                   if(!in_array($data['user_id'], $result)){
                        $result[] = $data['user_id'];
                        $store[$value] = implode(',', $result);
                        $this->Firebase->dataPush($store,'',$path.$value);
                   }
                }
            }
            if(!empty($data['unchecked_subservice_ids'])){
                $unchecked_subservice_ids = explode(',', trim($data['unchecked_subservice_ids'],'"'));
                foreach ($unchecked_subservice_ids as $key => $value) {
                   $result =  $this->Firebase->dataGet('SUBSERVICE-'.$value, $type);    
                   if(in_array($data['user_id'], $result)){
                        unset($result[array_search ($data['user_id'], $result)]);
                        $store[$value] = implode(',', $result);
                        $this->Firebase->dataPush($store,'',$path.$value);
                   }
                }   
            }
        }elseif ($data['service'] == 'subservicetag') {
            $type = ($data['type'] == 'need') ? '1' : '2';
            $path = '/service/'.$data['type'].'/'.$data['service'].'/';
            if(!empty($data['checked_subservicetag_ids'])){
                $checked_subservicetag_ids = explode(',', trim($data['checked_subservicetag_ids'],'"'));
                foreach ($checked_subservicetag_ids as $key => $value) {
                   $result =  $this->Firebase->dataGet('SUBSERVICETAG-'.$value, $type);    
                   if(!in_array($data['user_id'], $result)){
                        $result[] = $data['user_id'];
                        $store[$value] = implode(',', $result);
                        $this->Firebase->dataPush($store,'',$path.$value);
                   }
                }
            }
            if(!empty($data['unchecked_subservicetag_ids'])){
                $unchecked_subservicetag_ids = explode(',', trim($data['unchecked_subservicetag_ids'],'"'));
                foreach ($unchecked_subservicetag_ids as $key => $value) {
                   $result =  $this->Firebase->dataGet('SUBSERVICETAG-'.$value, $type);    
                   if(in_array($data['user_id'], $result)){
                        unset($result[array_search ($data['user_id'], $result)]);
                        $store[$value] = implode(',', $result);
                        $this->Firebase->dataPush($store, '', $path.$value);
                   }
                } 
            }
        }elseif ($data['service'] == 'service') {
            $subservice_need_path = '/service/need/subservice/';
            $subservicetag_need_path = '/service/need/subservicetag/';
            $subservice_offer_path = '/service/offer/subservice/';
            $subservicetag_offer_path = '/service/offer/subservicetag/';
            $services = TableRegistry::get('Services');
            if(!empty($data['unchecked_offer_service_id'])){
                $type = 2;
                $unchecked_offer_service_id = explode(',', trim($data['unchecked_offer_service_id'], '"'));
                $serviceData = $services->find()
                ->where(['Services.id IN' => $unchecked_offer_service_id])
                ->select(['Services.id'])
                ->contain(['Subservices.Subservicetags'])
                ->all();
                if($serviceData){
                    $serviceData = $serviceData->toArray();
                    foreach ($serviceData as $key => $val) {
                        $subservice_ids = Hash::extract($val['subservices'], '{n}.id');
                        foreach ($subservice_ids as $key => $value) {
                           $result =  $this->Firebase->dataGet('SUBSERVICE-'.$value, $type);    
                           if(in_array($data['user_id'], $result)){
                                unset($result[array_search ($data['user_id'], $result)]);
                                $store[$value] = implode(',', $result);
                                $this->Firebase->dataPush($store,'',$subservice_offer_path.$value);
                           }
                        }
                        $subservicetag_ids = Hash::extract($val['subservicetags'], '{n}.id');
                        foreach ($subservicetag_ids as $key => $value) {
                           $result =  $this->Firebase->dataGet('SUBSERVICETAG-'.$value, $type);    
                           if(in_array($data['user_id'], $result)){
                                unset($result[array_search ($data['user_id'], $result)]);
                                $store[$value] = implode(',', $result);
                                $this->Firebase->dataPush($store,'',$subservicetag_offer_path.$value);
                           }
                        }
                    }
                }
            }
            if(!empty($data['unchecked_need_service_id'])){
                $type = 1;
                $unchecked_need_service_id = explode(',', trim($data['unchecked_need_service_id'],'"'));
                $serviceData = $services->find()
                ->where(['Services.id IN' => $unchecked_need_service_id])
                ->select(['Services.id'])
                ->contain(['Subservices.Subservicetags'])
                ->all();
                if($serviceData){
                    $serviceData = $serviceData->toArray();
                    foreach ($serviceData as $key => $val) {
                        $subservice_ids = Hash::extract($val['subservices'], '{n}.id');
                        foreach ($subservice_ids as $key => $value) {
                           $result =  $this->Firebase->dataGet('SUBSERVICE-'.$value, $type);    
                           if(in_array($data['user_id'], $result)){
                                unset($result[array_search ($data['user_id'], $result)]);
                                $store[] = implode(',', $result);
                                $this->Firebase->dataPush($store,'',$subservice_need_path);
                           }
                        }
                        $subservicetag_ids = Hash::extract($val['subservicetags'], '{n}.id');
                        foreach ($subservicetag_ids as $key => $value) {
                           $result =  $this->Firebase->dataGet('SUBSERVICETAG-'.$value, $type);    
                           if(in_array($data['user_id'], $result)){
                                unset($result[array_search ($data['user_id'], $result)]);
                                $store[] = implode(',', $result);
                                $this->Firebase->dataPush($store,'',$subservicetag_need_path);
                           }
                        }
                    }
                }
            }
            if(!empty($data['checked_need_service_id'])){
                $type = 1;
                $checked_need_service_id = explode(',', trim($data['checked_need_service_id'],'"'));
                $serviceData = $services->find()
                ->where(['Services.id IN' => $checked_need_service_id])
                ->select(['Services.id'])
                ->contain(['Subservices.Subservicetags'])
                ->all();
                if($serviceData){
                    $serviceData = $serviceData->toArray();
                    foreach ($serviceData as $key => $val) {
                        $subservice_ids = Hash::extract($val['subservices'], '{n}.id');
                        foreach ($subservice_ids as $key => $value) {
                           $result =  $this->Firebase->dataGet('SUBSERVICE-'.$value, $type);    
                           if(!in_array($data['user_id'], $result)){
                                $result[] = $data['user_id'];
                                $store[] = implode(',', $result);
                                $this->Firebase->dataPush($store,'',$subservice_need_path);
                           }
                        }
                        $subservicetag_ids = Hash::extract($val['subservicetags'], '{n}.id');
                        foreach ($subservicetag_ids as $key => $value) {
                           $result =  $this->Firebase->dataGet('SUBSERVICETAG-'.$value, $type);    
                           if(!in_array($data['user_id'], $result)){
                                $result[] = $data['user_id'];
                                $store[] = implode(',', $result);
                                $this->Firebase->dataPush($store,'',$subservicetag_need_path);
                           }
                        }
                    }
                }
            }
            if(!empty($data['checked_offer_service_id'])){
                $type = 2;
                $checked_offer_service_id = explode(',', trim($data['checked_offer_service_id'],'"'));
                $serviceData = $services->find()
                ->where(['Services.id IN' => $checked_offer_service_id])
                ->select(['Services.id'])
                ->contain(['Subservices.Subservicetags'])
                ->all();
                if($serviceData){
                    $serviceData = $serviceData->toArray();
                    foreach ($serviceData as $key => $val) {
                        $subservice_ids = Hash::extract($val['subservices'], '{n}.id');
                        foreach ($subservice_ids as $key => $value) {
                           $result =  $this->Firebase->dataGet('SUBSERVICE-'.$value, $type);    
                           if(!in_array($data['user_id'], $result)){
                                $result[] = $data['user_id'];
                                $store[] = implode(',', $result);
                                $this->Firebase->dataPush($store,'', $subservice_offer_path);
                           }
                        }
                        $subservicetag_ids = Hash::extract($val['subservicetags'], '{n}.id');
                        foreach ($subservicetag_ids as $key => $value) {
                           $result =  $this->Firebase->dataGet('SUBSERVICETAG-'.$value, $type);    
                           if(!in_array($data['user_id'], $result)){
                                $result[] = $data['user_id'];
                                $store[] = implode(',', $result);
                                $this->Firebase->dataPush($store,'', $subservicetag_offer_path);
                           }
                        }
                    }
                }
            }
        }
        die();
    }
}
