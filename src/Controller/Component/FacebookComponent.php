<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\ORM\Table;
use Facebook;

class FacebookComponent extends Component
{
    private $fb;
        private $app_id;
        private $app_secret;

    public function __construct() 
    {
            $this->app_id = Configure::read('FB_APP_ID');
            $this->app_secret = Configure::read('FB_SECREAT_KEY');
        
        $this->fb = new Facebook\Facebook([
            'app_id' => $this->app_id,
            'app_secret' => $this->app_secret,
            'default_graph_version' => 'v2.5',
            ]);
    }

    public function login($loginCallbackUrl = null) 
    {
        $helper = $this->fb->getRedirectLoginHelper();

        $permissions = ['email, public_profile, user_friends, user_about_me'];
        $loginUrl = $helper->getLoginUrl($loginCallbackUrl, $permissions);
        
        return htmlspecialchars($loginUrl);
    }

    public function logout($logoutCallbackUrl = null) 
    {
        $helper = $this->fb->getRedirectLoginHelper();

        $logoutUrl = $helper->getLogoutUrl($_SESSION['facebook_access_token'], $logoutCallbackUrl);

        return htmlspecialchars($logoutUrl);
    }

    public function getAccessToken() 
    {
        $helper = $this->fb->getRedirectLoginHelper();

        try {
            $accessToken = $helper->getAccessToken();
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            // When Graph returns an error
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            // When validation fails or other local issues
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }

        if (isset($accessToken)) {
            return $accessToken;
        } else {
            return false;
        }
    }

    // Get data from facebook
    public function getData($session, $method, $api, $params = []) 
    {

        $request = $this->fb->request($method, $api, $params, $session);
        $response = $this->fb->getClient()->sendRequest($request);
        $data = $response->getDecodedBody();

        return $data;
    }
        
    /*
     * update access token with long-lived
     */
    public function getlonglivedtoken($fb_token=null,$user_id) {
        if($fb_token != '')
        {
            $params['grant_type'] = 'fb_exchange_token';
            $params['client_id'] = $this->app_id;
            $params['client_secret'] = $this->app_secret;
            $params['fb_exchange_token'] = $fb_token;
            $params['fields'] = 'context.fields(mutual_friends)';
            $result = $this->getData($fb_token,'GET','/oauth/access_token',$params);
            if(isset($result['access_token'])){
                $user = TableRegistry::get('Users');
                $datatosave['facebook_token'] = $result['access_token'];
                $result = $user->updatedata($user_id,$datatosave);
            }
        }
        return true;
    }

    /*
     * Get likes for user
     */
    public function getUserLikes($fb_token=null,$user_id) {
        $next = '';
        $params = array();
        $result = $this->getData($fb_token,'GET','/me/likes?limit=10',$params);
        if(!empty($result['data']))
        {
            $i = 0;
            foreach ($result['data'] as $key => $value){
                $datatosave[$i]['name'] = $value['name'];
                $datatosave[$i]['id']   = $value['id'];
                $i++;
            }
            if(isset($result['paging']['next'])){
                $next = $result['paging']['cursors']['after'];
            }else {
                $next = '';
            }
            while ($next) {
                $params = array();
                $params['after'] = $next;
                $result = $this->getData($fb_token,'GET','/me/likes?limit=10',$params);
                if(!empty($result['data']))
                {
                    foreach ($result['data'] as $key => $value){
                        $datatosave[$i]['name'] = $value['name'];
                        $datatosave[$i]['id']   = $value['id'];
                        $i++;
                    }
                }
                if(isset($result['paging']['next'])){
                    $next = $result['paging']['cursors']['after'];
                }else {
                    $next = '';
                }
            }
        }else {
            $datatosave = array();
        }
        return $datatosave;
    }
        
    /*
     * Get user mutual friends who use Jugglr
     */
    public function getmutualFriends($fb_token=null,$user_id) {
        $next = '';
        $params = array();
        try{
            $result = $this->getData($fb_token,'GET','/me/friends?limit=10',$params);
            if(!empty($result['data']))
            {
                
                $i = 0;
                foreach ($result['data'] as $key => $value){
                    $datatosave[$i]['name'] = $value['name'];
                    $datatosave[$i]['id']   = $value['id'];
                    $i++;
                }
                if(isset($result['paging']['next'])){
                    $next = $result['paging']['cursors']['after'];
                }else {
                    $next = '';
                }
                while ($next) {
                    $params = array();
                    $params['after'] = $next;
                    $result = $this->getData($fb_token,'GET','/me/friends?limit=10',$params);
                    if(!empty($result['data']))
                    {
                        foreach ($result['data'] as $key => $value){
                            $datatosave[$i]['name'] = $value['name'];
                            $datatosave[$i]['id']   = $value['id'];
                            $i++;
                        }
                    }
                    if(isset($result['paging']['next'])){
                        $next = $result['paging']['cursors']['after'];
                    }else {
                        $next = '';
                    }
                }
            }else {
                $datatosave = array();
            }
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            // When Graph returns an error
            $datatosave = array();
        }
        return $datatosave;
    }

    /*
     * Get user mutual friends who use Jugglr
     */
    public function getUserFBData($fb_token=null,$user_id) {
        $next = '';
        $params = array();
        try{
            $url = '';
            $result = $this->getData($fb_token,'GET','/me/picture?redirect=false',$params);
            if(isset($result['data']['url']))
            {
                if(!empty($result['data']['url'])){
                    $url = $result['data']['url'];
                }
            }
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            // When Graph returns an error
            $url = '';
        }
        return $url;
    }

    public function getUserFBImage($fb_id=null) {
        $next = '';
        $params = array();
        try{
            $url = '';
            $result = $this->getData('','GET','/'.$fb_id.'/picture',$params);
            print_r($result);die();
            if(isset($result['data']['url']))
            {
                if(!empty($result['data']['url'])){
                    $url = $result['data']['url'];
                }
            }
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            // When Graph returns an error
            $url = '';
        }
        return $url;
    }

}
