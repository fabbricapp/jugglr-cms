<?php
namespace App\Controller\Component;

include CAKE_VENDOR.'autoload.php'; 

use Cake\Controller\Component;
use Cake\Core\Configure;

//use Ktamas77\firebaseLib;
//require_once CAKE_VENDOR . "ktamas77/firebasephp/src/firebaseLib.php";
//use Ktamas77\firebasephp\src\firebaseInterface;

class FirebaseComponent extends Component
{
    protected $_firebase;
    private $DEFAULT_URL;

    function __construct()
    {
        //fcm notification
        $this->DEFAULT_URL = Configure::read('FIREBASE_DATABASE');
    }

    // --- set up your own database here
    //const DEFAULT_URL = 'https://jugglrapp.firebaseio.com/';
    //const DEFAULT_URL = 'https://jugglrtest.firebaseio.com/';
    //const DEFAULT_TOKEN = 'xAbuEhtpgvEnR2BVEG7IUguHSggpyZcLZlba0D2f';
    const DEFAULT_TOKEN = '';

    const DEFAULT_SERVICE_NEED_SUBSERVICE_PATH = '/service/need/subservice';
    const DEFAULT_SERVICE_OFFER_SUBSERVICE_PATH = '/service/offer/subservice';
    const DEFAULT_SERVICE_NEED_SUBSERVICETAG_PATH = '/service/need/subservicetag';
    const DEFAULT_SERVICE_OFFER_SUBSERVICETAG_PATH = '/service/offer/subservicetag';
    const DEFAULT_BUSINESS_USER_PATH = '/userprofile';

    const DEFAULT_SET_RESPONSE = '{"name":"Pick the milk","priority":1}';
    const DEFAULT_UPDATE_RESPONSE = '{"name":"Pick the beer","priority":2}';
    const DEFAULT_PUSH_RESPONSE = '{"name":"Pick the LEGO","priority":3}';
    const DEFAULT_DELETE_RESPONSE = 'null';
    const DEFAULT_URI_ERROR = 'You must provide a baseURI variable.';

    protected function setUp()
    {
        $this->_firebase = new \Firebase\FirebaseLib($this->DEFAULT_URL, self::DEFAULT_TOKEN);
    }

    protected $_todoLEGO = array(
        'name' => 'Nirav',
        'postcode' => 6033,
        'suburb' => 'Perth'
    );

    public function dataPush($data, $type, $path=null)
    {
        $this->setUp();
        foreach ($data as $key => $value) {
            if($type == 'DEFAULT_SERVICE_NEED_SUBSERVICE_PATH'){
                $path = self::DEFAULT_SERVICE_NEED_SUBSERVICE_PATH."/".$key;
            }elseif ($type == 'DEFAULT_SERVICE_OFFER_SUBSERVICE_PATH') {
                $path = self::DEFAULT_SERVICE_OFFER_SUBSERVICE_PATH."/".$key;
            }elseif ($type == 'DEFAULT_SERVICE_NEED_SUBSERVICETAG_PATH') {
                $path = self::DEFAULT_SERVICE_NEED_SUBSERVICETAG_PATH."/".$key;
            }elseif ($type == 'DEFAULT_SERVICE_OFFER_SUBSERVICETAG_PATH') {
                $path = self::DEFAULT_SERVICE_OFFER_SUBSERVICETAG_PATH."/".$key;
            }else {
                $path = $path;
            }
            $val = array('result' => $value);
            $response = $this->_firebase->set($path, $val);    
        }
        //$this->assertRegExp('/{"name"\s?:\s?".*?}/', $response);
        //return $this->_parsePushResponse($response);
    }

    public function dataGet($text, $toggle)
    {
        $this->setUp();
        $text_arr = explode('-', $text);
        $type = ($text_arr[0] == 'SUBSERVICE') ? 'subservice/' : 'subservicetag/';
        if($toggle == 1){
            $path = '/service/need/'.$type.$text_arr[1].'/result';
        }elseif ($toggle == 2) {
            $path = '/service/offer/'.$type.$text_arr[1].'/result';
        }elseif ($toggle == 3) {
            $path = '/service/need/'.$type.$text_arr[1].'/result';
            $path = '/service/offer/'.$type.$text_arr[1].'/result';
        }
        $response = $this->_firebase->get($path);
        $response = trim($response, '""');
        
        if($toggle == 3){
            $temp_arr = $this->_firebase->get($path2);
            $temp_arr = trim($temp_arr, '""');
            $response .= ",".$temp_arr;
        }
        $response = array_unique(explode(',', $response));
        return $response;
    }

    public function dataDelete()
    {
        $this->setUp();
        $response = $this->_firebase->delete(self::DEFAULT_USER_PATH);
        $this->assertEquals(self::DEFAULT_DELETE_RESPONSE, $response);
    }

    /**
     * @param $response
     * @return mixed
     */
    private function _parsePushResponse($response)
    {
        $responseObj = json_decode($response);
        return $responseObj->name;
    }
}
