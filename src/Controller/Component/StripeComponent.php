<?php
namespace App\Controller\Component;

include CAKE_VENDOR.'autoload.php';

use Cake\Controller\Component;
use Cake\Core\Configure;

use Stripe\Stripe;
use Stripe\Token;
use Stripe\Account;
use Stripe\Charge;
use Stripe\Error\Card;
use Stripe\Customer;
use Stripe\Transfer;
use Stripe\FileUpload;

class StripeComponent extends Component
{
    private $api_key;
    
    function __construct()
    {
        $this->api_key = Configure::read('STRIPE_API_KEY');
    }
    
    protected function authorizeFromEnv()
    {
        Stripe::setApiKey($this->api_key);
    }
    
    /*
    *   retrieve account details
    */
    public function retriveAccountDetail($accountId) {
        self::authorizeFromEnv();
        try{

            $account = Account::retrieve($accountId);

        }catch(\Stripe\Error\Authentication $e){
            
        }
    }

    /*
    *   add manged customer with full detail
    */
    public function createCustomerAccount($data=null)
    {
        self::authorizeFromEnv();
        try {
            $country_code = (isset($data['country_code'])) ? $data['country_code'] : 'US';
            $response = Account::create(
              array(
                "country" => $country_code,
                "type" => "custom"
              )
            );
        } catch (\Stripe\Error\Account $e) {
            $error = $e->getJsonBody();
            $response['status']     = 0;
            $response['error_msg']  = $error['error']['message'];
            return $response;
        }
        
        $stripe_acct_id = $response->id;
        
        $account = Account::retrieve($stripe_acct_id);

        // Create additional owners
        $account->legal_entity->first_name = $data['firstname'];
        $account->legal_entity->last_name = $data['lastname'];
        $account->legal_entity->type = 'individual';
        $account->legal_entity->dob->day = $data['day'];
        $account->legal_entity->dob->month = $data['month'];
        $account->legal_entity->dob->year = $data['year'];
        $account->legal_entity->address->city = $data['city'];
        $account->legal_entity->address->line1 = $data['suburb'];
        $account->legal_entity->address->postal_code = $data['postcode'];
        $account->legal_entity->address->state = $data['state'];
        $account->tos_acceptance->date = $data['date'];
        $account->tos_acceptance->ip = $data['ip'];
        try {
            $account->save();
            $responseData['status']         = 1;
            $responseData['stripe_acct_id'] = $stripe_acct_id;
        } catch (\Stripe\Error\Account $e) {
            $error = $e->getJsonBody();
            $responseData['status']     = 0;
            $responseData['error_msg']  = $error['error']['message'];
        }
        return $responseData;
    }
    
    /*
    *  Upload Verification document like passport/license jpeg/png
    */
    public function uploadFile($file,$stripe_acct_id){
        $fp = fopen($file['tmp_name'], 'r');
        self::authorizeFromEnv();
        try {
            $fileData = FileUpload::create(
                array(
                    'purpose' => 'identity_document',
                    'file' => $fp,
                ),
                array("stripe_account" => $stripe_acct_id)
            );
            fclose($fp);
            if($fileData->id){
                $responseData['status']     = 1;
                $responseData['fileToken']  = $fileData->id;
            }else {
                $responseData['status']     = 0;
                $responseData['error_msg']  = 'Document not uploaded successfully!';    
            }
        } catch (\Stripe\Error\FileUpload $e) {
            $error = $e->getJsonBody();
            $responseData['status']     = 0;
            $responseData['error_msg']  = $error['error']['message'];
        }
        return $responseData;
    }

    /*
    *  Create Bank token for bank account detail.
    */
    public function createBankToken()
    {
        self::authorizeFromEnv();
        
        $tokenResponse=Token::create(array(
                'bank_account' => array(
                'country' => 'US',
                'routing_number' => '110000000',
                'account_number' => '000123456789',
                'account_holder_name' => 'Ashwin Yadav',
                'account_holder_type' => 'individual'
                )
            ));
        
        return $tokenResponse->id;
    }

    /*
    *  Add bank detail to managed customer.
    */
    public function addCustomerBankDetail($stripe_acct_id, $token, $document)
    {
        self::authorizeFromEnv();
        
        // Create additional owners
        if(isset($document)){
            $result = $this->uploadFile($document,$stripe_acct_id);
            if($result['status'] == 1){
                $account = Account::retrieve($stripe_acct_id);
                $account->external_account = $token;
                $account->legal_entity->verification->document = $result['fileToken'];
                try {
                    $account->save();
                    $response['status']     = 1;
                } catch (\Stripe\Error\Account $e) {
                    $error = $e->getJsonBody();
                    $response['status']     = 0;
                    $response['error_msg']  = $error['error']['message'];
                } catch(\Stripe\Error\InvalidRequest $e) {

                    $error = $e->getJsonBody();
                    $response['status']     = 0;
                    $response['error']  = $error['error']['message'];

                } catch (\Stripe\Error\Authentication $e) {

                    // Authentication with Stripe's API failed
                    // (maybe you changed API keys recently)
                    $error = $e->getJsonBody();
                    $response['status']     = 0;
                    $response['error']  = $error['error']['message'];

                } catch (\Stripe\Error\ApiConnection $e) {

                    // Network communication with Stripe failed
                    $error = $e->getJsonBody();
                    $response['status']     = 0;
                    $response['error']  = $error['error']['message'];

                } catch (\Stripe\Error\Base $e) {

                    // Display a very generic error to the user, and maybe send
                    // yourself an email
                    $error = $e->getJsonBody();
                    $response['status']     = 0;
                    $response['error']  = $error['error']['message'];

                } catch (Exception $e) {

                    // Something else happened, completely unrelated to Stripe
                    $response['status'] = 0;
                    $response['error']  = Configure::read('REQUEST_FAILURE');

                }
            }else {
                $response['status']     = 0;
                $response['error_msg']  = $result['error_msg'];
            }
        }
        return $response;
    }


    /**
     * Create a valid test customer.
     */
    public function createTestCustomer(array $attributes = array())
    {
        self::authorizeFromEnv();

        $customer = Customer::create(
            $attributes + array(
                'card' => array(
                    'number' => '4242424242424242',
                    'exp_month' => 5,
                    'exp_year' => date('Y') + 3,
                ),
            )
        );
        return $customer->id;
    }


    /**
     * Create a valid test creditcard.
     */
    public function addCustomerCardDetail($token=null)
    {
        self::authorizeFromEnv();
        
        try
        {
            $customer = Customer::create([
                        "source" => $token,
                        "description" => ''
                ]);
            $response['status']         = 1;
            $response['customer_id']    = $customer->id;
        }catch(\Stripe\Error\Card $e){
            $error = $e->getJsonBody();
            $response['status']     = 0;
            $response['error_msg']  = $error['error']['message'];
        }
        return $response;
    }
    
    /**
     * Create a valid test creditcard.
     */
    public function deleteCustomerCardDetail($stripe_cust_id=null)
    {
        self::authorizeFromEnv();
        
        try
        {
            $customer = Customer::retrieve($stripe_cust_id);
            $customer->delete();
            $response['status']         = 1;
        }catch(\Stripe\Error\Customer $e){
            $error = $e->getJsonBody();
            $response['status']     = 0;
            $response['error_msg']  = $error['error']['message'];
        }
        return $response;
    }
    
    /*
     * Create Tranfer
     */
    public function createTranfer($param=null) 
    {
        self::authorizeFromEnv();

        $amount = (($param['amount'] + $param['tax'] + $param['fee'] - $param['discount']) * 100);
        $fee    = (($param['fee'] + $param['tax']) * 100);
        // Create the charge on Stripe's servers - this will charge the user's card
        $account = Account::retrieve($param['stripe_acct_id']);
        if($account['payouts_enabled'] == 1){
            try {
                $charge = Charge::create(
                  array(
                    "amount" => $amount, // amount in cents
                    "currency" => Configure::read('STRIPE_CURRENCY'),
                    "customer" => $param['stripe_cust_id'],
                    "description" => "Charge through Jugglr.",
                    "application_fee" => $fee, // amount in cents
                    "destination" => $param['stripe_acct_id']
                  )
                );

                $response['status']         = 1;
                $response['charge_id']      = $charge->id;
            } catch(\Stripe\Error\Charge $e) {
                $error = $e->getJsonBody();
                $response['status']     = 0;
                $response['error_msg']  = $error['error']['message'];
            }
            if($response['status'] == 1){
                try {
                    if(isset($param['discount'])){
                        if($param['discount'] > 0){
                            $transfer = Transfer::create(
                                array(
                                  "amount" => ($param['discount']*100),
                                  "currency" => Configure::read('STRIPE_CURRENCY'),
                                  "destination" => $param['stripe_acct_id'],
                                  "description" => "transfer for discount"
                                ));
                        }
                    }
                    $response['status']         = 1;
                } catch(\Stripe\Error\Transfer $e) {
                    $error = $e->getJsonBody();
                }
            }
        }else {
            $response['status']         = 0;
            $response['error_msg']      = 'Account Transfer is disabled!';
        }
        return $response;
    }
    
    
    public function CustomerUpdateCard($params)
    {
        self::authorizeFromEnv();
        $response = array();
        $customer = Customer::retrieve($params['customer_id']);
        $sources = $customer->sources->all();
        
        $temp_arr = explode('/',$params['exp_date']);
        $exp_mnth = $temp_arr[0];
        $exp_yr = $temp_arr[1];
        
        $card = $sources['data'][0];
        $card->exp_month = $exp_mnth;
        $card->exp_year = $exp_yr;
        if($card->save()){
            $response['status'] = 1;
            $response['customer_id'] = $customer->id;
        }else {
            $response['status'] = 0;
        }
        return $response;
    }
    
    
    public function ReplaceCard($params)
    {
        self::authorizeFromEnv();
        $response = array();
        
        $customer = Customer::retrieve($params['stripe_cust_id']);
        $customer->sources->create(array("source" => $params['stripe_card_token']));
        try {
            $customer->save();
            $response['status']     = 1;
        } catch (\Stripe\Error\Customer $e) {
            $error = $e->getJsonBody();
            $response['status']     = 0;
            $response['error_msg']  = $error['error']['message'];
        }

        $sources = $customer->sources->all();

        if(count($sources['data']) == 2 && isset($sources['data'][0])){
            $deletedSource = $sources['data'][0];
            $customer->sources->retrieve($deletedSource->id)->delete();
            try {
                $customer->save();
                $response['status']     = 1;
            } catch (\Stripe\Error\Customer $e) {
                $error = $e->getJsonBody();
                $response['status']     = 0;
                $response['error_msg']  = $error['error']['message'];
            }
        }
        return $response;
    }
}
