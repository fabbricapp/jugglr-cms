<?php
namespace App\Controller\Component;

//include ROOT.DS.'aws.phar';
include CAKE_VENDOR.'autoload.php';

use Aws\S3\S3Client;
use Cake\Controller\Component;
use Cake\Core\Configure;


class S3Component extends Component
{
    public function initialize(array $config)
    {
        parent::initialize($config);
        
    }
    
    private function createClient() {
        $key    = Configure::read('S3_KEY');
        $secret = Configure::read('S3_SECREAT_KEY');
        $s3 = new S3Client([
                'version' => 'latest',
                'region' => 'ap-southeast-2',
                'credentials' => array(
                        'key'    => $key,
                        'secret' => $secret
                ),
                'http'    => ['verify' => ROOT."/cacert.pem"
                        ]
        ]);
        return $s3;
    }
    
    function postRequest($name,$pathToFile) {
        $bucket = Configure::read('S3_BUCKET');
        $s3Client = $this->createClient();
        
        $result = $s3Client->putObject(array(
            'Bucket'     => $bucket,
            'Key'        => $name,
            'SourceFile' => $pathToFile,
            'ACL'        => 'public-read'
        ));
        return $result['ObjectURL'];
    }
    
    function deleteRequest($id) {
        $bucket = Configure::read('S3_BUCKET');
        $s3Client = $this->createClient();
        $client = $s3Client->deleteObject(array(
                    'Bucket' => $bucket,
                    'Key'    => $id
                ));
        return $client;
    }
    
    function listRequest() {
        $client = $this->createClient();
        return $client;
    }

    function ResourcePutObject($file, $id, $type)
    {
        $key    = Configure::read('S3_KEY');
        $secret = Configure::read('S3_SECREAT_KEY');
        $region = 'ap-southeast-2';
        $bucket = Configure::read('S3_BUCKET');
        $file_path = 'resource/'.$id.'/'.$type.'/'.$file['name'];

        $s3 = new S3Client([
                'version' => 'latest',
                'region' => $region,
                'credentials' => array(
                        'key'    => $key,
                        'secret' => $secret
                ),
                'http'    => ['verify' => ROOT."/cacert.pem"]
                        
        ]);

        $obj = $s3->putObject(array(
            'Bucket' => $bucket,
            'Key' => $file_path,
            'Body' => fopen($file['tmp_name'], 'r+'),
            'ACL'        => 'public-read'
        )); 

    }

    function PutObject($file, $file_path)
    {
        $region = 'ap-southeast-2';
        $bucket = Configure::read('S3_BUCKET');
        // $file_path = 'resource/'.$id.'/'.$type.'/'.$file['name'];

        $key    = Configure::read('S3_KEY');
        $secret = Configure::read('S3_SECREAT_KEY');
        $s3 = new S3Client([
                'version' => 'latest',
                'region' => $region,
                'credentials' => array(
                        'key'    => $key,
                        'secret' => $secret
                ),
                'http'    => ['verify' => ROOT."/cacert.pem"]
                        
        ]);

        $obj = $s3->putObject(array(
            'Bucket' => $bucket,
            'Key' => $file_path,
            'Body' => fopen($file['tmp_name'], 'r+'),
            'ACL'        => 'public-read'
        )); 

    }

}
