<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Core\Configure;

Class MailcontentComponent extends Component {

    public $components = array('Mailer');

    public function contactCustomerService($data) {
        $subject = 'User '.$data['user_id'].' complained about '.$data['reason'];
    
        $body = "<p>Hello, </p>";
        $body .= "<p>".$data['user_name'].", ".$data['user_id'].", complained about ".$data['reason']." in relation to task ".$data['task_id'].".</p>";
        $body .= "<p><b>Additional comment : </b>".$data['free_text']."</p><br>";
        $body .= "<p><b>Offer summary : </b></p>";
        $body .= "<p>User ID# (made offer) :".$data['offer_detail']['user_id']." - ".$data['user_email']."</p>";
        $body .= "<p>Service ID Needed : ".$data['offer_detail']['need_serviceid']."</p>";
        $body .= "<p>Subservice ID Needed : ".$data['offer_detail']['need_subserviceid']."</p>";
        $body .= "<p>Needed Service Date : ".$data['offer_detail']['need_datetime']."</p>";
        $body .= "<p>User ID# (service provider) : ".$data['offer_detail']['user_with']." - ".$data['userto_email']."</p>";
        $body .= "<p>Service ID Offered : ".$data['offer_detail']['offer_serviceid']."</p>";
        $body .= "<p>Subservice ID Offered ".$data['offer_detail']['offer_subserviceid']."</p>";
        $body .= "<p>Offered Service Date : ".$data['offer_detail']['offer_datetime']."</p>";
        $body .= "<p>Status : ".$data['offer_detail']['status']."</p><br>";
        $body .= "<p>Please follow up using email address below:</p>";
        $body .= "<p>1) Report from : <br>User name : ".$data['user_name'].", <br>ID# : ".$data['user_id'].", <br>Email address : ".$data['user_email']."</p>";
        $body .= "<p>2) Reported user : <br>User name : ".$data['userto_name'].", <br>ID# : ".$data['userto_id'].", <br>Email address : ".$data['userto_email']."</p><br>";
        $body .= "<p>CMS link : ".BASE_URL."/users/index</p>";

        return $this->Mailer->sendMail($subject, $body, null, null);
    }
    
    public function reportAbuse($data) {
        $subject = 'User ID# '.$data['userto_id'].' has been reported for '.$data['reason'];
    
        $body = "<p>Hello, </p>";
        $body .= "<p>".$data['user_name'].", ID# ".$data['user_id'].", reported user ID#".$data['userto_id']." for ".$data['reason'].".</p>";
        $body .= "<p><b>Additional comment : </b>".$data['free_text']."</p>";
        $body .= "<p>Please follow up using email address below:</p>";
        $body .= "<p>1) Report from : <br>User name : ".$data['user_name'].", <br>ID# : ".$data['user_id'].", <br>Email address : ".$data['user_email']."</p>";
        $body .= "<p>2) Reported user : <br>User name : ".$data['userto_name'].", <br>ID# : ".$data['userto_id'].", <br>Email address : ".$data['userto_email']."</p><br>";
        $body .= "<p>CMS link : ".BASE_URL."/users/index</p>";
    
        return $this->Mailer->sendMail($subject, $body, null, null);
    }
    
    public function negativeRating($data) {
        $subject = 'User '.$data['user_id'].' has been rate negatively for '.$data['reason'];
    
        $body = "<p>Hello, </p>";
        $body .= "<p>".$data['user_name'].", ".$data['user_id'].", provided negative rating for user ".$data['userto_id']." for ".$data['reason'].".</p>";
        $body .= "<p><b>Offer summary : </b></p>";
        $body .= "<p>User ID# (made offer) :".$data['user_id']." - ".$data['user_email']."</p>";
        $body .= "<p>Service ID Needed : ".$data['need_serviceid']."</p>";
        $body .= "<p>Subservice ID Needed : ".$data['need_subserviceid']."</p>";
        $body .= "<p>Needed Service Date : ".$data['need_datetime']."</p>";
        $body .= "<p>User ID# (service provider) : ".$data['user_with']." - ".$data['userto_email']."</p>";
        $body .= "<p>Service ID Offered : ".$data['offer_serviceid']."</p>";
        $body .= "<p>Subservice ID Offered ".$data['offer_subserviceid']."</p>";
        $body .= "<p>Offered Service Date : ".$data['offer_datetime']."</p>";
        $body .= "<p>Status : ".$data['status']."</p><br>";
        $body .= "<p><b>Additional comment : </b>".$data['free_text']."</p>";
        $body .= "<p>Please follow up using email address below:</p>";
        $body .= "<p>1) Report from : <br>User name : ".$data['user_name'].", <br>ID# : ".$data['user_id'].", <br>Email address : ".$data['user_email']."</p>";
        $body .= "<p>2) Reported user : <br>User name : ".$data['userto_name'].", <br>ID# : ".$data['userto_id'].", <br>Email address : ".$data['userto_email']."</p><br>";
        $body .= "<p>CMS link : ".BASE_URL."/users/index</p>";

        return $this->Mailer->sendMail($subject, $body, null, null);
    }
    
    public function cancelOffer($data) {
        $subject = 'User '.$data['user_id'].' cancelled offer '.$data['task_id'];
    
        $body = "<p>Hello, </p>";
        $body .= "<p>User ".$data['user_id'].", cancelled offer ".$data['task_id']." due to ".$data['reason'].".</p>";
    $body .= "<p><b>Offer summary : </b></p>";
        $body .= "<p>User ID# (made offer) :".$data['user_id']." - ".$data['user_email']."</p>";
        $body .= "<p>Service ID Needed : ".$data['need_serviceid']."</p>";
        $body .= "<p>Subservice ID Needed : ".$data['need_subserviceid']."</p>";
        $body .= "<p>Needed Service Date : ".$data['need_datetime']."</p>";
        $body .= "<p>User ID# (service provider) : ".$data['userto_id']." - ".$data['userto_email']."</p>";
        $body .= "<p>Service ID Offered : ".$data['offer_serviceid']."</p>";
        $body .= "<p>Subservice ID Offered ".$data['offer_subserviceid']."</p>";
        $body .= "<p>Offered Service Date : ".$data['offer_datetime']."</p>";
        $body .= "<p>Status : ".$data['status']."</p><br>";
        $body .= "<p><b>Additional comment : </b>".$data['free_text']."</p>";
        $body .= "<p>Please follow up using email address below:</p>";
        $body .= "<p>1) Report from : <br>User name : ".$data['user_name'].", <br>ID# : ".$data['user_id'].", <br>Email address : ".$data['user_email']."</p>";
        $body .= "<p>2) Reported user : <br>User name : ".$data['userto_name'].", <br>ID# : ".$data['userto_id'].", <br>Email address : ".$data['userto_email']."</p><br>";
        $body .= "<p>CMS link : ".BASE_URL."/users/index</p>";

        return $this->Mailer->sendMail($subject, $body, null, null);
    }

        public function forgotPassword($data) 
    {
        $subject = 'Jugglr CMS reset password link';
    
        $body = "<p>Hi,</p><br>";
        $body .= "<p>Administrator has generated password for you.
</p>";
        /*$body .= "<p>Please click on this password link and change your password.</p>";*/
        $body .= "<p>To change your Jugglr CMS password, login with this password and reset your password: " . $data['password'] . "</p>";
        $body .= "<p>Thanks for using Jugglr App! <br/> </p>";
        
        return $this->Mailer->sendMail($subject, $body, null, null, $data['to']);
    }

    public function registeredBusinessUser($data)
    {
        $subject = 'Welcome to Jugglr - Please complete sign up';
    
        $body = "<p>Hi,</p><br>";
        $body .= "<p>Welcome to Jugglr, your business has just been registered!  You are now visible to thousands of mums in Australia who need and value your services.";
        $body .= "<p>There are a just a couple of things that you need to do to complete the activation of your Jugglr Business Account:<br/></p>";
        $body .= "<p>1. Click on this link ".BASE_URL."/users/setpassword?token=".$data['password']." to set your new password</p>";
        $body .= "<p>2. Download Jugglr from the App Store or Google Play and log in using your email address and the new password</p>";
        $body .= "<p>3. After login update your Jugglr Business profile and pictures by going to User Settings > My Profile > Edit.<br/></p>";


        $body .= "<p>We hope you enjoy Jugglr and if you have any questions please contact us at support@jugglrapp.com.<br/></p>";
        $body .= "<p>Thank you for your support and for making Jugglr an awesome marketplace for mums helping mums! <br/> </p>";
        $body .= "<p>Jugglr Team</p>";

        $from = 'no-reply@jugglrapp.com';
        $cc = 'a@jugglrapp.com';
        return $this->Mailer->sendMail($subject, $body, $from, null, $data['to'], $cc);   
    }

}
