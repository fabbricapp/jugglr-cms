<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\ORM\Table;

class PushnotificationComponent extends Component
{
    private $api_key;
    private $path_to_firebase_cm;
    
    function __construct()
    {
        //fcm notification
        $this->api_key = Configure::read('PUSH_NOTIFICATION_API_KEY');
        $this->path_to_firebase_cm    = Configure::read('PUSH_NOTIFICATION_PATH_TO_FIREBASE');
    }

        public function getnotificationMessage($notification, $data) {
            $usertask_arr = ['ADDOFFER','UPDATEOFFER','ACCEPTOFFER','CANCELLOFFER','RATEINGOFFER','CONFRIMCOMPLETE'];
            if(in_array($notification, $usertask_arr)){
                $usertask = TableRegistry::get('Usertasks');
                $select = ['id','user_id','user_with','status','is_accepted','need_serviceid','need_completion_datetime','offer_serviceid','offer_completion_datetime'];
                
                $condition['Usertasks.id'] = $data['offer_id'];
                $contain = [
                    'Userwith'=> function ($q) {
                        return $q
                        ->select(['Userwith.firstname']);
                    },
                    'Users'=> function ($q) {
                        return $q
                        ->select(['Users.firstname']);
                    }
                ];
                $result = $usertask->find()
                        ->select($select)
                        ->where($condition)
                        ->contain($contain)
                        ->first();
                if($result){
                    $result = $result->toArray();
                    $offerData = $this->getSummaryOfferDataForNotification($result,$data['user_id']);
                }
            }
            switch ($notification) {
                case 'SHORTLIST':
                    $user = TableRegistry::get('Users');
                    $result = $user->find()->where(['id'=>$data['user_id']])->select(['id','firstname','type'])->first();
                    if($result)
                    {
                        $response['message'] = "You've been shortlisted by ".$result['firstname'].", tap to view her profile";
                        $response['params']['user_id'] = $data['user_id'];
                        $response['params']['type'] = $result['type'];
                        $response['user_ids'] = $data['user_with'];
                        $response['redirect'] = 'USERPROFILE';
                        $response['silent'] = '';
                    }else {
                        $response = array();
                    }
                    break;
                case 'ADDOFFER':
                    $response['message'] = "You have a new offer!";
                    $response['params']['offer_data'] = $offerData;
                    $response['user_ids'] = $data['user_with'];
                    $response['redirect'] = 'OFFERSUMMARY';
                    $response['silent'] = '';
                    break;
                case 'UPDATEOFFER':
                    $usertask = TableRegistry::get('Usertasks');
                    $result = $usertask->find()->where(['Usertasks.id'=>$data['offer_id']])->contain(['Users'])->first();
                    if($result)
                    {
                        $response['message'] = $result['user']['firstname']." has updated your offer!";
                        $response['params']['offer_data'] = $offerData;
                        $response['user_ids'] = $result['user_with'];
                        $response['redirect'] = 'OFFERSUMMARY';
                        $response['silent'] = '';
                    }else {
                        $response = array();
                    }
                    break;
                case 'ACCEPTOFFER':
                    $usertask = TableRegistry::get('Usertasks');
                    $result = $usertask->find()->where(['Usertasks.id'=>$data['offer_id']])->contain(['Userwith'])->first();
                    if($result)
                    {
                        $response['message'] = $result['userwith']['firstname']." has accepted your offer!";
                        $response['params']['offer_data'] = $offerData;
                        $response['user_ids'] = ($data['user_id'] == $result['user_id']) ? $result['user_with'] : $result['user_id'];
                        $response['redirect'] = 'OFFERSUMMARY';
                        $response['silent'] = '';
                    }else {
                        $response = array();
                    }
                    break;
                case 'CANCELLOFFER':
                    $usertask = TableRegistry::get('Usertasks');
                    $result = $usertask->find()->where(['Usertasks.id'=>$data['offer_id']])->contain(['Users','Userwith'])->first();
                    if($result)
                    {
                        if($data['user_id'] == $result['user_id']){
                            $response['message'] = $result['user']['firstname']." has cancelled your offer!";    
                        }else {
                            $response['message'] = $result['userwith']['firstname']." has cancelled your offer!";
                        }
                        $response['params']['offer_data'] = $offerData;
                        $response['user_ids'] = ($data['user_id'] == $result['user_id']) ? $result['user_with'] : $result['user_id'];
                        $response['redirect'] = 'OFFERSUMMARY';
                        $response['silent'] = '';
                    }else {
                        $response = array();
                    }
                    break;
                case 'RATEINGOFFER':
                    $usertask = TableRegistry::get('Usertasks');
                    $result = $usertask->find()->where(['Usertasks.id'=>$data['offer_id']])->contain(['Users','Userwith'])->first();
                    if($result)
                    {
                        if($data['user_id'] == $result['user_id']){
                            $response['message'] = $result['user']['firstname']." rated you!";    
                        }else {
                            $response['message'] = $result['userwith']['firstname']." rated you!";
                        }
                        $response['params']['offer_data'] = $offerData;
                        $response['user_ids'] = ($data['user_id'] == $result['user_id']) ? $result['user_with'] : $result['user_id'];
                        $response['redirect'] = 'MYTODO';
                        $response['silent'] = '';
                    }else {
                        $response = array();
                    }
                    break;
                case 'CONFRIMCOMPLETE':
                    $usertask = TableRegistry::get('Usertasks');
                    $result = $usertask->find()->where(['Usertasks.id'=>$data['offer_id']])->contain(['Users','Userwith'])->first();
                    if($result)
                    {
                        if($data['user_id'] == $result['user_id']){
                            $response['message'] = $result['user']['firstname']." has confirmed your offer!";    
                        }else {
                            $response['message'] = $result['userwith']['firstname']." has confirmed your offer!";
                        }
                        $response['params']['offer_data'] = $offerData;
                        $response['user_ids'] = ($data['user_id'] == $result['user_id']) ? $result['user_with'] : $result['user_id'];
                        $response['redirect'] = 'OFFERSUMMARY';
                        $response['silent'] = '';
                    }else {
                        $response = array();
                    }
                    break;
                case 'NOTIFICATIONTOVERIFIEDUSER':
                    if(isset($data['user_id'])){
                        $usertask = TableRegistry::get('Users');
                        $result = $usertask->find()->where(['Users.id'=>$data['user_id']])->select(['firstname'])->first();
                    }else {
                        $result['firstname'] = 'Admin';
                    }
                    if($result)
                    {
                        $response['message'] = $result['firstname']." has verified you!";
                        $response['params']['verified'] = 1;
                        $response['params']['user_id'] = $data['user_with'];
                        $response['user_ids'] = $data['user_with'];
                        $response['redirect'] = 'VERIFIED';
                        $response['silent'] = '';
                    }else {
                            $response = array();
                    }
                    break;
                case 'REGISTRATION':
                    $user = TableRegistry::get('Users');
                    $matchingComment = $user->association('Facebookconnections')->find()
                    ->select(['connection_fb_id'])
                    ->distinct()
                    ->where(['user_id' => $data['id']]);

                     $result = $user->find()
                    ->select(['id'])
                    ->where(['facebook_id IN' => $matchingComment]);

                    if($result)
                    {
                        $result = $result->toArray();
                        $user_ids = array();
                        foreach ($result as $key => $value) {
                            $user_ids[] = $value['id'];
                        }
                        $result = $user->find()->where(['id'=>$data['id']])->select(['type','id'])->first();

                        $response['message'] = $data['firstname']." ".$data['lastname']." just joined jugglr. If she is a trusted mum in your network please verify her status.";
                        $response['params']['user_id'] = $data['id'];
                        $response['params']['type'] = $result['type'];
                        $response['user_ids'] = $user_ids;
                        $response['redirect'] = 'USERPROFILE';
                        $response['silent'] = '';
                    }else {
                        $response = array();
                    }
                    break;
                case 'IOSMESSAGE':
                    $response['message']    = "You have a new message!";
                    $response['params']     = '';
                    $response['user_ids']   = $data['user_with'];
                    $response['redirect']   = 'CHAT';
                    $response['silent']     = '';
                    break;
                case 'SHAREJUGGLR':
                    if($data)
                    {
                        $response['message'] = "Jugglr is better with more mums, please keep sharing the good news";
                        $response['params']     = '';
                        $response['user_ids']   = $data;
                        $response['redirect']   = 'SHAREJUGGLR';
                        $response['silent']     = '';
                    }else {
                        $response = array();
                    }
                    break;
                case 'INVITEJUGGLR':
                    if($data)
                    {
                        $response['message'] = "Jugglr is better with more mums, please keep inviting your mum friends regularly";
                        $response['params'] = '';
                        $response['user_ids'] = $data;
                        $response['redirect'] = 'INVITEJUGGLR';
                        $response['silent'] = '';
                    }else {
                        $response = array();
                    }
                    break;
                case 'INACTIVEFROMCMS':
                    if($data)
                    {
                        $response['message'] = "Jugglr set you as inactive user!";
                        $response['params'] = '';
                        $response['user_ids'] = $data;
                        $response['redirect'] = 'SIGNOUT';
                        $response['silent'] = '1';
                    }else {
                        $response = array();
                    }
                    break;
                case 'ACCEPTJOBOFFER':
                    $usertask = TableRegistry::get('Users');
                    $result = $usertask->find()->where(['Users.id'=>$data['user_id']])->select(['firstname'])->first();
                    if($data)
                    {
                        $response['message'] = $result['firstname']." has accepted your offer!";
                        $response['params']['job_id'] = $data['job_id'];
                        $response['params']['user_id'] = $data['job_user'];
                        $response['params']['type'] = $data['type'];
                        $response['user_ids'] = $data['applicant_id'];
                        $response['redirect'] = 'JOBDETAIL';
                        $response['silent'] = '';
                    }else {
                        $response = array();
                    }
                    break;
                case 'NEWJOB':
                    $usertask = TableRegistry::get('Users');
                    $result = $usertask->find()->where(['Users.id'=>$data['user_id']])->select(['firstname'])->first();
                    if($data)
                    {
                        $response['message'] = $result['firstname']." has posted a new job!";
                        $response['params']['job_id'] = $data['job_id'];
                        $response['params']['user_id'] = $data['job_user'];
                        $response['params']['type'] = $data['type'];
                        $response['user_ids'] = $data['user_arr'];
                        $response['redirect'] = 'JOBDETAIL';
                        $response['silent'] = '';
                    }else {
                        $response = array();
                    }
                    break;
                case 'NEWJOBCOMMENTTOUSER':
                    $response['message'] = "Your job has a new comment!";
                    $response['params']['job_id'] = $data['job_id'];
                    $response['params']['user_id'] = $data['job_user'];
                    $response['params']['type'] = $data['type'];
                    $response['user_ids'] = $data['job_user'];
                    $response['redirect'] = 'JOBDETAIL';
                    $response['silent'] = '';
                    break;
                case 'NEWJOBCOMMENTTOOTHER':
                    $response['message'] = "A job you're interested in has a new comment!";
                    $response['params']['job_id'] = $data['job_id'];
                    $response['params']['user_id'] = $data['job_user'];
                    $response['params']['type'] = $data['type'];
                    $response['user_ids'] = $data['user_arr'];
                    $response['redirect'] = 'JOBDETAIL';
                    $response['silent'] = '';
                    break;
                case 'MAKEOFFERTOJOB':
                    $response['message'] = "You have a new offer!";
                    $response['params']['job_id'] = $data['job_id'];
                    $response['params']['user_id'] = $data['job_user'];
                    $response['params']['type'] = $data['type'];
                    $response['user_ids'] = $data['job_user'];
                    $response['redirect'] = 'JOBDETAIL';
                    $response['silent'] = '';
                    break;
                case 'ACCEPTJOBOFFER-1':
                    $response['message'] = "Your offer to job has been accepted!";
                    $response['params']['job_id'] = $data['job_id'];
                    $response['params']['type'] = $data['type'];
                    $response['user_ids'] = $data['applicant_id'];
                    $response['redirect'] = 'JOBDETAIL';
                    $response['silent'] = '';
                    break;
                case 'REJECTJOBOFFER':
                    $response['message'] = "Your offer to job has been rejected!";
                    $response['params']['job_id'] = $data['job_id'];
                    $response['params']['type'] = $data['type'];
                    $response['user_ids'] = $data['user_arr'];
                    $response['redirect'] = 'JOBDETAIL';
                    $response['silent'] = '';
                    break;
                case 'BANKDETAIL_VERIFIED':
                    $response['message']    = "Your bank details are verified by Stripe!";
                    $response['params']     = '';
                    $response['user_ids']   = $data['user_id'];
                    $response['redirect']   = '';
                    $response['silent']     = '';
                    break;
                case 'BANKDETAIL_NOT_VERIFIED':
                    $response['message']    = "Your bank details are not verified by Stripe!";
                    $response['params']     = '';
                    $response['user_ids']   = $data['user_id'];
                    $response['redirect']   = '';
                    $response['silent']     = '';
                    break;
                case 'CANCELLJOB':
                    $response['message'] = "A job you're interested in has been cancelled!";
                    $response['params']['job_id'] = $data['job_id'];
                    $response['user_ids'] = $data['user_arr'];
                    $response['redirect'] = '';
                    $response['silent'] = '';
                    break;
                default:
                    $response = array();
            }
            if(!empty($response)){
                $data = $this->sendPushNotification($response['user_ids'], $response['message'], $response['params'], $response['redirect'],$response['silent']);
            }else {
                return false;
            }
        }

    public function sendPushNotification($user_ids = '', $message = '', $params = array(), $redirect, $silent=null) {
        if (!empty($user_ids)) {
            if(is_array($user_ids)){
                $user_ids_arr = $user_ids;
            }else {
                $user_ids_arr = explode(",", $user_ids);    
            }
            foreach ($user_ids_arr as $user_key => $user_id) {
                $this->_sendNotification($user_id, $message, $params, $redirect, $silent);
            }
            return true;
        } else
            return false;
    }
    
    public function _sendNotification($user_id, $msg, $params = array(), $redirect, $silent=null) {
        $user = TableRegistry::get('Users');
        $device = $user->find()->select(['Users.id','Users.device_token','Users.device_type','Users.is_active','Usersettings.offer_message_mode','Usersettings.new_matches_mode'])->where(['Users.id'=>$user_id,'Users.is_active' => '1'])->contain(['Usersettings'])->first();
        //add notification to table
        $Usernotifications = TableRegistry::get('Usernotifications');
        if(!$silent){
            $notification_id = $Usernotifications->addnotification($user_id, $msg, $params, $redirect);
            if($notification_id){
                $params['notification_id'] = $notification_id;
            }
        }
        $badge_cnt = $Usernotifications->getbadgeCount($user_id);
        /*$output = print_r($badge_cnt).$user_id;
        file_put_contents(TMP."check_defaultjob-ashwin.txt", print_r($output,true),FILE_APPEND);*/
        if(!empty($device)){
            if (!empty($device['device_token']) && !empty($device['device_type']) && $device['is_active'] == 1){
                $type = $device['device_type'];
                $devicetoken = $device['device_token'];
                if($redirect == 'USERPROFILE'){
                    if ($device['usersetting']['new_matches_mode'] == 1) {        
                        return $this->_pushNotification($devicetoken, $type, $msg, $params, $redirect, $silent, $badge_cnt);
                    } else {
                        return false;
                    }
                }elseif ($redirect == 'OFFERSUMMARY' || $redirect == 'CHAT') {
                    if ($device['usersetting']['offer_message_mode'] == 1) {        
                        return $this->_pushNotification($devicetoken, $type, $msg, $params, $redirect, $silent, $badge_cnt);
                    } else {
                        return false;
                    }
                }else {
                    return $this->_pushNotification($devicetoken, $type, $msg, $params, $redirect, $silent, $badge_cnt);
                }
            }else {
                return false;
            }
        }
    }
    
    public function _pushNotification($devicetoken, $type, $msg, $params = array(), $redirect=null, $silent=null, $badge_cnt=0) 
    {
        $registrationIds = array($devicetoken);
        $params['notification_time'] = strtotime(date('Y-m-d h:i:s'));
        $contentavailable = ($silent == '') ? 0 : 1;

        $message = array(
                'message' => $msg,
                'params' => $params,
                'redirect' => $redirect,
                'vibrate' => 1,
                'sound' => 1,
                'content-available' => $contentavailable,
        );
    
        $fields = array(
                'registration_ids' => $registrationIds,
                'data' => $message
        );

        $result = null;
        if(!empty($fields)) {

            $headers = array(
                'Authorization:key=' . $this->api_key,
                'Content-Type:application/json'
            );

            $ch = curl_init();
     
            curl_setopt($ch, CURLOPT_URL, $this->path_to_firebase_cm); 
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
            curl_setopt($ch, CURLOPT_TIMEOUT_MS, 1000);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 ); 
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        
            $result = curl_exec($ch);
            
            curl_close($ch);
        }

    }


    /*public function _pushNotification_old($devicetoken, $type, $msg, $params = array(), $redirect=null, $silent=null, $badge_cnt=0) {
        switch ($type) {
            case 'ios':
                return $this->_pushToIos($devicetoken, $msg, $params, $redirect, $silent, $badge_cnt);
                break;
    
            case 'android':
                return $this->_pushToAndroid($devicetoken, $msg, $params, $redirect, $silent);
                break;
    
            default:
                return 'Invalid Type Passed';
                break;
        }
    }*/
    
    /*public function _pushToAndroid($devicetoken, $msg, $params = array(), $redirect=null, $silent=null) {
        $registrationIds = array($devicetoken);
        $params['notification_time'] = strtotime(date('Y-m-d h:i:s'));
            $contentavailable = ($silent == '') ? 0 : 1;
        $message = array(
                'message' => $msg,
                'params' => $params,
                        'redirect' => $redirect,
                'vibrate' => 1,
                'sound' => 1,
                        'content-available' => $contentavailable,
        );
    
        $fields = array(
                'registration_ids' => $registrationIds,
                'data' => $message
        );
    
        $headers = array(
                'Authorization: key=' . $this->api_key,
                'Content-Type: application/json'
        );
    
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://android.googleapis.com/gcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        curl_close($ch);
    
        return true;
    }*/
    
    /*public function _pushToIos($devicetoken, $message, $params = array(), $redirect=null, $silent=null, $badge_cnt=0) {
        $contentavailable = ($silent == '') ? 0 : 1;
        $badge = $badge_cnt;
        $body = array();
        $body['aps'] = array('alert' => $message, 'content-available' => $contentavailable, 'redirect' => $redirect, 'params' => $params);
        $body['message'] = array('action' => 'Circulers');
        if ($badge)
        $body['aps']['badge'] = $badge;
        if ($contentavailable){
            $body['aps']['sound'] = 0;
        }else {
            $body['aps']['sound'] = 1;
        }
        $ctx = stream_context_create();
        
        stream_context_set_option($ctx, 'ssl', 'local_cert', WWW_ROOT . 'pem' . DS . 'APNS.pem');
        $fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT, $ctx);
        //$fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT, $ctx);

        if (!$fp) 
        {
            return;
        }
        $payload = json_encode($body);
        $msg = chr(0) . pack("n",32) . pack('H*', str_replace(' ', '', $devicetoken)) . pack("n",strlen($payload)) . $payload;

        $return = fwrite($fp, $msg, strlen($msg));

        fclose($fp);
    }*/   

    /*
     * Function to set offer summary detail when notification fired and redirected to offer summary
     */
    public function getSummaryOfferDataForNotification($value=array(),$user_id)
    {
        /*
         * Logic for completed tab
         */
        $response = array();
        $response['id']             = $value['id'];
        $response['i_made_offer']   = ($value['user_id'] == $user_id) ? 0 : 1;
        $response['user_with_id']   = ($value['user_id'] == $user_id) ? $value['user_id'] : $value['user_with'];
        $response['user_with_name'] = ($value['user_id'] == $user_id) ? $value['user']['firstname'] : "";
        $response['is_accepted']    = ($value['is_accepted'] == '1') ? 1 : 0;
        $response['type']           = "need";
        $response['in_progress']    = ($value['offer_completion_datetime'] != '' || $value['need_completion_datetime'] != '') ? 1 : 0;
        $response['status']         = ($value['status'] == '0') ? "Current" : (($value['status'] == '1') ? "Completed" : "Cancelled");
        $response['is_completed']   = ($value['user_id'] == $user_id) ? (($value['offer_completion_datetime'] != '') ? 1 : 0) : (($value['need_completion_datetime'] != '') ? 1 : 0);
        $response['completed_by_me']= ($value['user_id'] == $user_id) ? (($value['offer_completion_datetime'] != '') ? 1 : 0) : (($value['need_completion_datetime'] != '') ? 1 : 0);
        $response['service_id']     = ($value['user_id'] == $user_id) ? $value['offer_serviceid'] : $value['need_serviceid'];
        return $response;
        }
    
}
