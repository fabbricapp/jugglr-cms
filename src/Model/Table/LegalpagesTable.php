<?php
namespace App\Model\Table;

use App\Model\Entity\Legalpage;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;


/**
 * Users Model
 *
 */
class LegalpagesTable extends AppTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('legalpages');
        $this->displayField('id');
        $this->primaryKey('id');
      
    }

}
