<?php
namespace App\Model\Table;

use App\Model\Entity\Adminsetting;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;


/**
 * Users Model
 *
 */
class AdminsTable extends AppTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('admins');
        $this->displayField('id');
        $this->primaryKey('id');
      
    }
    
    public function getData($id) {
    	$query = $this->find()
    	->where(['id' => $id]);
    	$results = $query->first();
        if($results){
            $results = $results->toArray();
            $results['image'] = ($results['image'] != '') ? $results['image'] : BASE_URL."/img/default_image.png";
            $results['name'] = ($results['name'] != '') ? $results['name'] : "Admin";
        }
    	return $results;
    }
    
    public function addData($data=array(),$id){
    	$admin = $this->find()->where(['id' => $id])->first();
    	foreach ($data as $key=>$value){
    		$admin->$key = $value;
    	}
    	if ($this->save($admin)) {
    		return 1;
    	} else {
    		return 0;
    	}
    }

    public function updatedata($id, $data) 
    {
        $user = $this->get($id);
        foreach ($data as $key=>$value)
        {
            $user->$key = $value;
        }
        if($this->save($user))
        {
            return true;
        }else {
            return false;
        }
    }
}
