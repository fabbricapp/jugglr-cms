<?php
namespace App\Model\Table;

use App\Model\Entity\Userimage;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;


/**
 * Users Model
 *
 */
class UserimagesTable extends AppTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('userimages');
        $this->displayField('id');
        $this->primaryKey('id');
      	
        $this->belongsTo('Users',[
        	'foreignKey' => 'user_id'
        ]);
    }

    public function updatedata($id, $data) 
    {
        $user = $this->get($id);
        foreach ($data as $key=>$value)
        {
            $user->$key = $value;
        }
        if($this->save($user))
        {
            return true;
        }else {
            return false;
        }
    }
    
    public function uploadimage($requestdata)
    {
    	$imagedata 		= $param = array();
    	$image_arr 		= array("image/png","image/jpg","image/jpeg");
    	
    	foreach ($requestdata as $key=>$value)
    	{
	    	if ($value['file']['error'] == 0 && in_array($value['file']['type'],$image_arr)) {
	    		if($value['file']['type'] == 'image/png'){
	    			$extension = 'png';
	    		}elseif ($value['file']['type'] == 'image/jpg'){
	    			$extension = 'jpg';
	    		}elseif ($value['file']['type'] == 'image/jpeg'){
	    			$extension = 'jpeg';
	    		}
	    		$tmp_name 	= $value['file']["tmp_name"];
	    		if($value['key'] == 'profile_image_thumb'){
	    			$name 	= "profile-image-".time().".".$extension;
	    			$uploads_dir 	= ROOT."/webroot/img/profileimages";
	    		}elseif($value['key'] == 'adminimages'){
                    $name   = "profile-image-".time().".".$extension;
                    $uploads_dir    = ROOT."/webroot/img/adminimages";
                }elseif($value['key'] == 'promotionimages'){
                    $name   = "promotionimages-".time().".".$extension;
                    $uploads_dir    = ROOT."/webroot/img/promotionimages";
                }else {
	    			$name 	= $value['key'].time().".".$extension;
	    			$uploads_dir 	= ROOT."/webroot/img/coverimages";
	    		}
	    		if(move_uploaded_file($tmp_name, $uploads_dir."/".$name))
	    		{
		    		$imagedata[$key]	= $name;
	    		}
	    	}
    	}
    	return $imagedata;
    }
    
    public function adduserimages($requestdata,$file)
    {
    	$images = $this->uploadimage($file);
    	$datatosave = array();
    	$i = 0;
    	foreach ($images as $key=>$value){
    		$datatosave[$i]['user_id']	 		= $requestdata['user_id'];
    		$datatosave[$i]['image_url'] 		= $value;
    		$i++;
    	}
    	$entities = $this->newEntities($datatosave);
    	foreach ($entities as $entity) {
    		$result = $this->save($entity);
    	}
    	$response 	= array();
    	$result 	= $this->get($result->id);
    	if($result){
    		$response['id']     	= $result['id'];
    		$response['image_url'] 	= BASE_URL."/img/coverimages/".$result['image_url'];
    	}
    	return $response;
    }
    
    public function adddefaultuserimages($user_id)
    {
    	for($i=1;$i<4;$i++){
    		$datatosave[$i]['user_id']	 		= $user_id;
    		$datatosave[$i]['coverimage_key']	= 'coverimage_'.$i;
    		$datatosave[$i]['image_url'] 		= '';
    	}
    	$entities = $this->newEntities($datatosave);
    	foreach ($entities as $entity) {
    		$result = $this->save($entity);
    	}
    	return $result;
    }

    public function updateuserimages($requestdata) 
    {
    	$images = $this->uploadimage($requestdata);
	    $response 	= array();
	    foreach ($requestdata as $key=>$value)
	    {
	    	if($images[$key])
	    	{
	    		$query = $this->find()
	    		->where(['user_id'=>$value['user_id'],'coverimage_key'=>$value['coverimage_key']])
	    		->select(['id']);
	    		$results = $query->first();
		    	if($results['id']){
		    		$userimage = $this->get($results['id']);
		    		$userimage->image_url = $images[$key];
		    		$this->save($userimage);
		    	}
		    	$response = 1;
	    	}
	    }
    	return $response;
    }

    public function deletecoverimage($requestdata)
    {
    	$image_no = (int) substr($requestdata['coverimage_key'],-1,1);
    	$response['coverimages'] = array();
    	for($i=$image_no;$i<4;$i++)
    	{
    		$cover_image = 'coverimage_'.$i;
    		$query = $this->find()
    		->where(['user_id'=>$requestdata['user_id'],'coverimage_key'=>$cover_image])
    		->select(['id']);
    		$currentimage = $query->first();
    		if($currentimage['id'])
    		{
    			$userimage = $this->get($currentimage['id']);
    			$userimage->image_url = '';
    			$this->save($userimage);
    		}
    		$cover_image = 'coverimage_'.($i+1);
    		$query = $this->find()
    		->where(['user_id'=>$requestdata['user_id'],'coverimage_key'=>$cover_image]);
    		$nextimage = $query->first();
    		if($nextimage['image_url'])
    		{
	    		$userimage = $this->get($currentimage['id']);
	    		$userimage->image_url = $nextimage['image_url'];
	    		$this->save($userimage);
	    		
	    		$userimage = $this->get($nextimage['id']);
	    		$userimage->image_url = '';
	    		$this->save($userimage);
    		}
    	}
    	$query = $this->find()
    	->where(['user_id'=>$requestdata['user_id'],'image_url IS NOT'=>NULL]);
    	$images = $query->all();
    	if($images){
    		$result = array();
    		$images = $images->toArray();
    		foreach ($images as $key=>$value){
    			if($value['image_url'] != ''){
    				$result[$key]['coverimage_key'] 	= $value['coverimage_key'];
    				$result[$key]['image_url'] 		= BASE_URL."/img/coverimages/".$value['image_url'];
    				//unlink(BASE_URL."/img/coverimages/".$value['image_url']);
    			}
    		}
    		$response['coverimages'] = array_values($result);
    	}
    	return $response;
    }
    
}
