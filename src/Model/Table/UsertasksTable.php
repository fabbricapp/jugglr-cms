<?php
namespace App\Model\Table;

use App\Model\Entity\Usertask;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;


/**
 * Users Model
 *
 */
class UsertasksTable extends AppTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('usertasks');
        $this->displayField('id');
        $this->primaryKey('id');
        
        $this->belongsTo('Users',[
            'foreignKey' => 'user_id'
        ]);
        $this->belongsTo('Userwith',[
            'className' => 'Users',
            'foreignKey' => 'user_with'
        ]);
        $this->belongsTo('Services',[
            'foreignKey' => 'need_serviceid'
        ]);
        $this->belongsTo('Subservices',[
            'foreignKey' => 'need_subserviceid'
        ]);
        $this->belongsTo('Subservicetags',[
            'foreignKey' => 'need_subservicetagid'
        ]);
        $this->belongsTo('offered_service',[
            'className' => 'Services',
            'foreignKey' => 'offer_serviceid'
        ]);
        $this->belongsTo('offered_subservice',[
            'className' => 'Subservices',
            'foreignKey' => 'offer_subserviceid'
        ]);
        $this->belongsTo('offered_subservicetag',[
            'className' => 'Subservicetags',
            'foreignKey' => 'offer_subservicetagid'
        ]);
    }
    
    /*
     * Add Transaction
     */
    public function add_transaction($data = null)
    {
        $usertask = $this->newEntity($data);
        $result = $this->save($usertask);
        if ($result) {
            $response['status'] = 1;
            $response['offer_id'] = $result->id;
        } else {
            $response['status'] = 0;
        }
        return $response;
    }
    
    /*
     * My to-do
     */
    public function gettransaction($results,$user_id,$request_type)
    {
        $response['Current'] = array(); 
        $response['Completed'] = array();
        
        if($results)
        {
            $results = $results->toArray();
            $i = 0;
            foreach ($results as $key=>$value)
            {
                if($value['status'] == '0'){
                    $type = "Current";
                }elseif ($value['status'] == '1' || $value['status'] == '2'){
                    $type = "Completed";
                }
                /*
                 * Logic for Current tab
                 */
                if($value['status'] == '0'){
                    if($value['user_id'] == $user_id)
                    {
                        if($value['need_rating'] == '')
                        {
                            $response[$type][$i]['id']          = $value['id'];
                            $response[$type][$i]['type']            = "need";
                            $response[$type][$i]['i_made_offer']            = ($value['user_id'] == $user_id) ? 1 : 0;
                            $response[$type][$i]['datetime']        = ($value['need_datetime'] != '') ? date_format($value['need_datetime'], 'Y-m-d H:i:s') : date_format($value['offer_datetime'], 'Y-m-d H:i:s');
                            $response[$type][$i]['user_with_id']            = ($value['user_id'] == $user_id) ? $value['user_with'] : $value['user_id'];
                            $response[$type][$i]['userwithname']            = ($value['user_id'] == $user_id) ? $value['userwith']['firstname'] : $value['user']['firstname'];
                            $response[$type][$i]['service_id']      = ($value['user_id'] == $user_id) ? $value['need_serviceid'] : $value['offer_serviceid'];
                            $response[$type][$i]['subservices']             = ($value['user_id'] == $user_id) ? $value['subservice']['subservice_name'] : $value['offered_subservice']['subservice_name'];
                            if($value['user_id'] == $user_id){
                                $response[$type][$i]['is_completed']    = (!empty($value['need_completion_datetime'])) ? 1 : 0;
                                $response[$type][$i]['completed_by_me'] = ($value['user_with'] == $user_id && !empty($value['need_completion_datetime'])) ? 1 : 0;
                            }else {
                                $response[$type][$i]['is_completed']    = (!empty($value['offer_completion_datetime'])) ? 1 : 0;
                                $response[$type][$i]['completed_by_me'] = ($value['user_id'] == $user_id && !empty($value['need_completion_datetime'])) ? 1 : 0;
                            }
                            $response[$type][$i]['is_accepted']             = ($value['is_accepted'] == '1') ? 1 : 0;
                            $response[$type][$i]['is_rated']        = ($value['need_rating'] != '') ? 1 : 0;
                            $response[$type][$i]['rating']          = ($value['need_rating'] != '') ? $value['need_rating'] : "";
                            $response[$type][$i]['in_progress']     = ($value['offer_completion_datetime'] != '' || $value['need_completion_datetime'] != '') ? 1 : 0;
                            $response[$type][$i]['amount']          = ($value['amount']) ? $value['amount'] : 0;
                            $i++;
                        }elseif($value['need_rating'] != '' && $value['need_rating'] != 3) {
                            $response["Completed"][$i]['id']            = $value['id'];
                            $response["Completed"][$i]['i_made_offer']  = ($value['user_id'] == $user_id) ? 1 : 0;
                            $response["Completed"][$i]['datetime']      = ($value['user_id'] == $user_id) ? ($value['need_datetime'] != '' ? date_format($value['need_datetime'], 'Y-m-d H:i:s') : date_format($value['offer_datetime'], 'Y-m-d H:i:s')) : ($value['offer_datetime'] != '' ? date_format($value['offer_datetime'], 'Y-m-d H:i:s') : date_format($value['need_datetime'], 'Y-m-d H:i:s'));
                            $response["Completed"][$i]['user_with_id']  = ($value['user_id'] == $user_id) ? $value['user_with'] : $value['user_id'];
                            $response["Completed"][$i]['userwithname']  = ($value['user_id'] == $user_id) ? $value['userwith']['firstname'] : $value['user']['firstname'];
                            $response["Completed"][$i]['service_id']    = ($value['user_id'] == $user_id) ? $value['need_serviceid'] : $value['offer_serviceid'];
                            $response["Completed"][$i]['subservices']   = ($value['user_id'] == $user_id) ? $value['subservice']['subservice_name'] : $value['offered_subservice']['subservice_name'];
                            $response["Completed"][$i]['is_rated']      = ($value['need_rating'] != '') ? 1 : 0;
                            $response["Completed"][$i]['rating']        = ($value['need_rating'] != '') ? $value['need_rating'] : "";
                            $response["Completed"][$i]['amount']        = ($value['amount']) ? $value['amount'] : 0;
                            $response["Completed"][$i]['status']        = ($value['status'] == '0') ? "Current" : "Completed";
                            $i++;
                        }   
                        if($value['offer_rating'] == '')
                        {
                            $response[$type][$i]['id']          = $value['id'];
                            $response[$type][$i]['type']            = "offer";
                            $response[$type][$i]['i_made_offer']            = ($value['user_id'] == $user_id) ? 1 : 0;
                            $response[$type][$i]['datetime']        = ($value['offer_datetime'] != '') ? date_format($value['offer_datetime'], 'Y-m-d H:i:s') : date_format($value['need_datetime'], 'Y-m-d H:i:s');
                            $response[$type][$i]['user_with_id']            = ($value['user_with'] == $user_id) ? $value['user_id'] : $value['user_with'];
                            $response[$type][$i]['userwithname']            = ($value['user_with'] == $user_id) ? $value['user']['firstname'] : $value['userwith']['firstname'];
                            $response[$type][$i]['service_id']      = ($value['user_with'] == $user_id) ? $value['need_serviceid'] : $value['offer_serviceid'];
                            $response[$type][$i]['subservices']             = ($value['user_with'] == $user_id) ? $value['subservice']['subservice_name'] : $value['offered_subservice']['subservice_name'];
                            $response[$type][$i]['is_completed']            = (!empty($value['offer_completion_datetime'])) ? 1 : 0;
                            if($value['user_with'] == $user_id){
                                $response[$type][$i]['is_completed']    = (!empty($value['need_completion_datetime'])) ? 1 : 0;
                                $response[$type][$i]['completed_by_me'] = (!empty($value['need_completion_datetime'])) ? 1 : 0;
                            }else {
                                $response[$type][$i]['is_completed']    = (!empty($value['offer_completion_datetime'])) ? 1 : 0;
                                $response[$type][$i]['completed_by_me'] = (!empty($value['offer_completion_datetime'])) ? 1 : 0;
                            }
                            $response[$type][$i]['is_accepted']             = ($value['is_accepted'] == '1') ? 1 : 0;
                            $response[$type][$i]['is_rated']        = ($value['offer_rating'] != '') ? 1 : 0;
                            $response[$type][$i]['rating']          = ($value['offer_rating'] != '') ? $value['offer_rating'] : "";
                            $response[$type][$i]['in_progress']     = ($value['offer_completion_datetime'] != '' || $value['need_completion_datetime'] != '') ? 1 : 0;
                            $response[$type][$i]['amount']          = ($value['amount']) ? $value['amount'] : 0;
                            $i++;
                        }elseif($value['offer_rating'] != '' && $value['offer_rating'] != 3) {
                            $response["Completed"][$i]['id']        = $value['id'];
                            $response["Completed"][$i]['type']      = "offer";
                            $response["Completed"][$i]['i_made_offer']  = ($value['user_id'] == $user_id) ? 1 : 0;
                            $response["Completed"][$i]['datetime']      = ($value['user_id'] == $user_id) ? ($value['need_datetime'] != '' ? date_format($value['need_datetime'], 'Y-m-d H:i:s') : date_format($value['offer_datetime'], 'Y-m-d H:i:s')) : ($value['offer_datetime'] != '' ? date_format($value['offer_datetime'], 'Y-m-d H:i:s') : date_format($value['need_datetime'], 'Y-m-d H:i:s'));
                            $response["Completed"][$i]['user_with_id']  = ($value['user_id'] == $user_id) ? $value['user_with'] : $value['user_id'];
                            $response["Completed"][$i]['userwithname']  = ($value['user_id'] == $user_id) ? $value['userwith']['firstname'] : $value['user']['firstname'];
                            $response["Completed"][$i]['service_id']    = ($value['user_id'] != $user_id) ? $value['need_serviceid'] : $value['offer_serviceid'];
                            $response["Completed"][$i]['subservices']   = ($value['user_id'] != $user_id) ? $value['subservice']['subservice_name'] : $value['offered_subservice']['subservice_name'];
                            $response["Completed"][$i]['is_rated']      = ($value['offer_rating'] != '') ? 1 : 0;
                            $response["Completed"][$i]['rating']        = ($value['offer_rating'] != '') ? $value['offer_rating'] : "";
                            $response["Completed"][$i]['amount']        = ($value['amount']) ? $value['amount'] : 0;
                            $response["Completed"][$i]['status']        = ($value['status'] == '0') ? "Current" : "Completed";
                            $i++;
                        }   
                    }elseif ($value['user_with'] == $user_id){
                        if($value['offer_rating'] == '')
                        {
                            $response[$type][$i]['id']          = $value['id'];
                            $response[$type][$i]['type']            = "need";
                            $response[$type][$i]['i_made_offer']            = ($value['user_id'] == $user_id) ? 1 : 0;
                            $response[$type][$i]['datetime']        = ($value['offer_datetime'] != '') ? date_format($value['offer_datetime'], 'Y-m-d H:i:s') : date_format($value['need_datetime'], 'Y-m-d H:i:s');
                            $response[$type][$i]['user_with_id']            = ($value['user_id'] == $user_id) ? $value['user_with'] : $value['user_id'];
                            $response[$type][$i]['userwithname']            = ($value['user_id'] == $user_id) ? $value['userwith']['firstname'] : $value['user']['firstname'];
                            $response[$type][$i]['service_id']      = ($value['user_id'] == $user_id) ? $value['need_serviceid'] : $value['offer_serviceid'];
                            $response[$type][$i]['subservices']             = ($value['user_id'] == $user_id) ? $value['subservice']['subservice_name'] : $value['offered_subservice']['subservice_name'];
                            if($value['user_id'] == $user_id){
                                $response[$type][$i]['is_completed']    = (!empty($value['need_completion_datetime'])) ? 1 : 0;
                                $response[$type][$i]['completed_by_me'] = ($value['user_with'] == $user_id && !empty($value['need_completion_datetime'])) ? 1 : 0;
                            }else {
                                $response[$type][$i]['is_completed']    = (!empty($value['offer_completion_datetime'])) ? 1 : 0;
                                $response[$type][$i]['completed_by_me'] = ($value['user_id'] == $user_id && !empty($value['need_completion_datetime'])) ? 1 : 0;
                            }
                            $response[$type][$i]['is_accepted']             = ($value['is_accepted'] == '1') ? 1 : 0;
                            $response[$type][$i]['is_rated']        = ($value['need_rating'] != '') ? 1 : 0;
                            $response[$type][$i]['rating']          = ($value['need_rating'] != '') ? $value['need_rating'] : "";
                            $response[$type][$i]['in_progress']     = ($value['offer_completion_datetime'] != '' || $value['need_completion_datetime'] != '') ? 1 : 0;
                            $response[$type][$i]['amount']          = ($value['amount']) ? $value['amount'] : 0;
                            $i++;
                        }elseif($value['offer_rating'] != '' && $value['offer_rating'] != 3) {
                            $response["Completed"][$i]['id']        = $value['id'];
                            $response["Completed"][$i]['type']              = "need";
                            $response["Completed"][$i]['i_made_offer']  = ($value['user_id'] == $user_id) ? 1 : 0;
                            $response["Completed"][$i]['datetime']      = ($value['user_id'] == $user_id) ? ($value['need_datetime'] != '' ? date_format($value['need_datetime'], 'Y-m-d H:i:s') : date_format($value['offer_datetime'], 'Y-m-d H:i:s')) : ($value['offer_datetime'] != '' ? date_format($value['offer_datetime'], 'Y-m-d H:i:s') : date_format($value['need_datetime'], 'Y-m-d H:i:s'));
                            $response["Completed"][$i]['user_with_id']  = ($value['user_id'] == $user_id) ? $value['user_with'] : $value['user_id'];
                            $response["Completed"][$i]['userwithname']  = ($value['user_id'] == $user_id) ? $value['userwith']['firstname'] : $value['user']['firstname'];
                            $response["Completed"][$i]['service_id']    = ($value['user_id'] == $user_id) ? $value['need_serviceid'] : $value['offer_serviceid'];
                            $response["Completed"][$i]['subservices']   = ($value['user_id'] == $user_id) ? $value['subservice']['subservice_name'] : $value['offered_subservice']['subservice_name'];
                            $response["Completed"][$i]['is_rated']      = ($value['offer_rating'] != '') ? 1 : 0;
                            $response["Completed"][$i]['rating']        = ($value['offer_rating'] != '') ? $value['offer_rating'] : "";
                            $response["Completed"][$i]['amount']        = ($value['amount']) ? $value['amount'] : 0;
                            $response["Completed"][$i]['status']        = ($value['status'] == '0') ? "Current" : "Completed";
                            $i++;
                        }
                        if($value['need_rating'] == '')
                        {
                            $response[$type][$i]['id']                      = $value['id'];
                            $response[$type][$i]['type']                    = "offer";
                            $response[$type][$i]['i_made_offer']            = ($value['user_id'] == $user_id) ? 1 : 0;
                            $response[$type][$i]['datetime']                = ($value['need_datetime'] != '') ? date_format($value['need_datetime'], 'Y-m-d H:i:s') : date_format($value['offer_datetime'], 'Y-m-d H:i:s');
                            $response[$type][$i]['user_with_id']            = ($value['user_with'] == $user_id) ? $value['user_id'] : $value['user_with'];
                            $response[$type][$i]['userwithname']            = ($value['user_with'] == $user_id) ? $value['user']['firstname'] : $value['userwith']['firstname'];
                            $response[$type][$i]['service_id']              = ($value['user_with'] == $user_id) ? $value['need_serviceid'] : $value['offer_serviceid'];
                            $response[$type][$i]['subservices']             = ($value['user_with'] == $user_id) ? $value['subservice']['subservice_name'] : $value['offered_subservice']['subservice_name'];
                            $response[$type][$i]['is_completed']            = (!empty($value['offer_completion_datetime'])) ? 1 : 0;
                            if($value['user_with'] == $user_id){
                                $response[$type][$i]['is_completed']    = (!empty($value['need_completion_datetime'])) ? 1 : 0;
                                $response[$type][$i]['completed_by_me'] = (!empty($value['need_completion_datetime'])) ? 1 : 0;
                            }else {
                                $response[$type][$i]['is_completed']    = (!empty($value['offer_completion_datetime'])) ? 1 : 0;
                                $response[$type][$i]['completed_by_me'] = (!empty($value['offer_completion_datetime'])) ? 1 : 0;
                            }
                            $response[$type][$i]['is_accepted']             = ($value['is_accepted'] == '1') ? 1 : 0;
                            $response[$type][$i]['is_rated']        = ($value['offer_rating'] != '') ? 1 : 0;
                            $response[$type][$i]['rating']          = ($value['offer_rating'] != '') ? $value['offer_rating'] : "";
                            $response[$type][$i]['in_progress']     = ($value['offer_completion_datetime'] != '' || $value['need_completion_datetime'] != '') ? 1 : 0;
                            $response[$type][$i]['amount']          = ($value['amount']) ? $value['amount'] : 0;
                            $i++;
                        }elseif($value['need_rating'] != '' && $value['need_rating'] != 3) {
                            $response["Completed"][$i]['id']        = $value['id'];
                            $response["Completed"][$i]['type']      = "offer";
                            $response["Completed"][$i]['i_made_offer']  = ($value['user_id'] == $user_id) ? 1 : 0;
                            $response["Completed"][$i]['datetime']      = ($value['user_id'] == $user_id) ? ($value['need_datetime'] != '' ? date_format($value['need_datetime'], 'Y-m-d H:i:s') : date_format($value['offer_datetime'], 'Y-m-d H:i:s')) : ($value['offer_datetime'] != '' ? date_format($value['offer_datetime'], 'Y-m-d H:i:s') : date_format($value['need_datetime'], 'Y-m-d H:i:s'));
                            $response["Completed"][$i]['user_with_id']  = ($value['user_id'] == $user_id) ? $value['user_with'] : $value['user_id'];
                            $response["Completed"][$i]['userwithname']  = ($value['user_id'] == $user_id) ? $value['userwith']['firstname'] : $value['user']['firstname'];
                            $response["Completed"][$i]['service_id']    = ($value['user_id'] == $user_id) ? $value['offer_serviceid'] : $value['need_serviceid'];
                            $response["Completed"][$i]['subservices']   = ($value['user_id'] == $user_id) ? $value['offered_subservice']['subservice_name'] : $value['subservice']['subservice_name'];
                            $response["Completed"][$i]['is_rated']      = ($value['need_rating'] != '') ? 1 : 0;
                            $response["Completed"][$i]['rating']        = ($value['need_rating'] != '') ? $value['need_rating'] : "";
                            $response["Completed"][$i]['amount']        = ($value['amount']) ? $value['amount'] : 0;
                            $response["Completed"][$i]['status']        = ($value['status'] == '0') ? "Current" : "Completed";
                            $i++;
                        }
                    }
                }
                /*
                 * Logic for completed tab
                 */
                if ($value['status'] == '1' || $value['status'] == '2'){
                    $response[$type][$i]['id']      = $value['id'];
                    $response[$type][$i]['i_made_offer']    = ($value['user_id'] == $user_id) ? 1 : 0;
                    $response[$type][$i]['datetime']    = ($value['user_id'] == $user_id) ? ($value['need_datetime'] != '' ? date_format($value['need_datetime'], 'Y-m-d H:i:s') : date_format($value['offer_datetime'], 'Y-m-d H:i:s')) : ($value['offer_datetime'] != '' ? date_format($value['offer_datetime'], 'Y-m-d H:i:s') : date_format($value['need_datetime'], 'Y-m-d H:i:s'));
                    $response[$type][$i]['user_with_id']    = ($value['user_id'] == $user_id) ? $value['user_with'] : $value['user_id'];
                    $response[$type][$i]['userwithname']    = ($value['user_id'] == $user_id) ? $value['userwith']['firstname'] : $value['user']['firstname'];
                    $response[$type][$i]['service_id']  = ($value['user_id'] == $user_id) ? $value['need_serviceid'] : $value['offer_serviceid'];
                    $response[$type][$i]['subservices']     = ($value['user_id'] == $user_id) ? $value['subservice']['subservice_name'] : $value['offered_subservice']['subservice_name'];
                    $response[$type][$i]['amount']      = ($value['amount']) ? $value['amount'] : 0;
                    $response[$type][$i]['status']      = ($value['status'] == '1') ? "Completed" : "Cancelled";
                                $response[$type][$i]['cancelled_by']    = ($value['cancelled_by']) ? $value['cancelled_by'] : "";
                    $i++;
                }
            }
                if($request_type == 'CURRENT'){
                    $response['Current'] = array_values($response['Current']);
                    $response['Completed'] = array();
                }else {
                    $response['Current'] = array();
                    $response['Completed'] = array_values($response['Completed']);
                }
        }
        return $response;
    }
    
    /*
     * Get transaction by tran_id
     */
    public function gettransactionwithid($select=array(),$condition=array(),$contain=array())
    {
        $results = $this->find()
        ->where($condition)
        ->select($select)
        ->contain($contain)
        ->first();
        
        $response = array();
        if($results)
        {
            $results = $results->toArray();
            $response['id']             = $results['id'];
            $response['user_id']            = $results['user']['id'];
            $response['user_name']          = $results['user']['firstname'].", ".substr($results['user']['lastname'],0,1).".";
            $response['user_image']         = $results['user']['profile_image_thumb'];
            $response['userwith_id']        = $results['userwith']['id'];
            $response['userwith_name']      = $results['userwith']['firstname'].", ".substr($results['userwith']['lastname'],0,1).".";
            $response['userwith_image']     = $results['userwith']['profile_image_thumb'];
            $response['need_serviceid']         = (isset($results['need_serviceid'])) ? $results['need_serviceid'] : 0;
            $response['need_subserviceid']      = (isset($results['need_subserviceid'])) ? $results['need_subserviceid'] : 0;
            $response['need_subservicename']    = (isset($results['subservice']['subservice_name'])) ? $results['subservice']['subservice_name'] : "";
            $response['need_note']          = ($results['need_note'] != '') ? $results['need_note'] : "";
            $response['need_datetime']      = ($results['need_datetime'] != '') ? date_format($results['need_datetime'], 'Y-m-d H:i:s') : "";
            $response['need_location']      = ($results['need_location'] != '') ? $results['need_location'] : "";
            $response['offer_serviceid']        = (isset($results['offer_serviceid'])) ? $results['offer_serviceid'] : 0;
            $response['offer_subserviceid']     = (isset($results['offer_subserviceid'])) ? $results['offer_subserviceid'] : 0;
            $response['offer_subservicename']   = (isset($results['offered_subservice']['subservice_name'])) ? $results['offered_subservice']['subservice_name'] : "";
            $response['offer_note']         = ($results['offer_note'] != '') ? $results['offer_note'] : "";
            $response['offer_datetime']         = ($results['offer_datetime'] != '') ? date_format($results['offer_datetime'], 'Y-m-d H:i:s') : "";
            $response['offer_location']         = ($results['offer_location'] != '') ? $results['offer_location'] : "";
            $response['amount']             = ($results['amount'] != '') ? $results['amount'] : 0;
        }
        return $response;
    }
    
    /*
     * For CMS rating summary page
     */
    public function getalltransaction($results)
    { 
        $results = $results->toArray();
        $response = array();
        if($results)
        {
            foreach ($results as $key=>$value)
            {
                $response[$key]['id']           = $value['id'];
                $response[$key]['Requestor']        = $value['user']['firstname'].", ".substr($value['user']['lastname'],0,1).".";
                $response[$key]['Provider']         = $value['userwith']['firstname'].", ".substr($value['userwith']['lastname'],0,1).".";
                $response[$key]['completion_date']  = isset($value['completion_date']) ? date_format($value['completion_date'], 'g:i A \o\n l, d F,Y') : "";
                $response[$key]['status']       = ($value['status']=='0') ? "Current" : ($value['status']=='1' ? "Completed" : "Cancelled");
                $response[$key]['service_requested']    = $value['service']['service_name'];
                $response[$key]['subservice_requested'] = $value['subservice']['subservice_name'];
                $response[$key]['requested_date']   = isset($value['need_datetime']) ? date_format($value['need_datetime'], 'g:i A \o\n l, d F,Y') : "";
                $response[$key]['requested_location']   = $value['need_location'];
                $response[$key]['requested_note']   = $value['need_note'];
                $response[$key]['requested_rating']     = $value['need_rating'];
                $response[$key]['offered_service']  = $value['offered_service']['service_name'];
                $response[$key]['offered_subservice']   = $value['offered_subservice']['subservice_name'];
                $response[$key]['offered_date']     = ($value['offer_datetime'] != '') ? date_format($value['offer_datetime'], 'g:i A \o\n l, d F,Y') : "";
                $response[$key]['offered_location']     = $value['offer_location'];
                $response[$key]['offered_note']     = $value['offer_note'];
                $response[$key]['offered_rating']   = $value['offer_rating'];
                $response[$key]['amount']       = $value['amount'];
                $response[$key]['fee']          = $value['fee'];
                $response[$key]['tax']          = $value['tax'];
                $response[$key]['invoice_value']    = $response[$key]['amount']+$response[$key]['fee']+$response[$key]['tax'];
            }
        }
        return $response;
    }
    
    /*
     * Function to update transaction
     */
    public function updatetransaction($id, $data)
    {
        $transaction = $this->get($id);
        if(is_null($transaction['requested_completion_datetime']) && is_null($transaction['offered_completion_datetime']))
        {
            foreach ($data as $key=>$value)
            {
                if(isset($value) && $value != '')
                {
                    $transaction->$key = $value;
                }
            }
            if($this->save($transaction))
            {
                return true;
            }else {
                return false;
            }
        }else {
            return false;
        }
    }
    
    /*
     * Function to rate user for transaction
     */
    public function addfeedback($param)
    {
        $transaction = $this->get($param['offer_id']);
        $data['rating']     = $param['rating'];
        if($transaction['user_id'] == $param['user_id'])
        {
            $transaction['need_rating']         = $param['rating'];
            $transaction['need_rating_datetime']    = $param['date'];
            $data['user_id']            = $transaction['user_with'];
            $data['service_id']             = $transaction['need_serviceid'];
            $data['subservice_id']          = $transaction['need_subserviceid'];
            $data['type']                           = '1';
        }elseif ($transaction['user_with'] == $param['user_id'])
        {
            $transaction['offer_rating']        = $param['rating'];
            $transaction['offer_rating_datetime']   = $param['date'];
            $data['user_id']            = $transaction['user_id'];
            $data['service_id']             = $transaction['offer_serviceid'];
            $data['subservice_id']          = $transaction['offer_subserviceid'];
            $data['type']                           = '1';
        }
        if($this->save($transaction))
        {
            if($transaction['need_rating'] && $transaction['offer_rating']){
                $transaction['status']      = '1';
                $this->save($transaction);
            }
            return $data;
        }else {
            return false;
        }
    }
    
    /*
     * Function to set transaction as complete and check for payment initiate
     */
    public function setoffercomplete($param)
    {
        $transaction = $this->get($param['offer_id']);
        $data['user_id']   = $param['user_id'];
        $time = strtotime($param['date']);
        $user_id = $transaction['user_id'];
        $user_with = $transaction['user_with'];
        $offered_serviceid = $transaction['offer_serviceid'];
        $need_serviceid = $transaction['need_serviceid'];
    $payment_status = ($transaction['payment_status'] == '0') ? 0 : 1;
        if($transaction['user_id'] == $param['user_id'])
        {
            $transaction['need_completion_datetime'] = date('Y-m-d H:i:s', $time);
        $need_completion = 1;
        $offered_completion = 0;
        }elseif ($transaction['user_with'] == $param['user_id'])
        {
            $transaction['offer_completion_datetime']   = date('Y-m-d H:i:s', $time);
        $need_completion = 0;
        $offered_completion = 1;
        }
        
        if($this->save($transaction))
        {
            if($user_id == $param['user_id'] && $payment_status == 0 && $offered_serviceid == 8 && $need_completion == 1)
            {
                    $data['initiate_payment'] = 1;
            }elseif ($user_with == $param['user_id'] && $payment_status == 0 && $need_serviceid == 8 && $offered_completion == 1)
            {
                    $data['initiate_payment'] = 1;
            }else {
            $data['initiate_payment'] = 0;
        }
            return $data;
        }else {
            return false;
        }
    }

    /*
     * Function to return result for invoice
     */    
    public function getinvoicedtransaction($select=array(),$condition=array(),$contain=array())
    {
        $results = $this->find()
        ->where($condition)
        ->select($select)
        ->contain($contain)
        ->all();
    
        if($results)
        {
            $results = $results->toArray();
            $results = $results[0];
        }
        return $results;
    }
    
    /*
     * Function to return result to controller where result can be set as per requirement
     */
    public function getRecordstoController($select=array(),$condition=array(),$contain=array())
    {
        $results = $this->find()
        ->where($condition)
        ->select($select)
        ->contain($contain)
        ->all();
    
        if($results)
        {
            $results = $results->toArray();
        }
        return $results;
    }
    
    public function updatedata($id, $data) 
    {
        $user = $this->get($id);
        foreach ($data as $key=>$value)
        {
            $user->$key = $value;
        }
        if($this->save($user))
        {
            return true;
        }else {
            return false;
        }
    }

    public function getDataForUsertaskView($data)
    {
        $data = $data->toArray();
        $response = array();
        $i = 0;
        foreach ($data as $key=>$value)
        {
            $type = ($value['status'] == '0') ? "Current" : ($value['status'] == '1' ? "Completed" : "Cancelled");
            $response[$i]['srno']               = $i;
            $response[$i]['id']                 = $value['id'];
            $response[$i]['requestor']          = $value['user']['firstname']." ".$value['user']['lastname'];
            $response[$i]['feedbackby']         = $value['userwith']['firstname']." ".$value['userwith']['lastname'];
            $response[$i]['request_date']       = ($value['need_datetime'] != '') ? date_format($value['need_datetime'], 'Y-m-d') : "";
            $response[$i]['completion_date']    = ($value['need_completion_datetime'] != '') ? date_format($value['need_completion_datetime'], 'Y-m-d') : "";
            $response[$i]['rating']             = ($value['need_rating'] != '') ? $value['need_rating'] : "";
            $response[$i]['rating_date']        = ($value['need_rating_datetime'] != '') ? date_format($value['need_rating_datetime'], 'Y-m-d') : "";
            $response[$i]['service']            = $value['service']['service_name'];
            $response[$i]['subservice']         = $value['subservice']['subservice_name'];
            $response[$i]['location']           = ($value['need_location'] != '') ? $value['need_location'] : "";
            $response[$i]['note']               = ($value['need_note'] != '') ? $value['need_note'] : "";
            $response[$i]['transaction_value']  = $value['amount'];
            $response[$i]['transaction_fee']    = $value['fee'];
            $response[$i]['transaction_tax']    = $value['tax'];
            $response[$i]['initiator']          = 1;
            $i++;
            $response[$i]['srno']               = $i;
            $response[$i]['id']                 = $value['id'];
            $response[$i]['requestor']          = $value['userwith']['firstname']." ".$value['userwith']['lastname'];
            $response[$i]['feedbackby']         = $value['user']['firstname']." ".$value['user']['lastname'];
            $response[$i]['request_date']       = ($value['offer_datetime'] != '') ? date_format($value['offer_datetime'], 'Y-m-d') : "";
            $response[$i]['completion_date']    = ($value['offer_completion_datetime'] != '') ? date_format($value['offer_completion_datetime'], 'Y-m-d') : "";
            $response[$i]['rating']             = ($value['offer_rating'] != '') ? $value['offer_rating'] : "";
            $response[$i]['rating_date']        = ($value['offer_rating_datetime'] != '') ? date_format($value['offer_rating_datetime'], 'Y-m-d') : "";
            $response[$i]['service']            = $value['offered_service']['service_name'];
            $response[$i]['subservice']         = $value['offered_subservice']['subservice_name'];
            $response[$i]['location']           = ($value['offer_location'] != '') ? $value['offer_location'] : "";
            $response[$i]['note']               = ($value['offer_note'] != '') ? $value['offer_note'] : "";
            $response[$i]['transaction_value']  = $value['amount'];
            $response[$i]['transaction_fee']    = $value['fee'];
            $response[$i]['transaction_tax']    = $value['tax'];
            $response[$i]['initiator']          = 0;
            $i++;
        }
        return $response;
    }

    public function getDataForUsertaskList($data)
    {
        $data = $data->toArray();
        $response = array();
        $i = 1;
        foreach ($data as $key=>$value)
        {
            $type = ($value['status'] == '0') ? "Current" : ($value['status'] == '1' ? "Completed" : "Cancelled");
            $response[$i]['srno']               = $i;
            $response[$i]['id']                 = $value['id'];
            $response[$i]['requestor']          = $value['user']['firstname']." ".$value['user']['lastname'];
            $response[$i]['serviceprovider']    = $value['userwith']['firstname']." ".$value['userwith']['lastname'];
            $response[$i]['accept_date']        = ($value['accept_date'] != '') ? date_format($value['accept_date'], 'Y-m-d h:i') : "";
            $response[$i]['status']             = $type;
            $response[$i]['service']            = ($value['need_serviceid']) ? $value['service']['service_name'] : $value['need_customservice'];
            $response[$i]['subservice']         = ($value['need_subserviceid']) ? $value['subservice']['subservice_name'] : $value['need_customservice'];
            $response[$i]['date']               = ($value['need_datetime'] != '') ? date_format($value['need_datetime'], 'Y-m-d h:i') : "";
            $response[$i]['location']           = ($value['need_location'] != '') ? $value['need_location'] : "";
            $response[$i]['note']               = ($value['need_note'] != '') ? $value['need_note'] : "";
            $response[$i]['transaction_value']  = $value['amount'];
            $response[$i]['transaction_fee']    = $value['fee'];
            $response[$i]['transaction_tax']    = $value['tax'];
            $response[$i]['invoice_value']      = $value['amount'] + $value['fee'] + $value['tax'];
            $response[$i]['initiator']          = 1;
            $i++;
            $response[$i]['srno']               = $i;
            $response[$i]['id']                 = $value['id'];
            $response[$i]['requestor']          = $value['userwith']['firstname']." ".$value['userwith']['lastname'];
            $response[$i]['serviceprovider']    = $value['user']['firstname']." ".$value['user']['lastname'];
            $response[$i]['accept_date']        = ($value['accept_date'] != '') ? date_format($value['accept_date'], 'Y-m-d h:i') : "";
            $response[$i]['status']             = $type;
            $response[$i]['service']            = ($value['offer_serviceid']) ? $value['offered_service']['service_name'] : ($value['offer_customservice']);
            $response[$i]['subservice']         = ($value['offer_subserviceid']) ? $value['offered_subservice']['subservice_name'] : ($value['offer_customservice']);
            $response[$i]['date']               = ($value['offer_datetime'] != '') ? date_format($value['offer_datetime'], 'Y-m-d h:i') : "";
            $response[$i]['location']           = ($value['offer_location'] != '') ? $value['offer_location'] : "";
            $response[$i]['note']               = ($value['offer_note'] != '') ? $value['offer_note'] : "";
            $response[$i]['transaction_value']  = $value['amount'];
            $response[$i]['transaction_fee']    = $value['fee'];
            $response[$i]['transaction_tax']    = $value['tax'];
            $response[$i]['invoice_value']      = $value['amount'] + $value['fee'] + $value['tax'];
            $response[$i]['initiator']          = 0;
            $i++;
        }
        return $response;
    }

    public function getDataForUserlistview($data)
    {
        $data = $data->toArray();
        $response = array();
        $i = 1;
        foreach ($data as $datakey=>$datavalue)
        {
            $response[$i]['srno']                       = $i;
            $response[$i]['transaction_id']             = $datavalue['id'];
            $response[$i]['firstname']                  = $datavalue['user']['firstname'];
            $response[$i]['lastname']                   = $datavalue['user']['lastname'];
            $response[$i]['email']                      = $datavalue['user']['email'];
            $response[$i]['postcode']                   = $datavalue['user']['postcode'];
            $response[$i]['suburb']                     = $datavalue['user']['suburb'];
            $response[$i]['city']                       = $datavalue['user']['city'];
            $response[$i]['state']                      = $datavalue['user']['state'];
            $response[$i]['country']                    = $datavalue['user']['country'];
            $response[$i]['subservice_needed']          = $datavalue['subservice']['subservice_name'];
            $response[$i]['subservice_offered']         = $datavalue['offered_subservice']['subservice_name'];
            $response[$i]['transaction_amount']         = $datavalue['amount'];
            $response[$i]['transaction_fee']            = $datavalue['fee'];
            $response[$i]['date_requested']             = ($datavalue['requested_datetime'] != '') ? date_format($datavalue['requested_datetime'], 'Y-m-d') : "";
            $response[$i]['date_completed']             = ($datavalue['completion_date'] != '') ? date_format($datavalue['completion_date'], 'Y-m-d') : "";
            $response[$i]['date_paid']                  = ($datavalue['payment_date'] != '') ? date_format($datavalue['payment_date'], 'Y-m-d') : "";
            if($datavalue['status'] == '1')
            {
                $response[$i]['transaction_status']     = 'completed';
            }elseif ($datavalue['status'] == '2'){
                $response[$i]['transaction_status']     = 'cancelled';
            }elseif ($datavalue['status'] == '0'){
                $response[$i]['transaction_status']     = 'current';
            }
            $i++;
        }
        return $response;
    }
}
