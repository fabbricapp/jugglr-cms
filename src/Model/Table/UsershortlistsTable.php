<?php
namespace App\Model\Table;

use App\Model\Entity\Userconnection;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;


/**
 * Users Model
 *
 */
class UsershortlistsTable extends AppTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('usershortlists');
        $this->displayField('id');
        $this->primaryKey('id');
      	
        $this->belongsTo('Userwith', [
        	'className' => 'Users',
        	'foreignKey' => 'user_with'
        ]);
        $this->belongsTo('Users', [
        	'foreignKey' => 'user_id'
        ]);
        $this->addBehavior('CounterCache', [
            'Userwith' => ['shortlist_cnt'],
            'Users' => ['usershortlist_cnt']
        ]);
    }

	public function adduser_shortlist($requestdata = null) 
	{
		$query = $this->find()
		->where(['Usershortlists.user_id' => $requestdata['user_with'],'Usershortlists.user_with' => $requestdata['user_id']])
		->orWhere(['Usershortlists.user_id' => $requestdata['user_id'],'Usershortlists.user_with' => $requestdata['user_with']]);
		$result = $query->first();
		
		if(!empty($result))
		{
			if($result['user_with'] == $requestdata['user_with'] && $result['user_id'] == $requestdata['user_id'])
			{
				$response['type'] = 'ALREADY_SHORTLISTED';
			}elseif($result['user_with'] == $requestdata['user_id'] && $result['user_id'] == $requestdata['user_with'])
			{
				$usermatches 		= TableRegistry::get('Usermatches');
				$datatosave = array();
				$datatosave['user_id'] 			= $requestdata['user_id'];
				$datatosave['user_with'] 		= $requestdata['user_with'];
				$response['type'] = $usermatches->adduser_matches($datatosave);

				$connection = $this->get($result['id']);
				$this->delete($connection);
			}else {
				$response['type'] = 'PERFECT_MATCH';
			}
		}else 
		{
			$datatosave = array();
			$datatosave['user_id'] 			= $requestdata['user_id'];
			$datatosave['user_with'] 		= $requestdata['user_with'];
			$connection = $this->newEntity($datatosave);
			$this->save($connection);
			$response['type'] = 'PROVIDER_SHORTLISTED';
		}
		return $response;
    }
    
    public function getShortlistedUserIds($where, $user_id)
    {
    	$result = $this->find('all')
    			->where($where)
    			->all();
    	$user_arr = array();
    	if($result){
    		$result = $result->toArray();
    		foreach ($result as $key => $value) {
    			if(!in_array($value['user_id'], $user_arr) && $value['user_id'] != $user_id){
    				$user_arr[] = $value['user_id'];
    			}
    			if(!in_array($value['user_with'], $user_arr) && $value['user_with'] != $user_id){
    				$user_arr[] = $value['user_with'];
    			}
    		}
    	}
    	return $user_arr;
    }

    public function isShortListed($curruser_id, $serviceprovider_id)
    {
    	$exists = $this->exists(['user_id' => $curruser_id, 'user_with' => $serviceprovider_id]);
    	if($exists == 1){
    		return 1;
    	}else {
    		return 0;
    	}
    }

    public function isBlocked($curruser_id, $serviceprovider_id)
    {
    	$exists1 = $this->exists(['user_id' => $curruser_id, 'user_with' => $serviceprovider_id, 'is_block' => '1']);
    	$exists2 = $this->exists(['user_with' => $curruser_id, 'user_id' => $serviceprovider_id, 'is_block' => '1']);
    	if($exists1 == 0 && $exists2 == 0){
    		return 0;
    	}else {
    		return 1;
    	}
    }

    public function blockedBy($curruser_id, $serviceprovider_id)
    {
    	$query = $this->find()
		->where(['Usershortlists.user_id' => $requestdata['user_with'],'Usershortlists.user_with' => $requestdata['user_id']])
		->orWhere(['Usershortlists.user_id' => $requestdata['user_id'],'Usershortlists.user_with' => $requestdata['user_with']]);
		$result = $query->first();
    	return $result['blocked_by'];
    }
    
    public function block_connection($requestdata) 
    {
    	$query = $this->find()
    	->where(['Usershortlists.user_id' => $requestdata['user_with'],'Usershortlists.user_with' => $requestdata['user_id']])
    	->orWhere(['Usershortlists.user_id' => $requestdata['user_id'],'Usershortlists.user_with' => $requestdata['user_with']]);
    	$result = $query->first();
    	
    	if(!empty($result))
    	{
    		$connection = $this->get($result['id']);
    		if($result['is_block'] == '1'){
    			$connection->is_block = '0';
    			$connection->blocked_by = '0';
    			$response['type'] = 'CONNECTION_UNBLOCKED';
    		}else {
	    		$connection->is_block = '1';
	    		$connection->blocked_by = $requestdata['user_id'];
    			$response['type'] = 'CONNECTION_BLOCKED';
    		}
    	}else 
    	{
    		$datatosave = array();
    		$datatosave['user_id'] 		= $requestdata['user_id'];
    		$datatosave['user_with'] 	= $requestdata['user_with'];
    		$datatosave['is_block'] 	= '1';
    		$datatosave['blocked_by'] 	= $requestdata['user_id'];
    		$connection = $this->newEntity($datatosave);
    		$response['type'] = 'CONNECTION_BLOCKED';
    	}
    	if ($this->save($connection)) 
    	{
    		return $response;
    	} else {
    		$response['type'] = 'ERROR';
    		return $response;
    	}
    }
    
    public function delete_connections($param)
    {
    	$query = $this->find()
    	->select(['id'])
    	->where(['connection_type'=>'0','Userconnections.user_id' => $param['user_id'],'Userconnections.user_with IN' => $param['user_with']])
    	->orWhere(['connection_type'=>'0','Userconnections.user_id IN' => $param['user_with'],'Userconnections.user_with' => $param['user_id']]);
    	$result = $query->find('all');
    	
    	foreach ($result as $key=>$value)
    	{
    	 	$connection = $this->get($value['id']);
    	 	$connection->connection_type = '1';
    	 	if($this->save($connection))
    	 	{
    	 		$result = true;
    	 	}else {
    	 		$result = false;
    	 	}
    	}
    	return $result;
    }
    
    public function getconnectionsdetails($connection=array(),$connectionwith=array(),$connection_id=null,$user_id=null)
    {
    	$response = array();
    	$response['shortlist_cnt'] 	= 0;
    	$response['matches_cnt'] 	= 0;
    	$response['is_shortlist'] 	= 0;
    	$response['is_matched'] 	= 0;
    	$response['is_blocked'] 	= 0;
    	$response['blocked_by'] 	= 0;
		if(!empty($connection))
		{
	    	foreach ($connection as $k=>$val)
	    	{
	    		if($val['connection_type'] == '0' && ($val['user_id'] == $connection_id || $val['user_with'] == $connection_id)) {
	    			$response['matches_cnt']++;
	    		}
	    		if($val['connection_type'] == '0' && ($val['user_id'] == $user_id || $val['user_with'] == $user_id)){
	    			$response['is_matched'] = 1;
	    		}
	    		if($val['connection_type'] == '1' && $val['user_with'] == $connection_id){
	    			$response['shortlist_cnt']++;
	    		}
	    		if($val['connection_type'] == '1' && $val['user_id'] == $user_id && $val['user_with'] == $connection_id){
	    			$response['is_shortlist'] = 1;
	    		}
	    		if($val['is_block'] == '1' && ($val['user_id'] == $user_id || $val['user_with'] == $user_id)){
	    			$response['is_blocked'] = 1;
	    			$response['blocked_by'] = $val['blocked_by'];
	    		}
	    	}
		}
		if(!empty($connectionwith))
		{
			foreach ($connectionwith as $k=>$val)
	    	{
	    		if($val['connection_type'] == '0' && ($val['user_id'] == $connection_id || $val['user_with'] == $connection_id)) {
	    			$response['matches_cnt']++;
	    		}
	    		if($val['connection_type'] == '0' && ($val['user_id'] == $user_id || $val['user_with'] == $user_id) && $response['is_matched'] == 0){
	    			$response['is_matched'] = 1;
	    		}
	    		if($val['connection_type'] == '1' && $val['user_with'] == $connection_id){
	    			$response['shortlist_cnt']++;
	    		}
	    		if($val['connection_type'] == '1' && $val['user_id'] == $user_id && $val['user_with'] == $connection_id  && $response['is_shortlist'] == 0){
	    			$response['is_shortlist'] = 1;
	    		}
	    		if($val['is_block'] == '1' && ($val['user_id'] == $user_id || $val['user_with'] == $user_id) && $response['is_blocked'] == 0){
	    			$response['is_blocked'] = 1;
	    			$response['blocked_by'] = $val['blocked_by'];
	    		}
	    	}
		}
    	return $response;
    }
    
    public function savedata($data) 
    {
        $user = $this->newEntity($data, ['associated' => ['Userdetails']]);
      
        if ($this->save($user)) 
        {
        	return 1;
        } else {
        	return 0;
        }
    }

    public function updatedata($id, $data) 
    {
    	$user = $this->get($id);
    	foreach ($data as $key=>$value)
    	{
    		$user->$key = $value;
    	}
    	$this->save($user);
    	return true;
    }

    public function syncUsermatches($data){
        foreach ($data as $key => $value) {
            $entity = $this->newEntity($value);
            $this->save($entity);
        }
        return 1;
    }

}
