<?php
namespace App\Model\Table;

use App\Model\Entity\Adminsetting;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;


/**
 * Users Model
 *
 */
class AdminsettingsTable extends AppTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('adminsettings');
        $this->displayField('id');
        $this->primaryKey('id');
      
    }
    
    public function getsettings($select=array()) {
    	$query = $this->find()
    	->select($select)
    	->where(['id' => 1]);
    	$results = $query->first()->toArray();
    	return $results;
    }
    
    public function addsetting($data=array()){
    	$setting = $this->get(1);
    	foreach ($data as $key=>$value){
    		$setting->$key = $value;
    	}
    	if ($this->save($setting)) {
    		return 1;
    	} else {
    		return 0;
    	}
    }

}
