<?php
namespace App\Model\Table;

use App\Model\Entity\Userinterest;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;


/**
 * Users Model
 *
 */
class UserinterestsTable extends AppTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('userinterests');
        $this->displayField('id');
        $this->primaryKey('id');
        
        $this->belongsTo('Users',[
            'foreignKey' => 'user_id'
        ]);
    }
    
    public function addUserInterests($user_id,$data) {
        if(!empty($data))
        {
            $datatosave = array();
            foreach ($data as $key=>$value){
                if($value){
                    $datatosave[$key]['user_id']            = $user_id;
                    $datatosave[$key]['interest']           = $value['name'];
                    $datatosave[$key]['createdAt']          = date("Y-m-d h:i:s");
                    $datatosave[$key]['modifiedAt']         = date("Y-m-d h:i:s");
                }
            }
            if($datatosave){
                $entities = $this->newEntities($datatosave);
                foreach ($entities as $entity) {
                        $this->save($entity);
                }
            }
        }
        return 1;
    }

    public function updateUserInterest($selected=null,$unselected=null)
    {
        $datatosave = array();
        $selected = explode(',', trim($selected,'"'));
        if(count($selected) > 0){
            foreach ($selected as $key=>$value){
                if($value){
                    $datatosave[$value]         = '1';
                }
            }
        }
        $unselected = explode(',', trim($unselected,'"'));
        if(count($unselected) > 0){
            foreach ($unselected as $key=>$value){
                if($value){
                    $datatosave[$value]         = '0';
                }
            }
        }
        if(!empty($datatosave)){
            foreach ($datatosave as $key => $value) {
                $user = $this->get($key);
                $user->display = $value;
                $this->save($user);
            }
        }
        return 1;
    }

    public function updatedata($id, $data) {
        $user = $this->get($id);
        foreach ($data as $key=>$value){
            $user->$key = $value;
        }
        $this->save($user);
        return true;
    }

}
