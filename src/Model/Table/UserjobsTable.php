<?php
namespace App\Model\Table;

use App\Model\Entity\Adminsetting;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Cake\Core\Configure;


/**
 * Users Model
 *
 */
class UserjobsTable extends AppTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('userjobs');
        $this->displayField('id');
        $this->primaryKey('id');
        
        $this->belongsTo('Users',[
            'foreignKey' => 'user_id'
        ]);
        $this->belongsTo('Userwith',[
            'className' => 'Users',
            'foreignKey' => 'user_with'
        ]);
        $this->belongsTo('Services',[
            'foreignKey' => 'service_id'
        ]);
        $this->belongsTo('Subservices',[
            'foreignKey' => 'subservice_id'
        ]);
        $this->belongsTo('Subservicetags',[
            'foreignKey' => 'subservicetag_id'
        ]);
        $this->hasMany('Savedjobs', [
            'foreignKey' => 'job_id',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);
        $this->hasMany('Jobapplicants', [
            'foreignKey' => 'job_id',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);
        /*$this->addBehavior('CounterCache', [
            'Users' => ['job_cnt']
        ]);*/
    }
    
    public function saveJob($data=array()){
        $job = $this->newEntity($data);
        
        foreach ($data as $key=>$value){
            $job->$key = $value;
        }
        if ($this->save($job)) {
            return $job->id;
        } else {
            return 0;
        }
    }

    public function updateJob($id, $data) 
    {
        $job = $this->get($id);
        foreach ($data as $key=>$value)
        {
            $job->$key = $value;
        }
        if($this->save($job))
        {
            return $job->id;
        }else {
            return 0;
        }
    }

    public function convertJobToOffer($data)
    {
        $job = $this->get($data['job_id']);
        $condition['Userjobs.id'] = $data['job_id'];
        $applicant_id = $data['applicant_id'];
        $contain = ['Jobapplicants'=> function ($q) use ($applicant_id){
                        return $q
                        ->where(['Jobapplicants.user_id' => $applicant_id]);
                    }];
        $job = $this->find()
        ->where($condition)
        ->contain($contain)
        ->first();
        
        $Usertasks = TableRegistry::get('Usertasks');
        $datatosave['type']                 = 1;
        $datatosave['status']               = '0';
        $datatosave['is_accepted']          = '1';
        if($job['type'] == 1){
            $datatosave['user_id']              = $job['user_with'];
            $datatosave['user_with']            = $job['user_id'];
        }else {
            $datatosave['user_id']              = $job['user_id'];
            $datatosave['user_with']            = $job['user_with'];
        }
        $datatosave['need_serviceid']       = ($job['service_id'] && $job['service_id'] != 0) ? $job['service_id'] : '';
        $datatosave['need_subserviceid']    = ($job['subservice_id'] && $job['subservice_id'] != 0) ? $job['subservice_id'] : 0;
        $datatosave['need_subservicetagid'] = ($job['subservicetag_id'] && $job['subservicetag_id'] != 0) ? $job['subservicetag_id'] : '';
        $datatosave['need_customservice']   = $job['customservice'];
        $datatosave['need_datetime']        = ($job['datetime']) ? $job['datetime'] : $job['jobapplicants'][0]['preferred_datetime'];
        $datatosave['need_location']        = ($job['location']) ? $job['location'] : $job['jobapplicants'][0]['preferred_location'];
        $datatosave['need_note']            = $job['note'];
        
        $datatosave['offer_serviceid']      = Configure::read('PAYMENT_SERVICE_ID');
        $datatosave['offer_subserviceid']   = Configure::read('PAYMENT_SUBSERVICE_ID');
        $datatosave['offer_note']           = $job['jobapplicants'][0]['message'];

        $Apiv4 = TableRegistry::get('Apiv4');
        $amountData = $Apiv4->getAmountData($job['amount']);
        $datatosave['amount']               = ($job['amount'] > 0) ? $job['amount'] : 0.00;
        $datatosave['fee']                  = $amountData['fee'];
        $datatosave['tax']                  = $amountData['tax'];
        $datatosave['promotion_id']         = $job['promotion_id'];
        $datatosave['discount']             = $job['discount'];
        $datatosave['job_id']               = $data['job_id'];
        $datatosave['createdAt']            = date('Y-m-d H:i:s');
        $datatosave['modifiedAt']           = date('Y-m-d H:i:s');

        $result = $Usertasks->add_transaction($datatosave);
        return $result;
    }

    public function getRecordstoController($select=array(), $condition=array(), $contain=array())
    {
        $results = $this->find()
        ->where($condition)
        ->select($select)
        ->contain($contain)
        ->all();
        if($results){
            $results = $results->toArray();
        }
        return $results;
    }
}
