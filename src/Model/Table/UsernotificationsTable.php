<?php
namespace App\Model\Table;

use App\Model\Entity\Adminsetting;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;


/**
 * Users Model
 *
 */
class UsernotificationsTable extends AppTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('usernotifications');
        $this->displayField('id');
        $this->primaryKey('id');
        
        $this->belongsTo('Users',[
                'foreignKey' => 'user_id'
                ]);
        $this->addBehavior('CounterCache', [
            'Users' => ['badge_cnt']
        ]);
    }
    
    public function getNotifications($results) {
        $response = array();
        if($results){
            $results = $results->toArray();
            foreach ($results as $key => $value) {
                $response[$key] = $value;
                if($value['data'] != ''){
                    $response[$key]['data'] = json_decode($value['data'], true);
                }
                $response[$key]['createdAt'] = $value['createdAt']->getTimestamp();
            }
        }
        return array_values($response);
    }
    
    public function addNotification($user_id, $msg, $params = array(), $redirect){
        $data = array();
        $data['msg'] = $msg;
        $data['params'] = $params;
        $data['redirect'] = $redirect;

        $promotion = $this->newEntity();
        $promotion->user_id = $user_id;
        $promotion->data = json_encode($data, true);
        $result = $this->save($promotion);
        if ($result) {
            return $result->id;
        }else {
            return 1;
        }
    }

    public function deleteNotification($id)
    {
        $notification = $this->get($id);
        if($this->delete($notification)){
            return 1;
        }else{
            return 0;
        }
    }

    public function getbadgeCount($user_id)
    {
        $count = 0;
        $query = $this->find()
        ->where(['user_id' => $user_id])
        ->select(['id'])
        ->all();
        $results = array();
        if($query){
            $results = $query->toArray();
            $count = count($results);
        }
        return $count;
    }
}
