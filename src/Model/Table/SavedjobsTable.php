<?php
namespace App\Model\Table;

use App\Model\Entity\Adminsetting;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;


/**
 * Users Model
 *
 */
class SavedjobsTable extends AppTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('savedjobs');
        $this->displayField('id');
        $this->primaryKey('id');
        
        $this->belongsTo('Users',[
            'foreignKey' => 'user_id'
        ]);
        $this->belongsTo('Userjobs',[
            'foreignKey' => 'job_id'
        ]);
        $this->addBehavior('CounterCache', [
            'Userjobs' => ['saved_cnt']
        ]);
    }
    
    public function addSavedJob($data=array()){
        $job = $this->newEntity($data);
    	
    	foreach ($data as $key=>$value){
    		$job->$key = $value;
    	}
    	if ($this->save($job)) {
    		return 1;
    	} else {
    		return 0;
    	}
    }
}
