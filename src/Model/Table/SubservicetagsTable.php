<?php
namespace App\Model\Table;

use App\Model\Entity\Subservicetag;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Users Model
 *
 */
class SubservicetagsTable extends AppTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('subservicetags');
        $this->displayField('id');
        $this->primaryKey('id');
        
        $this->belongsTo('Subservices',[
        		'foreignKey' => 'subservice_id'
        		]);
    }
	
    public function get_subservicetagwithid($subservice_id=null,$select=array(),$condition=array()) {
    	$services = $this->find('all')
    	->Where(["subservice_id"=>$subservice_id]);
    	$result = $services->all()->toArray();
    	 
    	foreach ($result as $key=>$value){
    		if(!empty($select)){
    			foreach ($select as $val)
    			{
    				$response[$key][$val] = $value[$val];
    			}
    		}else {
    			$response[$key]['id'] 					= $value['id'];
    			$response[$key]['subservicetag_name'] 	= $value['subservicetag_name'];
    			$response[$key]['is_active'] 			= $value['is_active'];
    		}
    	}
    	return $response;
    }
    
	public function savedata($data) {
        $services = $this->newEntity($data);
      
        if ($this->save($services)) {
            return $services->id;
        } else {
            return 0;
        }
    }

    public function updatedata($id, $data) {
    	$services = $this->get($id);
    	foreach ($data as $key=>$value){
    		$services->$key = $value;
    	}
    	$this->save($services);
    	return true;
    }
    
}
