<?php
namespace App\Model\Table;

use App\Model\Entity\Adminsetting;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;


/**
 * Users Model
 *
 */
class JobapplicantsTable extends AppTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('jobapplicants');
        $this->displayField('id');
        $this->primaryKey('id');
        
        $this->belongsTo('Users',[
            'foreignKey' => 'user_id'
        ]);
        $this->belongsTo('Userjobs',[
            'foreignKey' => 'job_id'
        ]);
    }
    
    public function addJobApplicant($data=array()){
        $applicant = $this->newEntity($data);
        
        foreach ($data as $key=>$value){
            $applicant->$key = $value;
        }
        if ($this->save($applicant)) {
            return 1;
        } else {
            return 0;
        }
    }

    public function updatedata($id, $data) 
    {
        $user = $this->get($id);
        foreach ($data as $key=>$value)
        {
            $user->$key = $value;
        }
        if($this->save($user))
        {
            return true;
        }else {
            return false;
        }
    }

    public function getRecordstoController($select=array(), $condition=array(), $contain=array())
    {
        $results = $this->find()
        ->where($condition)
        ->select($select)
        ->contain($contain)
        ->all();
        if($results){
            $results = $results->toArray();
        }
        return $results;
    }
}
