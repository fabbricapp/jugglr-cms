<?php
namespace App\Model\Table;

use App\Model\Entity\Adminsetting;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;


/**
 * Users Model
 *
 */
class UserratingsTable extends AppTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('userratings');
        $this->displayField('id');
        $this->primaryKey('id');
        
        $this->belongsTo('Users',[
            'foreignKey' => 'user_id'
        ]);
        $this->belongsTo('Subservices',[
            'foreignKey' => 'subservice_id'
        ]);
        $this->addBehavior('CounterCache', [
            'Users' => [
                'positive_rating'=> [
                    'conditions' => ['Userratings.rating' => 1]
                ],
                'neutral_rating'=> [
                    'conditions' => ['Userratings.rating' => 0]
                ]
            ]
        ]);
    }
    
    public function addUserRating($data=array()){
        $rating = $this->newEntity($data);
    	
    	foreach ($data as $key=>$value){
    		$rating->$key = $value;
    	}
    	if ($this->save($rating)) {
    		return 1;
    	} else {
    		return 0;
    	}
    }
}
