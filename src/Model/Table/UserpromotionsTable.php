<?php
namespace App\Model\Table;

use App\Model\Entity\Adminsetting;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;


/**
 * Users Model
 *
 */
class UserpromotionsTable extends AppTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('userpromotions');
        $this->displayField('id');
        $this->primaryKey('id');
        
        $this->belongsTo('Users',[
                'foreignKey' => 'user_id'
                ]);
        $this->belongsTo('Promotions',[
            'foreignKey' => 'promotion_id'
        ]);
        $this->addBehavior('CounterCache', [
            'Promotions' => ['used_cnt']
        ]);
    }
    
    public function getsettings($select=array()) {
    	$query = $this->find()
    	->select($select)
    	->where(['id' => 1]);
    	$results = $query->first()->toArray();
    	return $results;
    }
    
    public function addUserPromotion($data=array()){
        $promotion = $this->newEntity($data);
    	
    	foreach ($data as $key=>$value){
    		$promotion->$key = $value;
    	}
    	if ($this->save($promotion)) {
    		return 1;
    	} else {
    		return 0;
    	}
    }

    public function checkForPromotionUsed($user_id, $promotion_id)
    {
        if($this->exists(['user_id' => $user_id, 'promotion_id' => $promotion_id])){
            return 1;
        }else {
            return 0;
        }
    }
}
