<?php
namespace App\Model\Table;

use App\Model\Entity\User;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

/**
 * Users Model
 *
 */
class SubservicesTable extends AppTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('subservices');
        $this->displayField('id');
        $this->primaryKey('id');
        
        $this->hasMany('Subservicetags', [
                'foreignKey' => 'subservice_id',
                'dependent' => true,
                'cascadeCallbacks' => true
                ]);
        $this->hasMany('Usertasks', [
                'foreignKey' => 'need_serviceid'
                ]);
        $this->belongsTo('Services',[
                'foreignKey' => 'service_id'
                ]);
    }
    
    public function getall_subservices() {
        $services = $this->find('all');
        $result = $services->all()->toArray();
        foreach ($result as $key=>$value){
            $response[$value['service_id']][$key]['id']                 = $value['id'];
            $response[$value['service_id']][$key]['sub_service_name']   = $value['sub_service_name'];
        }
        return $response;
    }
    
    /*public function get_subservicewithid($service_id=null,$select=array(),$condition=array()) {
        $services = $this->find('all')
        ->Where(["service_id"=>$service_id]);
        $result = $services->all()->toArray();
        
        foreach ($result as $key=>$value){
            if(!empty($select)){
                foreach ($select as $val)
                {
                    $response[$key][$val] = $value[$val];
                }
            }else {
                $response[$key]['id']               = $value['id'];
                $response[$key]['subservice_name']  = $value['subservice_name'];
                $response[$key]['is_active']        = $value['is_active'];
            }
        }
        return $response;
    }*/

    public function get_subservicewithid($service_id=array(),$select=array(),$condition=array()) {
        $services = $this->find('all')
        ->select($select)
        ->Where(["service_id IN"=>$service_id]);
        $result = $services->all()->toArray();
        
        foreach ($result as $key=>$value){
            if(!empty($select)){
                $response[$value['service_id']][] = $value['id'];
            }else {
                $response[$key]['id']               = $value['id'];
                $response[$key]['subservice_name']  = $value['subservice_name'];
                $response[$key]['is_active']        = $value['is_active'];
            }
        }
        return $response;
    }
    
    public function searchbysubservicesandtags($text) {
        $inReview = $this->find()
        ->select(['subservice_name'])
        ->where(['subservice_name Like' => '%'.$text.'%']);
        
        $subservicetag = TableRegistry::get('Subservicetags');
        $unpublished = $subservicetag->find()
        ->select(['subservicetag_name'])
        ->where(['subservicetag_name Like' => '%'.$text.'%']);
        
        $unpublished->unionAll($inReview);
        
        $user = TableRegistry::get('Users');
        $published = $user->find()
        ->select(['firstname'])
        ->where(['firstname Like' => '%'.$text.'%']);
         
        $published->unionAll($unpublished);
        
        $user_arr = array();
        if($published){
            foreach ($published as $value){
                if(!in_array($value['firstname'],$user_arr)){
                    $user_arr[] = $value['firstname'];
                }
            }
        }
        return $user_arr;
    }
    
    public function get_tasksummary($query){
        $query = $query->toArray();
        $response = array();
        if($query)
        {
            foreach ($query as $key=>$value){
                $response[$value['id']]['subservice_name']  = $value['subservice_name'];
                $response[$value['id']]['service_name']     = $value['service']['service_name'];
                $response[$value['id']]['current_cnt']      = 0;
                $response[$value['id']]['complete_cnt']     = 0;
                $response[$value['id']]['cancelled_cnt']    = 0;
                $response[$value['id']]['fee']              = 0;
                $response[$value['id']]['tax']              = 0;
                $response[$value['id']]['amount']           = 0;
                $response[$value['id']]['volume']           = 0;
                $response[$value['id']]['invoiced']         = 0;
                if(!empty($value['usertasks']))
                {
                    foreach ($value['usertasks'] as $k=>$val)
                    {
                        if($val['status'] == 0)
                        {
                            $response[$value['id']]['current_cnt']++;
                        }elseif ($val['status'] == 1)
                        {
                            $response[$value['id']]['complete_cnt']++;
                        }elseif ($val['status'] == 2)
                        {
                            $response[$value['id']]['cancelled_cnt']++;
                        }elseif ($val['payment_status'] == 1)
                        {
                            $response[$value['id']]['invoiced']++;
                        }
                        $response[$value['id']]['fee'] += $val['fee'];
                        $response[$value['id']]['tax'] += $val['tax'];
                        $response[$value['id']]['amount'] += $val['amount'];
                        $response[$value['id']]['volume']++;
                    }
                }
            }
        }
        return $response;
    }
    
    public function savedata($data) {
        $services = $this->newEntity($data);
      
        if ($this->save($services)) {
            return $services->id;
        } else {
            return 0;
        }
    }

    public function updatedata($id, $data) {
        $services = $this->get($id);
        foreach ($data as $key=>$value){
            $services->$key = $value;
        }
        $this->save($services);
        
        /* logic of predective search */
        $subserviceData = $this->get($id);
        $serviceObj = TableRegistry::get('Services');
        $serviceData = $serviceObj->get($subserviceData->service_id);
        
        $subservicetagObj = TableRegistry::get('Subservicetags');
        $subservicetagData = $subservicetagObj->find()->where(['subservice_id' => $id])->all();
        $tableObj = TableRegistry::get('Predectivesearchs');
        if($subserviceData->is_active == 1){
            foreach ($subservicetagData as $key => $value) {
                $searchvalue = 'SUBSERVICETAG-'.$subserviceData['service_id']."-".$id."-".$value['id'];
                $exists = $tableObj->exists(['search_value' => $searchvalue]);
                if($exists == 0){
                    $datatosave = array();
                    $datatosave['search_index']   = $subserviceData['subservice_name']." - ".$value['subservicetag_name'];
                    $datatosave['search_value']   = $searchvalue;

                    $entity = $tableObj->newEntity($datatosave);
                    $tableObj->save($entity);
                }else {
                    $searchData = $tableObj->find()->where(['search_value' => $searchvalue])->first();
                    $searchData = $tableObj->get($searchData['id']);
                    $searchData->search_index = $subserviceData['subservice_name']." - ".$value['subservicetag_name'];
                    $tableObj->save($searchData);
                }
            }    
        }else {
            $searchvalue = 'SUBSERVICETAG-'.$subserviceData['service_id']."-".$id."-";
            $tableObj->deleteAll(['search_value LIKE' => '%'.$searchvalue.'%']);
        }
        
        return true;
    }
}
