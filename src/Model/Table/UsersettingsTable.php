<?php
namespace App\Model\Table;

use App\Model\Entity\Usersetting;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;


/**
 * Users Model
 *
 */
class UsersettingsTable extends AppTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('usersettings');
        $this->displayField('id');
        $this->primaryKey('id');
      	
        $this->belongsTo('Users', [
        	'foreignKey' => 'user_id'
       	]);
    }
	
    public function adduser_settings($user_id) {
    	$usersetting = $this->newEntity();
    	$usersetting->user_id 			= $user_id;
    	$usersetting->discover_mode 		= '1';
    	$usersetting->starting_age_range 	= 18;
    	$usersetting->ending_age_range 		= 56;
    	$usersetting->offer_message_mode 	= '1';
    	$usersetting->new_matches_mode 		= '1';
    	
    	if ($this->save($usersetting)) {
    		return 1;
    	}else {
    		return 0;
    	}
    }
    
    public function getuser_settings($user_id,$select){
    	$query = $this->find()
    	->select($select)
    	->where(['user_id' => $user_id])
    	->contain([]);
    	$results = $query->first();
    	if($results){
    		$results = $results->toArray();
    		$response['setting_id'] 						= $results['id'];
    		if(isset($results['discover_mode'])){
    			$response['discover_mode'] 			= ($results['discover_mode'] == "1") ? 1 : 0;
    		}
    		if(isset($results['starting_age_range'])){
    			$response['starting_age_range'] 	= $results['starting_age_range'];
    		}
    		if(isset($results['ending_age_range'])){
    			$response['ending_age_range'] 		= $results['ending_age_range'];
    		}
    		if(isset($results['offer_message_mode'])){
    			$response['offer_message_mode'] = ($results['offer_message_mode'] == "1") ? 1 : 0;
    		}
    		if(isset($results['new_matches_mode'])){
    			$response['new_matches_mode'] 	= ($results['new_matches_mode'] == "1") ? 1 : 0;
    		}
    	}else {
    		$response = '';
    	}
    	return $response;
    }
    
	public function updateuser_settings($requestdata=array(),$setting_id) {
		$usersetting = $this->get($setting_id);
        if(isset($requestdata['discover_mode'])){
            $usersetting->discover_mode = ($requestdata['discover_mode'] == 1) ? '1' : '0';
        }
	    if(isset($requestdata['starting_age_range'])){
	    	$usersetting->starting_age_range = $requestdata['starting_age_range'];
	    }
	    if(isset($requestdata['ending_age_range'])){
	    	$usersetting->ending_age_range = $requestdata['ending_age_range'];
	    }
	    if(isset($requestdata['offer_message_mode'])){
	    	$usersetting->offer_message_mode = ($requestdata['offer_message_mode'] == 1) ? '1' : '0';
	    }
	    if(isset($requestdata['new_matches_mode'])){
	    	$usersetting->new_matches_mode = ($requestdata['new_matches_mode'] == 1) ? '1' : '0';
	    }
        if(isset($requestdata['discover_mode'])){
            $users = TableRegistry::get('Users');
            $param['discover_mode'] = $requestdata['discover_mode'];
            $users->updatedata($usersetting['user_id'],$param);
        }
	    if($this->save($usersetting)){
	    	return 1;
	    }else {
	    	return 0;
	    }
    }

    public function updatedata($id, $data) {
    	$user = $this->get($id);
    	foreach ($data as $key=>$value){
    		$user->$key = $value;
    	}
    	$this->save($user);
    	return true;
    }

}
