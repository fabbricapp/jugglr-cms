<?php
namespace App\Model\Table;

use App\Model\Entity\Userconnection;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;


/**
 * Users Model
 *
 */
class UserblocklistsTable extends AppTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('userblocklists');
        $this->displayField('id');
        $this->primaryKey('id');
      	
        $this->belongsTo('Userwith', [
        	'className' => 'Users',
        	'foreignKey' => 'user_with'
        ]);
        $this->belongsTo('Users', [
        	'foreignKey' => 'user_id'
        ]);
    }

	public function adduser_block($requestdata = null) 
	{
		$query = $this->find()
		->where(['Userblocklists.user_id' => $requestdata['user_with'],'Userblocklists.user_with' => $requestdata['user_id']])
		->orWhere(['Userblocklists.user_id' => $requestdata['user_id'],'Userblocklists.user_with' => $requestdata['user_with']]);
		$result = $query->first();
		
		if(empty($result)) 
		{
			$datatosave = array();
			$datatosave['user_id'] 			= $requestdata['user_id'];
			$datatosave['user_with'] 		= $requestdata['user_with'];
			$datatosave['blocked_by'] 		= $requestdata['user_id'];
			$connection = $this->newEntity($datatosave);
			$this->save($connection);
			$response = 1;
		}else {
			$connection = $this->get($result['id']);
			$this->delete($connection);
			$response = 1;
		}
		return $response;
    }

    public function isBlocked($curruser_id, $serviceprovider_id)
    {
    	$query = $this->find()
		->where(['Userblocklists.user_id' => $curruser_id,'Userblocklists.user_with' => $serviceprovider_id])
		->orWhere(['Userblocklists.user_id' => $serviceprovider_id,'Userblocklists.user_with' => $curruser_id]);
		$result = $query->first();
		$response = array();
    	if($result){
    		$response['is_block'] = 1;
    		$response['blocked_by'] = $result['blocked_by'];
    	}else {
    		$response['is_block'] = 0;
    		$response['blocked_by'] = 0;
    	}
    	return $response;
    }
    
    public function unblock($curruser_id, $serviceprovider_id)
    {
    	$query = $this->find()
    	->select(['id'])
    	->where(['Userblocklists.user_id' => $curruser_id,'Userblocklists.user_with' => $serviceprovider_id])
    	->orWhere(['Userblocklists.user_id' => $serviceprovider_id,'Userblocklists.user_with' => $curruser_id]);
    	$result = $query->first();
    	
	 	$connection = $this->get($value['id']);
	 	if($this->delete($connection))
	 	{
	 		$result = 1;
	 	}else {
	 		$result = 0;
	 	}
    }

    public function syncUsermatches($data){
        foreach ($data as $key => $value) {
            $entity = $this->newEntity($value);
            $this->save($entity);
        }
        return 1;
    }

}
