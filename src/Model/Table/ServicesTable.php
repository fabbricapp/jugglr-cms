<?php
namespace App\Model\Table;

use App\Model\Entity\Services;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Users Model
 *
 */
class ServicesTable extends AppTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('services');
        $this->displayField('id');
        $this->primaryKey('id');
        
        $this->hasMany('Subservices', [
                'foreignKey' => 'service_id',
                'dependent' => true,
                'cascadeCallbacks' => true
                ]);
    }
    
    public function get_services($select=array(),$condition=array()) {
        $services = $this->find('all')
            ->select($select)
            ->where($condition);
        $result = $services->all()->toArray();
        if(!empty($result) > 0){
            foreach ($result as $key=>$value){
                if(!empty($select)){
                    foreach ($select as $val)
                    {
                        $response[$key][$val] = $value[$val];
                    }
                }else {
                    $response[$key]['id']           = $value['id'];
                    $response[$key]['service_name'] = $value['service_name'];
                    $response[$key]['is_active']    = $value['is_active'];
                }
            }
        }
        return $response;
    }
    
    public function get_subservices() {
        $services = $this->find('all')
        ->contain(['Subservices']);
        $result = $services->all()->toArray();
        if(!empty($result) > 0){
            foreach ($result as $key=>$value){
                $response[$value['id']]['service_name'] = $value['service_name'];
                if(!empty($value['subservices']) > 0){
                    foreach ($value['subservices'] as $subkey=>$subvalue){
                        $response[$value['id']]['subservice'][$subvalue['id']]['subservice_name'] = $subvalue['subservice_name'];
                    }
                }
            }
        }
        return $response;
    }
    
    public function getall_services($contain_arr=array()) {
        $data = $this->find('all')
        ->where(['type' => 1, 'is_active' => '1'])
        ->contain($contain_arr);
        $result = $data->toArray();
        if(!empty($result) > 0){
            foreach ($result as $key=>$value){
                $response[$key]['service_id']           = $value['id'];
                $response[$key]['service_name'] = $value['service_name'];
                if(!empty($value['subservices'])){
                    foreach ($value['subservices'] as $subkey=>$subvalue){
                        $response[$key]['subservice'][$subkey]['subservice_id'] = $subvalue['id'];
                        $response[$key]['subservice'][$subkey]['subservice_name'] = $subvalue['subservice_name'];
                        if(!empty($subvalue['subservicetags'])){
                            foreach ($subvalue['subservicetags'] as $subk=>$subval){
                                $response[$key]['subservice'][$subkey]['subservicetags'][$subk]['subservicetag_id'] = $subval['id'];
                                $response[$key]['subservice'][$subkey]['subservicetags'][$subk]['subservicetag_name'] = $subval['subservicetag_name'];
                            }
                        }else {
                            $response[$key]['subservice'][$subkey]['subservicetags'] = array();
                        }
                    }
                }else {
                    $response[$key]['subservice'] = array();
                }
            }
        }
        return $response;
    }
    
    public function savedata($data) {
        $services = $this->newEntity($data);
      
        if ($this->save($services)) {
        return 1;
        } else {
        return 0;
        }
    }

    public function updatedata($id, $data) {
        $services = $this->get($id);
        foreach ($data as $key=>$value){
            $services->$key = $value;
        }
        $this->save($services);
        return true;
    }

    public function getBusinessServicess()
    {
        $data = $this->find('all')
        ->where(['type' => 2,'is_active' => '1'])
        ->contain(['Subservices']);
        $response = array();
        if(!empty($data)){
            $result = $data->toArray();
            $i = 0;
            foreach ($result as $key=>$value){
                foreach ($value['subservices'] as $k => $val) {
                    $response[$i]['service_id'] = $value['id'];
                    $response[$i]['subservice_id'] = $val['id'];
                    $response[$i]['subservice_name'] = $val['subservice_name'];
                    $i++;
                }
            }
        }
        return $response;   
    }
}
