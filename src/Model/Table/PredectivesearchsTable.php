<?php
namespace App\Model\Table;

use App\Model\Entity\Adminsetting;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;


/**
 * Users Model
 *
 */
class PredectivesearchsTable extends AppTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('predectivesearchs');
        $this->displayField('id');
        $this->primaryKey('id');
    }
    
    public function saveData($data=array()){
        $job = $this->newEntity($data);
    	
    	foreach ($data as $key=>$value){
    		$job->$key = $value;
    	}
    	if ($this->save($job)) {
    		return 1;
    	} else {
    		return 0;
    	}
    }

    public function updateJob($id, $data) 
    {
        $user = $this->get($id);
        foreach ($data as $key=>$value)
        {
            $user->$key = $value;
        }
        if($this->save($user))
        {
            return 1;
        }else {
            return 0;
        }
    }
}
