<?php
namespace App\Model\Table;

use App\Model\Entity\Userconnection;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;


/**
 * Users Model
 *
 */
class FacebookconnectionsTable extends AppTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('facebookconnections');
        $this->displayField('id');
        $this->primaryKey('id');
      	
        $this->belongsTo('Users', [
        	'foreignKey' => 'user_id'
        ]);
    }

    public function addFbConnections($user_id,$data) {
        if(!empty($data))
        {
            $datatosave = array();
            foreach ($data as $key=>$value){
                if($value){
                    $datatosave[$key]['user_id']	 		= $user_id;
                    $datatosave[$key]['connection_fb_id'] 		= $value['id'];
                    $datatosave[$key]['connection_fb_name'] 	= $value['name'];
                    $datatosave[$key]['createdAt'] 			= date("Y-m-d h:i:s");
                    $datatosave[$key]['modifiedAt']			= date("Y-m-d h:i:s");
                }
            }
            if($datatosave){
                $entities = $this->newEntities($datatosave);
                foreach ($entities as $entity) {
                        $this->save($entity);
                }
            }
        }
    	return 1;
    }
    
    public function getMutualFriends($data) {
        $response = array();
        if($data){
            foreach ($data as $key => $value) {
                $response[$key]['id']       = $value['connection_fb_id'];
                $response[$key]['name']     = $value['connection_fb_name'];
            }
        }
        return $response;
    }

    public function savedata($data) 
    {
        $user = $this->newEntity($data, ['associated' => ['Userdetails']]);
      
        if ($this->save($user)) 
        {
        	return 1;
        } else {
        	return 0;
        }
    }

    public function updatedata($id, $data) 
    {
    	$user = $this->get($id);
    	foreach ($data as $key=>$value)
    	{
    		$user->$key = $value;
    	}
    	$this->save($user);
    	return true;
    }

}
