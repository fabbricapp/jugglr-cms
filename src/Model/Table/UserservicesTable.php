<?php
namespace App\Model\Table;

use App\Model\Entity\Userservice;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;


/**
 * Users Model
 *
 */
class UserservicesTable extends AppTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('userservices');
        $this->displayField('id');
        $this->primaryKey('id');
        
        $this->belongsTo('Users',[
                'foreignKey' => 'user_id'
                ]);
        $this->belongsTo('Services',[
            'foreignKey' => 'service_id'
        ]);
        $this->belongsTo('Subservices',[
            'foreignKey' => 'subservice_id'
        ]);
        $this->belongsTo('Subservicetags',[
            'foreignKey' => 'subservicetag_id'
        ]);
    }
    
    public function getuser_serviceforsettings($user_id,$param) {
        $query = $this->find()
        ->select(['service_id','subservice_id','subservicetag_id','type'])
        ->where(['user_id' => $user_id])
        ->contain([]);
        $results = $query->all();
        
        $response = array();
        if($results)
        {
            $results = $results->toArray();
            $match_arr['need']['service_ids'] = $match_arr['need']['subservice_ids'] = $match_arr['need']['subservicetag_ids'] = array();
            $match_arr['offer']['service_ids'] = $match_arr['offer']['subservice_ids'] = $match_arr['offer']['subservicetag_ids'] = array();
            if($results){
                foreach ($results as $key=>$value){
                    if($value['type'] == 0){
                        $match_arr['need']['service_ids'][]         = $value['service_id'];
                        $match_arr['need']['subservice_ids'][]      = $value['subservice_id'];
                        $match_arr['need']['subservicetag_ids'][]   = $value['subservicetag_id'];
                    }elseif ($value['type'] == 1){
                        $match_arr['offer']['service_ids'][]        = $value['service_id'];
                        $match_arr['offer']['subservice_ids'][]     = $value['subservice_id'];
                        $match_arr['offer']['subservicetag_ids'][]  = $value['subservicetag_id'];
                    }
                }
                $match_arr['need']['service_ids']           = array_unique($match_arr['need']['service_ids']); 
                $match_arr['need']['subservice_ids']        = array_unique($match_arr['need']['subservice_ids']);
                $match_arr['need']['subservicetag_ids']     = array_unique($match_arr['need']['subservicetag_ids']);
                $match_arr['offer']['service_ids']          = array_unique($match_arr['offer']['service_ids']);
                $match_arr['offer']['subservice_ids']       = array_unique($match_arr['offer']['subservice_ids']);
                $match_arr['offer']['subservicetag_ids']    = array_unique($match_arr['offer']['subservicetag_ids']);
            }
            foreach ($match_arr as $superkey=>$supervalue)
            {
                foreach ($param as $key=>$value)
                {
                    $value['is_selected'] = (in_array($value['service_id'],$supervalue['service_ids'])) ? 1 : 0;
                    if(!empty($value['subservice']))
                    {
                        foreach ($value['subservice'] as $k=>$val)
                        {
                            $value['subservice'][$k]['is_selected'] = (in_array($val['subservice_id'],$supervalue['subservice_ids'])) ? 1 : 0;
                            if(!empty($val['subservicetags']))
                            {
                                foreach ($val['subservicetags'] as $subk=>$subval)
                                {
                                    $value['subservice'][$k]['subservicetags'][$subk]['is_selected'] = (in_array($subval['subservicetag_id'],$supervalue['subservicetag_ids'])) ? 1 : 0;
                                }
                            }
                        }
                    }
                    $response[$superkey][] = $value; 
                }
            }
        }
        return $response;
    }
    
    public function getuserservices($user_id){
        $query = $this->find()
        ->where(['user_id' => $user_id]);
        $results = $query->all()->toArray();
        $response['service_needed'] = $response['service_offered'] = array();
        if($results)
        {
            foreach ($results as $key=>$value){
                if($value['type'] == 0){
                    if(!in_array($value['service_id'],$response['service_needed']))
                    {
                        $response['service_needed'][]   = $value['service_id'];
                    }
                }else {
                    if(!in_array($value['service_id'],$response['service_offered']))
                    {
                        $response['service_offered'][] = $value['service_id'];
                    }
                }
            }
        }
        return $response;
    }

    /* For map view webservice */
    public function getmultipleuserservices($user_id=array()){
        $query = $this->find()
        ->select(['service_id','type','user_id'])
        ->where(['user_id IN' => $user_id]);
        $results = $query->all()->toArray();
        $response = array();
        if(!empty($results))
        {
            foreach ($results as $key=>$value){
                if(!isset($response[$value['user_id']]['service_needed'])){
                    $response[$value['user_id']]['service_needed'] = array();
                    $response[$value['user_id']]['service_offered'] = array();
                }
                if($value['type'] == 0){
                    if(!in_array($value['service_id'],$response[$value['user_id']]['service_needed']))
                    {
                        $response[$value['user_id']]['service_needed'][]   = $value['service_id'];
                    }
                }else {
                    if(!in_array($value['service_id'],$response[$value['user_id']]['service_offered']))
                    {
                        $response[$value['user_id']]['service_offered'][] = $value['service_id'];
                    }
                }
            }
        }
        return $response;
    }
    
    public function adduser_services($user_id,$type,$userservices = null) {
        $datatosave = $service_arr = $userserviceData = array();
        $i = $total_sort_cnt = 0;
        foreach ($userservices as $key=>$value){
            foreach ($value as $k=>$val){
                    $service_arr[$k] = explode(',', $val);
            } 
        }
        if($type == '0'){
            $userserviceData['service_needed'] = implode(',', array_keys($service_arr));
        }else{
            $userserviceData['service_offered'] = implode(',', array_keys($service_arr));
        }
        $this->Users = TableRegistry::get('Users');
        $this->Users->updatedata($user_id,$userserviceData);
        if(count($service_arr) >= 5){
            $sort_cnt = 1;
        }elseif (count($service_arr) == 4 || count($service_arr) == 3){
            $sort_cnt = 2;
        }elseif (count($service_arr) == 2){
            $sort_cnt = 3;
        }elseif (count($service_arr) == 1){
            $sort_cnt = 5;
        }
        foreach ($service_arr as $key=>$value){
            if(!empty($value)){
                $count = 0;
                foreach ($value as $subkey=>$subvalue){
                    $datatosave[$i]['user_id']          = $user_id;
                    $datatosave[$i]['service_id']       = $key;
                    $datatosave[$i]['subservice_id']    = $subvalue;
                    $datatosave[$i]['type']             = $type;
                    if($count < $sort_cnt && $total_sort_cnt < 5){
                        $datatosave[$i]['sorting']      = '1';
                        $count++;
                        $total_sort_cnt++;
                    }
                    $i++;
                }
            }else {
                $datatosave[$i]['user_id']          = $user_id;
                $datatosave[$i]['service_id']       = $value->service_id;
                $datatosave[$i]['type']             = $type;
                $i++;
            }
        }
        $entities = $this->newEntities($datatosave);
        foreach ($entities as $entity) {
            $this->save($entity);
        }
        return 1;
    }
    
    public function getuserrating($param){
        $response = array();
        $rating_percent = $total_rating = 0; 
        if($param['positive'] > 0 || $param['neutral'] > 0){
            $rating_percent += (100 * $param['positive'])/($param['positive'] + $param['neutral']);
            $total_rating   += $param['positive'];
        }
        $response['total_rating'] = $total_rating;
        if($rating_percent != 0){
            $response['average_rating'] = round($rating_percent/($param['positive'] + $param['neutral']));
        }else {
            $response['average_rating'] = 0;
        }
        if($rating_percent > 0){
            $response['star_rating'] = round(((5 * ($rating_percent))/100), 1);
        }else {
            $response['star_rating'] = 0;
        }
        return $response;
    }
    
    public function increamentrating($param)
    {
        $query = $this->find()
        ->where(['user_id' => $param['user_id'],'type'=>$param['type'],'service_id'=>$param['service_id'],'subservice_id'=>$param['subservice_id'],'subservicetag_id IS'=>NULL]);
        $results = $query->first();
        $response['service_needed'] = $response['service_offered'] = array();
        if($results)
        {
            $results = $results->toArray();
            if($param['rating'] == 1){
                $datatosave['positive_rating']  = $results['positive_rating'] + 1;
            }elseif ($param['rating'] == 2){
                $datatosave['neutral_rating']   = $results['neutral_rating'] + 1;
            }
            $temp = $this->updatedata($results['id'],$datatosave);
        }else {
            $datatosave['user_id']          = $param['user_id'];
            $datatosave['service_id']       = $param['service_id'];
            $datatosave['subservice_id']        = $param['subservice_id'];
            $datatosave['type']             = '1';
            if($param['rating'] == 1){
                $datatosave['positive_rating']   = 1;
            }elseif ($param['rating'] == 2){
                $datatosave['neutral_rating']    = 1;
            }
            $entity = $this->newEntity($datatosave);
            $this->save($entity, ['checkExisting' => true]);
        }
        return $response;
    }
    
    public function getservicesforrating($user_id)
    {
        $query = $this->find()
        ->select(['id','positive_rating','neutral_rating','negative_rating','subservice_id'])
        ->where(['user_id' => $user_id, 'OR' => ['neutral_rating !=' => 0, 'positive_rating !=' => 0]])
        ->contain([
            'Subservices' => function ($q){
                return $q
                ->select(['Subservices.subservice_name']);
            },
            'Users' => function ($q){
                return $q
                ->select(['Users.firstname','Users.lastname','Users.profile_image_thumb']);
            }
        ]);
        $results = $query->all()->toArray();
        $response = array();
        if($results)
        {
            $rating_percent = $total_rating = 0; 
            $i = 0;
            $response['subservices'] = array();
            foreach ($results as $key=>$value){
                $response['name'] = $value['user']['firstname'].", ".substr($value['user']['lastname'],0,1).".";
                $response['profile_image_thumb'] = ($value['user']['profile_image_thumb']!='') ? $value['user']['profile_image_thumb'] : BASE_URL."/img/default_image.png";
                if($value['positive_rating'] > 0 || $value['neutral_rating'] > 0){
                    $rating_percent         += (100 * $value['positive_rating'])/($value['positive_rating'] + $value['neutral_rating']);
                    $total_rating           += $value['positive_rating'];
                    $rating_per_subservice  = (100 * $value['positive_rating'])/($value['positive_rating'] + $value['neutral_rating']);
                    if($rating_per_subservice > 0)
                    {
                        $response['subservices'][$key]['subservice_name']   = $value['subservice']['subservice_name'];
                        $response['subservices'][$key]['subservice_rating'] = round($rating_per_subservice);
                    }
                    $i++;
                }
            }
            $response['subservices'] = array_values($response['subservices']);
            $response['total_rating'] = $total_rating;
            if($rating_percent != 0){
                $response['average_rating'] = round($rating_percent/$i);
            }else {
                $response['average_rating'] = 0;
            }
            if($rating_percent > 0){
                $response['star_rating'] = round(((5 * ($rating_percent/$i))/100), 1);
            }else {
                $response['star_rating'] = 0;
            }
        }else {
            $query = $this->find()
            ->select(['id'])
            ->where(['user_id' => $user_id])
            ->contain([
                    'Users' => function ($q){
                        return $q
                        ->select(['Users.firstname','Users.lastname','Users.profile_image_thumb']);
                    }
            ])
            ->limit('1');
            $results = $query->all()->toArray();
            if($results)
            {
                $results = $results[0];
                $response['name'] = $results['user']['firstname'].", ".substr($results['user']['lastname'],0,1).".";
                $response['profile_image_thumb'] = ($results['user']['profile_image_thumb']!='') ? $results['user']['profile_image_thumb'] : BASE_URL."/img/default_image.png";
                $response['subservices'] = array();
                $response['total_rating'] = 0;
                $response['average_rating'] = 0;
                $response['star_rating'] = 0;
            }
        }
        return $response;
    }
    
    public function savedata($data) {
        $userservice = $this->newEntity();
        foreach ($data as $key => $value) {
            $userservice->$key = $value; 
        }
        if ($this->save($userservice)) {
            return 1;
        } else {
            return 0;
        }
    }

    public function updatedata($id, $data) {
        $user = $this->get($id);
        foreach ($data as $key=>$value){
            $user->$key = $value;
        }
        $this->save($user);
        return true;
    }
    
    public function updatemultiplesorting($data,$type,$user_id) {
        $data = json_decode($data, True);
        $result = 0;
        $query = $this->find()
        ->select(['id','sorting'])
        ->where(['user_id'=>$user_id,'type'=>$type,'subservicetag_id IS'=>NULL]);
        $results = $query->all();
        if($results)
        {
            $rq_arr = $db_arr = array();
            foreach ($results as $key=>$value)
            {
                $db_arr[$value['id']] = ($value['sorting'] == '0') ? 0 : 1;
            }
            foreach ($data as $key=>$value)
            {
                $rq_arr[$value['userservice_id']] = ($value['sorting'] == '0') ? 0 : 1;
            }
            $response = array_diff_assoc($rq_arr,$db_arr);
            if(!empty($response))
            {
                foreach ($response as $key=>$value)
                {
                    $datatosave[$key] = $value;
                }
                if(isset($datatosave))
                {
                    foreach ($datatosave as $key=>$value)
                    {
                        $user = $this->get($key);
                        $user->sorting = $value;
                        if($this->save($user)){
                            $result = 1;
                        }
                    }
                }
            }
        }
        return $result;
    }
    
    public function updateusersubservicetags($param, $type, $user_id)
    {
        $result = 1;
        if($param['checked_subservicetag_ids'] != '' && $param['service_id'] != '' && $param['subservice_id'] != '')
        {
            $subservicetag_ids = explode(',', $param['checked_subservicetag_ids']);
            $datatosave = array();
            $i = 0;
            foreach ($subservicetag_ids as $key=>$value){
                $exists = $this->exists(['user_id' => $user_id, 'type' => $type, 'service_id' => $param['service_id'],'subservice_id' => $param['subservice_id'], 'subservicetag_id' => $value]);
                if($value && $exists == 0){     
                    $datatosave[$i]['user_id']          = $user_id;
                    $datatosave[$i]['service_id']       = $param['service_id'];
                    $datatosave[$i]['subservice_id']    = $param['subservice_id'];
                    $datatosave[$i]['subservicetag_id'] = $value;
                    $datatosave[$i]['type']             = $type;
                    $i++;
                }
            }
            $entities = $this->newEntities($datatosave);
            foreach ($entities as $entity) {
                if($this->save($entity, ['checkExisting' => true])){
                    $result = 1;
                }else {
                    $result = 0;
                }
            }
        }
        if ($param['unchecked_subservicetag_ids'] != '' && $param['service_id'] != '' && $param['subservice_id'] != '')
        {
            $subservicetag_ids = explode(',', $param['unchecked_subservicetag_ids']);
            $this->deleteAll(['user_id' => $user_id, 'type' => $type, 'service_id' => $param['service_id'],'subservice_id' => $param['subservice_id'], 'subservicetag_id IN' => $subservicetag_ids]);
        }
        return $result;
    }
    
    public function updateusersubservices($param, $type, $user_id)
    {
        $result = 1;
        if($param['checked_subservice_ids'] != '' && $param['service_id'] != '')
        {
            $subservice_ids = explode(',', $param['checked_subservice_ids']);
            $datatosave = array();
            $i = 0;
            foreach ($subservice_ids as $key=>$value){
                $exists = $this->exists(['user_id' => $user_id, 'type' => $type, 'service_id' => $param['service_id'],'subservice_id' => $value]);
                if($value && $exists == 0){
                    $datatosave[$i]['user_id']          = $user_id;
                    $datatosave[$i]['service_id']       = $param['service_id'];
                    $datatosave[$i]['subservice_id']    = $value;
                    $datatosave[$i]['type']             = $type;
                    $i++;
                }
            }
            $entities = $this->newEntities($datatosave);
            foreach ($entities as $entity) {
                if($this->save($entity, ['checkExisting' => true])){
                    $result = 1;
                }else {
                    $result = 0;
                }
            }
        }
        if ($param['unchecked_subservice_ids'] != '' && $param['service_id'] != '')
        {
            $subservice_ids = explode(',', $param['unchecked_subservice_ids']);
            if(!empty($subservice_ids)){
                $this->deleteAll(['user_id' => $user_id, 'type' => $type, 'service_id' => $param['service_id'], 'subservice_id IN' => $subservice_ids]);
            }
        }
        return $result;
    }
    
    /*public function updateuserservices($param, $user_id)
    {
        $i = 0;
    $result = 1;
        $datatosave = array();
        if($param['checked_offer_service_id'] != '')
        {
            $service_ids = explode(',',$param['checked_offer_service_id']);
            foreach ($service_ids as $key=>$value){
                if($value){
                    $subservice = TableRegistry::get('Subservices');
                    $data = $subservice->get_subservicewithid($value);
                    foreach ($data as $k=>$val)
                    {
                        $exists = $this->exists(['user_id' => $user_id, 'type' => '1', 'service_id' => $value,'subservice_id' => $val['id']]);
                        if($exists == 0){
                            $datatosave[$i]['user_id']          = $user_id;
                            $datatosave[$i]['service_id']       = $value;
                            $datatosave[$i]['subservice_id']    = $val['id'];
                            $datatosave[$i]['type']             = '1';
                            $i++;
                        }
                    }
                }
            }
        }
        if($param['checked_need_service_id'] != '')
        {
            $service_ids = explode(',',$param['checked_need_service_id']);
            foreach ($service_ids as $key=>$value){
                if($value){
                    $subservice = TableRegistry::get('Subservices');
                    $data = $subservice->get_subservicewithid($value);
                    foreach ($data as $k=>$val)
                    {
                        $exists = $this->exists(['user_id' => $user_id, 'type' => '0', 'service_id' => $value,'subservice_id' => $val['id']]);
                        if($exists == 0){
                            $datatosave[$i]['user_id']          = $user_id;
                            $datatosave[$i]['service_id']       = $value;
                            $datatosave[$i]['subservice_id']    = $val['id'];
                            $datatosave[$i]['type']             = '0';
                            $i++;
                        }
                    }
                }
            }
        }
        if(isset($datatosave))
        {
            $entities = $this->newEntities($datatosave);
            foreach ($entities as $entity) {
                if($this->save($entity, ['checkExisting' => true])){
                    $result = 1;
                }else {
                $result = 0;
            }
            }
        }
        if($param['unchecked_offer_service_id'] != '')
        {
            $service_ids = explode(',',$param['unchecked_offer_service_id']);
            $this->deleteAll(['user_id' => $user_id, 'type' => '1', 'service_id IN' => $service_ids]);
        }
        if($param['unchecked_need_service_id'] != '')
        {
            $service_ids = explode(',',$param['unchecked_need_service_id']);
            $this->deleteAll(['user_id' => $user_id, 'type' => '0', 'service_id IN' => $service_ids]);
        }
        return $result;
    }*/

    public function updateuserservices($param, $user_id)
    {
        $i = 0;
        $result = 1;
        $datatosave = array();
        if($param['checked_offer_service_id'] != '')
        {
            $service_ids = explode(',', trim($param['checked_offer_service_id'],'"'));
            $subservice = TableRegistry::get('Subservices');
            $select = ['id','service_id'];
            $servicedata = $subservice->get_subservicewithid($service_ids,$select);
            foreach ($service_ids as $key=>$value){
                foreach ($servicedata[$value] as $k=>$val)
                {
                    $exists = $this->exists(['user_id' => $user_id, 'type' => '1', 'service_id' => $value,'subservice_id' => $val]);
                    if($exists == 0){
                        $datatosave[$i]['user_id']          = $user_id;
                        $datatosave[$i]['service_id']       = $value;
                        $datatosave[$i]['subservice_id']    = $val;
                        $datatosave[$i]['type']             = '1';
                        $i++;
                    }
                }
            }
        }
        if($param['checked_need_service_id'] != '')
        {
            $service_ids = explode(',', trim($param['checked_need_service_id'],'"'));
            $subservice = TableRegistry::get('Subservices');
            $select = ['id','service_id'];
            $servicedata = $subservice->get_subservicewithid($service_ids,$select);
            foreach ($service_ids as $key=>$value){
                foreach ($servicedata[$value] as $k=>$val)
                {
                    $exists = $this->exists(['user_id' => $user_id, 'type' => '0', 'service_id' => $value,'subservice_id' => $val]);
                    if($exists == 0){
                        $datatosave[$i]['user_id']          = $user_id;
                        $datatosave[$i]['service_id']       = $value;
                        $datatosave[$i]['subservice_id']    = $val;
                        $datatosave[$i]['type']             = '0';
                        $i++;
                    }
                }
            }
        }
        if(isset($datatosave))
        {
            $entities = $this->newEntities($datatosave);
            foreach ($entities as $entity) {
                if($this->save($entity, ['checkExisting' => true])){
                    $result = 1;
                }else {
                    $result = 0;
                }
            }
        }
        if($param['unchecked_offer_service_id'] != '')
        {
            $service_ids = explode(',',$param['unchecked_offer_service_id']);
            $this->deleteAll(['user_id' => $user_id, 'type' => '1', 'service_id IN' => $service_ids]);
        }
        if($param['unchecked_need_service_id'] != '')
        {
            $service_ids = explode(',',$param['unchecked_need_service_id']);
            $this->deleteAll(['user_id' => $user_id, 'type' => '0', 'service_id IN' => $service_ids]);
        }
        return $result;
    }

    public function dataToController($select, $condition, $contain)
    {
        $query = $this->find()
                ->where($condition)
                ->select($select)
                ->contain($contain)
                ->all();
        if($query){
            $query = $query->toArray();
        }
        return $query;
    }

    public function addBusinessSubservices($user_id, $data)
    {
        if($user_id && !empty($data)){
            foreach ($data as $key => $value) {
                $explode = explode('-', $value);
                $userservice = $this->newEntity();
                $userservice->user_id = $user_id;
                $userservice->service_id = $explode[0];
                $userservice->subservice_id = $explode[1];
                $userservice->type = '1';
                $this->save($userservice);
            }
        }
        return 1;
    }
}
