<?php
namespace App\Model\Table;

use App\Model\Entity\Adminsetting;
use Cake\ORM\Query;
use Cake\Core\Configure;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;


/**
 * Users Model
 *
 */
class PromotionsTable extends AppTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('promotions');
        $this->displayField('id');
        $this->primaryKey('id');
        $this->hasMany('Userpromotions', [
            'foreignKey' => 'promotion_id'
        ]);
      
    }
    
    public function getAllPromotions($select, $conditions, $contain) {
        $query = $this->find()
        ->select($select)
        ->where($conditions)
        ->all();
        if($query){
            $results = $query->toArray();
        }
        return $results;
    }
    
    /*
     * Function to apply promotion on amount and return discounted amount
    */
    public function returnDiscountedAmount($amount, $promotion_id, $user_id, $service_date = null)
    {
        $data = $this->find()->where(['id' => $promotion_id])->first();
        $start_date = date('Y-m-d', strtotime($data['startdate']));
        $end_date = date('Y-m-d', strtotime($data['enddate']));
        $service_date = date('Y-m-d', strtotime($service_date));
        if($service_date >= $start_date && $service_date <= $end_date){
            $userPromo = '';
            if($data['usage_type'] == 1){
                $Userpromotions = TableRegistry::get('Userpromotions');
                $userPromo = $Userpromotions->find()->where(['promotion_id' => $promotion_id, 'user_id' => $user_id])->first();
            }
            if($userPromo){
                $response['discountamount'] = 0;
                $response['message'] = Configure::read('PROMOTION_ALREADY_USED_BY_USER');
            }else {
                if(($data['usage_cnt'] > 0 && $data['used_cnt'] < $data['usage_cnt']) || $data['usage_cnt'] == NULL)
                {
                    $type = $data['type'];
                    switch ($type) {
                        case 1:
                            //percentage discount
                            $response['discountamount'] = (($amount * $data['percentage']) / 100);
                            $response['message'] = Configure::read('PROMOTION_APPLIED');
                            break;
                        case 2:
                            //flat discount
                            $response['discountamount'] = $data['amount'];
                            $response['message'] = Configure::read('PROMOTION_APPLIED');
                            break;
                        case 3:
                            // conditional discount
                            if($amount > $data['offeramount']){
                                if($data['conditionaltype'] == 1){
                                    $response['discountamount'] = (($amount * $data['percentage']) / 100);
                                    $response['message'] = Configure::read('PROMOTION_APPLIED');
                                }elseif ($data['conditionaltype'] == 2) {
                                    $response['discountamount'] = $data['amount'];
                                    $response['message'] = Configure::read('PROMOTION_APPLIED');
                                }else{
                                    $response['discountamount'] = 0;
                                    $response['message'] = Configure::read('PROMOTION_NOT_APPLIED');
                                }
                            }else{
                                $response['discountamount'] = 0;
                                $response['message'] = Configure::read('AMOUNT_NOT_ENOUGH_TO_APPLY_THIS_PROMOTION');
                            }
                            break;
                        default:
                            $response['discountamount'] = 0;
                            $response['message'] = Configure::read('PROMOTION_NOT_APPLIED');
                            break;
                    }
                    if($response['discountamount'] > $amount){
                        $response['discountamount'] = $amount;
                    }
                }else {
                    $response['discountamount'] = 0;
                    $response['message'] = Configure::read('PROMOTION_NOT_APPLICABLE');
                }
            }
        }else {
            $response['discountamount'] = 0;
            $response['message'] = Configure::read('SERVICE_DATE_NOT_BETWEEN_START_AND_END_DATES');
        }
        return $response;
    }

    public function addPromotion($data=array()){
        $promotion = $this->newEntity($data);
        
        foreach ($data as $key=>$value){
            $promotion->$key = $value;
        }
        $response = $this->save($promotion);
        if ($response) {
            return $response->id;
        } else {
            return 0;
        }
    }

    public function updatedata($id, $data) 
    {
        $user = $this->get($id);
        foreach ($data as $key=>$value)
        {
            $user->$key = $value;
        }
        if($this->save($user))
        {
            return 1;
        }else {
            return 0;
        }
    }

    /*public function getUserPromotion($user_id)
    {
        $date = date('Y-m-d');
        $query = $this->find()
        ->where(['is_active' => 1, 'startdate <=' => $date, 'enddate >=' => $date])
        ->all();
        $data = array();
        if($query){
            $results = $query->toArray();
            foreach ($results as $key => $value) {
                $valid_offer = 1;
                if(!empty($value['usage_cnt']) && $value['usage_cnt'] > 0){
                    if($value['usage_cnt'] >= $value['used_cnt']){
                        $valid_offer = 0;
                    }
                }
                if($valid_offer == 1){
                    if($value['accessibility'] == 1){
                        //public
                        if($value['usage_type'] == 1){
                            //single time
                            $Userpromotions = TableRegistry::get('Userpromotions');
                            $promotion_cnt = $Userpromotions->checkForPromotionUsed($user_id, $value['id']);
                            if($promotion_cnt > 0){
                                $valid_offer = 0;
                            }
                        }
                    }elseif ($value['accessibility'] == 2) {
                        //private
                        $user_ids = explode(',', $value['users']);
                        if(!in_array($user_id, $user_ids)){
                            $valid_offer = 0;
                        }
                        if($value['usage_type'] == 1){
                            //single time
                            $Userpromotions = TableRegistry::get('Userpromotions');
                            $promotion_cnt = $Userpromotions->checkForPromotionUsed($user_id, $value['id']);
                            if($promotion_cnt > 0){
                                $valid_offer = 0;
                            }
                        }
                    }
                }
                if($valid_offer == 1){
                    $data[$key]['id'] = $value['id'];
                    $data[$key]['image'] = ($value['image']) ? $value['image'] : BASE_URL."/img/promotionimages/default.png";
                }
            }
        }
        return $data;
    }*/

    public function getUserPromotion($type,$user_id=null)
    {
        $date = date('Y-m-d');
        $condition['is_active'] = 1;
        $condition['startdate <='] = $date;
        $condition['enddate >='] = $date;
        $condition['promotion_type'] = 1;
        if($type == 'image'){
            $condition['OR'][]['promotion_type'] = 2;
            $condition['OR'][]['is_active'] = 1;
        }
        $query = $this->find()
        ->where($condition)
        ->all();
        $data = array();
        if($query){
            $results = $query->toArray();
            foreach ($results as $key => $value) {
                $valid_offer = 1;
                if(!empty($value['usage_cnt']) && $value['usage_cnt'] > 0){
                    if($value['usage_cnt'] <= $value['used_cnt']){
                        $valid_offer = 0;
                    }
                }
                if($value['usage_type'] == 1 && $user_id != ''){
                    //single time
                    $Userpromotions = TableRegistry::get('Userpromotions');
                    $promotion_cnt = $Userpromotions->checkForPromotionUsed($user_id, $value['id']);
                    if($promotion_cnt == 1){
                        $valid_offer = 0;
                    }
                }
                if($valid_offer == 1){
                    if($type == 'image'){
                        $data[$key]['id'] = $value['id'];
                        $data[$key]['image'] = ($value['image']) ? $value['image'] : BASE_URL."/img/promotionimages/default.png";
                        $data[$key]['url'] = $value['url'];
                    }else {
                        $data[$key]['id'] = $value['id'];
                        $data[$key]['title'] = $value['title'];
                        $data[$key]['description'] = $value['description'];
                    }
                }
            }
        }
        return array_values($data);
    }

    /*public function getUserPromotion_old()
    {
        $date = date('Y-m-d');
        $query = $this->find()
        ->where(['is_active' => 1, 'startdate <=' => $date, 'enddate >=' => $date, 'promotion_type' => 1])
        ->all();
        $data = array();
        $i = 0;
        if($query){
            $results = $query->toArray();
            foreach ($results as $key => $value) {
                $valid_offer = 1;
                if(!empty($value['usage_cnt']) && $value['usage_cnt'] > 0){
                    if($value['usage_cnt'] <= $value['used_cnt']){
                        $valid_offer = 0;
                    }
                }
                if($valid_offer == 1){
                    $data[$i]['id'] = $value['id'];
                    $data[$i]['image'] = ($value['image']) ? $value['image'] : BASE_URL."/img/promotionimages/default.png";
                    $data[$i]['url'] = ($value['url']) ? $value['url'] : '';
                }
                $i++;
            }
        }
        $query = $this->find()
        ->where(['is_active' => 1, 'promotion_type' => 2])
        ->all();
        if($query){
            $results = $query->toArray();
            foreach ($results as $key => $value) {
                $data[$i]['id'] = $value['id'];
                $data[$i]['image'] = ($value['image']) ? $value['image'] : BASE_URL."/img/promotionimages/default.png";
                $data[$i]['url'] = ($value['url']) ? $value['url'] : '';
                $i++;
            }
        }
        return $data;
    }*/

}
