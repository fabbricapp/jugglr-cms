<?php
namespace App\Model\Table;

use App\Model\Entity\Userconnection;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;


/**
 * Users Model
 *
 */
class UsermatchesTable extends AppTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('usermatches');
        $this->displayField('id');
        $this->primaryKey('id');
      	
        $this->belongsTo('Userwith', [
        	'className' => 'Users',
        	'foreignKey' => 'user_with'
        ]);
        $this->belongsTo('Users', [
        	'foreignKey' => 'user_id'
        ]);
        $this->addBehavior('CounterCache', [
            'Userwith' => ['match_cnt'],
            'Users' => ['match_cnt']
        ]);
    }

	public function adduser_matches($requestdata = null) 
	{
		$query = $this->find()
		->where(['Usermatches.user_id' => $requestdata['user_with'],'Usermatches.user_with' => $requestdata['user_id']])
		->orWhere(['Usermatches.user_id' => $requestdata['user_id'],'Usermatches.user_with' => $requestdata['user_with']]);
		$result = $query->first();
		
		if(empty($result)) 
		{
			$datatosave = array();
			$datatosave['user_id'] 			= $requestdata['user_id'];
			$datatosave['user_with'] 		= $requestdata['user_with'];
			$connection = $this->newEntity($datatosave);
			$this->save($connection);
			$response = 'PERFECT_MATCH';
		}
		return $response;
    }
    
    public function getMatchedUserIds($where, $user_id)
    {
    	$result = $this->find()
    			->where($where)
    			->all();
    	$user_arr = array();
    	if($result){
    		$result = $result->toArray();
    		foreach ($result as $key => $value) {
    			if(!in_array($value['user_id'], $user_arr) && $value['user_id'] != $user_id){
    				$user_arr[] = $value['user_id'];
    			}
    			if(!in_array($value['user_with'], $user_arr) && $value['user_with'] != $user_id){
    				$user_arr[] = $value['user_with'];
    			}
    		}
    	}
    	return $user_arr;
    }

    public function isMatched($curruser_id, $serviceprovider_id)
    {
    	$exists = $this->exists(['user_id' => $curruser_id, 'user_with' => $serviceprovider_id]);
    	if($exists == 1){
    		return 1;
    	}else {
    		$exists = $this->exists(['user_id' => $serviceprovider_id, 'user_with' => $curruser_id]);
    		if($exists == 1){
	    		return 1;
	    	}else {
	    		return 0;
	    	}
    	}
    }

    public function getuser_connections($results,$user_id,$lat=null,$long=null) 
    {
    	$data = array();
    	if($results)
    	{
    		$user_id = (int)$user_id;
    		$results = $results->toArray();
    		
	    	foreach ($results as $key=>$value)
	    	{
	    		$userdata = '';
	    		if(isset($value['userwith'])){
	    			$userdata = ($value['userwith']['id'] != $user_id) ? $value['userwith'] : "";
	    		}
	    		if(isset($value['user']) && $userdata == ''){
	    			$userdata = ($value['user']['id'] != $user_id) ? $value['user'] : "";
	    		}
	    		if($userdata)
	    		{
	    			/*
	    			 * get connection shortlist count
	    			 */
	    			$usertable 		= TableRegistry::get('Users');
	    			$select 		= ['Users.id'];
	    			$contain 		= ['Userconnections_with','Userconnections'];
	    			$where 			= ['id' => $userdata['id']];
                                $query = $usertable->find('all')
                                        ->where($where)
                                        ->select($select)
                                        ->contain($contain)
                                        ->all()->toArray();
                                
                                $userconnection             = $this->getconnectionsdetails($query[0]['userconnections_with'],$query[0]['userconnections'],$query[0]['id'],$user_id);
                                //$userconnection           = $usertable->getuserconnections($where,$contain,$select,$userdata['id']);
	    			
		    		$data[$key]['user_id'] 			= $userdata['id'];
				$data[$key]['facebook_id'] 		= $userdata['facebook_id'];
		    		$data[$key]['profile_image']            = ($userdata['profile_image_thumb']) ? $userdata['profile_image_thumb'] : BASE_URL."/img/default_thumb.jpg";
		    		$data[$key]['name'] 			= $userdata['firstname'].", ".substr($userdata['lastname'],0,1).".";
		    		if($lat != '' && $long != ''){
					$data[$key]['distance']		= $usertable->getdistance($lat,$long,$userdata['latitude'],$userdata['longitude'],"K");
		    		}
		    		if(($lat == '' && $long == '') || $data[$key]['distance'] == 0) {
		    			$data[$key]['distance']		= 2;
		    		}
				$data[$key]['shortlist_cnt']            = $userconnection['shortlist_cnt'];
		    		$rating = $data[$key]['service_need_ids'] = $data[$key]['subservice_needed'] = $data[$key]['service_offer_ids'] = $data[$key]['subservice_offered'] = array();
			    		
		    		if(!empty($userdata['userservices']))
		    		{
		    			foreach ($userdata['userservices'] as $k=>$val)
		    			{
		    				if($val['subservice']['subservice_name'] != '')
		    				{
			    				if($val['type'] == 0)
			    				{
			    					if(!in_array($val['service_id'],$data[$key]['service_need_ids']))
			    					{
			    						$data[$key]['service_need_ids'][] = $val['service_id'];
			    					}
			    					$data[$key]['subservice_needed'][$k]['sorting'] 		= ($val['sorting'] == '1') ? 1 : 0;
			    					$data[$key]['subservice_needed'][$k]['subservice_name'] = $val['subservice']['subservice_name'];
			    				}else 
			    				{
			    					if(!in_array($val['service_id'],$data[$key]['service_offer_ids']))
			    					{
			    						$data[$key]['service_offer_ids'][] = $val['service_id'];
			    					}
			    					$data[$key]['subservice_offered'][$k]['sorting'] 		= ($val['sorting'] == '1') ? 1 : 0;
			    					$data[$key]['subservice_offered'][$k]['subservice_name']= $val['subservice']['subservice_name'];
			    				}
			    				$rating[$val['subservice_id']]['positive'] = $val['positive_rating'];
			    				$rating[$val['subservice_id']]['neutral'] = $val['neutral_rating'];
		    				}
		    				$data[$key]['subservice_offered'] = array_values($data[$key]['subservice_offered']);
		    				$data[$key]['subservice_needed'] = array_values($data[$key]['subservice_needed']);
		    			}
		    			array_multisort($data[$key]['subservice_offered'], SORT_DESC);
		    			array_multisort($data[$key]['subservice_needed'], SORT_DESC);
		    		}
		    		if(!empty($rating))
		    		{
		    			$userservice = TableRegistry::get('Userservices');
		    			$result = $userservice->getuserrating($rating);
		    			$data[$key]['total_rating'] = $result['total_rating'];
		    			$data[$key]['star_rating'] 	= $result['star_rating'];
		    		}else 
		    		{
		    			$data[$key]['total_rating'] = 0;
		    			$data[$key]['star_rating'] 	= 0;
		    		}
	    		}
	    	}
    	}
    	return $data;
    }
    
    public function delete_connections($param)
    {
    	$query = $this->find()
    	->select(['id'])
    	->where(['connection_type'=>'0','Userconnections.user_id' => $param['user_id'],'Userconnections.user_with IN' => $param['user_with']])
    	->orWhere(['connection_type'=>'0','Userconnections.user_id IN' => $param['user_with'],'Userconnections.user_with' => $param['user_id']]);
    	$result = $query->find('all');
    	
    	foreach ($result as $key=>$value)
    	{
    	 	$connection = $this->get($value['id']);
    	 	$connection->connection_type = '1';
    	 	if($this->save($connection))
    	 	{
    	 		$result = true;
    	 	}else {
    	 		$result = false;
    	 	}
    	}
    	return $result;
    }
    
    public function getconnectionsdetails($connection=array(),$connectionwith=array(),$connection_id=null,$user_id=null)
    {
    	$response = array();
    	$response['shortlist_cnt'] 	= 0;
    	$response['matches_cnt'] 	= 0;
    	$response['is_shortlist'] 	= 0;
    	$response['is_matched'] 	= 0;
    	$response['is_blocked'] 	= 0;
    	$response['blocked_by'] 	= 0;
		if(!empty($connection))
		{
	    	foreach ($connection as $k=>$val)
	    	{
	    		if($val['connection_type'] == '0' && ($val['user_id'] == $connection_id || $val['user_with'] == $connection_id)) {
	    			$response['matches_cnt']++;
	    		}
	    		if($val['connection_type'] == '0' && ($val['user_id'] == $user_id || $val['user_with'] == $user_id)){
	    			$response['is_matched'] = 1;
	    		}
	    		if($val['connection_type'] == '1' && $val['user_with'] == $connection_id){
	    			$response['shortlist_cnt']++;
	    		}
	    		if($val['connection_type'] == '1' && $val['user_id'] == $user_id && $val['user_with'] == $connection_id){
	    			$response['is_shortlist'] = 1;
	    		}
	    		if($val['is_block'] == '1' && ($val['user_id'] == $user_id || $val['user_with'] == $user_id)){
	    			$response['is_blocked'] = 1;
	    			$response['blocked_by'] = $val['blocked_by'];
	    		}
	    	}
		}
		if(!empty($connectionwith))
		{
			foreach ($connectionwith as $k=>$val)
	    	{
	    		if($val['connection_type'] == '0' && ($val['user_id'] == $connection_id || $val['user_with'] == $connection_id)) {
	    			$response['matches_cnt']++;
	    		}
	    		if($val['connection_type'] == '0' && ($val['user_id'] == $user_id || $val['user_with'] == $user_id) && $response['is_matched'] == 0){
	    			$response['is_matched'] = 1;
	    		}
	    		if($val['connection_type'] == '1' && $val['user_with'] == $connection_id){
	    			$response['shortlist_cnt']++;
	    		}
	    		if($val['connection_type'] == '1' && $val['user_id'] == $user_id && $val['user_with'] == $connection_id  && $response['is_shortlist'] == 0){
	    			$response['is_shortlist'] = 1;
	    		}
	    		if($val['is_block'] == '1' && ($val['user_id'] == $user_id || $val['user_with'] == $user_id) && $response['is_blocked'] == 0){
	    			$response['is_blocked'] = 1;
	    			$response['blocked_by'] = $val['blocked_by'];
	    		}
	    	}
		}
    	return $response;
    }
    
    public function savedata($data) 
    {
        $user = $this->newEntity($data);
      
        if ($this->save($user)) 
        {
        	return 1;
        } else {
        	return 0;
        }
    }

    public function updatedata($id, $data) 
    {
    	$user = $this->get($id);
    	foreach ($data as $key=>$value)
    	{
    		$user->$key = $value;
    	}
    	$this->save($user);
    	return true;
    }

    public function syncUsermatches($data){
        foreach ($data as $key => $value) {
            $entity = $this->newEntity($value);
            $this->save($entity);
        }
        return 1;
    }

}
