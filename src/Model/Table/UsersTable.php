<?php
namespace App\Model\Table;

use App\Model\Entity\User;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Utility\Hash;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Core\Configure;


/**
 * Users Model
 *
 */
class UsersTable extends AppTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('users');
        $this->displayField('id');
        $this->primaryKey('id');
      
        $this->addBehavior('Timestamp');
        $this->addBehavior('Api');
        
        $this->hasMany('Userconnections', [
                'foreignKey' => 'user_id',
                'dependent' => true,
                'cascadeCallbacks' => true
                ]);
        $this->hasMany('Userconnections_with', [
                'className' => 'Userconnections',
                'foreignKey' => 'user_with',
                'dependent' => true,
                'cascadeCallbacks' => true
                ]);
        $this->hasMany('Usershortlists', [
                'foreignKey' => 'user_id',
                'dependent' => true,
                'cascadeCallbacks' => true
                ]);
        $this->hasMany('Usershortlists_with', [
                'className' => 'Usershortlists',
                'foreignKey' => 'user_with',
                'dependent' => true,
                'cascadeCallbacks' => true
                ]);
        $this->hasMany('Usermatches', [
                'foreignKey' => 'user_id',
                'dependent' => true,
                'cascadeCallbacks' => true
                ]);
        $this->hasMany('Usermatches_with', [
                'className' => 'Usermatches',
                'foreignKey' => 'user_with',
                'dependent' => true,
                'cascadeCallbacks' => true
                ]);
        $this->hasMany('Userservices', [
                'foreignKey' => 'user_id',
                'dependent' => true,
                'cascadeCallbacks' => true
                ]);
        $this->hasMany('Userinterests', [
                'foreignKey' => 'user_id',
                'dependent' => true,
                'cascadeCallbacks' => true
                ]);
        $this->hasOne('Usersettings');
        $this->hasMany('Userimages', [
                'foreignKey' => 'user_id',
                'dependent' => true,
                'cascadeCallbacks' => true
                ]);
        $this->hasMany('Usertasks', [
                'foreignKey' => 'user_id',
                'dependent' => true,
                'cascadeCallbacks' => true
                ]);
        $this->hasMany('Usertasks_with', [
                'className'=>'Usertasks',
                'foreignKey' => 'user_with',
                'dependent' => true,
                'cascadeCallbacks' => true
                ]);
        $this->hasMany('Facebookconnections', [
                'className'=>'Facebookconnections',
                'foreignKey' => 'user_id',
                'dependent' => true,
                'cascadeCallbacks' => true
                ]);
        $this->hasMany('Usernotifications', [
                'foreignKey' => 'user_id',
                'dependent' => true,
                'cascadeCallbacks' => true
                ]);
        $this->hasMany('Userratings', [
                'foreignKey' => 'user_id',
                'dependent' => true,
                'cascadeCallbacks' => true
                ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    /*public function validationDefault(Validator $validator) 
    {
        $validator = new Validator();
        $validator
            ->requirePresence('facebook_id')
            ->add('facebook_id', [
                'unique' => [
                'rule' => ['validateUnique', ['scope' => 'facebook_id']],
                'provider' => 'table',
                'message' => 'You already have an username!'
                ]
            ])
            ->notEmpty('facebook_id', 'Give your facebook_id!');
        return $validator;
    }*/

    public function signup($data = null) 
    {
        $user = $this->newEntity($data);
        
        if ($this->save($user)) {  
            return $user->id;
        } else {
            return 0;
        }
    }
    
    public function __getGUID() 
    {
        mt_srand((double) microtime() * 10000); //optional for php 4.2.0 and up.
        $charid = strtoupper(md5(uniqid(rand(), true)));
        $uuid = substr($charid, 0, 8)
                . substr($charid, 8, 4)
                . substr($charid, 12, 4)
                . substr($charid, 16, 4)
                . substr($charid, 20, 12)
                . time();
        return strtolower($uuid);
    }
    
    public function getuser_dashboard($select=array(),$contain=array(),$condition=array()) 
    {
        $query = $this->find()
        ->where($condition)
        ->select($select)
        ->contain($contain);
        $results = $query->first();
        
        if($results)
        {
            $results = $results->toArray();
            foreach ($select as $value)
            {
                if(isset($results[$value]))
                {
                    $response[$value] = $results[$value];
                }else 
                {
                    $response[$value] = "";
                }
            }
            /** logic for user interest **/
            $response['userinterests'] = $response['userservices'] = '';
            if(isset($results['userinterests']))
            {
                foreach ($results['userinterests'] as $key=>$value)
                {
                    $response['userinterests'][] = $value['interest'];
                }
            }
            /** logic for user services **/
            if(isset($results['userservices']) && $results['userservices'] != '')
            {
                $response['userservices']['offer'] = $response['userservices']['need'] = array();
                foreach ($results['userservices'] as $key1=>$value1)
                {
                    if($value1 != '')
                    {
                        if($value1['type'] == 1)
                        {
                            $response['userservices']['offer'][$key1]['subservice_name']    = $value1['subservice']['subservice_name'];
                            $response['userservices']['offer'][$key1]['postive_rating']     = $value1['positive_rating'];
                            $response['userservices']['offer'][$key1]['neutral_rating']     = $value1['neutral_rating'];
                        }else 
                        {
                            $response['userservices']['need'][$key1]['subservice_name']     = $value1['subservice']['subservice_name'];
                            $response['userservices']['need'][$key1]['postive_rating']      = $value1['positive_rating'];
                            $response['userservices']['need'][$key1]['neutral_rating']      = $value1['neutral_rating'];
                        }
                    }
                }
            }
            /** logic for user connection **/
            $response['userconnections']['Matches'] = $results['match_cnt'];
            $response['userconnections']['Shortlisted'] = $results['shortlist_cnt'];
            /*if(isset($results['userconnections']) && $results['userconnections'] != '')
            {
                foreach ($results['userconnections'] as $key1=>$value1)
                {
                    if($value1['connection_type'] == 0)
                    {
                        $response['userconnections']['Matches']++;
                    }
                }
            }
            if(isset($results['userconnections_with']) && $results['userconnections_with'] != '')
            {
                foreach ($results['userconnections_with'] as $key1=>$value1)
                {
                    if($value1['connection_type'] == 0)
                    {
                        $response['userconnections']['Matches']++;
                    }elseif($value1['connection_type'] == 1 && $value1['user_with'] == $response['id'])
                    {
                        $response['userconnections']['Shortlisted']++;
                    }
                }
            }*/
            /** logic for user rating **/
            $response['average_rating'] = (100 * $results['positive_rating'])/($results['positive_rating'] + $results['neutral_rating']);
            /** logic for user task **/
            $response['task_cnt'] = $response['complete_task'] = $response['current_task'] = $response['cancel_task'] = $response['paid_transaction'] = $response['paid_amount'] = 0;
            if(isset($results['usertasks']) && $results['usertasks'] != '')
            {
                foreach ($results['usertasks'] as $key1=>$value1)
                {
                    $response['task_cnt']++;
                    if($value1['status'] == 0)
                    {
                        $response['current_task']++;
                    }elseif($value1['status'] == 1) 
                    {
                        $response['complete_task']++;
                    }else
                    {
                        $response['cancel_task']++;
                    }
                    if($value1['payment_status'] == 1)
                    {
                        $response['paid_transaction']++;
                        $response['paid_amount'] += $value1['amount'];
                    }
                }
            }
        }else 
        {
            $response = 'NO_USER_FOUND';
        }
        return $response;
    }
    
    public function getuser_profile($select=array(),$contain=array(),$condition=array())
    {
        $query = $this->find()
        ->where($condition)
        ->select($select)
        ->contain($contain);
        $results = $query->first();
        if($results)
        {
            $results = $results->toArray();
            $response['id']                 = $results['id'];
            $response['name']               = $results['firstname'].", ".substr($results['lastname'],0,1).".";
            $response['profile_image_thumb']= $results['profile_image_thumb'];
            $response['about_me']           = $results['about_me'];
            /** logic for user images **/
            $response['coverimages'] = array();
            if(isset($results['userimages']))
            {
                foreach ($results['userimages'] as $key=>$value)
                {
                    if($value['image_url'] != '')
                    {
                        $response['coverimages'][$key]['coverimage_key']    = $value['coverimage_key'];
                        $response['coverimages'][$key]['image_url']         = BASE_URL."/img/coverimages/".$value['image_url'];
                    }
                }
                $response['coverimages'] = array_values($response['coverimages']);
            }
            /** logic for user interest **/
            $response['userinterests'] = $response['subservice_offered'] = $response['subservice_needed'] = array();
            if(isset($results['userinterests']))
            {
                foreach ($results['userinterests'] as $key=>$value)
                {
                    $response['userinterests'][$key]['id'] = $value['id'];
                            $response['userinterests'][$key]['interest'] = $value['interest'];
                            $response['userinterests'][$key]['display'] = ($value['display'] == '1') ? 1 : 0;
                }
            }
            
            /** logic for user services **/
            $rating = $response['subservice_offered'] = $response['subservice_needed'] = array();
            if(isset($results['userservices']) && $results['userservices'] != '')
            {
                foreach ($results['userservices'] as $key1=>$value1)
                {
                    if(!empty($value1['subservice']))
                    {
                        if($value1['type'] == 1)
                        {
                            $response['subservice_offered'][$key1]['userservice_id']    = $value1['id'];
                            $response['subservice_offered'][$key1]['subservice_name']   = $value1['subservice']['subservice_name'];
                            $response['subservice_offered'][$key1]['sorting']           = ($value1['sorting'] == '0') ? 0 : 1;
                        }else
                        {
                            $response['subservice_needed'][$key1]['userservice_id']     = $value1['id'];
                            $response['subservice_needed'][$key1]['subservice_name']    = $value1['subservice']['subservice_name'];
                            $response['subservice_needed'][$key1]['sorting']            = ($value1['sorting'] == '0') ? 0 : 1;
                        }
                    if(($value1['positive_rating'] || $value1['neutral_rating']) && $value1['type'] == '1')
                    {
                            $rating[$value1['subservice_id']]['positive']   = $value1['positive_rating'];
                            $rating[$value1['subservice_id']]['neutral']    = $value1['neutral_rating'];
                    }
                    }
                }
                $response['subservice_offered'] = array_values($response['subservice_offered']);
                $response['subservice_needed']  = array_values($response['subservice_needed']);
                $response['total_rating']       = $response['star_rating'] = 0;
                if(!empty($rating))
                {
                    $userservice = TableRegistry::get('Userservices');
                    $data = $userservice->getuserrating($rating);
                    $response['total_rating']   = $data['total_rating'];
                    $response['star_rating']    = $data['star_rating'];
                }
            }else
            {
                $response['total_rating']   = 0;
                $response['star_rating']    = 0;
            }
            /** logic for user connection **/
            if(isset($results['match_cnt'])){
                $response['matches_cnt'] = $results['match_cnt'];
            }
            if(isset($results['shortlist_cnt'])){
                $response['shortlist_cnt'] = $results['shortlist_cnt'];
            }
            /*if(isset($results['userconnections']) && $results['userconnections'] != '')
            {
                foreach ($results['userconnections'] as $key1=>$value1)
                {
                    if($value1['connection_type'] == 0)
                    {
                        $response['matches_cnt']++;
                    }
                }
            }
            if(isset($results['userconnections_with']) && $results['userconnections_with'] != '')
            {
                foreach ($results['userconnections_with'] as $key1=>$value1)
                {
                    if($value1['connection_type'] == 0)
                    {
                        $response['matches_cnt']++;
                    }elseif($value1['connection_type'] == 1 && $value1['user_with'] == $response['id'])
                    {
                        $response['shortlist_cnt']++;
                    }
                }
            }*/
        }else
        {
            $response = 'NO_USER_FOUND';
        }
        return $response;
    }
    
    public function getuser_summary()
    {
        $query = $this->find()
        ->where(['id' => $user_id])
        ->select(['Users.id','Users.firstname','Users.lastname','Users.profile_image','Users.about_me','Users.latitude','Users.longitude'])
        ->contain(['Userservices.Subservices','Userconnections','Userinterests']);
        $results = $query->first()->toArray();
         
        if($results)
        {
            $response['user_id'] = $results['id'];
            $response['name'] = $results['firstname'].", ".substr($results['lastname'],0,1).".";
            $response['profile_image'] = ($results['profile_image'] == '') ? 'img/user2-160x160.jpg' : $results['profile_image'];
            $response['about_me'] = $results['about_me'];
            $response['userinterests'] = $response['userservices'] = '';
            if($results['userinterests'] != '')
            {
                foreach ($results['userinterests'] as $key=>$value)
                {
                    $response['userinterests'][] = $value['interest'];
                }
            }else 
            {
                $response['userinterests'] = '';
            }
            if($results['userservices'] != '')
            {
                foreach ($results['userservices'] as $key1=>$value1)
                {
                    if($value1['type'] == 1)
                    {
                        $response['userservices']['offer'][] = $value1['subservice']['sub_service_name'];
                    }else 
                    {
                        $response['userservices']['need'][] = $value1['subservice']['sub_service_name'];
                    }
                }
            }else 
            {
                $response['userservices'] = '';
            }
            $response['userconnections']['matched'] = $response['userconnections']['shortlist'] = 0;
            if($results['userconnections'] != '')
            {
                foreach ($results['userconnections'] as $key1=>$value1)
                {
                    if($value1['connection_type'] == 0)
                    {
                        $response['userconnections']['Matches']++;
                    }elseif($value1['connection_type'] == 1) 
                    {
                        $response['userconnections']['Shortlisted']++;
                    }
                }
            }
        }else 
        {
            $response = 'NO_USER_FOUND';
        }
        return $response;
    }

    public function getlocationwithradius($latitude,$longitude,$user_id,$starting_age_range,$ending_age_range,$search_user_ids=array())
    {  
        $where['Users.is_active'] = '1';
        $where['Users.id !='] = $user_id;
        $where['Users.discover_mode'] = '1';
        if($starting_age_range > 0 && $ending_age_range > 0 && empty($search_user_ids)){
            $where['(YEAR(NOW()) - YEAR(`dob`)) >='] = $starting_age_range;
            if($ending_age_range < 56){
              $where['(YEAR(NOW()) - YEAR(`dob`)) <='] = $ending_age_range;
            }
            //$where['( 3959 * acos( cos( radians('.$latitude.') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('.$longitude.') ) + sin( radians('.$latitude.') ) * sin( radians( latitude ) ) ) ) <='] = $radius;
        }
        if(!empty($search_user_ids)){
            $where['Users.id IN'] = $search_user_ids;
        }
        $query = $this->find('all')
        ->where($where)
        ->select(['Users.id','Users.suburb','Users.latitude','Users.longitude'])
        ->contain([
            'Userservices'=> function ($q) {
                    return $q
                    ->select(['Userservices.service_id','Userservices.type','Userservices.user_id']);
                }
            ])
        ->all();
        $mapdata = $response = array();
        if($query)
        {
            $query = $query->toArray();
            $i = 0;
            foreach ($query as $article) 
            {
                $mapdata[$article['latitude']."x".$article['longitude']]['latitude'] = $article['latitude'];
                $mapdata[$article['latitude']."x".$article['longitude']]['longitude'] = $article['longitude'];
                $mapdata[$article['latitude']."x".$article['longitude']]['suburb'] = $article['suburb'];
                $mapdata[$article['latitude']."x".$article['longitude']]['users'][$i]['user_id'] = $article['id'];
                $mapdata[$article['latitude']."x".$article['longitude']]['users'][$i]['service_needed'] = array();
                $mapdata[$article['latitude']."x".$article['longitude']]['users'][$i]['service_offered'] = array();
                if(!empty($article['userservices']))
                {
                    $service_ids = Hash::extract($article['userservices'], '{n}[type=0].service_id');
                    $mapdata[$article['latitude']."x".$article['longitude']]['users'][$i]['service_needed'] = $service_ids;

                    $service_ids = Hash::extract($article['userservices'], '{n}[type=1].service_id');
                    $mapdata[$article['latitude']."x".$article['longitude']]['users'][$i]['service_offered'] = $service_ids;
                }else 
                {
                    $mapdata[$article['latitude']."x".$article['longitude']]['users'][$i]['service_needed'] = array();
                    $mapdata[$article['latitude']."x".$article['longitude']]['users'][$i]['service_offered'] = array();
                }
                $mapdata[$article['latitude']."x".$article['longitude']]['users'] = array_values($mapdata[$article['latitude']."x".$article['longitude']]['users']);
                $i++;
            }
            $response = array_values($mapdata);
        }
        return $response;
    }
    
    public function getuserwithfbid($where=array(),$contain=array(),$select=array())
    {
        $query = $this->find()
        ->select($select)
        ->where($where)
        ->contain($contain);
        $result = $query->first();
        if($result)
        {
            $result = $result->toArray(); 
            $rating = $result['interest'] = $result['coverimages'] = array();
            if(!empty($result['userimages']))
            {
                foreach ($result['userimages'] as $key=>$value)
                {
                    $result['coverimages'][$key]['coverimage_key']  = $value['coverimage_key'];
                    $result['coverimages'][$key]['image_url']       = BASE_URL."/img/coverimages/".$value['image_url'];
                }
                unset($result['userimages']);
            }
            $result['verified'] = ($result['verified'] == "0") ? 0 : 1;
            /*if(!empty($result['userservices']))
            {
                foreach ($result['userservices'] as $key=>$value)
                {
                    $result['service_needed'] = array_values(array_unique(Hash::extract($result['userservices'], '{n}[type=0].service_id')));
                    $result['service_offered'] = array_values(array_unique(Hash::extract($result['userservices'], '{n}[type=1].service_id')));
                    if($value['type'] == 1)
                    {
                        $rating[$value['subservice_id']]['positive'] = $value['positive_rating'];
                        $rating[$value['subservice_id']]['neutral'] = $value['neutral_rating'];
                    }
                }
            }*/
            $result['service_needed'] = ($result['service_needed'] != '') ? array_values(explode(',', $result['service_needed'])) : array();
            $result['service_offered'] = ($result['service_offered'] != '') ? array_values(explode(',', $result['service_offered'])) : array();
            $result['total_rating'] = $result['star_rating'] = 0;
            if(!empty($rating))
            {
                $userservice = TableRegistry::get('Userservices');
                $data = $userservice->getuserrating($rating);
                $result['total_rating'] = $data['total_rating'];
                $result['star_rating']  = $data['star_rating'];
            }
            if(!empty($result['userinterests']))
            {
                $result['interest'] = Hash::extract($result['userinterests'], '{n}.interest');
            }
            unset($result['userinterests']);
            $result['usertask'] = $result['matches'] = $result['shortlist'] = 0;
            if(!empty($result['usertasks_with']))
            {
                foreach ($result['usertasks_with'] as $key=>$value)
                {
                    $result['usertask']++;
                }
            }
            unset($result['usertasks_with']);
            $result['matches'] = $result['match_cnt'];
            $result['shortlist'] = $result['shortlist_cnt'];
            unset($result['match_cnt'],$result['shortlist_cnt'],$result['userservices']);
            if(!empty($result['usersetting']))
            {
                $result['starting_age_range']   = $result['usersetting']['starting_age_range'];
                $result['ending_age_range']     = $result['usersetting']['ending_age_range'];
                $result['setting_id']           = $result['usersetting']['id'];
                $result['stripe_bank_flag']     = ($result['usersetting']['stripe_bank_flag'] == 0) ? 0 : 1; 
            }
            unset($result['usersetting']);
            $adminsetting = TableRegistry::get('adminsettings');
            $select = ['map_radius','max_amount','fee_percent','gst_tax','android_zoom_level','ios_zoom_level'];
            $data = $adminsetting->getsettings($select);
            if(!empty($data))
            {
                $result['map_radius']   = $data['map_radius'];
                $result['max_amount']   = $data['max_amount'];
                $result['fee_percent']  = $data['fee_percent'];
                $result['gst_tax']      = $data['gst_tax'];
                $result['android_zoom_level']   = $data['android_zoom_level'];
                $result['ios_zoom_level']       = $data['ios_zoom_level'];
            }
            return $result;
        }else 
        {
            return false;
        }
    }
    
    public function getuserconnections($where=array(),$contain=array(),$select=array(),$user_id)
    {
        $query = $this->find()
        ->select($select)
        ->where($where)
        ->contain($contain);
        $result = $query->first();
        if($result)
        {
            $result = $result->toArray();
            $rating = $result['coverimages'] = array();
            if(isset($result['profile_image_thumb'])){
                $response['profile_image_thumb'] = $result['profile_image_thumb'];
            }
            if(isset($result['userimages']))
            {
                foreach ($result['userimages'] as $key=>$value)
                {
                    if($value['image_url'] != '')
                    {
                        $result['coverimages'][$key]['coverimage_key']    = $value['coverimage_key'];
                        $result['coverimages'][$key]['image_url']       = BASE_URL."/img/coverimages/".$value['image_url'];
                    }
                }
                $result['coverimages'] = array_values($result['coverimages']);
            }

            if(!empty($result['userservices']))
            {
                foreach ($result['userservices'] as $key=>$value)
                {
                    if($value['type'] == '1'){
                        $rating[$value['subservice_id']]['positive'] = (!isset($rating[$value['subservice_id']]['positive'])) ? $value['positive_rating'] : ($rating[$value['subservice_id']]['positive'] + $value['positive_rating']);
                        $rating[$value['subservice_id']]['neutral'] = (!isset($rating[$value['subservice_id']]['neutral'])) ? $value['neutral_rating'] : ($rating[$value['subservice_id']]['neutral'] + $value['neutral_rating']);
                            //$rating[$value['subservice_id']]['positive'] = $value['positive_rating'];
                            //$rating[$value['subservice_id']]['neutral'] = $value['neutral_rating'];
                    }
                }
            }

            $result['total_rating'] = $result['star_rating'] = 0;
            if(!empty($rating))
            {
                $userservice = TableRegistry::get('Userservices');
                $data = $userservice->getuserrating($rating);
                $result['total_rating'] = $data['total_rating'];
                $result['star_rating']  = $data['star_rating'];
            }else
            {
                $response['total_rating']   = 0;
                $response['star_rating']    = 0;
            }
            $result['usertask'] = $result['matches'] = $result['shortlist'] = 0;
            if(!empty($result['usertasks_with']))
            {
                foreach ($result['usertasks_with'] as $key => $value)
                {
                    if($value['status'] == '0'){
                        if($value['need_rating'] == '')
                        {
                            $result['usertask']++;
                        }
                        if($value['offer_rating'] == '')
                        {
                            $result['usertask']++;
                        }
                    }
                }
            }
            if(!empty($result['usertasks']))
            {
                foreach ($result['usertasks'] as $key => $value)
                {
                    if($value['status'] == '0'){
                        if($value['need_rating'] == '')
                        {
                            $result['usertask']++;
                        }
                        if($value['offer_rating'] == '')
                        {
                            $result['usertask']++;
                        }
                    }
                }
            }
            $result['matches'] = $result['match_cnt'];
            $result['shortlist'] = $result['usershortlist_cnt'];
            unset($result['userimages'],$result['usertasks_with'],$result['usertasks'],$result['userservices']);
            return $result;
        }else
        {
            return false;
        }
    }
    
    public function getserviceprovider($select=array(),$condition=array(),$contain=array(),$user_id=null,$lat=null,$long=null)
    {
        if($lat && $long){
            $latitude = $lat;
            $longitude = $long;
        }else {
            $user = $this->find()
            ->where(['id' => $user_id])
            ->select(['latitude','longitude'])
            ->first();
            $latitude = $user['latitude'];
            $longitude = $user['longitude'];
        }
        
        $query = $this->find()
        ->where($condition)
        ->select($select)
        ->contain($contain)
        ->all();

        $response = array();
        if($query)
        {
            $user_fb_friends = $this->getUserFBFriends($user_id);
            foreach ($query as $key=>$article) 
            {
                $response[$key]['id']           = $article['id'];
                $response[$key]['name']         = $article['firstname'].", ".substr($article['lastname'],0,1).".";
                $response[$key]['profile_image']    = ($article['profile_image_thumb']) ? $article['profile_image_thumb'] : BASE_URL."/img/default_thumb.jpg";
                $response[$key]['about_me']         = ($article['about_me']) ? $article['about_me'] : "";
                $response[$key]['verified']         = ($article['verified'] == "0") ? 0 : 1;
                $response[$key]['facebook_id']      = $article['facebook_id'];
                $response[$key]['shortlist_cnt']    = $article['shortlist_cnt'];
                $response[$key]['matches_cnt']      = $article['match_cnt'];
                $response[$key]['is_shortlist']     = 0;
                $response[$key]['is_matched']       = 0;
                $response[$key]['is_blocked']       = 0;
                if($latitude != '' && $longitude != ''){
                    $response[$key]['distance']     = $this->getdistance($latitude,$longitude,$article['latitude'],$article['longitude'],"K");
                }
                if(($latitude == '' && $longitude == '') || $response[$key]['distance'] == 0){
                    $response[$key]['distance']     = 2;
                }

                $response[$key]['fb_mutual_cnt']  = 0;
                $response[$key]['fb_friend_ids'] = '';
                if(!empty($article['facebookconnections']))
                {
                    $curruser_fb_friend_ids = array();
                    foreach ($article['facebookconnections'] as $k=>$val)
                    {
                        if(in_array($val['connection_fb_id'],$user_fb_friends) && !in_array($val['connection_fb_id'],$curruser_fb_friend_ids)){
                            $curruser_fb_friend_ids[] = $val['connection_fb_id'];                            
                            $response[$key]['fb_mutual_cnt']++;
                            $response[$key]['fb_friend_ids'] = implode(',',$curruser_fb_friend_ids);
                        }
                    }
                }
                
                $usermatches = TableRegistry::get('Usermatches');
                $response[$key]['is_matched']     = $usermatches->isMatched($user_id, $article['id']);
                if($response[$key]['is_matched'] != 1){
                    $usershortlists = TableRegistry::get('Usershortlists');
                    $response[$key]['is_shortlist']   = $usershortlists->isShortListed($user_id, $article['id']);
                }else {
                    $response[$key]['is_shortlist']   = 1;
                }
                $Userblocklists = TableRegistry::get('Userblocklists');
                $blockedData = $Userblocklists->isBlocked($user_id, $article['id']);
                $response[$key]['is_blocked']     = $blockedData['is_block'];
                $response[$key]['blocked_by']     = $blockedData['blocked_by'];
                
                $response[$key]['cover_image'] = array();
                if(isset($article['userimages']))
                {
                    foreach ($article['userimages'] as $k=>$val)
                    {
                        if($val['image_url'] != '')
                        {
                            $response[$key]['cover_image'][$k]['coverimage_key']    = $val['coverimage_key'];
                            $response[$key]['cover_image'][$k]['image_url']         = BASE_URL."/img/coverimages/".$val['image_url'];
                        }
                    }
                    $response[$key]['cover_image'] = array_values($response[$key]['cover_image']);
                }
                if(!empty($article['userinterests']))
                {
                    $response[$key]['userinterests'] = Hash::extract($article['userinterests'], '{n}.interest');
                    $response[$key]['userinterests'] = array_values($response[$key]['userinterests']);
                }else 
                {
                    $response[$key]['userinterests'] = array();
                }
                $rating = $response[$key]['service_need_ids'] = $response[$key]['subservice_needed'] = $response[$key]['service_offer_ids'] = $response[$key]['subservice_offered'] = array();
                if(!empty($article['userservices']))
                {
                    foreach ($article['userservices'] as $k=>$val)
                    {
                        if($val['subservice']['subservice_name'] != '')
                        {
                            if($val['type'] == 0)
                            {
                                if(!in_array($val['service_id'],$response[$key]['service_need_ids']))
                                {
                                    $response[$key]['service_need_ids'][] = $val['service_id'];
                                }
                                $response[$key]['subservice_needed'][$k]['sorting']             = ($val['sorting'] == '1') ? 1 : 0;
                                $response[$key]['subservice_needed'][$k]['subservice_name']     = $val['subservice']['subservice_name'];
                            }else
                            {
                                if(!in_array($val['service_id'],$response[$key]['service_offer_ids']))
                                {
                                    $response[$key]['service_offer_ids'][] = $val['service_id'];
                                }
                                $response[$key]['subservice_offered'][$k]['sorting']            = ($val['sorting'] == '1') ? 1 : 0;
                                $response[$key]['subservice_offered'][$k]['subservice_name']    = $val['subservice']['subservice_name'];
                            }
                            if($val['type'] == '1'){
                                $rating[$val['subservice_id']]['positive'] = $val['positive_rating'];
                                $rating[$val['subservice_id']]['neutral'] = $val['neutral_rating'];
                            }
                        }
                        $response[$key]['subservice_offered']   = array_values($response[$key]['subservice_offered']);
                        $response[$key]['subservice_needed']    = array_values($response[$key]['subservice_needed']);
                    }
                    array_multisort($response[$key]['subservice_needed'], SORT_DESC);
                    array_multisort($response[$key]['subservice_offered'], SORT_DESC);
                }
                if(!empty($rating))
                {
                    $userservice = TableRegistry::get('Userservices');
                    $result = $userservice->getuserrating($rating);
                    $response[$key]['total_rating'] = $result['total_rating'];
                    $response[$key]['star_rating']  = $result['star_rating'];
                }else 
                {
                    $response[$key]['total_rating'] = 0;
                    $response[$key]['star_rating']  = 0;
                }
            }
        }
        return $response;
    }
    
    public function getlistingserviceprovider($results,$user_id=null,$lat=null,$long=null)
    {
        $data = array();
        if($results)
        {
            $user_fb_friends = $this->getUserFBFriends($user_id);
            foreach ($results as $key=>$value)
            {
                if(isset($value['suburb'])){
                    $data[$value['suburb']]['suburb'] = $value['suburb'];
                }
                $temp['user_id']        = $value['id'];
                $temp['facebook_id']    = $value['facebook_id'];
                $temp['profile_image']  = ($value['profile_image_thumb']) ? $value['profile_image_thumb'] : BASE_URL."/img/default_thumb.jpg";
                $temp['name']           = $value['firstname'].", ".substr($value['lastname'],0,1).".";
                $temp['verified']       = ($value['verified'] == "0") ? 0 : 1;
                $temp['facebook_id']    = $value['facebook_id'];
                $temp['distance']       = ($value['distance_in_km'] < 2) ? 2 : round($value['distance_in_km']);
                $temp['shortlist_cnt']  = $value['shortlist_cnt'];
                
                $usershortlists = TableRegistry::get('Usershortlists');
                $temp['is_shortlist']   = $usershortlists->isShortListed($user_id, $value['id']);
                $usermatches = TableRegistry::get('Usermatches');
                $temp['is_matched']     = $usermatches->isMatched($user_id, $value['id']);
                $Userblocklists = TableRegistry::get('Userblocklists');
                $blockedData = $Userblocklists->isBlocked($user_id, $value['id']);
                $temp['is_blocked']     = $blockedData['is_block'];
                
                $rating = $temp['service_need_ids'] = $temp['subservice_needed'] = $temp['service_offer_ids'] = $temp['subservice_offered'] = array();
                
                $temp['fb_mutual_cnt']  = 0;
                $temp['fb_friend_ids'] = '';
                if(!empty($value['facebookconnections']))
                {
                    $curruser_fb_friend_ids = array();
                    foreach ($value['facebookconnections'] as $k=>$val)
                    {
                        if(in_array($val['connection_fb_id'],$user_fb_friends) && !in_array($val['connection_fb_id'],$curruser_fb_friend_ids)){
                            $curruser_fb_friend_ids[] = $val['connection_fb_id'];
                            $temp['fb_mutual_cnt']++;
                            $temp['fb_friend_ids'] = implode(',',$curruser_fb_friend_ids);
                        }
                    }
                }

                if(!empty($value['userservices']))
                {
                    $temp_subservice = array();
                    foreach ($value['userservices'] as $k=>$val)
                    {
                        if($val['subservice']['subservice_name'] != '' && !in_array($val['subservice']['subservice_name'], $temp_subservice))
                        {
                            if($val['type'] == 0)
                            {
                                if(!in_array($val['service_id'],$temp['service_need_ids']))
                                {
                                    $temp['service_need_ids'][] = $val['service_id'];
                                }
                                $temp['subservice_needed'][$k]['sorting']           = ($val['sorting'] == '1') ? 1 : 0; 
                                $temp['subservice_needed'][$k]['subservice_name']   = $val['subservice']['subservice_name'];
                            }else 
                            {
                                if(!in_array($val['service_id'],$temp['service_offer_ids']))
                                {
                                    $temp['service_offer_ids'][] = $val['service_id'];
                                }
                                $temp['subservice_offered'][$k]['sorting']          = ($val['sorting'] == '1') ? 1 : 0;
                                $temp['subservice_offered'][$k]['subservice_name']  = $val['subservice']['subservice_name'];
                            }
                            $temp_subservice[] = $val['subservice']['id'];
                        }
                        if(($val['positive_rating'] || $val['neutral_rating']) && $val['type'] == '1')
                        {
                            $rating[$val['subservice_id']]['positive'] = $val['positive_rating'];
                            $rating[$val['subservice_id']]['neutral'] = $val['neutral_rating'];
                        }
                        $temp['subservice_offered']     = array_values($temp['subservice_offered']);
                        $temp['subservice_needed']      = array_values($temp['subservice_needed']);
                    }
                    array_multisort($temp['subservice_offered'], SORT_DESC);
                    array_multisort($temp['subservice_needed'], SORT_DESC);
                    
                }
                if(!empty($rating))
                {
                    $userservice = TableRegistry::get('Userservices');
                    $result = $userservice->getuserrating($rating);
                    $temp['total_rating'] = $result['total_rating'];
                    $temp['star_rating']    = $result['star_rating'];
                }else 
                {
                    $temp['total_rating'] = 0;
                    $temp['star_rating']    = 0;
                }
                if(isset($value['suburb'])){
                    $data[$value['suburb']]['userData'][$key] = $temp;
                    $data[$value['suburb']]['userData'] = array_values($data[$value['suburb']]['userData']);
                }else {
                    $data[$key] = $temp;
                    $data = array_values($data);
                }
            }
            $data = array_values($data);
        }
        return $data;
    }
    
    public function getusers($condition)
    {
        $query = $this->find()
        ->where($condition)
        ->select(['id','firstname','lastname','profile_image_thumb','email','postcode','suburb','city','state','country'])
        ->all();
        $response = array();
        if($query)
        {
            foreach ($query as $key=>$value)
            {
                if(!empty($value))
                {
                    $response[$key]['id']       =  $value['id'];
                    $response[$key]['name']         =  $value['firstname'].", ".substr($value['lastname'],0,1).".";
                    $response[$key]['profile_image']=  ($value['profile_image_thumb']!='') ? $value['profile_image_thumb'] : BASE_URL."/img/default_image.png"; 
                    $response[$key]['email']        =  $value['email'];
                    $response[$key]['postcode']     =  $value['postcode'];
                    $response[$key]['suburb']       =  $value['suburb'];
                    $response[$key]['city']         =  $value['city'];
                    $response[$key]['state']        =  $value['state'];
                    $response[$key]['country']      =  $value['country'];
                }
            }
        }
        return $response;
    }
    
    /* Function for CMS */
    public function getratingsummary($data)
    {
        $results = $data->toArray();
        $response = $rating = array();
        if($results)
        {
            foreach ($results as $key=>$value)
            {
                $response[$key]['id'] = $value['id'];
                $response[$key]['name'] = $value['firstname'].", ".substr($value['lastname'],0,1).".";
                $response[$key]['image'] = ($value['profile_image_thumb']) ? $value['profile_image_thumb'] : BASE_URL."/img/default_image.png";
                $response[$key]['image'] = $response[$key]['image'] = $response[$key]['image'] = $response[$key]['image'];
                $response[$key]['total_value'] = $response[$key]['total_fee'] = $response[$key]['total_positive_rating'] = $response[$key]['total_neutral_rating'] = $response[$key]['total_negative_rating'] = 0;
                if(!empty($value['usertasks']))
                {
                    foreach ($value['usertasks'] as $k=>$val)
                    {
                        $response[$key]['total_value']  += $val['amount'];
                        $response[$key]['total_fee']    += $val['fee'];
                    }
                }
                $rating['positive']  = $value['positive_rating'];
                $rating['neutral']   = $value['neutral_rating'];
                $response[$key]['total_positive_rating']    = $value['positive_rating'];
                $response[$key]['total_neutral_rating']     = $value['neutral_rating'];
                if(!empty($rating))
                {
                    $userservice = TableRegistry::get('Userservices');
                    $result = $userservice->getuserrating($rating);
                    $response[$key]['total_rating']     = $result['total_rating'];
                    $response[$key]['average_rating']   = $result['average_rating'];
                }else
                {
                    $response[$key]['total_rating']     = 0;
                    $response[$key]['average_rating']   = 0;
                }
            }
        }
        return $response;
    }
    
    public function savedata($data) 
    {
        $user = $this->newEntity($data);
      
        if ($this->save($user)) 
        {
            return $user->id;
        } else 
        {
            return 0;
        }
    }

    public function updatedata($id, $data) 
    {
        $user = $this->get($id);
        foreach ($data as $key=>$value)
        {
            $user->$key = $value;
        }
        if($this->save($user))
        {
            return true;
        }else {
            return false;
        }
    }
    
    public function updateuserservicedata($data)
    {
        $user = $this->get($data['user_id']);
        $service_offered = explode(',',$user['service_offered']);
        $service_needed = explode(',',$user['service_needed']);
        if($data['unchecked_offer_service_id'] != '' || $data['checked_offer_service_id'] != ''){
            foreach ($service_offered as $key => $value) {
                foreach (explode(',',trim($data['unchecked_offer_service_id'],'"')) as $k => $val) {
                    unset($service_offered[array_search($val,$service_offered)]);
                }
                foreach (explode(',',trim($data['checked_offer_service_id'],'"')) as $k => $val) {
                    if(!in_array($val, $service_offered)){
                        $service_offered[] = $val;
                    }
                }
            }
        }
        if($data['unchecked_need_service_id'] != '' || $data['checked_need_service_id'] != ''){
            foreach ($service_needed as $key => $value) {
                foreach (explode(',',trim($data['unchecked_need_service_id'],'"')) as $k => $val) {
                    unset($service_needed[array_search($val,$service_needed)]);
                }
                foreach (explode(',',trim($data['checked_need_service_id'],'"')) as $k => $val) {
                    if(!in_array($val, $service_needed)){
                        $service_needed[] = $val;
                    }
                }
            }
        }
        $user->service_needed = implode(',', $service_needed);
        $user->service_offered = implode(',', $service_offered);
        if($this->save($user))
        {
            return 1;
        }else {
            return 0;
        }
    }
    
    public function updatedatawithfbid($fbid, $data)
    {
        $user = $this->find()
        ->where(['facebook_id' => $fbid])
        ->select(['id'])
        ->first();
        if(isset($user['id']))
        {
            $user = $this->get($user['id']);
            foreach ($data as $key=>$value)
            {
                $user->$key = $value;
            }
            if($this->save($user))
            {
                return true;
            }else {
                return false;
            }
        }else {
            return false;
        }
    }
    
    public function getRecordstoController($select=array(), $condition=array(), $contain=array())
    {
        $results = $this->find()
        ->where($condition)
        ->select($select)
        ->contain($contain)
        ->all();
        if($results){
            $results = $results->toArray();
        }
        return $results;
    }

    public function getdistance($lat1, $lon1, $lat2, $lon2, $unit) {
        $earthRadius = 6371000;
        $latFrom = deg2rad($lat1);
        $lonFrom = deg2rad($lon1);
        $latTo = deg2rad($lat2);
        $lonTo = deg2rad($lon2);

        $latDelta = $latTo - $latFrom;
        $lonDelta = $lonTo - $lonFrom;

        $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
            cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
        $distance = ($angle * $earthRadius) / 1000;
        $distance = round($distance);
        if($distance > 100 ){
            $distance = 99;
        }
        return $distance;
    }
    
    public function searchText($text) {
        $subser_ids = $subsertag_ids = array(-1,0);
        $user_ids = array();
        
        $Subservices = TableRegistry::get('Subservices');
        $inReview = $Subservices->find('all')
        ->where(['subservice_name LIKE' => '%'.$text.'%'])
        ->select(['id'])
        ->all();
        $inReview = $inReview->toArray();
        if($inReview)
        {    
            $subser_ids = array();
            foreach ($inReview as $key => $value)
            {
                $subser_ids[] = $value['id'];
            }
            $subser_ids = array_values($subser_ids);
        }
        
        $Subservicetags = TableRegistry::get('Subservicetags');
        $unpublished = $Subservicetags->find('all')
        ->where(['subservicetag_name LIKE' => '%'.$text.'%'])
        ->select(['id'])
        ->all();
        $unpublished = $unpublished->toArray();
        if($unpublished)
        {    
            foreach ($unpublished as $key => $value)
            {
                $subsertag_ids[] = $value['id'];
            }
            $subsertag_ids = array_values($subsertag_ids);
        }
        
        $condition = array();
        $condition['OR'][]['subservice_id IN'] = $subser_ids;
        $condition['OR'][]['subservicetag_id IN'] = $subsertag_ids;
        $Userservices = TableRegistry::get('Userservices');
        $query = $Userservices->find('all')
        ->where($condition)
        ->select(['user_id'])
        ->all();
        $query = $query->toArray();
        if($query)
        {    
            foreach ($query as $key => $value)
            {
                $user_ids[] = $value['user_id'];
            }
        }
        
        $condition = array();
        $condition['OR'][]['Users.postcode'] = $text;
        $condition['OR'][]['Users.firstname LIKE'] = '%'.$text.'%';
        $condition['OR'][]['Users.lastname LIKE'] = '%'.$text.'%';
        $condition['OR'][]['Users.suburb LIKE'] = '%'.$text.'%';
        
        $result = $this->find('all')
        ->where($condition)
        ->select(['id'])->all();
        $result = $result->toArray();
        if($result)
        {    
            foreach ($result as $key => $value)
            {
                $user_ids[] = $value['id'];
            }
        }
        $user_ids = array_unique($user_ids);
        
        return $user_ids;
    }

    /*
    * Get user fb friends
    */
    public function getUserFBFriends($user_id)
    {
        $Facebookconnections = TableRegistry::get('Facebookconnections');
        $result = $Facebookconnections->find('all')->where(['user_id' => $user_id])->select(['user_id','connection_fb_id'])->all();
        $user_fb_friends = array();
        if(!empty($result))
        {
            /*foreach ($result as $key => $value) {
                $user_fb_friends[] = $value['connection_fb_id'];
            }*/
            $user_fb_friends = Hash::extract($result->toArray(), '{n}.connection_fb_id');
            if(!empty($user_fb_friends)){
                $data = $this->find('all')->select(['facebook_id'])->where(['facebook_id IN' => array_values($user_fb_friends)])->all();
                if(!empty($data)){
                    $user_fb_friends = Hash::extract($data->toArray(), '{n}.facebook_id');
                }
            }
        }
        return $user_fb_friends;
    }

    public function deleteUserData($user_id){
        $entity = $this->get($user_id);
        $result = $this->delete($entity);
        return 1;
    }

    public function syncUserservices($data){
        foreach ($data as $key => $value) {
            $entity = $this->get($key);
            $entity->service_needed = implode(',', $value['service_needed']);
            $entity->service_offered = implode(',', $value['service_offered']);
            $this->save($entity);
        }
        return 1;
    }

    /*
    stripeWebHookCall if any selected events fire from stripe (Ex. account.updated).
    @return Response
    */
    public function stripeWebHookCall($webHookData) {
        $type = $webHookData->type;
        $data = $webHookData->data;
        $result = array();
        switch ($type) {
            case 'account.updated':
                $status = $data->object->legal_entity->verification->status;
                $email = $data->object->email;

                $userData = $this->find('all',[
                        'fields'=>['id'],
                        'conditions'=>['is_active'=>1,'email'=>$email]
                        ])->first();
                $datatosave = array();
                if($status=='verified') {
                    $datatosave['stripe_bank_verified'] = 1;
                }elseif($status=='unverified') {
                    $datatosave['stripe_bank_flag'] = '0';
                }
                $usersetting = TableRegistry::get('Usersettings');
                $settingData = $usersetting->find('all',[
                        'fields'=>['id'],
                        'conditions'=>['user_id'=>$userData['id']]
                        ])->first();
                $usersetting->updatedata($settingData['id'],$datatosave);
                $result['user_id'] = $userData['id'];
                $result['status'] = $status;
            break;

            case 'customer.subscription.updated':
            break;
        }
        return $result;
    }

    public function addBusinessUser($data)
    {
        $exists = $this->exists(['email' => $data['email']]);
        if($exists == 0)
        {
            $data['type'] = 2;
            $data['positive_rating']= $data['rating'];
            $data['neutral_rating'] = 100 - $data['rating'];
            $data['about_me']       = str_replace('<username>',$data['firstname'],Configure::read("DEFAULT_ABOUT_BUSINESS"));
            $password = $this->getRandomPassword();
            $data['password_token'] = $password;
            $data['dob']            = date('Y-m-d', strtotime($data['dob']));
            $data['gender']         = 'female';
            $data['verified']       = '1';
            $data['verified_by']    = 'admin';
            $service_offered= '';
            foreach ($data['subservice'] as $key => $value) {
                $explode_arr = explode('-', $value);
                $service_offered .= $explode_arr[1].",";
            }
            $data['service_offered']= trim($service_offered,',');
            $data['verified_at']    = date('Y-m-d H:i:s');
            $data['createdAt']      = date('Y-m-d H:i:s');
            $data['modifiedAt']     = date('Y-m-d H:i:s');
            unset($data['rating']);
            $userData['to']         = $data['email'];
            $userData['password']   = $password;
            $response['status']     = $this->savedata($data);
            $userData['id']         = $response['status'];
            $response['data']       = $userData;
            $response['message']    = 'Done';
        }else {
            $response['status']     = 0;
            $response['message']    = 'User already exists!';
        }
        return $response;
    }

    public function editBusinessUser($data)
    {
        $data['positive_rating']= $data['rating'];
        $data['neutral_rating'] = 100 - $data['rating'];
        $data['dob']            = date('Y-m-d', strtotime($data['dob']));
        $data['gender']         = 'female';
        unset($data['rating']);
        $response['status']     = $this->updatedata($data['id'], $data);
        return $response;
    }
    
    public function getEncryptedPassword($password) {
        if (strlen($password) > 0) {
            return (new DefaultPasswordHasher)->hash($password);
        }
    }
}
