<?php

namespace App\Model\Behavior;

use Cake\Filesystem\File;
use Cake\Filesystem\Folder;
use Cake\ORM\Behavior;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Auth\DefaultPasswordHasher;

class ApiBehavior extends Behavior {

    public function initialize(array $config) {
       
    }

    public function checkPassword($passedPassword, $actualPassword) {
        if (md5($passedPassword) === $actualPassword) {
            return true;
        } else {
            return false;
        }
    }

    public function getEncryptedPassword($password) {
        if (strlen($password) > 0) {
            return (new DefaultPasswordHasher)->hash($password);
        }
    }

    public function __checkRequestData($requiredfields = array()) {
        $request_data = $_REQUEST;
        $flag = 0;
        foreach ($requiredfields as $requiredfield) {
            if (!in_array($requiredfield, array_keys($request_data)) || (empty($request_data[$requiredfield]) && $request_data[$requiredfield] != 0)) {
                $flag = 1;
                break;
            }
        }

        if ($flag == 1) {
            $response['success'] = 0;
            $response['Message'] = "An Error Occurred: Please try again.";
        } else {
            $response['success'] = 1;
        }
        return $response;
    }

    public function __checkEmail($email = null, $facebook_id = null) {
        $users = TableRegistry::get('Patients');
        if($email){
            $conditions['OR'][]['Patients.email']       = $email;
        }
        if($facebook_id){
            $conditions['OR'][]['Patients.facebook_id']    = $facebook_id;
        }
        $userdata = $users->find('all', array('conditions' => $conditions));
        $result = $userdata->first(); 
        if (!empty($result)) {
            return 1;
        } else {
            return 0;
        }
    }

    public function __getGUID() 
    {
        mt_srand((double) microtime() * 10000); //optional for php 4.2.0 and up.
        $charid = strtoupper(md5(uniqid(rand(), true)));
        $uuid = substr($charid, 0, 8)
                . substr($charid, 8, 4)
                . substr($charid, 12, 4)
                . substr($charid, 16, 4)
                . substr($charid, 20, 12)
                . time();
        return strtolower($uuid);
    }

    public function uploadimage($requestdata)
    {
        $imagedata = $param = array();
        $filename = '';
        $image_arr      = array("image/png","image/jpg","image/jpeg");
        
        if ($value['file']['error'] == 0 && in_array($value['file']['type'],$image_arr)) {
            if($value['file']['type'] == 'image/png'){
                $extension = 'png';
            }elseif ($value['file']['type'] == 'image/jpg'){
                $extension = 'jpg';
            }elseif ($value['file']['type'] == 'image/jpeg'){
                $extension = 'jpeg';
            }
            $tmp_name   = $value['file']["tmp_name"];
            $name   = "profile-image-".time().".".$extension;
            $uploads_dir    = ROOT."/webroot/img/profileimages";
            if(move_uploaded_file($tmp_name, $uploads_dir."/".$name))
            {
                $filename = $name;
            }
        }
        return $filename;
    }

    public function getRandomPassword()
    {
        $token = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        $max = strlen($codeAlphabet); // edited

        for ($i=0; $i < 8; $i++) {
            $char = $codeAlphabet[rand(0, $max-1)];
            $token .= $char;
        }
        return $token;
    }
}

