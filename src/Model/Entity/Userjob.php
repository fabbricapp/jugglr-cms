<?php
namespace App\Model\Entity;

use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Entity;
use Cake\ORM\Table;
use DateTime;

/**
 * User Entity.
 */
class Userjob extends Entity
{
    protected function _getDateFormate()
    {
        $myDateTime = new DateTime($this->_properties['datetime']);
        return $myDateTime->format('Y-m-d H:i:s');
    }    

    protected function _getCreatedFormate()
    {
        $myDateTime = new DateTime($this->_properties['createdAt']);
        return $myDateTime->format('Y-m-d H:i:s');
    }
}
