<?php

/**
 * CakePHP(tm) : Openxcell Portfolio (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'Jugglr';
?>
<!DOCTYPE html>
<html>
    <head>
        <?php echo $this->Html->charset() ?>
                <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/vis/4.21.0/vis.min.js"></script>
                <link href="https://cdnjs.cloudflare.com/ajax/libs/vis/4.21.0/vis.min.css" rel="stylesheet" type="text/css" />

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>
            <?php echo $cakeDescription ?>:
            <?php echo $this->fetch('title') ?>
        </title>
        <?php echo $this->Html->meta('') ?>

        <?php echo $this->Html->css('animate.css') ?>
        <link rel="stylesheet" href="<?php echo $this->request->webroot; ?>admin_theme/bootstrap/css/bootstrap.min.css">
        <?php echo $this->Html->css('bootstrap-switch.min.css') ?>
        <link rel="stylesheet" href="<?php echo $this->request->webroot; ?>admin_theme/dist/css/AdminLTE.min.css">
        <link rel="stylesheet" href="<?php echo $this->request->webroot; ?>admin_theme/dist/css/skins/_all-skins.min.css">
        <link rel="stylesheet" href="<?php echo $this->request->webroot; ?>admin_theme/dist/css/ionicons.min.css">
        <link rel="stylesheet" href="<?php echo $this->request->webroot; ?>admin_theme/dist/css/font-awesome.min.css">
        <?php echo $this->Html->css('custom.css') ?>

        <?php echo $this->fetch('meta') ?>
        <?php echo $this->fetch('css') ?>
        <?php echo $this->fetch('script') ?>

        <script type="text/javascript" src="<?php echo $this->request->webroot; ?>admin_theme/plugins/jQuery/jQuery-2.1.4.min.js"></script>
        <script type="text/javascript" src="<?php echo $this->request->webroot; ?>admin_theme/dist/js/jquery.validate.min.js"></script>
        <script type="text/javascript" src="<?php echo $this->request->webroot; ?>admin_theme/bootstrap/js/bootstrap.min.js"></script>
        <?php echo $this->Html->script('bootstrap-switch.min.js'); ?>
        <script type = "text/javascript" src = "<?php echo $this->request->webroot; ?>admin_theme/plugins/jQueryUI/jquery-ui.min.js"></script>
        <script type="text/javascript" src="<?php echo $this->request->webroot; ?>admin_theme/dist/js/app.min.js"></script>
        <script type="text/javascript" src="<?php echo $this->request->webroot; ?>admin_theme/dist/js/Chart.min.js"></script>
        <?php echo $this->Html->script('materialize.min.js') ?>
        <SCRIPT TYPE="text/javascript">var _ROOT = "<?php echo $this->request->webroot; ?>";</SCRIPT>
        <?php
        $theme_folder = "/gritter";

        //Put this to the Head
        echo $this->Html->css("$theme_folder/css/jquery.gritter.css");//Put this to the Footer
        echo $this->Html->script("$theme_folder/js/jquery.gritter.min.js");
        ?>
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            <?php echo $this->element('header'); ?>
            <!-- Left side column. contains the logo and sidebar -->
            <?php echo $this->element('left_sidebar'); ?>
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Main content -->
                <section class="content">
                    <?php echo $this->fetch('content') ?>
                    <div class="modal fade" id="invoice_popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"></div>
                </section>
            </div>
        </div>
    </body>
    <footer>
        <?php $flash_message = strip_tags(trim($this->Flash->render())); ?>
        <?php if (!empty($flash_message)) { ?>
            <script>
                  /*
                   * For Notification with alert.
                   * glitterCallAlert("Title","Message","HideSpeed","SlowSpeed","NotificationClassName");
                   * E.g glitterCallAlert("Notification :","Mayur GOdhani",5000,500,"my-sticky-class");                
                   */
                  $(document).ready(function () {
                      glitterCallAlert("Notification :", '<?php echo $flash_message; ?>', 5000, 500);
                  });
            </script> 
        <?php } ?> 
        <script>
            jQuery(document).ready(function () {
                $(document).on('click', '.ajaxviewmodel', function (event) {
                    event.preventDefault();
                    var link = $(this).attr("href");
                    $('#invoice_popup').modal('show');
                    
                    $.ajax({
                        url: link,
                        success: function (data) {
                            $(".ajaxloader").hide();
                            $("#invoice_popup").html(data);
                        }
                    });
                });
            });
        </script>
    </footer>
</html>
