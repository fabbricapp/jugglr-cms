<?php

/**
 * CakePHP(tm) : Openxcell Portfolio (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'Juggler';
?>
<!DOCTYPE html>
<html>
    <head>
        <?php echo $this->Html->charset() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>
            <?php echo $cakeDescription ?>:
            <?php echo $this->fetch('title') ?>
        </title>

        <?php echo $this->Html->meta('') ?>

        <?php echo $this->Html->css('animate.css') ?>
        <?php echo $this->Html->css('bootstrap.css') ?>
        <?php echo $this->Html->css('bootstrap-switch.min.css') ?>
        <?php echo $this->Html->css('AdminLTE.min.css') ?>
        <?php echo $this->Html->css('skins/_all-skins.min.css') ?>
        <link rel="stylesheet" href="<?php echo $this->request->webroot; ?>admin_theme/dist/css/ionicons.min.css">
        <link rel="stylesheet" href="<?php echo $this->request->webroot; ?>admin_theme/dist/css/font-awesome.min.css">
        <?php echo $this->Html->css('custom.css') ?>

        <?php echo $this->fetch('meta') ?>
        <?php echo $this->fetch('css') ?>
        <?php echo $this->fetch('script') ?>

        <?php echo $this->Html->script('jquery.js'); ?>
        <?php echo $this->Html->script('bootstrap.js'); ?>
        <?php echo $this->Html->script('bootstrap-switch.min.js'); ?>
        <script type="text/javascript" src="<?php echo $this->request->webroot; ?>admin_theme/plugins/jQuery/jQuery-2.1.4.min.js"></script>
        <script type = "text/javascript" src = "<?php echo $this->request->webroot; ?>admin_theme/plugins/jQueryUI/jquery-ui.min.js"></script>
         <?php echo $this->Html->script('app.min.js'); ?>
        <?php echo $this->Html->script('materialize.min.js') ?>
        <SCRIPT TYPE="text/javascript">var _ROOT = "<?php echo $this->request->webroot; ?>";</SCRIPT>
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            <?php echo $this->element('header'); ?>
            <!-- Left side column. contains the logo and sidebar -->
            <?php echo $this->element('left_sidebar'); ?>
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <?php echo $this->element('breadcrumb'); ?>
                <!-- Main content -->
                <section class="content">
                    <?php echo $this->fetch('content') ?>
                </section>
            </div>
        </div>
    </body>
    <footer>
    </footer>
</html>
