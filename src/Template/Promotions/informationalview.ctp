<link rel="stylesheet" href="<?php echo $this->request->webroot;?>css/custom-with-material.css">
<link rel="stylesheet" href="<?php echo $this->request->webroot;?>css/dist/datatables/dataTables.bootstrap.css">
<link rel="stylesheet" href="<?php echo $this->request->webroot;?>css/buttons.dataTables.min.css">

<section class="content-header margin-bottom-lg padding-left-none">
    <h1>
        Promotions
        <small>Page</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Informational Promotion</li>
    </ol>
</section>
<div class="row margin-none">
    <div class="col-xs-12 padding-none">
        <section class="content padding-none"> 
          <div class="box">
            <div class="box-header">
                
            </div>
            <div class="box-body" style="overflow-y:auto;">
                <table id="example2" class="table table-hover">
                    <thead>
                        <tr>
                            <th>Sr. #</th>
                            <th>Image</th>
                            <th>URL</th>
                            <th>CreatedAt</th>
                            <th>Active/Inactive</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if($listing){
                        foreach($listing as $key=>$value){ ?>
                        <tr id="<?php echo $value['id'];?>">
                        <td><?php echo $key + 1;?></td>
                            <td>
                                <div class="user-panel text-center padding-none">
                                    <div class="image">
                                        <img class="img-circle" src='<?php if($value["image"]){ echo $value["image"];}else {echo BASE_URL."/img/promotionimages/default.png";}?>'/>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <a href="<?php echo $this->Url->build(["controller" => "promotions", "action" => "editinformationalimage", "id" => $value['id']]); ?>"><?php echo $value['url'];?>
                                </a>
                            </td>
                            <td><?php echo $value['createdAt'];?></td>
                            <td>
                              <div class="text-center">
                                <div id="<?php echo $value['id'];?>" class="switch">
                                    <label>
                                      <input id="<?php echo $value['id'];?>" type="checkbox" data-status=<?php if($value['is_active'] ==1){echo 1;}else{ echo 0;}?> <?php if($value['is_active'] ==1){echo 'checked="checked"';}?>>
                                      <span class="lever"></span>
                                    </label>
                                  </div>
                              </div>
                            </td>
                        </tr>
                        <?php }
                        }else { ?>
                            <tr>
                                <td>
                                    No records found!
                                </td>
                            </tr>
                        <?php }?>
                    </tbody>
                </table>
            </div>
        </div>
        </section>
    </div>
</div>
<script type="text/javascript">
jQuery(document).ready(function() {
    $('#example2').on("change", ".switch",function (event) {
        event.preventDefault();
        var promotion_id = $(this).attr('id');
        $.ajax({
          url: _ROOT+'promotions/updatepromotionstatus',
          type: 'POST',
          dataType: 'Json',
          data:{'promotion_id':promotion_id},
          success:function(data){
              var result = jQuery.parseJSON( data );
          }
        });
    });
});
</script>