<link rel="stylesheet" href="<?php echo $this->request->webroot; ?>admin_theme/plugins/daterangepicker/daterangepicker-bs3.css">
<section class="content-header margin-bottom-lg padding-left-none">
    <h1>
        Promotions
        <small>Page</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Informational Images</li>
    </ol>
</section>
<div class="row margin-none">
    <div class="col-xs-12 padding-none">
        <section class="content padding-none"> 
            <div class="box">
                <div class="box-header">
                    
                </div>
                <div class="box-body"> 
                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                            <?php echo $this->Form->create('Promotion',array('id' => 'promotion','role'=>'form','url'=>'/promotions/informationalimage','enctype'=>'multipart/form-data')); ?>
                              <div class="form-group">
                                  <div class="row margin-none padding-bottom-lg" id="promotionimage">
                                      <div class="col-md-4 text-right">Image : </div>
                                      <div class="col-md-8">
                                        <?php 
                                          echo $this->form->input('Promotion.image',array(
                                            'label'=>false,
                                            'name'=>'image',
                                            'type'=>'file',
                                            'placeholder'=>'Discount image',
                                            'class'=>'required',
                                            'div'=>'form-group'
                                          ));
                                        ?>
                                        <div class="input-sm">Upload discount image. Only png, jpg, jpeg extension files are allowed.</div>
                                      </div>
                                  </div>
                                  <div class="row margin-none padding-bottom-lg">
                                    <div class="col-md-4 text-right"> URL :</div>
                                    <div class="col-md-8">
                                      <?php 
                                        echo $this->form->input('Promotion.url',array(
                                          'label'=>false,
                                          'name'=>'url',
                                          'type'=>'text',
                                          'placeholder'=>'Promotion Redirect URL',
                                          'value'=>$promotion['url'],
                                          'class'=>'form-control required',
                                          'div'=>'form-group'
                                        ));
                                      ?>
                                      <div class="input-sm">Promotion redirect url where user will be redirected for more information.</div>
                                    </div>
                                  </div>
                                  <div class="row margin-none padding-bottom-lg padding-top-lg margin-top-lg">
                                      <div class="col-md-4 text-right"></div>
                                      <div class="col-md-8">
                                          <button class="updateBtn" type="submit">Save</button>       
                                      </div>
                                  </div>
                              </div>
                            <?php echo $this->Form->end(); ?>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
