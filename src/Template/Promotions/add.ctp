<link rel="stylesheet" href="<?php echo $this->request->webroot; ?>admin_theme/plugins/daterangepicker/daterangepicker-bs3.css">
<section class="content-header margin-bottom-lg padding-left-none">
    <h1>
        Promotions
        <small>Page</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Promotion</li>
    </ol>
</section>
<div class="row margin-none">
    <div class="col-xs-12 padding-none">
        <section class="content padding-none"> 
            <div class="box">
                <div class="box-header">
                    
                </div>
                <div class="box-body"> 
                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                            <?php echo $this->Form->create('Promotion',array('id' => 'promotion','role'=>'form','url'=>'/promotions/add','enctype'=>'multipart/form-data')); ?>
                              <div class="form-group">
                                  <div class="row margin-none padding-bottom-lg">
                                    <div class="col-md-4 text-right"> Title :</div>
                                    <div class="col-md-8">
                                      <?php 
                                        echo $this->form->input('Promotion.title',array(
                                          'label'=>false,
                                          'name'=>'title',
                                          'type'=>'text',
                                          'placeholder'=>'Promotion Title',
                                          'value'=>$promotion['title'],
                                          'class'=>'form-control required',
                                          'div'=>'form-group'
                                        ));
                                      ?>
                                      <div class="input-sm">Promotion title that will be displayed to users in check list.</div>
                                    </div>
                                  </div>
                                  <div class="row margin-none padding-bottom-lg">
                                    <div class="col-md-4 text-right"> Description :</div>
                                    <div class="col-md-8">
                                      <?php 
                                        echo $this->form->input('Promotion.description',array(
                                          'label'=>false,
                                          'name'=>'description',
                                          'type'=>'text',
                                          'placeholder'=>'Promotion description',
                                          'value'=>$promotion['description'],
                                          'class'=>'form-control required',
                                          'div'=>'form-group'
                                        ));
                                      ?>
                                      <div class="input-sm">Promotion description that will be displayed to users in check list.</div>
                                    </div>
                                  </div>
                                  <div class="row margin-none padding-bottom-lg">
                                    <div class="col-md-4 text-right"> Promotion usage count :</div>
                                    <div class="col-md-8">
                                      <?php 
                                        echo $this->form->input('Promotion.usage_cnt',array(
                                          'label'=>false,
                                          'name'=>'usage_cnt',
                                          'type'=>'text',
                                          'placeholder'=>'Discount usage count',
                                          'value'=>$promotion['usage_cnt'],
                                          'class'=>'form-control',
                                          'div'=>'form-group'
                                        ));
                                      ?>
                                      <div class="input-sm">Promotion usage count ex: promotion applicable for first 1000 users.</div>
                                    </div>
                                  </div>
                                  <div class="row margin-none padding-bottom-lg">
                                      <div class="col-md-4 text-right"> Date Range :</div>
                                      <div class="col-md-8">
                                        <div class="form-group">
                                          <div class="input-group">
                                            <div class="input-group-addon">
                                              <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" value="<?php echo $promotion['promotiondate'];?>" name="promotiondate" class="form-control pull-right" id="reservation">
                                          </div>
                                        </div>
                                        <div class="input-sm">Select start and end date for promotion to be valid.</div>
                                      </div>
                                  </div>
                                  <div class="row margin-none padding-bottom-lg">
                                      <div class="col-md-4 text-right"> Type :</div>
                                      <div class="col-md-8">
                                        <div class="">
                                              <label class="padding-right-lg">
                                                <input name="type" value="1" type="radio">
                                                <span class="lever">Percentage</span>
                                              </label>
                                              <label class="padding-right-lg">
                                                <input name="type" value="2" type="radio">
                                                <span class="lever">Flat</span>
                                              </label>
                                              <label>
                                                <input name="type" value="3" type="radio">
                                                <span class="lever">GreaterThan Offer Amount</span>
                                              </label>
                                        </div>
                                        <div class="input-sm">Select any one of the option to set promotion.</div>
                                      </div>
                                  </div>
                                  <div class="row margin-none padding-bottom-lg" id="conditionalRadio">
                                      <div class="col-md-4 text-right"> Conditional Discount Type :</div>
                                      <div class="col-md-8">
                                        <div class="">
                                              <label class="padding-right-lg">
                                                <input name="conditionaltype" value="1" type="radio">
                                                <span class="lever">Percentage</span>
                                              </label>
                                              <label class="padding-right-lg">
                                                <input name="conditionaltype" value="2" type="radio">
                                                <span class="lever">Flat</span>
                                              </label>
                                        </div>
                                        <div class="input-sm">Select any one of the option to set promotion.</div>
                                      </div>
                                  </div>
                                  <div class="row margin-none padding-bottom-lg" id="percentageDiscount">
                                      <div class="col-md-4 text-right"> </div>
                                      <div class="col-md-8">
                                        <?php 
                                          echo $this->form->input('Promotion.percent',array(
                                            'label'=>false,
                                            'name'=>'percent',
                                            'type'=>'text',
                                            'placeholder'=>'Discount percentage',
                                            'value'=>$promotion['percent'],
                                            'class'=>'form-control required',
                                            'div'=>'form-group'
                                          ));
                                        ?>
                                        <div class="input-sm">Percentage discount that must be applied on offer amount.</div>
                                      </div>
                                  </div>
                                  <div class="row margin-none padding-bottom-lg" id="amountDiscount">
                                      <div class="col-md-4 text-right"> </div>
                                      <div class="col-md-8">
                                        <?php 
                                          echo $this->form->input('Promotion.amount',array(
                                            'label'=>false,
                                            'name'=>'amount',
                                            'type'=>'text',
                                            'placeholder'=>'Discount amount',
                                            'value'=>$promotion['amount'],
                                            'class'=>'form-control required',
                                            'div'=>'form-group'
                                          ));
                                        ?>
                                        <div class="input-sm">Discount amount that need to be applied on offer amount.</div>
                                      </div>
                                  </div>
                                  <div class="row margin-none padding-bottom-lg" id="offerAmount">
                                      <div class="col-md-4 text-right"> </div>
                                      <div class="col-md-8">
                                        <?php 
                                          echo $this->form->input('Promotion.offeramount',array(
                                            'label'=>false,
                                            'name'=>'offeramount',
                                            'type'=>'text',
                                            'placeholder'=>'Offer amount greater than',
                                            'value'=>$promotion['offeramount'],
                                            'class'=>'form-control required',
                                            'div'=>'form-group'
                                          ));
                                        ?>
                                        <div class="input-sm">Promotion will be applied if offer amount is greather then this amount.</div>
                                      </div>
                                  </div>
                                  <div class="row margin-none padding-bottom-lg">
                                      <div class="col-md-4 text-right"> Accessibility :</div>
                                      <div class="col-md-8">
                                        <div class="">
                                              <label class="padding-right-lg">
                                                <input name="accessibility" value="1" type="radio">
                                                <span class="lever">Public</span>
                                              </label>
                                              <label>
                                                <input name="accessibility" value="2" type="radio">
                                                <span class="lever">Private</span>
                                              </label>
                                        </div>
                                        <div class="input-sm">Set accessibility to public if promotio must apply to all users or private if for any specific users.</div>
                                      </div>
                                  </div>
                                  <div class="row margin-none padding-bottom-lg" id="userIDs">
                                      <div class="col-md-4 text-right"> </div>
                                      <div class="col-md-8">
                                        <?php 
                                          echo $this->form->input('Promotion.users',array(
                                            'label'=>false,
                                            'name'=>'users',
                                            'placeholder'=>'users IDs',
                                            'type'=>'text',
                                            'value'=>$promotion['users'],
                                            'class'=>'form-control required',
                                            'div'=>'form-group'
                                          ));
                                        ?>
                                        <div class="input-sm">Enter user id with comma seperator. ex: 2213,2234,2341</div>
                                      </div>
                                  </div>
                                  <div class="row margin-none padding-bottom-lg">
                                      <div class="col-md-4 text-right"> Usage :</div>
                                      <div class="col-md-8">
                                        <div class="">
                                              <label class="padding-right-lg">
                                                <input name="usage_type" value="1" type="radio">
                                                <span class="lever">Single time</span>
                                              </label>
                                              <label>
                                                <input name="usage_type" value="2" type="radio">
                                                <span class="lever">Multiple time</span>
                                              </label>
                                        </div>
                                        <div class="input-sm">How many times user can use this promotion (usgae per user).</div>
                                      </div>
                                  </div>
                                  <div class="row margin-none padding-bottom-lg" id="promotionimage">
                                      <div class="col-md-4 text-right">Image : </div>
                                      <div class="col-md-8">
                                        <?php 
                                          echo $this->form->input('Promotion.image',array(
                                            'label'=>false,
                                            'name'=>'image',
                                            'type'=>'file',
                                            'placeholder'=>'Discount image',
                                            'class'=>'required',
                                            'div'=>'form-group'
                                          ));
                                        ?>
                                        <div class="input-sm">Upload discount image. Only png, jpg, jpeg extension files are allowed.</div>
                                      </div>
                                  </div>
                                  <div class="row margin-none padding-bottom-lg">
                                    <div class="col-md-4 text-right"> URL :</div>
                                    <div class="col-md-8">
                                      <?php 
                                        echo $this->form->input('Promotion.url',array(
                                          'label'=>false,
                                          'name'=>'url',
                                          'type'=>'text',
                                          'placeholder'=>'Promotion Redirect URL',
                                          'value'=>$promotion['url'],
                                          'class'=>'form-control required',
                                          'div'=>'form-group'
                                        ));
                                      ?>
                                      <div class="input-sm">Promotion redirect url where user will be redirected for more information.</div>
                                    </div>
                                  </div>
                                  <div class="row margin-none padding-bottom-lg padding-top-lg margin-top-lg">
                                      <div class="col-md-4 text-right"></div>
                                      <div class="col-md-8">
                                          <button class="updateBtn" type="submit">Save</button>       
                                      </div>
                                  </div>
                              </div>
                            <?php echo $this->Form->end(); ?>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script src="<?php echo $this->request->webroot; ?>admin_theme/plugins/daterangepicker/moment.min.js"></script>
<script src="<?php echo $this->request->webroot; ?>admin_theme/plugins/daterangepicker/daterangepicker.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() {
    var conditionaltype = 0;
    $('#reservation').daterangepicker();
    $('#offerAmount, #amountDiscount, #percentageDiscount, #userIDs, #conditionalRadio').css('display', 'none');
    $('input[type=radio][name=type]').on('change', function() {
      var that = $(this);
      var thatVal = that.val();
      
      if(thatVal == 1) {
        $('#percentageDiscount').css('display', 'block');
        $('#offerAmount, #amountDiscount, #conditionalRadio').css('display', 'none');
      } else if(thatVal == 2) {
        $('#amountDiscount').css('display', 'block');
        $('#offerAmount, #percentageDiscount, #conditionalRadio').css('display', 'none');
      } else {
        if(conditionaltype == 1){
          $( "[name=conditionaltype]" ).removeAttr('checked');
        }
        $('#conditionalRadio').css('display', 'block');
        $('#offerAmount, #amountDiscount, #percentageDiscount').css('display', 'none');
      }
    });
    $('input[type=radio][name=conditionaltype]').on('change', function() {
      var that = $(this);
      var thatVal = that.val();
      conditionaltype = 1;
      $('#offerAmount').css('display', 'block');
      if(thatVal == 1) {
        $('#percentageDiscount').css('display', 'block');
        $('#amountDiscount').css('display', 'none');
      } else if(thatVal == 2) {
        $('#amountDiscount').css('display', 'block');
        $('#percentageDiscount').css('display', 'none');
      }
    });
    $('input[type=radio][name=accessibility]').on('change', function() {
      var that = $(this);
      var thatVal = that.val();
      
      if(thatVal == 1) {
        $('#userIDs').css('display', 'none');
      } else if(thatVal == 2) {
        $('#userIDs').css('display', 'block');
      }
    });
});
</script>