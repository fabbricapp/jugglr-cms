<section class="content-header margin-bottom-lg padding-left-none">
    <h1>
        Settings
        <small>Page</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Settings</li>
    </ol>
</section>
<div class="row margin-none">
    <div class="col-xs-12 padding-none">
        <section class="content padding-none"> 
            <div class="box">
                <div class="box-header">
                    
                </div>
                <div class="box-body"> 
                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                            <?php echo $this->Form->create('User',array('id' => 'admin_setting','role'=>'form','url'=>'/legalpages/setting')); ?>
                              <div class="form-group">
                                  <div class="row margin-none padding-bottom-lg">
                                      <div class="col-md-4 text-right"></div>
                                      <div class="col-md-8 text-right">
                                        <?php 
                                          echo $this->form->input('Setting.id',array(
                                            'label'=>false,
                                            'name'=>'id',
                                            'type'=>'hidden',
                                            'value'=>$settings['id'],
                                            'class'=>'form-control required',
                                            'div'=>'form-group'
                                          ));
                                        ?>
                                      </div>
                                  </div>
                                  <div class="row margin-none padding-bottom-lg">
                                      <div class="col-md-4 text-right"> Jugglr Fee (%) :</div>
                                      <div class="col-md-8">
                                        <?php 
                                          echo $this->form->input('Setting.fee_percent',array(
                                            'label'=>false,
                                            'name'=>'fee_percent',
                                            'type'=>'text',
                                            'value'=>$settings['fee_percent'],
                                            'class'=>'form-control required',
                                            'div'=>'form-group'
                                          ));
                                        ?>
                                        <div class="input-sm">Percentage of transaction value charged as a Jugglr fee.</div>
                                      </div>
                                  </div>
                                  <div class="row margin-none padding-bottom-lg">
                                      <div class="col-md-4 text-right"> GST Tax (%) :</div>
                                      <div class="col-md-8">
                                        <?php 
                                          echo $this->form->input('Setting.gst_tax',array(
                                            'label'=>false,
                                            'name'=>'gst_tax',
                                            'type'=>'text',
                                            'value'=>$settings['gst_tax'],
                                            'class'=>'form-control required',
                                            'div'=>'form-group'
                                          ));
                                        ?>
                                        <div class="input-sm">GST Australia tax % of the Jugglr fee.</div>
                                      </div>
                                  </div>
                                  <div class="row margin-none padding-bottom-lg">
                                      <div class="col-md-4 text-right"> Stripe Fee (%) :</div>
                                      <div class="col-md-8">
                                        <?php 
                                          echo $this->form->input('Setting.stripe_fee',array(
                                            'label'=>false,
                                            'name'=>'stripe_fee',
                                            'type'=>'text',
                                            'value'=>$settings['stripe_fee'],
                                            'class'=>'form-control required',
                                            'div'=>'form-group'
                                          ));
                                        ?>
                                        <div class="input-sm">stripe fee %, deducted from the amount recieved by reciever.</div>
                                      </div>
                                  </div>
                                  <div class="row margin-none padding-bottom-lg">
                                      <div class="col-md-4 text-right"> Map Radius :</div>
                                      <div class="col-md-8">
                                        <?php 
                                          echo $this->form->input('Setting.map_radius',array(
                                            'label'=>false,
                                            'name'=>'map_radius',
                                            'type'=>'text',
                                            'value'=>$settings['map_radius'],
                                            'class'=>'form-control required',
                                            'div'=>'form-group'
                                          ));
                                        ?>
                                        <div class="input-sm">Service provider within this range(in miles) will be fetched by default in map view.</div>
                                      </div>
                                  </div>
                                  <div class="row margin-none padding-bottom-lg">
                                      <div class="col-md-4 text-right"> Max Payment :</div>
                                      <div class="col-md-8">
                                        <?php 
                                          echo $this->form->input('Setting.max_amount',array(
                                            'label'=>false,
                                            'name'=>'max_amount',
                                            'type'=>'text',
                                            'value'=>$settings['max_amount'],
                                            'class'=>'form-control required',
                                            'div'=>'form-group'
                                          ));
                                        ?>
                                        <div class="input-sm">Maximum amount displayed in dropdown when payment is selected as subservice in make offer.</div>
                                      </div>
                                  </div>
                                  <div class="row margin-none padding-bottom-lg">
                                      <div class="col-md-4 text-right"> iOS Map Zoom Level :</div>
                                      <div class="col-md-8">
                                        <?php 
                                          echo $this->form->input('Setting.ios_zoom_level',array(
                                            'label'=>false,
                                            'name'=>'ios_zoom_level',
                                            'type'=>'text',
                                            'value'=>$settings['ios_zoom_level'],
                                            'class'=>'form-control required',
                                            'div'=>'form-group'
                                          ));
                                        ?>
                                        <div class="input-sm">This would be the default iOS map zoom level when the app is launched.</div>
                                      </div>
                                  </div>
                                  <div class="row margin-none padding-bottom-lg">
                                      <div class="col-md-4 text-right"> Android Map Zoom Level :</div>
                                      <div class="col-md-8">
                                        <?php 
                                          echo $this->form->input('Setting.android_zoom_level',array(
                                            'label'=>false,
                                            'name'=>'android_zoom_level',
                                            'type'=>'text',
                                            'value'=>$settings['android_zoom_level'],
                                            'class'=>'form-control required',
                                            'div'=>'form-group'
                                          ));
                                        ?>
                                        <div class="input-sm">This would be the default Android map zoom level when the app is launched.</div>
                                      </div>
                                  </div>
                                  <div class="row margin-none padding-bottom-lg">
                                      <div class="col-md-4 text-right"> Android Current App Version :</div>
                                      <div class="col-md-8">
                                        <?php 
                                          echo $this->form->input('Setting.android_version',array(
                                            'label'=>false,
                                            'name'=>'android_version',
                                            'type'=>'text',
                                            'placeholder'=>'Android Version',
                                            'value'=>$settings['android_version'],
                                            'class'=>'form-control required',
                                            'div'=>'form-group'
                                          ));
                                        ?>
                                        <div class="input-sm">Set current version of Android application.</div>
                                      </div>
                                  </div>
                                  <div class="row margin-none padding-bottom-lg">
                                      <div class="col-md-4 text-right"> iOS Current App Version :</div>
                                      <div class="col-md-8">
                                        <?php 
                                          echo $this->form->input('Setting.ios_version',array(
                                            'label'=>false,
                                            'name'=>'ios_version',
                                            'type'=>'text',
                                            'placeholder'=>'iOS Version',
                                            'value'=>$settings['ios_version'],
                                            'class'=>'form-control required',
                                            'div'=>'form-group'
                                          ));
                                        ?>
                                        <div class="input-sm">Set current version of iOS application.</div>
                                      </div>
                                  </div>
                                  <div class="row margin-none padding-bottom-lg">
                                      <div class="col-md-4 text-right"> Update Options :</div>
                                      <div class="col-md-8">
                                        <div class="">
                                              <label class="padding-right-lg">
                                                <input name="update_option" value="0" type="radio">
                                                <span class="lever">Optional</span>
                                              </label>
                                              <label>
                                                <input name="update_option" value="1" type="radio">
                                                <span class="lever">Forcefull</span>
                                              </label>
                                        </div>
                                        <div class="input-sm">Set to ON if you want users forcefully updated Jugglr application.</div>
                                      </div>
                                  </div>
                                  <div class="row margin-none padding-bottom-lg">
                                      <div class="col-md-4 text-right"> From Address :</div>
                                      <div class="col-md-8">
                                        <?php 
                                          echo $this->form->input('Setting.from_address',array(
                                            'label'=>false,
                                            'name'=>'from_address',
                                            'type'=>'textarea',
                                            'value'=>$settings['from_address'],
                                            'class'=>'form-control required',
                                            'div'=>'form-group'
                                          ));
                                        ?>
                                        <div class="input-sm">From address that will appear in invoice.</div>
                                      </div>
                                  </div>
                                  <div class="row margin-none padding-bottom-lg">
                                      <div class="col-md-4 text-right"></div>
                                      <div class="col-md-8">
                                          <button class="updateBtn" type="submit">Save</button>       
                                      </div>
                                  </div>
                              </div>
                            <?php echo $this->Form->end(); ?>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">
jQuery(document).ready(function() {
  
});
</script>