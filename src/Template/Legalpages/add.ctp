<section class="content-header margin-bottom-lg padding-left-none">
    <h1>
        Static Page
        <small>Management</small>
    </h1>
</section>
<div class="row margin-none">
  <div class="col-xs-12 padding-none">
    <section class="content padding-none"> 
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Add Static Page</h3>
            </div>
            <div class="box-body"> 
                <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#add_termcondition" data-toggle="tab">Terms and Conditions</a></li>
                  <li><a href="#add_privatepolicy" data-toggle="tab">Private Policy</a></li>
                  <li><a href="#add_faq" data-toggle="tab">FAQs</a></li>
                </ul>
                <div class="tab-content">
                  <div class="active tab-pane" id="add_termcondition">
                      <?php echo $this->Form->create('addtermcondition', array( 'id' => 'addservice', 'class' => '')); ?>
                          <input name="service_type" value="services" type="hidden">
                          <div class="row margin-none padding-right">
                              <textarea id="editor1" name="editor1" rows="10" cols="80">
                                <?php echo $response['TC']['page_content'];?>
                              </textarea>
                          </div>
                          <div class="row margin">
                              <button class="CanacelBtn pull-right margin-left" type="button">Cancel</button>
                              <button class="updateBtn pull-right" type="submit">Save</button>
                          </div>
                      <?php echo $this->Form->end(); ?>
                  </div>
                  <div class="tab-pane" id="add_privatepolicy">
                      <?php echo $this->Form->create('addprivatepolicy', array( 'id' => 'addsubservice', 'class' => '')); ?>
                            <input name="service_type" value="subservices" type="hidden">
                            <div class="row margin-none padding-right">
                                <textarea id="editor2" name="editor2" rows="10" cols="80">
                                    <?php echo $response['PP']['page_content'];?>
                                </textarea>
                            </div>
                            <div class="row margin">
                                <button class="CanacelBtn pull-right margin-left" type="button">Cancel</button>
                                <button class="updateBtn pull-right" type="submit">Save</button>
                            </div>
                        <?php echo $this->Form->end(); ?>
                  </div>
                  <div class="tab-pane" id="add_faq">
                    <?php echo $this->Form->create('addfaq', array( 'id' => 'addsubservicetag', 'class' => '')); ?>
                        <input name="service_type" value="subservicetags" type="hidden">
                        <div class="row margin-none padding-right">
                            <textarea id="editor3" name="editor3" rows="10" cols="80">
                                <?php echo $response['FQ']['page_content'];?>
                            </textarea>
                        </div>
                        <div class="row margin">
                            <button class="CanacelBtn pull-right margin-left" type="reset">Cancel</button>
                            <button class="updateBtn pull-right" type="submit">Save</button>
                        </div>
                    <?php echo $this->Form->end(); ?>
                  </div>
                </div>
            </div>
        </div>
    </section>
  </div>
</div>
<script src="https://cdn.ckeditor.com/4.4.3/standard/ckeditor.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() {
  CKEDITOR.replace('editor1');
  CKEDITOR.replace('editor2');
  CKEDITOR.replace('editor3');
});
</script>