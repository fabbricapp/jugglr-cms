<div aria-hidden="true" role="dialog" tabindex="-1" id="confirmCopyItemModal" class="modal fade" style="display: none;">
  <div class="modal-dialog modal-dialog-sm" style="width: 425px !important;">
    <div class="modal-content">
      <div class="modal-header">
        <div aria-hidden="true" data-dismiss="modal" class="close pull-right" type="button">×</div>
        <h4 class="modal-title text-center"><?php echo $title;?></h4>
      </div>
      <div class="modal-body">
        <p class="margin-bottom-lg"><?php echo $message;?></p>
        <div class="form-group text-center">  
          <button data-dismiss="modal" itemvalue="" id="confirmCopyItem" class="btn btn-success confirmCopyItem margin-right-sm" deleteurl="<?php echo $controller.'/'.$action;?>">
            <i class="fa fa-check"></i> Proceed
          </button>          
          <button data-dismiss="modal" class="btn btn-danger" type="button">
            <i class="fa fa-warning"></i> Cancel
          </button>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).on('click','.copyItem',function(){
    var itemId;
    itemId = $(this).attr('itemvalue');
    $('#confirmCopyItem').attr('itemvalue', itemId);
});
$(document).on('click','#confirmCopyItem',function(){
    var user_id;
    user_id = $(this).attr('itemvalue');
    $.ajax({
      url: _ROOT+$(this).attr('deleteurl'),
      type: 'GET',
      dataType: 'Html',
      data:{'id':user_id},
      success:function(data){
        var result = jQuery.parseJSON( data );
          if(result == 1){
            $('table tr#'+user_id).remove();
          }
      }
    });
});
</script>