<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="<?php echo $authUser['image'];?>" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p><?php echo $authUser['name'];?></p>
      </div>
    </div>
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
      <li class="header">MAIN NAVIGATION</li>
      <li class="<?php if($current_page=='Users-index'){echo "active";}?> treeview">
        <a href="../users/index">
          <i class="fa fa-dashboard"></i> <span>Dashboard</span>
        </a>
      </li>
      <li class="<?php if($current_page=='Users-activeuser' || $current_page=='Users-pendinguser' || $current_page=='Users-inactiveuser' || $current_page=='Usertasks-userlistview' || $current_page=='Users-summary' || $current_page=='Users-add' || $current_page=='Users-businessuser' || $current_page=='Users-usersbyvolume'){echo "active";}?> treeview">
        <a href="#">
          <i class="fa fa-users"></i> <span>Manage User</span>
          <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li class="<?php if($current_page=='Users-add'){echo "active";}?>"><a href="../users/add"><i class="fa fa-circle-o"></i>Add Business Profile</a></li>
          <li class="<?php if($current_page=='Users-businessuser'){echo "active";}?>"><a href="../users/businessuser"><i class="fa fa-circle-o"></i>List Business Profile</a></li>
          <li class="<?php if($current_page=='Users-summary'){echo "active";}?>"><a href="../users/summary"><i class="fa fa-circle-o"></i>Users Summary</a></li>
          <li class="<?php if($current_page=='Usertasks-userlistview'){echo "active";}?>"><a href="../Usertasks/userlistview"><i class="fa fa-circle-o"></i>Users List View</a></li>
          <li class="<?php if($current_page=='Users-activeuser'){echo "active";}?>"><a href="../users/activeuser"><i class="fa fa-circle-o"></i> Active Users</a></li>
          <li class="<?php if($current_page=='Users-inactiveuser'){echo "active";}?>"><a href="../users/inactiveuser"><i class="fa fa-circle-o"></i> Inactive Users</a></li>
          <li class="<?php if($current_page=='Users-pendinguser'){echo "active";}?>"><a href="../users/pendinguser"><i class="fa fa-circle-o"></i> Pending Users</a></li>
          <li class="<?php if($current_page=='Users-usersbyvolume'){echo "active";}?>"><a href="../users/usersbyvolume"><i class="fa fa-circle-o"></i> Users ByVolume</a></li>
        </ul>
      </li>
      <li class="<?php if($current_page=='Services-add' || $current_page=='Services-edit' || $current_page=='Services-view'){echo "active";}?> treeview">
        <a href="#">
          <i class="fa fa-gear"></i>
          <span>Services</span>
          <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li class="<?php if($current_page=='Services-view'){echo "active";}?>"><a href="../services/view"><i class="fa fa-circle-o"></i> List Services</a></li>
          <li class="<?php if($current_page=='Services-add'){echo "active";}?>"><a href="../services/add"><i class="fa fa-circle-o"></i> Add Services</a></li>
          <li class="<?php if($current_page=='Services-edit'){echo "active";}?>"><a href="../services/edit"><i class="fa fa-circle-o"></i> Edit Services</a></li>
        </ul>
      </li>
      <li class="<?php if($current_page=='Usertasks-tasklist' || $current_page=='Usertasks-tasksummary'){echo "active";}?> treeview">
        <a href="#">
          <i class="fa fa-fw fa-tasks"></i> <span>Request/Task</span>
          <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li class="<?php if($current_page=='Usertasks-tasksummary'){echo "active";}?>"><a href="../usertasks/tasksummary"><i class="fa fa-circle-o"></i>Tasks Summary</a></li>
          <li class="<?php if($current_page=='Usertasks-tasklist'){echo "active";}?>"><a href="../usertasks/tasklist"><i class="fa fa-circle-o"></i> Tasks List</a></li>
        </ul>
      </li>
      <li class="<?php if($current_page=='Userjobs-joblist' || $current_page=='Userjobs-offerlist'){echo "active";}?> treeview">
        <a href="#">
          <i class="fa fa-fw fa-bullhorn"></i> <span>Marketplace</span>
          <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li class="<?php if($current_page=='Userjobs-joblist'){echo "active";}?>"><a href="../userjobs/joblist"><i class="fa fa-circle-o"></i>Jobs List</a></li>
          <li class="<?php if($current_page=='Userjobs-offerlist'){echo "active";}?>"><a href="../userjobs/offerlist"><i class="fa fa-circle-o"></i> Offers List</a></li>
        </ul>
      </li>
      <li class="<?php if($current_page=='Usertasks-taskview' || $current_page=='Users-ratingsummary'){echo "active";}?> treeview">
        <a href="#">
          <i class="fa fa-fw fa-thumbs-up"></i> <span>Ratings</span>
          <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li class="<?php if($current_page=='Users-ratingsummary'){echo "active";}?>"><a href="../Users/ratingsummary"><i class="fa fa-circle-o"></i>Rating Summary</a></li>
          <li class="<?php if($current_page=='Usertasks-taskview'){echo "active";}?>"><a href="../Usertasks/taskview"><i class="fa fa-circle-o"></i> Rating List</a></li>
        </ul>
      </li>
      <li class="<?php if($current_page=='Usertasks-invoicelist' || $current_page=='Usertasks-currentoffer'){echo "active";}?> treeview">
        <a href="#">
          <i class="fa fa-fw fa-credit-card"></i>
          <span>Invoices</span>
          <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li class="<?php if($current_page=='Usertasks-invoicelist'){echo "active";}?>"><a href="../usertasks/invoicelist"><i class="fa fa-circle-o"></i> Completed Offers</a></li>
          <li class="<?php if($current_page=='Usertasks-currentoffer'){echo "active";}?>"><a href="../usertasks/currentoffer"><i class="fa fa-circle-o"></i> Current Offers</a></li>
        </ul>
      </li>
      <li class="<?php if($current_page=='Promotions-add' || $current_page=='Promotions-view' || $current_page=='Promotions-informationalimage' || $current_page=='Promotions-informationalview'){echo "active";}?> treeview">
        <a href="#">
          <i class="fa fa-trophy"></i>
          <span>Promotions</span>
          <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li class="<?php if($current_page=='Promotions-view'){echo "active";}?>"><a href="../promotions/view"><i class="fa fa-circle-o"></i> List</a></li>
          <li class="<?php if($current_page=='Promotions-add'){echo "active";}?>"><a href="../promotions/add"><i class="fa fa-circle-o"></i> Add</a></li>
          <li class="<?php if($current_page=='Promotions-informationalview'){echo "active";}?>"><a href="../promotions/informationalview"><i class="fa fa-circle-o"></i> List Images</a></li>
          <li class="<?php if($current_page=='Promotions-informationalimage'){echo "active";}?>"><a href="../promotions/informationalimage"><i class="fa fa-circle-o"></i> Add Images</a></li>
        </ul>
      </li>
      <li class="<?php if($current_page=='Legalpages-setting' || $current_page=='Admins-account'){echo "active";}?> treeview">
        <a href="#">
          <i class="fa fa-book"></i>
          <span>General</span>
          <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <li class="<?php if($current_page=='Legalpages-setting'){echo "active";}?>"><a href="../legalpages/setting"><i class="fa fa-circle-o"></i> Settings</a></li>
          <li class="<?php if($current_page=='Admins-account'){echo "active";}?>"><a href="../admins/account"><i class="fa fa-circle-o"></i> Account</a></li>
          <li class="<?php if($current_page=='Admins-stat'){echo "active";}?>"><a href="../admins/stat"><i class="fa fa-circle-o"></i> Statistics</a></li>
        </ul>
      </li>
    </ul>
  </section>
</aside>
