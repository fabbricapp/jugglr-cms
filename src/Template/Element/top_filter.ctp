<form class="" method="get" id="filterform">
    <div class="col-md-3 search-box">
        <input type="text" placeholder="Search..." value="<?php echo isset($_GET['q']) ? $_GET['q'] : "" ?>" class="form-control" name="q">                               
    </div>
    <?php $action = (isset($action))?$action:'index'; ?>
    <?php $controller = (isset($controller))?$controller:'users'; ?>
    <div class="action" id="<?php echo $action;?>"></div>
    <div class="controller" id="<?php echo $controller;?>"></div>
    <?php if (isset($_GET['q'])) { ?>
        <div class="col-md-1 no-padding margin-top">
            <a class="" href="<?php echo $this->Url->build(array('action' => $action)) ?>">Reset</a>
        </div>
    <?php } ?>
    <div class="col-md-6 pull-right">
        <?php if ($csv_export == 1) { ?>
            <div class="col-md-3 no-padding pull-right">
                <?php if (isset($_GET['q'])) { $get_data = $_GET['q'];}else{$get_data = '';}?>
                <div class="get_data" id="<?php echo $get_data;?>"></div>
                <div class="btn btn-success csv_export">Export to CSV</div>
            </div>
        <?php } ?>
        <!--span class="select-span">entries</span>                                
        <span class="select-limit">
            <?php
            echo $this->Form->input('limit', array(
                'value' => (isset($_GET['limit']) ? $_GET['limit'] : ""),
                'type' => 'select',
                'options' => array(10 => 10, 25 => 25, 50 => 50, 100 => 100),
                'class' => 'form-control select2',
                'style' => 'width: 100%;',
                'label' => FALSE,
                'div' => FALSE,
                'id' => 'selectlimit'
            ));
            ?>
        </span>
        <span class="select-span">Show</span-->
    </div>                                
</form>
<script type="text/javascript">
jQuery(document).ready(function() {

    $('.csv_export').on("click", function () {
        var get_data = $('.get_data').attr('id');
        var action = $('.action').attr('id');
        var controller = $('.controller').attr('id');
        document.location.href = _ROOT+controller+'/exportcsvs?action='+action+'&get_data='+get_data;
        /*$.ajax({
          url: _ROOT+controller+'/'+action,
          type: 'POST',
          dataType: 'Json',
          data:{'q':get_date},
          success:function(data){
          }
        });*/
    });
});
</script>