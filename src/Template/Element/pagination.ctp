<div class="box-footer clearfix">
    <ul class="pagination pagination-sm no-margin pull-right">
        <?php
        echo $this->Paginator->first('First', array(), null, array('class' => 'first'));
        echo $this->Paginator->prev('Previous', array(), null, array('class' => 'prev disabled'));
        echo $this->Paginator->numbers(array('separator' => ''));
        echo $this->Paginator->next('Next', array(), null, array('class' => 'next disabled'));
        echo $this->Paginator->last('Last', array(), null, array('class' => 'last'));

        ?>
        
    </ul>
</div>

<script>
    $(document).ready(function () {
        $('th a').append(' <i class="icon-sort"></i>');
        $('th a.asc i').attr('class', 'icon-sort-down');
        $('th a.desc i').attr('class', 'icon-sort-up');
        $('.pagination li a').bind('click', function (e) {
            e.preventDefault();
            var this_url = $(this).attr('href').trim();
            if (this_url == '') {
                return false;
            } else {
                window.location.href = this_url;
            }
        });
    });
</script>