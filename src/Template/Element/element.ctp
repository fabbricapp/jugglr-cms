<!-- header -->

  <header id="header" class="app-header navbar" role="menu">

          <!-- navbar header -->

      <div class="navbar-header bg-dark">

        <button class="pull-right visible-xs dk" ui-toggle="show" target=".navbar-collapse">

          <i class="glyphicon glyphicon-cog"></i>

        </button>

        <button class="pull-right visible-xs" ui-toggle="off-screen" target=".app-aside" ui-scroll="app">

          <i class="glyphicon glyphicon-align-justify"></i>

        </button>

        <!-- brand -->

        <a href='<?php echo $this->Url->build(["controller" => "users", "action" => "index"]); ?>' class="navbar-brand text-lt">

      <span>Ascension Ladder</span>

         

        </a>

        <!-- / brand -->

      </div>

      <!-- / navbar header -->



      <!-- navbar collapse -->

      <div class="collapse pos-rlt navbar-collapse box-shadow bg-white-only">

        <!-- buttons -->

        <div class="nav navbar-nav hidden-xs">

          

        

        </div>

        <!-- / buttons -->



        <!-- link and dropdown -->

        <ul class="nav navbar-nav hidden-sm">

          <li class="dropdown pos-stc">

            

            <div class="dropdown-menu wrapper w-full bg-white">

              <div class="row">

                <div class="col-sm-4">

                  <div class="m-l-xs m-t-xs m-b-xs font-bold">Pages <span class="badge badge-sm bg-success">10</span></div>

                  <div class="row">

                    <div class="col-xs-6">

                      <ul class="list-unstyled l-h-2x">

                        <li>

                          <a href><i class="fa fa-fw fa-angle-right text-muted m-r-xs"></i>Profile</a>

                        </li>

                        <li>

                          <a href><i class="fa fa-fw fa-angle-right text-muted m-r-xs"></i>Post</a>

                        </li>

                        <li>

                          <a href><i class="fa fa-fw fa-angle-right text-muted m-r-xs"></i>Search</a>

                        </li>

                        <li>

                          <a href><i class="fa fa-fw fa-angle-right text-muted m-r-xs"></i>Invoice</a>

                        </li>

                      </ul>

                    </div>

                    <div class="col-xs-6">

                      <ul class="list-unstyled l-h-2x">

                        <li>

                          <a href><i class="fa fa-fw fa-angle-right text-muted m-r-xs"></i>Price</a>

                        </li>

                        <li>

                          <a href><i class="fa fa-fw fa-angle-right text-muted m-r-xs"></i>Lock screen</a>

                        </li>

                        <li>

                          <a href><i class="fa fa-fw fa-angle-right text-muted m-r-xs"></i>Sign in</a>

                        </li>

                        <li>

                          <a href><i class="fa fa-fw fa-angle-right text-muted m-r-xs"></i>Sign up</a>

                        </li>

                      </ul>

                    </div>

                  </div>

                </div>

                <div class="col-sm-4 b-l b-light">

                  <div class="m-l-xs m-t-xs m-b-xs font-bold">UI Kits <span class="label label-sm bg-primary">12</span></div>

                  <div class="row">

                    <div class="col-xs-6">

                      <ul class="list-unstyled l-h-2x">

                        <li>

                          <a href><i class="fa fa-fw fa-angle-right text-muted m-r-xs"></i>Buttons</a>

                        </li>

                        <li>

                          <a href><i class="fa fa-fw fa-angle-right text-muted m-r-xs"></i>Icons <span class="badge badge-sm bg-warning">1000+</span></a>

                        </li>

                        <li>

                          <a href><i class="fa fa-fw fa-angle-right text-muted m-r-xs"></i>Grid</a>

                        </li>

                        <li>

                          <a href><i class="fa fa-fw fa-angle-right text-muted m-r-xs"></i>Widgets</a>

                        </li>

                      </ul>

                    </div>

                    <div class="col-xs-6">

                      <ul class="list-unstyled l-h-2x">

                        <li>

                          <a href><i class="fa fa-fw fa-angle-right text-muted m-r-xs"></i>Bootstap</a>

                        </li>

                        <li>

                          <a href><i class="fa fa-fw fa-angle-right text-muted m-r-xs"></i>Sortable</a>

                        </li>

                        <li>

                          <a href><i class="fa fa-fw fa-angle-right text-muted m-r-xs"></i>Portlet</a>

                        </li>

                        <li>

                          <a href><i class="fa fa-fw fa-angle-right text-muted m-r-xs"></i>Timeline</a>

                        </li>

                      </ul>

                    </div>

                  </div>

                </div>

                <div class="col-sm-4 b-l b-light">

                  <div class="m-l-xs m-t-xs m-b-sm font-bold">Analysis</div>

                  <div class="text-center">

                    <div class="inline">

                      <div ui-jq="easyPieChart" ui-options="{

                          percent: 65,

                          lineWidth: 50,

                          trackColor: '#e8eff0',

                          barColor: '#23b7e5',

                          scaleColor: false,

                          size: 100,

                          rotate: 90,

                          lineCap: 'butt',

                          animate: 2000

                        }">

                      </div>

                    </div>

                  </div>

                </div>

              </div>

            </div>

          </li>

          <li class="dropdown">

           

            <ul class="dropdown-menu" role="menu">

              <li><a href="#" translate="header.navbar.new.PROJECT">Projects</a></li>

              <li>

                <a href>

                  <span class="badge bg-info pull-right">5</span>

                  <span translate="header.navbar.new.TASK">Task</span>

                </a>

              </li>

              <li><a href translate="header.navbar.new.USER">User</a></li>

              <li class="divider"></li>

              <li>

                <a href>

                  <span class="badge bg-danger pull-right">4</span>

                  <span translate="header.navbar.new.EMAIL">Email</span>

                </a>

              </li>

            </ul>

          </li>

        </ul>

        <!-- / link and dropdown -->



        <!-- search form -->

        <form class="navbar-form navbar-form-sm navbar-left shift" ui-shift="prependTo" data-target=".navbar-collapse" role="search" ng-controller="TypeaheadDemoCtrl">

          <div class="form-group">

            

          </div>

        </form>

        <!-- / search form -->


          <!-- link and dropdown -->
        <ul class="nav navbar-nav hidden-sm navbar-right">
          <li class="dropdown">
            <a href="javascript:void(0);" data-toggle="dropdown" class="dropdown-toggle">
              <i class="fa fa-fw fa-plus visible-xs-inline-block"></i>
               <span class="hidden-sm hidden-md">John.Smith</span>  <span class="caret"></span>
               <span class="thumb-sm avatar pull-right m-t-n-sm m-b-n-sm m-l-sm">
                  <img src="<?php echo $this->request->webroot . 'img/a0.jpg'; ?>" alt="...">
              </span>
            </a>
            <ul class="dropdown-menu" role="menu">
              <li><a href='<?php echo $this->request->webroot?>users/profile' translate="header.navbar.new.PROJECT">Profile</a></li>
              <li>
                <a href='<?php echo $this->request->webroot?>users/logout' >
                  <span translate="header.navbar.new.TASK">Logout</span>
                </a>
              </li>
            </ul>
          </li>
        </ul>
        <!-- / link and dropdown -->


        <!-- / navbar right -->

      </div>

      <!-- / navbar collapse -->



  </header>

  <!-- / header -->





