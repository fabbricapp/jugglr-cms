<header class="main-header">

        <!-- Logo -->
        <a href="../users/index" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><img height="30px" src="<?php echo $this->request->webroot;?>img/logo.png" alt="logo_image"></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg">
            <img height="30px" src="<?php echo $this->request->webroot;?>img/logo.png" alt="logo_image">
            <b>Jugglr</b>
          </span>
        </a>

        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <!-- Navbar Right Menu -->
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="<?php echo $authUser['image'];?>" class="user-image" alt="User Image">
                  <span class="hidden-xs"><?php echo $authUser['name'];?></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img src="<?php echo $authUser['image'];?>" class="img-circle" alt="User Image">
                    <p>
                      <?php echo $authUser['name'];?> - Admin
                    </p>
                  </li>
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      
                    </div>
                    <div class="pull-right">
                      <a href="../admins/logout" class="btn btn-loginpink btn-block btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
            </ul>
          </div>

        </nav>
      </header>