<link rel="stylesheet" href="<?php echo $this->request->webroot;?>css/custom-with-material.css">

<section class="content-header margin-bottom-lg padding-left-none">
    <h1>
        Marketplace
        <small>Offer List</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Offers List</li>
    </ol>
</section>
<div class="row margin-none">
  <div class="col-xs-12 padding-none">
    <section class="content padding-none"> 
        <div class="box">
            <div class="box-header">
                
            </div>
            <div class="box-body" style="overflow-y:auto;">
                <div class="row table-header-element">                        
                    <?php echo $this->element('top_filter'); ?>
                </div>
                <table id="example1" class="table table-hover">
                    <thead>
                        <tr>
                            <th>Sr. #</th>
                            <th>Offer ID</th>
                            <th>Owner</th>
                            <th>Assigned To</th>
                            <th>Status</th>
                            <th>Service</th>
                            <th>Date/Time</th>
                            <th>Location</th>
                            <th>Note</th>
                            <th>Amount</th>
                            <th>Discount</th>
                            <th>CreatedAt</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        $page = $this->Paginator->params()['page'];
                        $limit = $this->Paginator->params()['perPage'];
                        $i = ((($page * $limit) - $limit) * 2) + 1;
                        foreach($data as $key=>$value){ ?>
                        <tr>
                            <td><?php echo $i++;?></td>
                            <td><?php echo $value->id;?></td>
                            <td>
                                <?php echo $value->user->firstname;?>
                            </td>
                            <td>
                                <?php if(isset($value->userwith->firstname)){echo $value->userwith->firstname;}else{ echo "---";}?>
                            </td>
                            <td>
                                <?php if($value->status == 0){echo 'Expired';}elseif ($value->status == 1) {
                                    echo 'Open';
                                }elseif ($value->status == 2) {
                                    echo 'Completed';
                                }elseif ($value->status == 3) {
                                    echo 'Cancelled';
                                }?>
                            </td>
                            <td><?php echo $value->customservice;?></td>
                            <td><?php echo $value->date_formate;?></td>
                            <td><?php echo $value->location;?></td>
                            <td><?php echo $value->note;?></td>
                            <td><?php echo $value->amount;?></td>
                            <td><?php echo $value->discount;?></td>
                            <td><?php echo $value->created_formate;?></td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <?php echo $this->element('pagination'); ?>
            </div>
        </div>
    </section>
  </div>
</div>
