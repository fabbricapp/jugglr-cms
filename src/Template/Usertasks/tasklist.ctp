<link rel="stylesheet" href="<?php echo $this->request->webroot;?>css/custom-with-material.css">

<section class="content-header margin-bottom-lg padding-left-none">
    <h1>
        User Tasks
        <small>List</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">User Tasks List</li>
    </ol>
</section>
<div class="row margin-none">
  <div class="col-xs-12 padding-none">
    <section class="content padding-none"> 
        <div class="box">
            <div class="box-header">
                
            </div>
            <div class="box-body" style="overflow-y:auto;">
                <div class="row table-header-element">                        
                    <?php echo $this->element('top_filter'); ?>
                </div>
                <table id="example1" class="table table-hover">
                    <thead>
                        <tr>
                            <th>Sr. #</th>
                            <th><?php echo $this->Paginator->sort('Usertasks.id', 'Trans ID'); ?></th>
                            <th>Requestor</th>
                            <th>Service Provider</th>
                            <th>Transaction Status</th>
                            <th>Accept Date</th>
                            <th>Initiator</th>
                            <th>Service</th>
                            <th>Subservice</th>
                            <th>Service Date</th>
                            <th>Location</th>
                            <th>Note</th>
                            <th>Transaction Value</th>
                            <th>Transaction Fee</th>
                            <th>Transaction Tax</th>
                            <th>Invoice Value</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        $page = $this->Paginator->params()['page'];
                        $limit = $this->Paginator->params()['perPage'];
                        $i = ((($page * $limit) - $limit) * 2) + 1;
                        foreach($response as $key=>$value){ ?>
                        <tr>
                            <td><?php echo $i++;?></td>
                            <td><?php echo $value['id'];?></td>
                            <td>
                                <?php echo $value['requestor'];?>
                            </td>
                            <td>
                                <?php echo $value['serviceprovider'];?>
                            </td>
                            <td>
                                <?php echo $value['status'];?>
                            </td>
                            <td><?php echo $value['accept_date'];?></td>
                            <td><?php echo $value['initiator'];?></td>
                            <td><?php echo $value['service'];?></td>
                            <td><?php echo $value['subservice'];?></td>
                            <td><?php echo $value['date'];?></td>
                            <td><?php echo $value['location'];?></td>
                            <td><?php echo $value['note'];?></td>
                            <td><?php echo $value['transaction_value'];?></td>
                            <td><?php echo $value['transaction_fee'];?></td>
                            <td><?php echo $value['transaction_tax'];?></td>
                            <td><?php echo $value['invoice_value'];?></td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <?php echo $this->element('pagination'); ?>
            </div>
        </div>
    </section>
  </div>
</div>
