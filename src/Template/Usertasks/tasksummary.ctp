<link rel="stylesheet" href="<?php echo $this->request->webroot;?>css/custom-with-material.css">

<section class="content-header margin-bottom-lg padding-left-none">
    <h1>
        User Tasks
        <small>Summary</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">User Tasks Summary</li>
    </ol>
</section>
<div class="row margin-none">
  <div class="col-xs-12 padding-none">
    <section class="content padding-none"> 
        <div class="box">
            <div class="box-header">
                
            </div>
            <div class="box-body" style="overflow-y:auto;">
                <div class="row table-header-element">                        
                    <?php echo $this->element('top_filter'); ?>
                </div>
                <table id="example1" class="table table-hover">
                    <thead>
                        <tr>
                            <th>Sr. #</th>
                            <th>Subservice</th>
                            <th>Service</th>
                            <th>Total Volume</th>
                            <th>Total Value</th>
                            <th>Total Fee</th>
                            <th>Total Tax</th>
                            <th>Total Invoiced</th>
                            <th>Total Completed</th>
                            <th>Total Current</th>
                            <th>Total Cancelled</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        if (!empty($listing)) {
                            $page = $this->Paginator->params()['page'];
                            $limit = $this->Paginator->params()['perPage'];
                            $i = ($page * $limit) - $limit + 1;
                        foreach($listing as $key=>$value){ ?>
                        <tr>
                            <td><?php echo $i++;?></td>
                            <td><?php echo $value['subservice_name'];?></td>
                            <td><?php echo $value['service_name'];?></td>
                            <td><?php echo $value['volume'];?></td>
                            <td><?php echo $value['amount'];?></td>
                            <td><?php echo $value['fee'];?></td>
                            <td><?php echo $value['tax'];?></td>
                            <td><?php echo $value['invoiced'];?></td>
                            <td><?php echo $value['complete_cnt'];?></td>
                            <td><?php echo $value['current_cnt'];?></td>
                            <td><?php echo $value['cancelled_cnt'];?></td>
                        </tr>
                        <?php } 
                        }else{
                        ?>
                          <tr>
                              <td colspan="10" style="text-align: center;">
                                  No records found
                              </td>
                          </tr>
                      <?php } ?>
                    </tbody>
                </table>
                <?php echo $this->element('pagination'); ?>
            </div>
        </div>
    </section>
  </div>
</div>