<link rel="stylesheet" href="<?php echo $this->request->webroot;?>css/custom-with-material.css">

<section class="content-header margin-bottom-lg padding-left-none">
    <h1>
        Invoice
        <small>Management</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Completed Tasks</li>
    </ol>
</section>
<div class="row margin-none">
  <div class="col-xs-12 padding-none">
    <section class="content padding-none"> 
        <div class="box">
            <div class="box-header">
                
            </div>
            <div class="box-body" style="overflow-y:auto;">
                <div class="row table-header-element">                        
                    <?php echo $this->element('top_filter'); ?>
                </div>
                <table id="example1" class="table table-hover">
                    <thead>
                        <tr>
                            <th>Sr. #</th>
                            <th>Trans ID</th>
                            <th>Request</th>
                            <th>Provider</th>
                            <th>Status</th>
                            <th>Completion Date</th>
                            <th>Amount</th>
                            <th>Fee</th>
                            <th>Tax</th>
                            <th>Invoice Amount</th>
                            <th class="text-center">Invoice</th>
                            <!-- <th class="text-center">Pay Now</th> -->
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (!empty($listing)) {
                            $page = $this->Paginator->params()['page'];
                            $limit = $this->Paginator->params()['perPage'];
                            $i = ($page * $limit) - $limit + 1;
                        foreach($listing as $key=>$value){ ?>
                        <tr>
                            <td><?php echo $i++;?></td>
                            <td><?php echo $value['id'];?></td>
                            <td><?php echo $value['Requestor'];?></td>
                            <td><?php echo $value['Provider'];?></td>
                            <td>
                                <span class="label <?php if($value['status']=='Completed'){echo "label-success";}else{echo "label-warning";}?>"><?php echo $value['status'];?></span>
                            </td>
                            <td><?php echo $value['completion_date'];?></td>
                            <td><?php echo $value['amount'];?></td>
                            <td><?php echo $value['fee'];?></td>
                            <td><?php echo $value['tax'];?></td>
                            <td><?php echo $value['invoice_value'];?></td>
                            <td>
                                <a href = "<?php echo $this->Url->build(array('controller' => 'usertasks','action' => 'getinvoicedata', $value['id'])) ?>" class="ajaxviewmodel">
                                    <i class="fa fa-download"></i> Invoice
                                </a>
                            </td>
                            <!-- <td>
                                <a href="../usertasks/payByStrip">
                                    <button class="btn btn-primary pull-right" style="margin-right: 5px;"><i class="fa fa-download"></i> Pay Now</button>
                                </a>
                            </td> -->
                        </tr>
                        <?php } 
                        }else{
                        ?>
                          <tr>
                              <td colspan="10" style="text-align: center;">
                                  No records found
                              </td>
                          </tr>
                      <?php } ?>
                    </tbody>
                </table>
                <?php echo $this->element('pagination'); ?>
            </div>
        </div>
    </section>
  </div>
</div>