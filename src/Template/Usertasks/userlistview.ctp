<link rel="stylesheet" href="<?php echo $this->request->webroot;?>css/custom-with-material.css">

<section class="content-header margin-bottom-lg padding-left-none">
    <h1>
        User List
        <small>View</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">User List View</li>
    </ol>
</section>
<div class="row margin-none">
  <div class="col-xs-12 padding-none">
    <section class="content padding-none"> 
        <div class="box">
            <div class="box-header">
                
            </div>
            <div class="box-body" style="overflow-y:auto;">
                <div class="row table-header-element">                        
                    <?php echo $this->element('top_filter'); ?>
                </div>
                <table id="example1" class="table table-hover">
                    <thead>
                        <tr>
                           <th>Sr. #</th>
                           <th>Tran ID</th>
                           <th>FirstName</th>
                           <th>LastName</th>
                           <th>Email</th>
                           <th>Postcode</th>
                           <th>Suburb</th>
                           <th>City</th>
                           <th>State</th>
                           <th>Country</th>
                           <th>Status</th>
                           <th>Subservice Needed</th>
                           <th>Subservice Offered</th>
                           <th>Transaction Amount</th>
                           <th>Transaction Fee</th>
                           <th>Date Requested</th>
                           <th>Date Completed</th>
                           <th>Date Paid</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        if (!empty($response)) {
                            $page = $this->Paginator->params()['page'];
                            $limit = $this->Paginator->params()['perPage'];
                            $i = ($page * $limit) - $limit + 1;
                        foreach($response as $key=>$value){ ?>
                        <tr>
                            <td><?php echo $i++;?></td>
                            <td><?php echo $value['transaction_id'];?></td>
                            <td><?php echo $value['firstname'];?></td>
                            <td><?php echo $value['lastname'];?></td>
                            <td><?php echo $value['email'];?></td>
                            <td><?php echo $value['postcode'];?></td>
                            <td><?php echo $value['suburb'];?></td>
                            <td><?php echo $value['city'];?></td>
                            <td><?php echo $value['state'];?></td>
                            <td><?php echo $value['country'];?></td>
                            <td><?php echo $value['transaction_status'];?></td>
                            <td><?php echo $value['subservice_needed'];?></td>
                            <td><?php echo $value['subservice_offered'];?></td>
                            <td><?php echo $value['transaction_amount'];?></td>
                            <td><?php echo $value['transaction_fee'];?></td>
                            <td><?php echo $value['date_requested'];?></td>
                            <td><?php echo $value['date_completed'];?></td>
                            <td><?php echo $value['date_paid'];?></td>
                        </tr>
                        <?php } 
                        }else{
                        ?>
                          <tr>
                              <td colspan="10" style="text-align: center;">
                                  No records found
                              </td>
                          </tr>
                      <?php } ?>
                    </tbody>
                </table>
                <?php echo $this->element('pagination'); ?>
            </div>
        </div>
    </section>
  </div>
</div>