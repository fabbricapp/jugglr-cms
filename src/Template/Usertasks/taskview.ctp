<link rel="stylesheet" href="<?php echo $this->request->webroot;?>css/custom-with-material.css">

<section class="content-header margin-bottom-lg padding-left-none">
    <h1>
        User Ratings
        <small>List</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">User Rating List</li>
    </ol>
</section>
<div class="row margin-none">
  <div class="col-xs-12 padding-none">
    <section class="content padding-none"> 
        <div class="box">
            <div class="box-header">
                
            </div>
            <div class="box-body" style="overflow-y:auto;">
                <div class="row table-header-element">                        
                    <?php echo $this->element('top_filter'); ?>
                </div>
                <table id="example1" class="table table-hover">
                    <thead>
                        <tr>
                            <th>Sr. #</th>
                            <th>Trans ID</th>
                            <th>Provider</th>
                            <th>FeedbackBy</th>
                            <th>Initiator</th>
                            <th>Requested Date</th>
                            <th>Completion Date</th>
                            <th>Rating</th>
                            <th>Rating Date</th>
                            <th>Service</th>
                            <th>Subservice</th>
                            <th>Location</th>
                            <th>Note</th>
                            <th>Trans Value</th>
                            <th>Fee</th>
                            <th>Tax</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        if (!empty($response)) {
                            $page = $this->Paginator->params()['page'];
                            $limit = $this->Paginator->params()['perPage'];
                            $i = ($page * $limit) - $limit + 1;
                        foreach($response as $key=>$value){ ?>
                        <tr>
                            <td><?php echo $i++;?></td>
                            <td><?php echo $value['id'];?></td>
                            <td><?php echo $value['requestor'];?></td>
                            <td><?php echo $value['feedbackby'];?></td>
                            <td><?php echo $value['initiator'];?></td>
                            <td><?php echo $value['request_date'];?></td>
                            <td><?php echo $value['completion_date'];?></td>
                            <td><?php if($value['rating'] == '1'){echo "Positive";}elseif($value['rating'] == '2'){echo "Neutral";}?></td>
                            <td><?php echo $value['rating_date'];?></td>
                            <td><?php echo $value['service'];?></td>
                            <td><?php echo $value['subservice'];?></td>
                            <td><?php echo $value['location'];?></td>
                            <td><?php echo $value['note'];?></td>
                            <td><?php echo $value['transaction_value'];?></td>
                            <td><?php echo $value['transaction_fee'];?></td>
                            <td><?php echo $value['transaction_tax'];?></td>
                        </tr>
                        <?php } 
                        }else{
                        ?>
                          <tr>
                              <td colspan="10" style="text-align: center;">
                                  No records found
                              </td>
                          </tr>
                      <?php } ?>
                    </tbody>
                </table>
                <?php echo $this->element('pagination'); ?>
            </div>
        </div>
    </section>
  </div>
</div>