<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="pull-right" aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Invoice</h4>
        </div>
        <div class="modal-body clearfix padding-sm" id = "scroll">
            <section class="invoice margin-none">
                <div class="row">
                    <div class="col-xs-12">
                        <h2 class="page-header">
                            <i class="fa fa-globe"></i> Tax Invoice - Services exchanged through Jugglr
                            <small class="pull-right">Date: <?php echo $data['date'];?></small>
                        </h2>
                    </div>
                </div>
                <div class="row invoice-info">
                    <div class="col-sm-4 invoice-col">
                        From
                        <address style="width:65%;">
                            <strong>M3B Labs Pty Ltd.</strong><br>
                            <?php echo $data['admin_address'];?>
                        </address>
                    </div>
                    <div class="col-sm-4 invoice-col">
                        To
                        <address>
                            <strong><?php echo $data['name'];?></strong><br>
                            <?php if($data['postcode'] != ''){echo $data['postcode'].", ";} if($data['suburb']!=''){echo $data['suburb'];}?><br>
                            <?php if($data['city']!=''){echo $data['city'].", ";} if($data['state']!=''){echo $data['state'].",<br>";}?>
                            <?php if($data['country']!=''){echo $data['country']."<br>";}?>
                            Email: <?php echo $data['email'];?>
                        </address>
                    </div>
                    <div class="col-sm-4 invoice-col">
                        <b>Invoice #<?php echo $data['invoice_no'];?></b><br>
                        <br>
                        <b>Invoice Date:</b> <?php echo $data['date'];?><br>
                        <b>Account Charged:</b> <?php echo $data['account_no'];?><br>
                        <b>Payment Due:</b> <?php echo $data['date'];?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 table-responsive margin-top-lg margin-bottom-lg">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Service ID</th>
                                    <th>Service Provider</th>
                                    <th>Description</th>
                                    <th>Completion Date</th>
                                    <th>Location</th>
                                    <th>Price</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><?php echo $data['item'];?></td>
                                    <td><?php echo $data['service_id'];?></td>
                                    <td><?php echo $data['service_provider_name'];?></td>
                                    <td><?php echo $data['description'];?></td>
                                    <td><?php echo $data['completion_date'];?></td>
                                    <td><?php echo $data['location'];?></td>
                                    <td><?php echo $data['total'];?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-6">
                        <p class="lead">Payment Methods:</p>
                        <img src="../img/stripe_logo.jpg" alt="Visa">
                        <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                          Payment through Jugglr using Stripe.
                        </p>
                    </div>
                    <div class="col-xs-6">
                        <p class="lead">Total Amount Payable</p>
                        <div class="table-responsive">
                            <table class="table">
                                <tr>
                                    <th style="width:50%">Total Price:</th>
                                    <td><?php echo $data['total'];?></td>
                                </tr>
                                <tr>
                                    <th>Fee (<?php echo $data['fee_percent'];?>%):</th>
                                    <td><?php echo $data['fee_amount'];?></td>
                                </tr>
                                <tr>
                                    <th>GST (<?php echo $data['gst_tax'];?>%):</th>
                                    <td><?php echo $data['tax_amount'];?></td>
                                </tr>
                                <tr>
                                    <th>Total:</th>
                                    <td><?php echo $data['grand_total'];?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
		<?php if($data['status'] == 0 && $data['total'] > 0){?>
		        <div class="row">
		            <div class="col-md-12 text-right">
		                <div class="btn btn-success initiate_payment" id="<?php echo $data['id'];?>">Initiate Payment</div>
		            </div>
		        </div>
		<?php }?>
            </section>
        </div>
    </div>
</div>
<script type="text/javascript">
jQuery(document).ready(function() {
    $(document).on('click', '.initiate_payment', function (event) {
        var offer_id = $(this).attr("id");
	$(this).attr('disabled','disabled');
        $.ajax({
          url: _ROOT+'usertasks/payByStrip',
          type: 'GET',
          dataType: 'Json',
          data:{'offer_id':offer_id},
          success:function(data){
		if(data == 1){
	              location.reload();
		}else {
			alert('Transaction Failed!');
		}
          }
        });
    });
});
