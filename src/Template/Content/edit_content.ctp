<div class="bg-light lter b-b wrapper-md" style="margin-top: 20px;">
  <a href = "<?php echo $this->request->webroot?>content">Content</a>
  <span style = "color:black;">/ Edit Content</span> 
</div>


<div class="wrapper-md">

  <div class="panel panel-default">
    <div class="panel panel-default users">
      <div class="panel-heading font-bold">
         Edit Content info
      </div>
   <div class="panel-body">
      <?php echo $this->Form->create('Service', array('id' => 'form-input-data', 'class' => '')); ?>
      <input id="id" type="hidden" name="id" value = "<?php echo $result->id;?>">
      
        <div class="col-md-12">
          <div class="block"> 
               <div class="form-horizontal" >
                 <div class="input text">
                  <label class="col-md-2 control-label input-label" for="username">Page Name</label>
                    <div class= "col-md-5">
                      <input id="name" class="form-control required" disabled = "disabled" type="text" name="UserDetails[name]" value = "<?php echo $result->page_name ?>">
                    </div>
                 </div>
            </div>            
          </div>                   
       </div>   
    
        <div class="line line-dashed b-b line-lg pull-in"></div>

        <div class="col-md-12">
          <div class="block"> 
              <div class="form-horizontal" >
                <div class="input text">
                  <label class="col-md-2 control-label input-label" for="username">Content</label>
                    <div class= "col-md-5">
                      <input id="username" class="form-control required" type="text" rows="3" cols="2"   name="description" value = "<?php echo $result->description ?>">
                    </div>
                </div>
            </div>               
          </div>                   
       </div> 

       <!--  <div class="line line-dashed b-b line-lg pull-in"></div>
         <div class="col-md-12">
           <div class="block"> 
              <div class="form-horizontal" >
                 <div class="input text">
                  <label class="col-md-1 control-label input-label" for="username">Password</label>
                    <div class= "col-md-5">
                      <input id="password" class="form-control required" type="password" name="password">
                    </div>
                </div>
              </div>                
           </div>                   
         </div>  -->


        <div class="line line-dashed b-b line-lg pull-in"></div>
       
<div class="col-sm-12">
  <button type="submit" class="btn btn-sm btn-primary" style="float:right;margin-bottom:15px;">Submit</button>
</div>
<div class="line line-dashed b-b line-lg pull-in"></div>
<?php echo $this->Form->end(); ?> 

      </div>
    </div>
  </div>
</div>



