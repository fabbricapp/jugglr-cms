

<body>  
 <div class="bg-light lter b-b wrapper-md" style="margin-top: 20px;">
  <h1 class="m-n font-thin h3">Content Pages</h1>
 </div>

<div class="wrapper-md">

  <div class="panel panel-default">

  <a href='<?php echo $this->Url->build(["controller" => "content", "action" => "add_content"]); ?>'  <?php if ($this->request->params['action'] == 'add_content') echo 'class="active"'; ?>>



  <button type="submit" class="btn btn-sm btn-primary" style="float:right;margin-right:22px;margin-top:1px;">Add content</button></a>

  
    <div class="panel-heading">Content Pages</div>

    <div class="table-responsive">

      <table ui-jq="dataTable" ui-options="{

          sAjaxSource: 'api/datatable.json',

          aoColumns: [

            { mData: 'engine' },

            { mData: 'browser' },

            { mData: 'platform' },

            { mData: 'version' },

            { mData: 'grade' }

          ]

        }" class="table table-striped m-b-none">

        <thead>
          <tr>
            <th  style="width:30%">Page Name</th>
            <th  style="width:40%">Content</th>
            <th  style="width:10%">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($query as $user_key => $user_value) {
          

          ?>
            <tr>
              <td>
              <?php echo $user_value->page_name; ?>
              </td>
              <td>
                <?php  echo $user_value->description;?>
              </td>
              
           <td>
             <a href="<?php echo $this->request->webroot?>content/edit_content/<?php echo $user_value->id ?>">
               <span class="glyphicon glyphicon-pencil"></span></a>
             <a href="<?php echo $this->request->webroot?>content/delete_content/<?php echo $user_value->id ?>">
               <span class="glyphicon glyphicon-trash"></span>
             </a>
           </td>

          </tr>
        
<?php  
}?>


       

        </tbody>

      </table>

    </div>



  </div>





    </div>



</body>

</html>