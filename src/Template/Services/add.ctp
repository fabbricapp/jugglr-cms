<link rel="stylesheet" href="<?php echo $this->request->webroot;?>css/custom-with-material.css">
<section class="content-header margin-bottom-lg padding-left-none">
    <h1>
        Service
        <small>Management</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Service</li>
    </ol>
</section>
<div class="row margin-none">
  <div class="col-xs-12 padding-none">
    <section class="content padding-none"> 
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Add Services</h3>
            </div>
            <div class="box-body"> 
                <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#add_service" data-toggle="tab">Services</a></li>
                  <li><a href="#add_sub_service" data-toggle="tab">Sub-Service</a></li>
                  <li><a href="#add_sub_service_tag" data-toggle="tab">Sub-Service Tag</a></li>
                </ul>
                <div class="tab-content">
                  <div class="active tab-pane" id="add_service">
                      <?php echo $this->Form->create('addservice', array( 'id' => 'addservice', 'class' => '')); ?>
                          <input name="service_type" value="services" type="hidden">
                          <div class="row margin-none">
                              <div class="col-md-6 servicebox">
                                <div class="input-field ServicecloneInput">
                                    <input id="service_name" name="service_name[]" type="text">
                                    <label for="service_name">Service Name <i class="colorRed">*</i></label>
                                </div>
                              </div>
                              <div class="col-md-6 text-right">
                                  <span class="pull-left">
                                      <input type="checkbox" name="type" class="filled-in" id="filled-in-box" />
                                      <label for="filled-in-box">Business Service</label>
                                  </span>
                                  <span data-toggle="tooltip" title="Add New Service" class="btn btn-success Serviceclone"><i class="fa fa-plus"></i></span>
                              </div>
                          </div>
                          <div class="row margin-none">
                              <div class="col-md-6 addServiceclass">
                              </div>
                              <div class="col-md-6">
                              </div>
                          </div>
                          <div class="row margin-none padding-right">
                              <button class="CanacelBtn pull-right margin-left" type="button">Cancel</button>
                              <button class="updateBtn pull-right" type="submit">Save</button>
                          </div>
                      <?php echo $this->Form->end(); ?>
                  </div>
                  <div class="tab-pane" id="add_sub_service">
                      <?php echo $this->Form->create('addsubservice', array( 'id' => 'addsubservice', 'class' => '')); ?>
                          
                            <div class="row margin-none">
                                <div class="col-md-6">      
                                    <div class="row margin-left-none input-field">
                                        <select name="service_id" class="form-control">
                                          <option selected="" disabled="" value="">Choose service</option>
                                          <?php foreach($services as $key=>$value){ ?>
                                              <option value="<?php echo $value['id'];?>"><?php echo $value['service_name'];?></option>
                                          <?php } ?>
                                        </select>
                                    </div>
                                    <div class="row margin-left-none">
                                        <span class="description">Select service to add its related sub services.</span>
                                    </div>
                                    <div class="subservicebox">
                                      <div class="row margin-left-none input-field SubserviceclonedInput">
                                          <input id="sub_service_name" name="service_name[]" type="text">
                                          <label for="sub_service_name">Sub-Service Name <i class="colorRed">*</i></label>
                                      </div>
                                    </div>
                                </div>
                                <div class="col-md-6 text-right">
                                    <span data-toggle="tooltip" title="Add New Subservice" class="btn btn-success Subserviceclone"><i class="fa fa-plus"></i></span>
                                </div>
                            </div>
                            <div class="row margin-left-none margin-none">
                                <div class="col-md-6 addSubserviceclass">
                                </div>
                                <div class="col-md-6">
                                </div>
                            </div>
                            <div class="row margin-none padding-right">
                                <button class="CanacelBtn pull-right margin-left" type="button">Cancel</button>
                                <button class="updateBtn pull-right" type="submit">Save</button>
                            </div>
                            <input name="service_type" value="subservices" type="hidden">
                        <?php echo $this->Form->end(); ?>
                  </div>
                  <div class="tab-pane" id="add_sub_service_tag">
                    <?php echo $this->Form->create('addsubservicetag', array( 'id' => 'addsubservicetag', 'class' => '')); ?>
                        <input name="service_type" value="subservicetags" type="hidden">
                        <div class="row margin-none">
                            <div class="col-md-6">
                                <div class="row margin-left-none input-field">
                                    <select name="subservice_id" class="form-control">
                                        <?php foreach($subservices as $key=>$value){ ?>
                                            <optgroup label="<?php echo $value['service_name'];?>">
                                                <?php foreach($value['subservice'] as $key1=>$val){ ?>
                                                    <option value="<?php echo $key1;?>"><?php echo $val['subservice_name'];?></option>
                                                <?php } ?>
                                            </optgroup>
                                        <?php } ?>
                                      </select>
                                </div>
                                <div class="row margin-left-none">
                                    <span class="description">Select subservice to add its related sub services tags.</span>
                                </div>
                                <div class="subservicetagbox">
                                  <div class="row margin-left-none input-field SubservicetagclonedInput">
                                      <input id="sub_service_tag_name" name="service_name[]" type="text">
                                      <label for="sub_service_tag_name">Sub-Service Tag Name <i class="colorRed">*</i></label>
                                  </div>
                                </div>
                            </div>
                            <div class="col-md-6 text-right">
                                <span data-toggle="tooltip" title="Add New Subservicetag" class="btn btn-success Subservicetagclone"><i class="fa fa-plus"></i></span>
                            </div>
                        </div>
                        <div class="row margin-left-none">
                            <div class="col-md-6 addSubservicetagclass">
                            </div>
                            <div class="col-md-6">
                            </div>
                        </div>
                        <div class="row margin-none padding-right">
                            <button class="CanacelBtn pull-right margin-left" type="reset">Cancel</button>
                            <button class="updateBtn pull-right" type="submit">Save</button>
                        </div>
                    <?php echo $this->Form->end(); ?>
                  </div>
                </div>
            </div>
        </div>
    </section>
  </div>
</div>
<script type="text/javascript">
jQuery(document).ready(function() {
  $('.Subserviceclone').click(function () {
    var test =  $(".subservicebox").children('.SubserviceclonedInput').clone()
    $(".addSubserviceclass").append(test);
  });
  $('.Serviceclone').click(function () {
    var test =  $(".servicebox").children('.ServicecloneInput').clone()
    $(".addServiceclass").append(test);
  });
  $('.Subservicetagclone').click(function () {
    var test =  $(".subservicetagbox").children('.SubservicetagclonedInput').clone()
    $(".addSubservicetagclass").append(test);
  });
});
</script>