<section class="content-header margin-bottom-lg padding-left-none">
    <h1>
        Service
        <small>Management</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">View Service</li>
    </ol>
</section>
<div class="row margin-none">
  <div class="col-xs-12 padding-none">
    <section class="content padding-none"> 
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">View Services</h3>
            </div>
            <div class="box-body"> 
                <table with="100%" id="example2" class="table table-bordered">
                  <thead>
                      <tr>
                          <th>Sr. #</th>
                          <th>Services</th>
                          <th>Subservices</th>
                          <th>Edit</th>
                      </tr>
                  </thead>
                  <tbody>
                    <?php foreach($services as $key=>$value){ ?>
                      <tr with="100%">
                            <td with="10%"><?php echo $value['service_id'];?></td>
                            <td with="40%"><?php echo $value['service_name'];?></td>
                            <td with="50%" class="padding-none">
                                <?php if(isset($value['subservice'])){ ?>
                                    <table id="" class="table">
                                        <tbody>
                                          <?php foreach($value['subservice'] as $key1=>$value1){ ?>
                                              <tr with="100%">
                                                  <td with="50%"><?php echo $value1['subservice_name'];?></td>
                                                  <td with="50%" class="padding-none">
                                                      <?php if(isset($value1['subservicetag'])){ ?>
                                                            <?php foreach($value1['subservicetag'] as $key2=>$value2){ ?>
                                                                  <?php echo $value2.", ";?>
                                                            <?php } ?>
                                                      <?php }?>
                                                  </td>
                                              </tr>
                                          <?php } ?>
                                        </tbody>
                                    </table>
                                <?php }?>
                            </td>
                            <td>
                              <div class="padding text-center">
                                <a data-toggle="tooltip" title="edit" href="<?php echo $this->Url->build(["controller" => "services", "action" => "edit"]); ?>"><i class="fa fa-edit"></i></a>
                              </div>
                            </td>
                      </tr>
                      <?php } ?>
                  </tbody>
                </table>
            </div>
        </div>
    </section>
  </div>
</div>
<script type="text/javascript">
jQuery(document).ready(function() {
  
});
</script>