<link rel="stylesheet" href="<?php echo $this->request->webroot;?>css/custom-with-material.css">
<section class="content-header margin-bottom-lg padding-left-none">
    <h1>
        Service
        <small>Management</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Service Edit</li>
    </ol>
</section>
<div class="row margin-none">
  <div class="col-xs-4 notification-alert padding-none pull-right">
      <div id="notification_success" class="alert alert-success alert-dismissable display-none">
          Updated successfully..!!
      </div>
      <div id="notification_fail" class="alert alert-danger alert-dismissable display-none">
          Oops..something went wrong please try again..!!
      </div>
  </div>
  <div class="col-xs-12 padding-none">
    <section class="content padding-none"> 
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Edit Services</h3>
            </div>
            <div class="box-body"> 
                <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#add_service" data-toggle="tab">Services</a></li>
                  <li><a href="#add_sub_service" data-toggle="tab">Sub-Service</a></li>
                  <li><a href="#add_sub_service_tag" data-toggle="tab">Sub-Service Tag</a></li>
                </ul>
                <div class="tab-content">
                  <div class="active tab-pane" id="add_service">
                      <?php foreach($services as $key=>$value){ ?>
                          <div class="row margin-none">
                              <div class="col-md-6">
                                  <div class="input-field ServicecloneInput">
                                      <input id="servicename_<?php echo $value['id'];?>" name="service_<?php echo $value['id'];?>" type="text" value="<?php echo $value['service_name'];?>" <?php if($value['is_active'] != '1'){echo "readonly";} ?>>
                                  </div>
                              </div>
                              <div id="serviceactionblock_<?php echo $value['id'];?>" class="col-md-3 padding-none padding-top-lg">
                                  <?php if($value['is_active'] == '1'){?>
                                      <a href="javascript:void(0)" id="<?php echo $value['id'];?>" class="saveserviceBtn btn">
                                          Save
                                      </a> /
                                      <a href="javascript:void(0)" id="<?php echo $value['id'];?>" class="inactiveserviceBtn btn">
                                          Make Inactive
                                      </a>
                                  <?php }else { ?>
                                      <a href="javascript:void(0)" id="<?php echo $value['id'];?>" class="activeserviceBtn btn">
                                          Make Active
                                      </a>
                                  <?php } ?>
                              </div>
                              <div class="col-md-3"></div>
                          </div>
                      <?php } ?>
                  </div>
                  <div class="tab-pane" id="add_sub_service">
                      <div class="row margin-none">
                          <div class="col-md-6">      
                              <div class="row margin-left-none input-field">
                                  <select name="service_id" class="form-control selectService">
                                    <option selected="" disabled="" value="">Choose service</option>
                                    <?php foreach($services as $key=>$value){ ?>
                                        <option value="<?php echo $value['id'];?>"><?php echo $value['service_name'];?></option>
                                    <?php } ?>
                                  </select>
                              </div>
                              <div class="row margin-left-none">
                                  <span class="description">Select service to edit its related sub services.</span>
                              </div>
                          </div>
                          <div class="col-md-6">
                          </div>
                      </div>
                      <div class="updateSubserviceclass">
                      </div>
                  </div>
                  <div class="tab-pane" id="add_sub_service_tag">
                      <div class="row margin-none">
                          <div class="col-md-6">
                              <div class="row margin-left-none input-field">
                                  <select name="subservice_id" class="form-control selectSubService">
                                    <?php foreach($subservices as $key=>$value){ ?>
                                        <optgroup label="<?php echo $value['service_name'];?>">
                                            <?php foreach($value['subservice'] as $key1=>$val){ ?>
                                                <option value="<?php echo $key1;?>"><?php echo $val['subservice_name'];?></option>
                                            <?php } ?>
                                        </optgroup>
                                    <?php } ?>
                                  </select>
                              </div>
                              <span class="description">Select Subservice to edit its related services tags.</span>
                          </div>
                          <div class="col-md-6">
                          </div>
                      </div>
                      <div class="updateSubservicetagclass">
                      </div>
                  </div>
                </div>
            </div>
        </div>
    </section>
  </div>
</div>
<script type="text/javascript">
jQuery(document).ready(function() {
    $('.selectService').change(function () {
        var service_id = $('.selectService').val();
        $.ajax({
          url: _ROOT+'services/getsubservicesandtags',
          type: 'GET',
          dataType: 'Html',
          data:{'service_id':service_id,'gettype':'subservice'},
          success:function(data){
              var result = jQuery.parseJSON( data );
              var html = '';
              $(".updateSubserviceclass").html(html);
              $.each( result, function( key, value ) {
                  html += '<div class="row margin-none"><div class="col-md-6"><div class="input-field">';
                  html += '<input id="subservice_'+value.id+'" name="subservice_'+value.id+'" class="margintop20" type="text" value="'+ value.subservice_name +'"></div></div><div id="subserviceactionblock_'+value.id+'" class="col-md-4 padding-none padding-top-lg margin-top-lg">';
                  if(value.is_active == 1){
                    html += '<a href="javascript:void(0)" id="'+value.id+'" class="savesubserviceBtn btn">Save</a> / ';
                    html += '<a href="javascript:void(0)" id="'+value.id+'" class="inactivesubserviceBtn btn">Make InActive</a>';
                  }
                  if(value.is_active == 0){
                    html += '<a href="javascript:void(0)" id="'+value.id+'" class="activesubserviceBtn btn">Make Active</a>';
                  }
                  html += '</div></div>';
              });
              $(".updateSubserviceclass").append(html);
          }
        });
      });

    $('.selectSubService').change(function () {
        var service_id = $('.selectSubService').val();
        $.ajax({
          url: _ROOT+'services/getsubservicesandtags',
          type: 'GET',
          dataType: 'Html',
          data:{'subservice_id':service_id,'gettype':'servicetag'},
          success:function(data){
              var result = jQuery.parseJSON( data );
              var html = '';
              $(".updateSubservicetagclass").html(html);
              $.each( result, function( key, value ) {
                  html += '<div class="row margin-none"><div class="col-md-6"><div class="input-field">';
                  html += '<input id="subservicetag_'+value.id+'" name="subservicetag_'+value.id+'" class="margintop20" type="text" value="'+ value.subservicetag_name +'"></div></div><div id="subservicetagactionblock_'+value.id+'" class="col-md-4 padding-none padding-top-lg margin-top-lg">';
                  if(value.is_active == 1){
                    html += '<a href="javascript:void(0)" id="'+value.id+'" class="savesubservicetagBtn btn">Save</a> / ';
                    html += '<a href="javascript:void(0)" id="'+value.id+'" class="inactivesubservicetagBtn btn">Make InActive</a>';
                  }
                  if(value.is_active == 0){
                    html += '<a href="javascript:void(0)" id="'+value.id+'" class="activesubservicetagBtn btn">Make Active</a>';
                  }
                  html += '</div></div>';
              });
              $(".updateSubservicetagclass").append(html);
          }
        });
      });

      $('#add_service').on("click", ".saveserviceBtn",function () {
        var service_id = $(this).attr("id");
        var service_name = $("#servicename_"+service_id).val();
        if(service_name != ''){
            $.ajax({
              url: _ROOT+'services/updateservice',
              type: 'POST',
              dataType: 'Json',
              data:{'service_id':service_id,'service_name':service_name},
              success:function(data){
                  var result = jQuery.parseJSON( data );
                  if(data == true){
                    $('#notification_success').fadeIn('fast').delay(2000).fadeOut('slow');
                  }else if(data == 0) {
                    $('#notification_fail').fadeIn('fast').delay(2000).fadeOut('slow');
                  }
              }
            });
        }else {
          $('#notification_fail').fadeIn('fast').delay(2000).fadeOut('slow');
        }
      });

      $('#add_service').on("click", ".inactiveserviceBtn",function () {
        var result = confirm("Are you sure you want to inactive this service?");
        if (result) {
          var service_id = $(this).attr("id");
          var thiselement = $(this);
          $.ajax({
            url: _ROOT+'services/updateservice',
            type: 'POST',
            dataType: 'Json',
            data:{'service_id':service_id,'is_active':'0'},
            success:function(data){
                var result = jQuery.parseJSON( data );
                $(thiselement).addClass('activeserviceBtn').removeClass('inactiveserviceBtn');
                if(data == true){
                  $('#notification_success').fadeIn('fast').delay(2000).fadeOut('slow');
                }else if(data == 0) {
                  $('#notification_fail').fadeIn('fast').delay(2000).fadeOut('slow');
                }
                $('#serviceactionblock_'+service_id).html('<a href="javascript:void(0)" id="'+service_id+'" class="activeserviceBtn btn">Make Active</a>');
            }
          });
        }
      });

      $('#add_service').on("click", ".activeserviceBtn",function () {
        var result = confirm("Are you sure you want to make active this service?");
        if (result) {
          var service_id = $(this).attr("id");
          var thiselement = $(this);
          $.ajax({
            url: _ROOT+'services/updateservice',
            type: 'POST',
            dataType: 'Json',
            data:{'service_id':service_id,'is_active':'1'},
            success:function(data){
                var result = jQuery.parseJSON( data );
                $(thiselement).addClass('inactiveserviceBtn').removeClass('activeserviceBtn');
                if(data == true){
                  $('#notification_success').fadeIn('fast').delay(2000).fadeOut('slow');
                }else if(data == 0) {
                  $('#notification_fail').fadeIn('fast').delay(2000).fadeOut('slow');
                }
                $('#serviceactionblock_'+service_id).html('<a href="javascript:void(0)" id="'+service_id+'" class="saveserviceBtn btn">Save</a> / <a href="javascript:void(0)" id="'+service_id+'" class="inactiveserviceBtn btn">Make Inactive</a>');
            }
          });
        }
      });

      $('.updateSubserviceclass').on("click", ".savesubserviceBtn",function () {
        var subservice_id = $(this).attr("id");
        var subservice_name = $("#subservice_"+subservice_id).val();
        if(subservice_name != ''){
            $.ajax({
              url: _ROOT+'services/updatesubservice',
              type: 'POST',
              dataType: 'Json',
              data:{'subservice_id':subservice_id,'subservice_name':subservice_name},
              success:function(data){
                  var result = jQuery.parseJSON( data );
                  if(data == true){
                    $('#notification_success').fadeIn('fast').delay(2000).fadeOut('slow');
                  }else if(data == 0) {
                    $('#notification_fail').fadeIn('fast').delay(2000).fadeOut('slow');
                  }
              }
            });
        }else {
          $('#notification_fail').fadeIn('fast').delay(2000).fadeOut('slow');
        }
      });

      $('.updateSubserviceclass').on("click",".inactivesubserviceBtn",function () {
        var result = confirm("Are you sure you want to inactive this subservice?");
        if (result) {
          var subservice_id = $(this).attr("id");
          var thiselement = $(this);
          $.ajax({
            url: _ROOT+'services/updatesubservice',
            type: 'POST',
            dataType: 'Json',
            data:{'subservice_id':subservice_id,'is_active':'0'},
            success:function(data){
                var result = jQuery.parseJSON( data );
                if(data == true){
                  $(thiselement).addClass('activesubserviceBtn').removeClass('inactivesubserviceBtn');
                  $('#notification_success').fadeIn('fast').delay(2000).fadeOut('slow');
                }else if(data == 0) {
                  $('#notification_fail').fadeIn('fast').delay(2000).fadeOut('slow');
                }
                $('#subserviceactionblock_'+subservice_id).html('<a href="javascript:void(0)" id="'+subservice_id+'" class="activesubserviceBtn btn">Make Active</a>');
            }
          });
        }
      });

      $('.updateSubserviceclass').on("click",".activesubserviceBtn",function () {
        var result = confirm("Are you sure you want to make active this subservice?");
        if (result) {
          var subservice_id = $(this).attr("id");
          var thiselement = $(this);
          $.ajax({
            url: _ROOT+'services/updatesubservice',
            type: 'POST',
            dataType: 'Json',
            data:{'subservice_id':subservice_id,'is_active':'1'},
            success:function(data){
                var result = jQuery.parseJSON( data );
                if(data == true){
                  $(thiselement).addClass('inactivesubserviceBtn').removeClass('activesubserviceBtn');
                  $('#notification_success').fadeIn('fast').delay(2000).fadeOut('slow');
                }else if(data == 0) {
                  $('#notification_fail').fadeIn('fast').delay(2000).fadeOut('slow');
                }
                $('#subserviceactionblock_'+subservice_id).html('<a href="javascript:void(0)" id="'+subservice_id+'" class="savesubserviceBtn btn">Save</a> / <a href="javascript:void(0)" id="'+subservice_id+'" class="inactivesubserviceBtn btn">Make InActive</a>');
            }
          });
        }
      });

      $('.updateSubservicetagclass').on("click", ".savesubservicetagBtn",function () {
        var subservicetag_id = $(this).attr("id");
        var subservicetag_name = $("#subservicetag_"+subservicetag_id).val();
        if(subservicetag_name != ''){
            $.ajax({
              url: _ROOT+'services/updatesubservicetag',
              type: 'POST',
              dataType: 'Json',
              data:{'subservicetag_id':subservicetag_id,'subservicetag_name':subservicetag_name},
              success:function(data){
                  var result = jQuery.parseJSON( data );
                  if(data == true){
                    $('#notification_success').fadeIn('fast').delay(2000).fadeOut('slow');
                  }else if(data == 0) {
                    $('#notification_fail').fadeIn('fast').delay(2000).fadeOut('slow');
                  }
              }
            });
        }else {
          $('#notification_fail').fadeIn('fast').delay(2000).fadeOut('slow');
        }
      });

      $('.updateSubservicetagclass').on("click",".inactivesubservicetagBtn",function () {
        var result = confirm("Are you sure you want to inactive this subservice tag?");
        if (result) {
          var subservicetag_id = $(this).attr("id");
          var thiselement = $(this);
          $.ajax({
            url: _ROOT+'services/updatesubservicetag',
            type: 'POST',
            dataType: 'Json',
            data:{'subservicetag_id':subservicetag_id,'is_active':'0'},
            success:function(data){
                var result = jQuery.parseJSON( data );
                if(data == true){
                  $(thiselement).addClass('activesubservicetagBtn').removeClass('inactivesubservicetagBtn');
                  $('#notification_success').fadeIn('fast').delay(2000).fadeOut('slow');
                }else if(data == 0) {
                  $('#notification_fail').fadeIn('fast').delay(2000).fadeOut('slow');
                }
                $('#subservicetagactionblock_'+subservicetag_id).html('<a href="javascript:void(0)" id="'+subservicetag_id+'" class="activesubservicetagBtn btn">Make Active</a>');
            }
          });
        }
      });


      $('.updateSubservicetagclass').on("click",".activesubservicetagBtn",function () {
        var result = confirm("Are you sure you want to make active this subservice tag?");
        if (result) {
          var subservicetag_id = $(this).attr("id");
          var thiselement = $(this);
          $.ajax({
            url: _ROOT+'services/updatesubservicetag',
            type: 'POST',
            dataType: 'Json',
            data:{'subservicetag_id':subservicetag_id,'is_active':'1'},
            success:function(data){
                var result = jQuery.parseJSON( data );
                if(data == true){
                  $(thiselement).addClass('inactivesubservicetagBtn').removeClass('activesubservicetagBtn');
                  $('#notification_success').fadeIn('fast').delay(2000).fadeOut('slow');
                }else if(data == 0) {
                  $('#notification_fail').fadeIn('fast').delay(2000).fadeOut('slow');
                }
                $('#subservicetagactionblock_'+subservicetag_id).html('<a href="javascript:void(0)" id="'+subservicetag_id+'" class="savesubservicetagBtn btn">Save</a> / <a href="javascript:void(0)" id="'+subservicetag_id+'" class="inactivesubservicetagBtn btn">Make InActive</a>');
            }
          });
        }
      });
});
</script>