<link rel="stylesheet" href="<?php echo $this->request->webroot;?>css/custom-with-material.css">
<link rel="stylesheet" href="<?php echo $this->request->webroot;?>css/dist/datatables/dataTables.bootstrap.css">
<link rel="stylesheet" href="<?php echo $this->request->webroot;?>css/buttons.dataTables.min.css">
<script src="<?php echo $this->request->webroot;?>js/dist/datatables/jquery.dataTables.js"></script>
<script src="<?php echo $this->request->webroot;?>js/dist/datatables/dataTables.bootstrap.js"></script>
<script src="https://cdn.datatables.net/buttons/1.1.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.1.2/js/buttons.flash.min.js"></script>

<section class="content-header margin-bottom-lg padding-left-none">
    <h1>
        User Ratings
        <small>List</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">User Rating List</li>
    </ol>
</section>
<div class="row margin-none">
  <div class="col-xs-12 padding-none">
    <section class="content padding-none"> 
        <div class="box">
            <div class="box-header">
                
            </div>
            <div class="box-body">
                <div class="row margin-none">
                    <div class="col-md-12 padding-none" style="overflow-y:auto;">
                        <table id="example1" class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Sr. #</th>
                                    <th>Trans ID</th>
                                    <th>Provider</th>
                                    <th>FeedbackBy</th>
                                    <th>Initiator</th>
                                    <th>Requested Date</th>
                                    <th>Completion Date</th>
                                    <th>Rating</th>
                                    <th>Rating Date</th>
                                    <th>Service</th>
                                    <th>Subservice</th>
                                    <th>Location</th>
                                    <th>Note</th>
                                    <th>Trans Value</th>
                                    <th>Fee</th>
                                    <th>Tax</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($response as $key=>$value){ ?>
                                <tr>
                                    <td><?php echo $key + 1;?></td>
                                    <td><?php echo $value['id'];?></td>
                                    <td><?php echo $value['requestor'];?></td>
                                    <td><?php echo $value['feedbackby'];?></td>
                                    <td><?php echo $value['initiator'];?></td>
                                    <td><?php echo $value['request_date'];?></td>
                                    <td><?php echo $value['completion_date'];?></td>
                                    <td><?php if($value['rating'] == '1'){echo "Positive";}elseif($value['rating'] == '2'){echo "Neutral";}else{echo "Negative";}?></td>
                                    <td><?php echo $value['rating_date'];?></td>
                                    <td><?php echo $value['service'];?></td>
                                    <td><?php echo $value['subservice'];?></td>
                                    <td><?php echo $value['location'];?></td>
                                    <td><?php echo $value['note'];?></td>
                                    <td><?php echo $value['transaction_value'];?></td>
                                    <td><?php echo $value['transaction_fee'];?></td>
                                    <td><?php echo $value['transaction_tax'];?></td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
  </div>
</div>
<script type="text/javascript">
jQuery(document).ready(function() {
    $('#example1').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": false,
      "info": true,
      "autoWidth": false,
      "dom": 'Bfrtip',
      "buttons": [
            'csv'
        ]
    });
});
</script>