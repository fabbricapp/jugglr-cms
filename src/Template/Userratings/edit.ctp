<link rel="stylesheet" href="<?php echo $this->request->webroot;?>admin_theme/plugins/ionslider/ion.rangeSlider.css">
<link rel="stylesheet" href="<?php echo $this->request->webroot;?>admin_theme/plugins/ionslider/ion.rangeSlider.skinNice.css">
<section class="content-header margin-bottom-lg padding-left-none">
    <h1>
        Update Rating
        <small></small>
    </h1>
</section>
<div class="box box-primary margin-top">
    <div class="row margin-none">
        <div class="col-md-1 col-sm-2 col-xs-3 text-right margin-top margin-bottom">
            <div class="user-panel padding-none">
                <div class="image">
                    <img class="img-circle" src="<?php echo $this->request->webroot;?>img/user1-128x128.jpg"/>
                </div>
            </div>
        </div>
        <div class="col-md-11 col-sm-10 col-xs-9 margin-top margin-bottom">
            <h4>Elio Adragna</h4>
        </div>
    </div>
    <?php foreach($data['subservice'] as $key=>$value){ ?>
        <div class="row margin-none padding">
            <div class="col-sm-4 margin-top rating_organediv padding-sm padding-left">
                <?php echo $value['name'];?>
            </div>
            <div class="col-sm-7">
                <input class="slider_input" id="range_<?php echo $key;?>" type="text" name="range_<?php echo $key;?>" value="0;100">
            </div>
            <div class="col-sm-1 margin-top padding-sm">
                <span class="rating_organediv padding-sm">
                    <?php echo $value['rating']." %";?>
                </span>
            </div>
        </div>
        <script type="text/javascript">
            jQuery(document).ready(function() {
                var id = <?php echo $key;?>; 
                var rating = <?php echo $value['rating'];?>; 
                $("#range_"+id).ionRangeSlider({
                  min: 0,
                  max: 100,
                  type: "single",
                  step: 1,
                  postfix: " %",
                  from: rating,
                  hideMinMax: true,
                  hideFromTo: false
                });
            });
        </script>
    <?php } ?>
    <div class="row margin-none">
        <div class="col-md-12 padding-top-lg margin-top margin-bottom">
            <button class="CanacelBtn pull-right margin-left" type="button">Cancel</button>
            <button class="updateBtn pull-right" type="submit">Save</button>
        </div>
    </div>
</div>
<script src="<?php echo $this->request->webroot;?>admin_theme/plugins/ionslider/ion.rangeSlider.min.js"></script>
