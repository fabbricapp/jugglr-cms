<link rel="stylesheet" href="<?php echo $this->request->webroot;?>css/custom-with-material.css">
<link rel="stylesheet" href="<?php echo $this->request->webroot;?>css/dist/datatables/dataTables.bootstrap.css">
<link rel="stylesheet" href="<?php echo $this->request->webroot;?>css/buttons.dataTables.min.css">
<script src="<?php echo $this->request->webroot;?>js/dist/datatables/jquery.dataTables.js"></script>
<script src="<?php echo $this->request->webroot;?>js/dist/datatables/dataTables.bootstrap.js"></script>
<script src="https://cdn.datatables.net/buttons/1.1.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.1.2/js/buttons.flash.min.js"></script>

<section class="content-header margin-bottom-lg padding-left-none">
    <h1>
        User Rating
        <small>Summary</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">User Ratings Summary</li>
    </ol>
</section>
<div class="row margin-none">
  <div class="col-xs-12 padding-none">
    <section class="content padding-none"> 
        <div class="box">
            <div class="box-header">
                
            </div>
            <div class="box-body" style="overflow-y:auto;">
                <table id="example1" class="table table-hover">
                    <thead>
                        <tr>
                            <th>Sr. #</th>
                            <th>User ID</th>
                            <th>Image</th>
                            <th>Name</th>
                            <th>Average Rating</th>
                            <th>Total Ratings</th>
                            <th>Positive Rating</th>
                            <th>Neutral Rating</th>
                            <th>Total Value</th>
                            <th>Total Fee</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($listing as $key=>$value){ ?>
                        <tr>
                            <td><?php echo $key + 1;?></td>
                            <td><?php echo $value['id'];?></td>
                            <td>
                                <div class="user-panel padding-none">
                                    <div class="image">
                                        <img class="img-circle" src="<?php echo $value['image'];?>"/>
                                    </div>
                                </div>
                            </td>
                            <td><?php echo $value['name'];?></td>
                            <td><?php echo $value['average_rating']."%";?></td>
                            <td><?php echo $value['total_rating'];?></td>
                            <td><?php echo $value['total_positive_rating'];?></td>
                            <td><?php echo $value['total_neutral_rating'];?></td>
                            <td><?php echo $value['total_value'];?></td>
                            <td><?php echo $value['total_fee'];?></td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
  </div>
</div>
<script type="text/javascript">
jQuery(document).ready(function() {
    $('#example1').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": false,
      "info": true,
      "autoWidth": false,
      "dom": 'Bfrtip',
      "buttons": [
            'csv'
        ]
    });
});
</script>