<div class="login-box">
    <div class="login-logo">
        <span class="logo-lg">
            <img height="30px" src="<?php echo $this->request->webroot;?>img/logo.png" alt="logo_image">
        </span>
        <b>Admin</b>Jugglr
    </div>
    <?php if($token == ''){?>
        <div class="login-box-body">
            <form action="../admins/resetpassword" method="post">
                <div class="form-group has-feedback">
                    <input type="email" name="username" class="form-control" placeholder="Email">
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>
                <div class="row margin-none">
                    <div class="col-xs-4 padding-right-none pull-right">
                        <button type="submit" class="btn btn-loginpink btn-block btn-flat">Send Mail</button>
                    </div>
                </div>
            </form>
        </div>
    <?php }else {?>
        <div class="login-box-body">
            <p class="login-box-msg">Change Password</p>
            <form action="../admins/resetpassword" method="post">
                <div class="form-group has-feedback">
                    <input type="password" name="password" class="form-control" placeholder="Password">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="password" name="password" class="form-control" placeholder="Confirm Password">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div class="row margin-none">
                    <div class="col-xs-4 padding-right-none pull-right">
                        <button type="submit" class="btn btn-loginpink btn-block btn-flat">Change Password</button>
                    </div>
                </div>
            </form>
        </div>
    <?php }?>
</div>