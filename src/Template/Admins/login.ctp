<div class="login-box">
  <div class="login-logo">
    <span class="logo-lg">
        <img height="30px" src="<?php echo $this->request->webroot;?>img/logo.png" alt="logo_image">
    </span>
    <b>Admin</b>Jugglr
  </div><!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Sign in to start your session</p>
    <form action="../admins/login" method="post">
      <div class="form-group has-feedback">
        <input type="email" name="username" class="form-control" placeholder="Email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" name="password" class="form-control" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row margin-none">
        <div class="col-xs-7 margin-left-sm">
          
        </div><!-- /.col -->
        <div class="col-xs-4 padding-right-none pull-right">
          <button type="submit" class="btn btn-loginpink btn-block btn-flat">Sign In</button>
        </div><!-- /.col -->
      </div>
    </form>

    <a href="../admins/resetpassword">I forgot my password</a>

  </div><!-- /.login-box-body -->
</div><!-- /.login-box -->