<section class="content-header margin-bottom-lg padding-left-none">
    <h1>
        Jugglr
        <small>Statistics</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Stat</li>
    </ol>
     <style type="text/css">
            #mynetwork {
                width: 100%;
                height: 650px;
                border: 5px solid #F36;
            }
        </style>
</section>
<div class="row margin-none">
    <div class="col-xs-12 padding-none">
        <section class="content padding-none">
            <div class="box">
                <div class="box-header">

                </div>
                <div class="box-body">
                <div class="row">

                        <!-- Map -->
                        <div class="col-md-12 col-sm-12 col-xs-12" id="mapsection">
                          <div class="x_panel">

                            <div class="clearfix">
                              <div class ="col-md-8">
                                <label id="rangeText">Distance Range</label>
                                <input   type="range" min="0" max="100" step="1" value="3"  id="rangeid" class="active"/>
                              </div>
                              <a class="btn btn-default col-md-3" href="#statesection" type="button" id="refreshBarChart">refresh !</a>
                            </div>

                            <div id="map-container" style="height:560px">
                              <input id="pac-input" class="controls form-control" type="text" placeholder="Search Box">
                              <div id="map" style="height:560px"></div>
                            </div>

                          </div>
                        </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <!-- Bar graph -->
                <div id ="statesection" class="row">
                  <div class="col-md-12 h-100">
                    <div class="x_panel">

                      <div class="x_title">
                        <h2>Services <small>needs/offers</small></h2>
                        <div class="clearfix"></div>
                      </div>

                      <div class="x_content col-md-12 h-100" id="barchartservices">
                        <canvas id="mybarChart"></canvas>
                      </div>

                    </div>
                  </div>
                </div>

<div class="row margin-none">
  <div class="box-body">
                    <div class="row">
                        <div class="col-md-12 h-100">
                        <div class="x_title">
                                                <h2>Users <small>network</small></h2>
                                                <div class="clearfix"></div>
                                              </div>
                            <div class ="card bg-danger h-100" id="mynetwork"></div>
                        </div>
                        <div class="col-md-8 h-100">
                         <div class="x_title">
                        <h2>Marketplace <small>Most active</small></h2>
                        <div class="clearfix"></div>
                      </div>
                        <table id="datatable" class="table table-striped table-bordered">
                            <thead>
                              <tr>
                                <th>#jobs on marketplace</th>
                                <th>Id</th>
                                <th>Image</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Postcode</th>
                                <th>Suburb</th>
                                <th>City</th>
                                <th>State</th>
                              </tr>
                            </thead>
                            <tbody id="userTbody">
                            </tbody>
                        </table>
                        </div>
                        <div class ="col-md-3 h-100 "">
                        <div class="x_title">
                                                <h2>Intrests<small>the most shared interests on thet area</small></h2>
                                                <div class="clearfix"></div>
                                              </div>
                          <table id="datatable" class="table table-striped table-bordered">
                              <thead>
                                <tr>
                                  <th>interest</th>
                                  <th>count</th>
                                </tr>
                              </thead>
                              <tbody id="interstTbody">
                              </tbody>
                          </table>
                          </div>

                      </div>
                    </div>
                </div>
            </div>

  </div>

  <div class="row margin-none">
    <div class="box-body">
                      <div class="row">

                  </div>
              </div>
    </section>
<section id="servicesstat">
 <div class="row margin-none">
    <div class="box-body">
<div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">

            <div class="x_title">
              <h2>Top User</h2>
              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" id="tags" value="" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-search" type="button" id="refreshtable2">search</button>
                       <button class="btn btn-green" type="button" id="exportusersbyservices">export</button>
                    </span>
                  </div>
                </div>
              </div>
              <div class="clearfix"></div>
            </div>

            <div class="x_content">
              <table id="datatable" class="table table-striped table-bordered">
                <thead>
                  <tr>
                    <th>Type</th>
                    <th>Id</th>
                    <th>Photo</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Postcode</th>
                    <th>Suburb</th>
                    <th>City</th>
                    <th>State</th>
                  </tr>
                </thead>
                <tbody id="userbyserviceTbody">
                </tbody>
              </table>
            </div>

          </div>
        </div>
          </div>
                </div>
</section>
    </div>

        </div>

</div>
<script type="text/javascript">
jQuery(document).ready(function() {
var availableTags = [
"Kids pick up and drop offs",
"Babysitting",
"Check in on older family members",
"Tutoring",
"Homework",
"Sport training",
"Cooking",
"Housework",
"Gardening",
"Pet care",
"Home maintenance and renovations",
"Nutrition advice",
"Personal training",
"Hair, nails and beauty",
"Massage",
"Career advice",
"Parenting tips",
"Family budgeting",
"Personal coaching",
"Relationship advice",
"Girls' time out",
"Music and/or dance",
"Art and design",
"Languages",
"Cooking, baking and growing",
"Wellness and wellbeing",
"Gift",
"Payment / Money / Cash (through Jugglr)",
"Travel & holiday planning",
"Special needs",
"pick up address",
"drop off address",
"Wedding planner",
"Parcel pick up and drop off",
"House moving",
"Playdates",
"Food delivery",
"Small items delivery",
"Big items delivery",
"House moving and removals",
"Cleaning and ironing",
"Bookkeeping & accounting",
"Admin & temp work",
"Office cleaning",
"Birthday planner",
"Kids & family event planner",
"Relocation planner",
"Market research",
"Social media marketing",
"Content creation & comms",
"Focus groups",
"Website & app testing",
"UX design",
"Graphic design",
"Web design",
"Marketing strategy",
"Flyer distribution",
"Photography",
"Video production",
"Scriptwriting",
"Animated videos",
"Furniture assembly",
"Cooking classes",
"TV set up",
"Internet set up",
"iTunes help",
"Computer & IT support",
"Apple help",
"Customer service",
"social network",
"mums helping mums",
"restaurant (OLD)",
"catering (OLD)",
"events",
"cafe'",
"sailing classes",
"kids' activities",
"kids' sports",
"restaurant and catering",
"restaurant",
"catering",
"plumbing",
"bathroom renovations",
"kitchen renovations",
"hot water service",
"24/7 emergency responses",
"pregnancy",
"baby",
"birth",
"postpartum",
"real estate",
"renovations",
"money",
"Personal finance",
"Investing",
"kids' photography",
"hypnobirthing",
"childbirth education",
"on-demand transport",
"pre-bookings - rides",
"school pick up & drop off - rides",
"kids' toys",
"wellness & fitness",
"household services",
"household gadgets"
];

$("#tags").autocomplete({
  source : availableTags
});

});

document.getElementById("refreshtable2").addEventListener("click", function(){
  loaduserbyserviceDatatable(document.getElementById('rangeid').value,placex.lat(),placex.lng())
});


document.getElementById("tags").addEventListener("keyup", function(event) {
  event.preventDefault();
  if (event.keyCode === 13) {
    document.getElementById("refreshtable2").click();
  }
});
document.getElementById("exportusersbyservices").addEventListener("click", function(){
download(xxx)
});







 var cityCircle=null;
 var map;
 var placex;
	// Map
	function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: -25.922038, lng: 133.478603},
          zoom: 5,
          maxZoom:10,
          mapTypeId: 'roadmap'
        });

        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');

        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
          searchBox.setBounds(map.getBounds());
        });

        var markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
          var places = searchBox.getPlaces();

          if (places.length == 0) {
            return;
          }

          // Clear out the old markers.
          markers.forEach(function(marker) {
            marker.setMap(null);
          });
          markers = [];

          // For each place, get the icon, name and location.
          var bounds = new google.maps.LatLngBounds();
          places.forEach(function(place) {
            if (!place.geometry) {
              console.log("Returned place contains no geometry");
              return;
            }
            var icon = {
              url: "images/custommarker.png",
              size: new google.maps.Size(71, 71),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(17, 34),
              scaledSize: new google.maps.Size(50, 50)
            };
            placex=place.geometry.location;

            var range = document.getElementById('rangeid');
            if(cityCircle)
            cityCircle.setMap(null)
             cityCircle = new google.maps.Circle({
	            strokeColor: '#ff3366',
	            strokeOpacity: 0.8,
	            strokeWeight: 2,
	            fillColor: '#ff33222',
	            fillOpacity: 0.35,
	            map: map,
	            center: place.geometry.location,
	            radius: parseInt(rangeid.value)*1000
          	});


            // Create a marker for each place.
            markers.push(new google.maps.Marker({
              map: map,
              icon: icon,
              title: place.name,
              position: place.geometry.location
            }));

            if (place.geometry.viewport) {
              // Only geocodes have viewport.
              bounds.union(place.geometry.viewport);
            } else {
              bounds.extend(place.geometry.location);
            }
          });
          map.fitBounds(bounds);
        });


      }

      document.getElementById("refreshBarChart").addEventListener("click", function(){

			console.log(placex.lat());
            nodesx=[];
            edgex=[];
			httpGetAsync("http://localhost/jugglr/dashboardapi/getconnections.php?lat="+placex.lat()+"&long="+placex.lng()+"&distance="+document.getElementById('rangeid').value,callback);
                function httpGetAsync(theUrl, callback)
                {
                    var xmlHttp = new XMLHttpRequest();
                    xmlHttp.onreadystatechange = function() {
                        if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
                            callback(xmlHttp.responseText);
                    }
                    xmlHttp.open("GET", theUrl, true); // true for asynchronous
                    xmlHttp.send(null);
                }


			link = "http://localhost/jugglr/dashboardapi/gettopservices.php?lat="+placex.lat()+"&long="+placex.lng()+"&distance="+document.getElementById('rangeid').value
				RetrieveDatabar(link);
			loadDatatable(document.getElementById('rangeid').value,placex.lat(),placex.lng())
			loadinterestDatatable(document.getElementById('rangeid').value,placex.lat(),placex.lng())
			console.log(link)
			});


  var datax;
    var nodesx=[];
    var edgex=[];
    httpGetAsync("http://localhost/jugglr/dashboardapi/getconnections.php",callback);
    function httpGetAsync(theUrl, callback)
    {
        var xmlHttp = new XMLHttpRequest();
        xmlHttp.onreadystatechange = function() {
            if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
                callback(xmlHttp.responseText);
        }
        xmlHttp.open("GET", theUrl, true); // true for asynchronous
        xmlHttp.send(null);
    }

    function callback(response) {
        datax=JSON.parse(response)
        lastid=0;
        for (i=0;i<datax.nodes.length;i++){
            if(lastid!=datax.nodes[i].id){
                lastid=datax.nodes[i].id;
                nodesx.push({id: datax.nodes[i].id, label: datax.nodes[i].label,shape: 'circularImage',image:datax.nodes[i].profile_image_thumb});
            }

        }
        for (i=0;i<datax.edges.length;i++){
            if(datax.edges[i].connection_type==0)
                edgex.push({from: datax.edges[i].from, to: datax.edges[i].to, width:4, color: { color: '#ff3366'}, arrows: {to: true,from:true}});
            if(datax.edges[i].connection_type==1)
                edgex.push({from: datax.edges[i].from, to: datax.edges[i].to, width:2, color: { color: '#08af5b'}, arrows: {to: true}});
            if(datax.edges[i].connection_type==2)
                edgex.push({from: datax.edges[i].from, to: datax.edges[i].to, width:1, color: { color: '#3B5998'}});
        }
        console.log("done")

        var nodes = new vis.DataSet(nodesx);

        // create an array with edges
        var edges = new vis.DataSet(edgex);

        // create a network
        var container = document.getElementById('mynetwork');

        // provide the data in the vis format
        var data = {
            nodes: nodes,
            edges: edges
        };
        var options = {
            layout: {
                randomSeed: undefined,
                improvedLayout:false,
                hierarchical: {
                    enabled:false,
                    levelSeparation: 150,
                    nodeSpacing: 30,
                    treeSpacing: 30,
                    blockShifting: true,
                    edgeMinimization: true,
                    parentCentralization: true,
                    direction: 'UD',        // UD, DU, LR, RL
                    sortMethod: 'hubsize'   // hubsize, directed
                }
            }
        }
        // initialize your network!
        var network = new vis.Network(container, data, options);
    }

    //bar
    function initbar (services,countneed,countoffer){
    				if ($('#mybarChart').length ){

    			  	var ctx = document.getElementById("mybarChart");
    			  	var mybarChart = new Chart(ctx, {
    			  		type: 'horizontalBar',
    			  		data: {
    			  			labels: services,
    			  			datasets: [{
    			  				label: 'services needed',
    			  				backgroundColor: "#ff3366",
    			  				data: countneed
    			  			}, {
    			  				label: 'services offered',
    			  				backgroundColor: "#03586A",
    			  				data: countoffer
    			  			}]
    			  		},

    			  		options: {
    			  			scales: {
    			  				yAxes: [{
    			  					ticks: {
    			  						beginAtZero: true
    			  					}
    			  				}]
    			  			}
    			  		}
    			  	});

    			  }
    			}

    			function RetrieveDatabar(url){
    				var map = {};
    				var countneed=[]
    						var countoffer=[]
    						var services=[]
    				document.getElementById("barchartservices").innerHTML='<canvas id="mybarChart"></canvas>';
    				$.getJSON(url, function(jsonData) {
    					for(var k in jsonData){
    						if (map[jsonData[k]['label']]==null){
    							var service = [0,0] ;
    							if (jsonData[k]['type']=="0")
    								service[0]=parseInt(jsonData[k]['count_services']);
    							else
    								service[1]=parseInt(jsonData[k]['count_services']);
    							map[jsonData[k]['label']]=service;
    						}
    						else{
    							if (jsonData[k]['type']=="0")
    								map[jsonData[k]['label']][0]=parseInt(jsonData[k]['count_services']);
    							else
    								map[jsonData[k]['label']][1]=parseInt(jsonData[k]['count_services']);
    						}
    					}

    					for(var k in map){

    							countneed.push(map[k][0]);
    							countoffer.push(map[k][1]);
    							services.push(k);
    							console.log(k + " " + map[k]);
    						}
    						initbar (services,countneed,countoffer);
    				});
    			}

 $(function () {

    // on page load, set the text of the label based the value of the range
    $('#rangeText').text("Distance :" +$('#rangeid').val()+ "Km");

    // setup an event handler to set the text when the range value is dragged (see event for input) or changed (see event for change)
    $('#rangeid').on('input change', function () {
    	    	$('#rangeText').text("Distance :" +$('#rangeid').val()+ "Km");

    	cityCircle.setMap(null);
    	cityCircle = new google.maps.Circle({
        strokeColor: '#ff3366',
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: '#ff336622',
        fillOpacity: 0.35,
        map: map,
        center: placex,
        radius: parseInt(rangeid.value)*1000
      });

    });

  });


//table
function loadDatatable(dis,lat,long){
	var url;
	var postecode = document.getElementById('userref');
	url = 'http://localhost/jugglr/dashboardapi/getmostactiveusersbystate.php?lat='+lat+'&long='+long+'&distance='+dis;
	console.log(url)
	console.log(url)
	$.getJSON(url, function(jsonData) {
		document.getElementById('userTbody').innerHTML="";
		const $table = $('#userTbody');
		for(var k in jsonData){
			const field = jsonData[k];
			$table.append(
				`<tr>
				<td>${field.jobcount}</td>
				<td><a href ="../users/view?id=${field.user_id}" target="_blank">${field.user_id}</a></td>
				<td><img class="img-circle" src="${field.profile_image_thumb}" style="width:35px"/></td>
				<td>${field.names}</td>
				<td>${field.email}</td>
				<td>${field.postcode}</td>
				<td>${field.suburb}</td>
				<td>${field.city}</td>
				<td>${field.state}</td>
				</tr>`);
				}

	});
}

function loadinterestDatatable(dis,lat,long){
	var url;
	var postecode = document.getElementById('userref');
	url = 'http://localhost/jugglr/dashboardapi/getintersts.php?lat='+lat+'&long='+long+'&distance='+dis;
	console.log(url)
	console.log(url)
	$.getJSON(url, function(jsonData) {
		document.getElementById('interstTbody').innerHTML="";
		const $table = $('#interstTbody');
		for(var k in jsonData){
			const field = jsonData[k];
			$table.append(
				`<tr>
				<td><a href="https://www.facebook.com/search/str/${field.interest}/keywords_search" target="_blank">${field.interest}</a></td>
				<td>${field.usersinterested}</td>
				</tr>`);
		}

	});
}

var xxx=[];
function loaduserbyserviceDatatable(dis,lat,long){
 var url;
 	var postecode = document.getElementById('userref');
 	url = 'http://localhost/jugglr/dashboardapi/getusersbyservice.php?lat='+lat+'&long='+long+'&distance='+dis+'&name='+document.getElementById("tags").value;
 	console.log(url)
 	console.log(url)
 	$.getJSON(url, function(jsonData) {
 	    xxx=jsonData;
 		document.getElementById('userbyserviceTbody').innerHTML="";
 		const $table = $('#userbyserviceTbody');
 		for(var k in jsonData){
 			const field = jsonData[k];
 			$table.append(
                `<tr>
                <td>${field.type}</td>
                <td><a href ="../users/view?id=${field.user_id}" target="_blank" >${field.user_id}</a></td>
                <td><img class="img-circle" src="${field.profile_image_thumb}" style="width:35px"/></td>
                <td>${field.names}</td>
                <td>${field.email}</td>
                <td>${field.postcode}</td>
                <td>${field.suburb}</td>
                <td>${field.city}</td>
                <td>${field.state}</td>
                </tr>`);
            }

    	});
}
function download(json){

var csv = JSON2CSV(json);
var downloadLink = document.createElement("a");
var blob = new Blob(["\ufeff", csv]);
var url = URL.createObjectURL(blob);
downloadLink.href = url;
downloadLink.download = "data.csv";

document.body.appendChild(downloadLink);
downloadLink.click();
document.body.removeChild(downloadLink);
}
function JSON2CSV(objArray) {
    var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
    var str = '';
    var line = '';

    if ($("#labels").is(':checked')) {
        var head = array[0];
        if ($("#quote").is(':checked')) {
            for (var index in array[0]) {
                var value = index + "";
                line += '"' + value.replace(/"/g, '""') + '",';
            }
        } else {
            for (var index in array[0]) {
                line += index + ',';
            }
        }

        line = line.slice(0, -1);
        str += line + '\r\n';
    }

    for (var i = 0; i < array.length; i++) {
        var line = '';

        if ($("#quote").is(':checked')) {
            for (var index in array[i]) {
                var value = array[i][index] + "";
                line += '"' + value.replace(/"/g, '""') + '",';
            }
        } else {
            for (var index in array[i]) {
                line += array[i][index] + ',';
            }
        }

        line = line.slice(0, -1);
        str += line + '\r\n';
    }
    return str;
}
</script>
 <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCo7EfW0XvoUFz9Qx1UwwggxsdwPwjcGas&libraries=places&callback=initMap"
async defer></script>
