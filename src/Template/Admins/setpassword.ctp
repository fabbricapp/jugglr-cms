<div class="login-box">
    <div class="login-logo">
        <span class="logo-lg">
            <img height="30px" src="<?php echo $this->request->webroot;?>img/logo.png" alt="logo_image">
        </span>
        <b>Admin</b>Jugglr
    </div>
    <div class="login-box-body">
        <p class="login-box-msg">Change Password</p>
        <form action="../admins/setpassword" method="post">
            <input type="hidden" name="token" value="<?php echo $token;?>">
            <div class="form-group has-feedback">
                <input type="password" name="password" class="form-control" placeholder="Password">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" name="confirmpassword" class="form-control" placeholder="Confirm Password">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row margin-none">
                <div class="col-xs-4 padding-right-none pull-right">
                    <button type="submit" class="btn btn-loginpink btn-block btn-flat">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>