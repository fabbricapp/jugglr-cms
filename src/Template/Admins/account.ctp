<section class="content-header margin-bottom-lg padding-left-none">
    <h1>
        Account
        <small>Page</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Account</li>
    </ol>
</section>
<div class="row margin-none">
    <div class="col-xs-12 padding-none">
        <section class="content padding-none">
            <div class="box">
                <div class="box-header">

                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                            <?php echo $this->Form->create('User',array('id' => 'admin','role'=>'form','url'=>'/admins/account','enctype'=>'multipart/form-data')); ?>
                              <div class="form-group">
                                  <div class="row margin-none padding-bottom-lg">
                                      <div class="col-md-4 text-right"> Name :</div>
                                      <div class="col-md-8">
                                        <?php
                                          echo $this->form->input('Account.name',array(
                                            'label'=>false,
                                            'name'=>'name',
                                            'type'=>'text',
                                            'value'=>$account['name'],
                                            'class'=>'form-control required',
                                            'div'=>'form-group'
                                          ));
                                        ?>
                                        <div class="input-sm">Put your full name that will display in CMS.</div>
                                      </div>
                                  </div>
                                  <div class="row margin-none padding-bottom-lg">
                                      <div class="col-md-4 text-right"> Upload Image :</div>
                                      <div class="col-md-8">
                                        <div class="form-group">
                                          <input name="image" type="file" id="">
                                        </div>
                                        <div class="input-sm">Upload your image that will display in CMS.</div>
                                      </div>
                                  </div>

                                  <div class="row margin-none padding-bottom-lg">
                                      <div class="col-md-4 text-right"></div>
                                      <div class="col-md-8">
                                          <button class="updateBtn" type="submit">Save</button>
                                      </div>
                                  </div>
                              </div>
                            <?php echo $this->Form->end(); ?>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script type="text/javascript">
jQuery(document).ready(function() {
  
});
</script>