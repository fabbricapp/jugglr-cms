<link rel="stylesheet" href="<?php echo $this->request->webroot;?>css/custom-with-material.css">

<section class="content-header margin-bottom-lg padding-left-none">
    <h1>
        User
        <small>By Volume</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">User ByVolume</li>
    </ol>
</section>
<div class="row margin-none">
  <div class="col-xs-12 padding-none">
    <section class="content padding-none"> 
        <div class="box">
            <div class=" row box-header">
                <div class="col-xs-6"><b>Total :</b> <?php echo ($android_cnt + $ios_cnt);?>
                </div>
                <div class="col-xs-6 text-right">
                  <b>IOS :</b> <?php echo $ios_cnt;?><br/>
                  <b>Android :</b> <?php echo $android_cnt;?>
                </div>
            </div>
            <div class="box-body" style="overflow-y:auto;">
                <table id="example1" class="table table-hover">
                    <thead>
                        <tr>
                          <th>Sr. #</th>
                           <th>State</th>
                           <th>Count</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $page = $this->Paginator->params()['page'];
                        $limit = $this->Paginator->params()['perPage'];
                        $i = ($page * $limit) - $limit + 1; ?>
                        <?php foreach($data as $key=>$value){ ?>
                        <tr>
                            <td><?php echo $i++;?></td>
                            <td><?php echo $value['state'];?></td>
                            <td><?php echo $value['count'];?></td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <?php echo $this->element('pagination'); ?>
            </div>
        </div>
    </section>
  </div>
</div>
<script type="text/javascript">
jQuery(document).ready(function() {
    
});
</script>
