<link rel="stylesheet" href="<?php echo $this->request->webroot;?>css/custom-with-material.css">
<section class="content-header margin-bottom-lg padding-left-none">
    <h1>
        User Summary
        <small>Card</small>
    </h1>
</section>

<div class="row margin-none">
    <div class="col-md-4 col-sm-4 padding-none">
        <div class="box box-primary editpage margin-top padding">
            <div class="margin-top-lg margin-bottom-lg">
              <img class="profile-user-img img-responsive img-circle" src="<?php echo $data['profile_image_thumb'];?>" alt="User profile picture">
                <div class="col-md-12 padding-none text-center">
                    <label class="color-9e9e9e padding-right">Verified</label><i class="verified fa fa-check"></i>
                </div>
            </div>
            <div class="row margin-none padding-top">
                <ul class="list-group list-group-unbordered">
                    <li class="list-group-item padding-right">
                      <b>Matches</b> <a class="pull-right"><?php echo $data['userconnections']['Matches'];?></a>
                    </li>
                    <li class="list-group-item padding-right">
                      <b>Shortlist</b> <a class="pull-right"><?php echo $data['userconnections']['Shortlisted'];?></a>
                    </li>
                </ul>
            </div>
            <div class="row margin-none padding-top-lg">
                <div class="col-md-12 padding-none">
                    <div class="col-md-4 padding-none"><label class="color-9e9e9e">Name :</label></div>
                    <div class="col-md-8"><?php echo $data['firstname']." ".$data['lastname'];?></div>
                </div>
                <div class="col-md-12 padding-none">
                    <div class="col-md-4 padding-none"><label class="color-9e9e9e">DOB :</label></div>
                    <div class="col-md-8"><?php if(isset($data['dob'])){echo $data['dob'];}?></div>
                </div>
                <div class="col-md-12 padding-none">
                    <div class="col-md-4 padding-none"><label class="color-9e9e9e">Gender :</label></div>
                    <div class="col-md-8"><?php if(isset($data['gender'])){ echo $data['gender'];}?></div>
                </div>
                <div class="col-md-12 padding-none">
                    <div class="col-md-4 padding-none"><label class="color-9e9e9e">Email :</label></div>
                    <div class="col-md-8">
                        <span class="progress-description"><?php if(isset($data['email'])){echo $data['email'];}?></span>
                    </div>
                </div>
                <div class="col-md-12 padding-none">
                    <div class="col-md-4 padding-none"><label class="color-9e9e9e">Postcode :</label></div>
                    <div class="col-md-8"><?php if(isset($data['postcode'])){echo $data['postcode'];}?></div>
                </div>
                <div class="col-md-12 padding-none">
                    <div class="col-md-4 padding-none"><label class="color-9e9e9e">Suburb :</label></div>
                    <div class="col-md-8"><?php if(isset($data['suburb'])){echo $data['suburb'];}?></div>
                </div>
                <div class="col-md-12 padding-none">
                    <div class="col-md-4 padding-none"><label class="color-9e9e9e">City :</label></div>
                    <div class="col-md-8"><?php if(isset($data['city'])){echo $data['city'];}?></div>
                </div>
                <div class="col-md-12 padding-none">
                    <div class="col-md-4 padding-none"><label class="color-9e9e9e">State :</label></div>
                    <div class="col-md-8"><?php if(isset($data['state'])){echo $data['state'];}?></div>
                </div>
                <div class="col-md-12 padding-none">
                    <div class="col-md-4 padding-none"><label class="color-9e9e9e">Country :</label> </div>
                    <div class="col-md-8"><?php if(isset($data['country'])){echo $data['country'];}?></div>
                </div>
                <div class="col-md-12 padding-none">
                    <div class="col-md-4 padding-none"><label class="color-9e9e9e">Registered at :</label> </div>
                    <div class="col-md-8"><?php if(isset($data['createdAt'])){echo $data['createdAt'];}?></div>
                </div>
            </div>
        </div>
        <div class="box box-primary editpage margin-top padding">
            <div class="box-header with-border">
              <h3 class="box-title">Interest</h3>
            </div>
            <div class="box-body row margin-none">
                <?php if(!empty($data['userinterests'])){
                        foreach($data['userinterests'] as $key=>$value) {?>
                            <span class="label label-info" style="line-height:2;"><?php echo $value;?></span>
                    <?php }
                 }else {?>
                    <label class="color-9e9e9e">No Interest Found.</label>
                 <?php } ?>
            </div>
        </div>
        <div class="box box-primary editpage margin-top padding">
            <div class="box-header with-border">
              <h3 class="box-title">About Me</h3>
            </div>
            <div class="box-body padding-none">
                <div class="row margin-none">
                    <div class="col-md-12 padding">
                        <?php echo $data['about_me'];?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-8 col-sm-8 padding-none padding-left">
        <div class="row margin-top">
            <div class="col-md-4">
                <div class="info-box bg-yellow">
                    <span class="info-box-icon"><i class="ion ion-ios-pricetag-outline"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Current</span>
                        <span class="info-box-number"><?php echo $data['current_task'];?></span>
                        <div class="progress">
                            <div class="progress-bar" style="width: <?php echo (($data['current_task']*100)/$data['task_cnt']);?>%"></div>
                        </div>
                        <span class="progress-description">
                            out of <?php echo $data['task_cnt'];?> Transactions.
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="info-box bg-green">
                    <span class="info-box-icon"><i class="ion ion-ios-pricetag-outline"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Completed</span>
                        <span class="info-box-number"><?php echo $data['complete_task'];?></span>
                        <div class="progress">
                            <div class="progress-bar" style="width: <?php echo (($data['complete_task']*100)/$data['task_cnt']);?>%"></div>
                        </div>
                        <span class="progress-description">
                            out of <?php echo $data['task_cnt'];?> Transactions.
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="info-box bg-red">
                    <span class="info-box-icon"><i class="ion ion-ios-pricetag-outline"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Cancelled</span>
                        <span class="info-box-number"><?php echo $data['cancel_task'];?></span>
                        <div class="progress">
                            <div class="progress-bar" style="width: <?php echo (($data['cancel_task']*100)/$data['task_cnt']);?>%"></div>
                        </div>
                        <span class="progress-description">
                            out of <?php echo $data['task_cnt'];?> Transactions.
                        </span>
                    </div>
                </div>
            </div>
        </div>
            
        <div class="box box-primary editpage margin-top padding">
            <div class="row margin-none">
                <div class="col-md-12 padding-none">
                    <div class="row margin-none padding-top padding-bottom text-center">
                        <h4>Average Rating : <?php echo $data['average_rating']." %";?></h4>
                    </div>
                    <div class="row margin-none padding-top-lg">
                        <div class="col-md-6 col-sm-6 padding-sm"> 
                            <?php if(!empty($data['userservices']['need'])){?>
                                <div class="row margin-none padding-bottom">
                                    <div class="col-md-8 padding-none">Sub-Service Needed</div>
                                    <div class="col-md-2 padding-none">Positive</div>
                                    <div class="col-md-2 padding-none">Neutral</div>
                                </div>
                                <?php foreach($data['userservices']['need'] as $key=>$value) {?>
                                    <div class="row margin-none padding-bottom-sm">
                                        <div class="col-md-8 padding-none">
                                            <span class="need_service_wrapper progress-description"><?php echo $value['subservice_name'];?></span>
                                        </div>
                                        <div class="col-md-2 padding-none">
                                            <span class="postive_rating pull-right margin-none"><?php echo $value['postive_rating'];?></span>
                                        </div>
                                        <div class="col-md-2 padding-none">
                                            <span class="neutral_rating pull-right"><?php echo $value['neutral_rating'];?></span>
                                        </div>
                                    </div>
                                <?php } 
                            }?>       
                        </div>
                        <div class="col-md-6 col-sm-6 padding-sm">
                            <?php if(!empty($data['userservices']['offer'])){?>
                                <div class="row margin-none padding-bottom">
                                    <div class="col-md-8 padding-none">Sub-Service Offered</div>
                                    <div class="col-md-2 padding-none">Positive</div>
                                    <div class="col-md-2 padding-none">Neutral</div>
                                </div>
                                <?php foreach($data['userservices']['offer'] as $key=>$value) {?>
                                    <div class="row margin-none padding-bottom-sm">
                                        <div class="col-md-8 padding-none">
                                            <span class="offer_service_wrapper progress-description"><?php echo $value['subservice_name'];?></span>
                                        </div>
                                        <div class="col-md-2 padding-none">
                                            <span class="postive_rating pull-right margin-none"><?php echo $value['postive_rating'];?></span>
                                        </div>
                                        <div class="col-md-2 padding-none">
                                            <span class="neutral_rating pull-right"><?php echo $value['neutral_rating'];?></span>
                                        </div>
                                    </div>
                                <?php } 
                            }?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    
<script type="text/javascript">
jQuery(document).ready(function() {
  
});
</script>
