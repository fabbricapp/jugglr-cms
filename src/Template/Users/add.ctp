<link rel="stylesheet" href="<?php echo $this->request->webroot; ?>admin_theme/plugins/daterangepicker/daterangepicker-bs3.css">
<section class="content-header margin-bottom-lg padding-left-none">
    <h1>
        Add
        <small>Business Profile</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">BusinessProfile</li>
    </ol>
</section>
<div class="row margin-none">
    <div class="col-xs-12 padding-none">
        <section class="content padding-none"> 
            <div class="box">
                <div class="box-header">
                    
                </div>
                <div class="box-body"> 
                    <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                            <?php echo $this->Form->create('User',array('id' => 'adduser','role'=>'form','url'=>'/users/add','enctype'=>'multipart/form-data')); ?>
                              <div class="form-group">
                                  <div class="row margin-none padding-bottom-lg">
                                    <div class="col-md-4 text-right"> Firstname :</div>
                                    <div class="col-md-8">
                                      <?php 
                                        echo $this->form->input('User.firstname',array(
                                          'label'=>false,
                                          'name'=>'firstname',
                                          'type'=>'text',
                                          'placeholder'=>'User Firstname',
                                          'class'=>'form-control required',
                                          'div'=>'form-group'
                                        ));
                                      ?>
                                      <div class="input-sm">First name of user</div>
                                    </div>
                                  </div>
                                  <div class="row margin-none padding-bottom-lg">
                                      <div class="col-md-4 text-right"> Date of Birth :</div>
                                      <div class="col-md-8">
                                        <div class="form-group">
                                          <div class="input-group">
                                            <div class="input-group-addon">
                                              <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" value="" name="dob" class="form-control pull-right" id="reservation">
                                          </div>
                                        </div>
                                        <div class="input-sm">Select date of birth for user</div>
                                      </div>
                                  </div>
                                  <div class="row margin-none padding-bottom-lg">
                                    <div class="col-md-4 text-right"> Suburb :</div>
                                    <div class="col-md-8">
                                      <?php 
                                        echo $this->form->input('User.suburb',array(
                                          'label'=>false,
                                          'name'=>'suburb',
                                          'type'=>'text',
                                          'placeholder'=>'User Suburb',
                                          'class'=>'form-control required',
                                          'div'=>'form-group'
                                        ));
                                      ?>
                                      <div class="input-sm">User Suburb</div>
                                    </div>
                                  </div>
                                  <div class="row margin-none padding-bottom-lg">
                                    <div class="col-md-4 text-right"> Postcode :</div>
                                    <div class="col-md-8">
                                      <?php 
                                        echo $this->form->input('User.postcode',array(
                                          'label'=>false,
                                          'name'=>'postcode',
                                          'type'=>'text',
                                          'placeholder'=>'User postcode',
                                          'class'=>'form-control required',
                                          'div'=>'form-group'
                                        ));
                                      ?>
                                      <div class="input-sm">User Postcode</div>
                                    </div>
                                  </div>
                                  <div class="row margin-none padding-bottom-lg">
                                    <div class="col-md-4 text-right"> Website :</div>
                                    <div class="col-md-8">
                                      <?php 
                                        echo $this->form->input('User.webaddress',array(
                                          'label'=>false,
                                          'name'=>'webaddress',
                                          'type'=>'text',
                                          'placeholder'=>'User webaddress',
                                          'class'=>'form-control required',
                                          'div'=>'form-group'
                                        ));
                                      ?>
                                      <div class="input-sm">User webaddress</div>
                                    </div>
                                  </div>
                                  <div class="row margin-none padding-bottom-lg">
                                    <div class="col-md-4 text-right"> Contactus :</div>
                                    <div class="col-md-8">
                                      <?php 
                                        echo $this->form->input('User.contactus',array(
                                          'label'=>false,
                                          'name'=>'contactus',
                                          'type'=>'text',
                                          'placeholder'=>'User Contactus',
                                          'class'=>'form-control required',
                                          'div'=>'form-group'
                                        ));
                                      ?>
                                      <div class="input-sm">Contactus Url</div>
                                    </div>
                                  </div>
                                  <div class="row margin-none padding-bottom-lg">
                                    <div class="col-md-4 text-right"> Email :</div>
                                    <div class="col-md-8">
                                      <?php 
                                        echo $this->form->input('User.email',array(
                                          'label'=>false,
                                          'name'=>'email',
                                          'type'=>'text',
                                          'placeholder'=>'User email',
                                          'class'=>'form-control required',
                                          'div'=>'form-group'
                                        ));
                                      ?>
                                      <div class="input-sm">User email id</div>
                                    </div>
                                  </div>
                                  <div class="row margin-none padding-bottom-lg">
                                    <div class="col-md-4 text-right"> Rating :</div>
                                    <div class="col-md-8">
                                      <?php 
                                        echo $this->form->input('User.rating',array(
                                          'label'=>false,
                                          'name'=>'rating',
                                          'type'=>'text',
                                          'placeholder'=>'User rating in percentage',
                                          'class'=>'form-control required',
                                          'div'=>'form-group'
                                        ));
                                      ?>
                                      <div class="input-sm">User Rating</div>
                                    </div>
                                  </div>
                                  <div class="row margin-none padding-bottom-lg">
                                    <div class="col-md-4 text-right"> FB Likes :</div>
                                    <div class="col-md-8">
                                      <?php 
                                        echo $this->form->input('User.fb_likes',array(
                                          'label'=>false,
                                          'name'=>'fb_likes',
                                          'type'=>'text',
                                          'placeholder'=>'User FB likes',
                                          'class'=>'form-control',
                                          'div'=>'form-group'
                                        ));
                                      ?>
                                      <div class="input-sm">User FB Likes</div>
                                    </div>
                                  </div>
                                  <div class="row margin-none padding-bottom-lg">
                                    <div class="col-md-4 text-right"> Phone :</div>
                                    <div class="col-md-8">
                                      <?php 
                                        echo $this->form->input('User.phone',array(
                                          'label'=>false,
                                          'name'=>'phone',
                                          'type'=>'text',
                                          'placeholder'=>'User Phone',
                                          'class'=>'form-control',
                                          'div'=>'form-group'
                                        ));
                                      ?>
                                      <div class="input-sm">User Phone</div>
                                    </div>
                                  </div>
                                  <div class="row margin-none margin-top">
                                      <div class="col-md-4 text-right"> Address :</div>
                                      <div class="col-md-8">
                                          <?php 
                                            echo $this->form->input('User.address',array(
                                              'label'=>false,
                                              'name'=>'address',
                                              'type'=>'text',
                                              'placeholder'=>'Search address',
                                              'class'=>'form-control',
                                              'id'=>'searchgooglemap',
                                              'div'=>'form-group'
                                            ));
                                          ?>
                                          <div class="input-sm">Address searched from google map api</div>
                                      </div>
                                  </div>
                                  <div class="row margin-none margin-top">
                                      <div class="col-md-4 text-right"></div>
                                      <div class="col-md-4">
                                          <?php 
                                            echo $this->form->input('User.latitude',array(
                                              'label'=>false,
                                              'name'=>'latitude',
                                              'type'=>'text',
                                              'readonly'=>true,
                                              'placeholder'=>'Latitude',
                                              'class'=>'form-control required',
                                              'div'=>'form-group',
                                              'id'=>'latitude'
                                            )); ?>
                                            <div class="input-sm">Latitude for user</div>
                                      </div>
                                      <div class="col-md-4">
                                          <?php
                                            echo $this->form->input('User.longitude',array(
                                              'label'=>false,
                                              'name'=>'longitude',
                                              'type'=>'text',
                                              'readonly'=>true,
                                              'placeholder'=>'Longitude',
                                              'class'=>'form-control required',
                                              'div'=>'form-group',
                                              'id'=>'longitude'
                                            ));
                                          ?>
                                          <div class="input-sm">Longitude for user</div>
                                      </div>
                                  </div>
                                  <div class="row margin-none margin-top">
                                      <div class="col-md-4 text-right"></div>
                                      <div class="col-md-8">
                                          <?php 
                                            echo $this->form->input('User.city',array(
                                              'label'=>false,
                                              'name'=>'city',
                                              'type'=>'text',
                                              'readonly'=>true,
                                              'placeholder'=>'City',
                                              'class'=>'form-control required',
                                              'div'=>'form-group',
                                              'id'=>'city'
                                            )); ?>
                                            <div class="input-sm">City for user</div>
                                      </div>
                                  </div>
                                  <div class="row margin-none margin-top">
                                    <div class="col-md-4 text-right"></div>
                                    <div class="col-md-8 text-right">
                                        <div id="map"></div>
                                    </div>
                                  </div>
                                  <div class="row margin-none margin-top">
                                      <div class="col-md-4 text-right"></div>
                                      <div class="col-md-8">
                                          <?php 
                                            echo $this->form->input('User.state',array(
                                              'label'=>false,
                                              'name'=>'state',
                                              'type'=>'text',
                                              'placeholder'=>'State',
                                              'class'=>'form-control required',
                                              'div'=>'form-group'
                                            )); ?>
                                            <div class="input-sm">State for user</div>
                                      </div>
                                  </div>
                                  <div class="row margin-none margin-top">
                                      <div class="col-md-4 text-right"></div>
                                      <div class="col-md-8">
                                          <?php 
                                            echo $this->form->input('User.country',array(
                                              'label'=>false,
                                              'name'=>'country',
                                              'type'=>'text',
                                              'placeholder'=>'country',
                                              'class'=>'form-control required',
                                              'div'=>'form-group'
                                            )); ?>
                                            <div class="input-sm">country for user</div>
                                      </div>
                                  </div>
                                  <div class="row margin-none margin-top">
                                      <div class="col-md-4 text-right">Select Subservices :</div>
                                      <div class="col-md-8">
                                          <?php
                                          if(!empty($services)){
                                              foreach ($services as $key => $value) {
                                                  echo '<div>
                                                      <input type="checkbox" name="subservice[]" value="'.$value['service_id']."-".$value['subservice_id'].'" class="filled-in" />
                                                      <label>'.$value['subservice_name'].'</label>
                                                  </div>';
                                              }
                                          }
                                          ?>
                                      </div>
                                  </div>
                                  <div class="row margin-none padding-bottom-lg padding-top-lg margin-top-lg">
                                      <div class="col-md-4 text-right"></div>
                                      <div class="col-md-8">
                                          <button class="updateBtn" type="submit">Save</button>       
                                      </div>
                                  </div>
                              </div>
                            <?php echo $this->Form->end(); ?>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDKPCoTjN4-to35Dc6blid9FCqPb7mofOo&libraries=places&sensor=false"></script>
<script src="<?php echo $this->request->webroot; ?>admin_theme/plugins/daterangepicker/moment.min.js"></script>
<script src="<?php echo $this->request->webroot; ?>admin_theme/plugins/daterangepicker/daterangepicker.js"></script>
<script type="text/javascript">
    $('#reservation').daterangepicker({
      singleDatePicker: true,
      showDropdowns: true
    });
    var markersArray = [];
    google.maps.event.addDomListener(window, 'load', function () {
      var places = new google.maps.places.Autocomplete(document.getElementById('searchgooglemap'));
      google.maps.event.addListener(places, 'place_changed', function () {
          var place = places.getPlace();
          $('#searchgooglemap').val(place.formatted_address);
          $("#map").css("width", "100%");
          $("#map").css("height", "200px");
          var uluru = {lat: place.geometry.location.lat(), lng: place.geometry.location.lng()};
          var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 13,
            center: uluru
          });
          $('#latitude').val(place.geometry.location.lat());
          $('#longitude').val(place.geometry.location.lng());
          for (var i = 0; i < markersArray.length; i++ ) {
            markersArray[i].setMap(null);
          }
          markersArray.length = 0;
          var location = uluru;
          var marker = new google.maps.Marker({
              position: uluru, 
              map: map
          });
          markersArray.push(marker);
          codeLatLng(place.geometry.location.lat(), place.geometry.location.lng());
      });
    });
    function codeLatLng(lat, lng) {
      var latlng = new google.maps.LatLng(lat, lng);
      geocoder = new google.maps.Geocoder();
      geocoder.geocode({'latLng': latlng}, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          if (results[1]) {
            //find country name
            for (var i=0; i<results[0].address_components.length; i++) {
              for (var b=0;b<results[0].address_components[i].types.length;b++) {
                //there are different types that might hold a city admin_area_lvl_1 usually does in come cases looking for sublocality type will be more appropriate
                if (results[0].address_components[i].types[b] == "administrative_area_level_2") {
                  //this is the object you are looking for
                  city= results[0].address_components[i];
                  break;
                }
              }
            }
            $('#city').val(city.long_name);
          } else {
            alert("No city found");
          }
        } else {
          alert("Geocoder failed due to: " + status);
        }
      });
    }
    $(document).ready(function () {
        $("#adduser").validate({
            errorClass: 'error m-error',
            errorElement: 'small',
            rules: {
                "firstname": {
                    required: true
                },
                "postcode": {
                    required: true
                },
                "suburb": {
                    required: true
                },
                "latitude": {
                    required: true
                },
                "longitude": {
                    required: true
                },
                "rating": {
                    required: true,
                    number: true
                },
                "state": {
                    required: true
                },
                "city": {
                    required: true
                },
                "country": {
                    required: true
                },
                "email": {
                    required: true
                }
            },
            messages: {
                "firstname": {
                    remote: "Please add user firstname."
                },
                "postcode": {
                    remote: "Please add postcode."
                },
                "suburb": {
                    remote: "Please add suburb."
                },
                "city": {
                    remote: "Please add city."
                },
                "state": {
                    remote: "Please add state."
                },
                "country": {
                    remote: "Please add country."
                },
                "email": {
                    remote: "Please add email."
                },
                "rating": {
                    remote: "Please add rating."
                },
                "latitude": {
                    remote: "Pick latitude."
                },
                "longitude": {
                    remote: "Pick longitude."
                }
            }
        });
    });
</script>