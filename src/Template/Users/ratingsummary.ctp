<link rel="stylesheet" href="<?php echo $this->request->webroot;?>css/custom-with-material.css">

<section class="content-header margin-bottom-lg padding-left-none">
    <h1>
        User Rating
        <small>Summary</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">User Ratings Summary</li>
    </ol>
</section>
<div class="row margin-none">
  <div class="col-xs-12 padding-none">
    <section class="content padding-none"> 
        <div class="box">
            <div class="box-header">
                
            </div>
            <div class="box-body" style="overflow-y:auto;">
                <div class="row table-header-element">                        
                    <?php echo $this->element('top_filter'); ?>
                </div>
                <table id="example1" class="table table-hover">
                    <thead>
                        <tr>
                            <th>Sr. #</th>
                            <th>User ID</th>
                            <th>Image</th>
                            <th>Name</th>
                            <th>Average Rating</th>
                            <th>Total Ratings</th>
                            <th>Positive Rating</th>
                            <th>Neutral Rating</th>
                            <th>Total Value</th>
                            <th>Total Fee</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        $page = $this->Paginator->params()['page'];
                        $limit = $this->Paginator->params()['perPage'];
                        $i = ($page * $limit) - $limit + 1;
                        foreach($listing as $key=>$value){ ?>
                        <tr>
                            <td><?php echo $i++;?></td>
                            <td><?php echo $value['id'];?></td>
                            <td>
                                <div class="user-panel padding-none">
                                    <div class="image">
                                        <img class="img-circle" src="<?php echo $value['image'];?>"/>
                                    </div>
                                </div>
                            </td>
                            <td><?php echo $value['name'];?></td>
                            <td><?php echo $value['average_rating']."%";?></td>
                            <td><?php echo $value['total_rating'];?></td>
                            <td><?php echo $value['total_positive_rating'];?></td>
                            <td><?php echo $value['total_neutral_rating'];?></td>
                            <td><?php echo $value['total_value'];?></td>
                            <td><?php echo $value['total_fee'];?></td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <?php echo $this->element('pagination'); ?>
            </div>
        </div>
    </section>
  </div>
</div>