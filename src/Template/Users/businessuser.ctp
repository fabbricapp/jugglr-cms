<link rel="stylesheet" href="<?php echo $this->request->webroot;?>css/custom-with-material.css">

<section class="content-header margin-bottom-lg padding-left-none">
    <h1>
        Business
        <small>Users</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Business Users</li>
    </ol>
</section>
<div class="row margin-none">
	<div class="col-xs-12 padding-none">
		<section class="content padding-none">	
      		<div class="box">
      			<div class="box-header">
          			
        		</div>
        		<div class="box-body">
                <div class="row table-header-element">                        
                    <?php echo $this->element('top_filter'); ?>
                </div>
          			<table id="example2" class="table table-hover">
            			<thead>
              				<tr>
                          <th>#</th>
                          <th>UserID</th>
                          <th>Image</th>
                          <th>Name</th>
                          <th>Email</th>
                          <th>Postcode</th>
                          <th>Suburb</th>
                          <th>Webaddress</th>
                          <th>Phone</th>
                          <!--th>Contactus</th-->
                          <th>In/Active</th>
              				</tr>
            			</thead>
            			<tbody>
            				<?php 
                      $page = $this->Paginator->params()['page'];
                      $limit = $this->Paginator->params()['perPage'];
                      $i = ($page * $limit) - $limit + 1;
                      foreach($users as $key=>$value){ ?>
              				  <tr id="<?php echo $value['id'];?>">
                            <td><?php echo $i++;?></td>
                            <td><a href="<?php echo $this->Url->build(["controller" => "users", "action" => "editbusinessuser", "id" => $value['id']]); ?>"><?php echo $value['id'];?></a>
                            </td>
                        		<td>
                        			<div class="user-panel text-center padding-none">
                        				<div class="image">
                        					<img class="img-circle" src="<?php if($value['profile_image_thumb'] != ''){ echo $value['profile_image_thumb'];}else{ echo BASE_URL."/img/default_image.png";}?>"/>
                        				</div>
                        			</div>
                        		</td>
                        		<td><?php echo $value['firstname']." ".$value['lastname'];?></td>
                        		<td><?php echo $value['email'];?></td>
                            <td><?php echo $value['postcode'];?></td>
                        		<td><?php echo $value['suburb'];?></td>
                            <td><?php echo $value['webaddress'];?></td>
                            <td><?php echo $value['phone'];?></td>
                            <!--td><?php echo $value['contactus'];?></td-->
                        		<td>
                        			<div class="text-center">
                        				<div id="<?php echo $value['id'];?>" class="switch">
                                    <label>
                                      <input type="checkbox" <?php if($value['is_active'] == '1'){ echo 'checked="checked"';}?>>
                                      <span class="lever"></span>
                                    </label>
                                  </div>
                        			</div>
                        		</td>
              				</tr>
              				<?php } ?>
            			</tbody>
          			</table>
                <?php echo $this->element('pagination'); ?>
        		</div>
      		</div>
		</section>
	</div>
</div>

    
<script type="text/javascript">
jQuery(document).ready(function() {

    $('#example2').on("change", ".switch",function () {
        var user_id = $(this).attr('id');
        $.ajax({
          url: _ROOT+'users/updatebusinessuserstatus',
          type: 'POST',
          dataType: 'Json',
          data:{'user_id':user_id},
          success:function(data){
          }
        });
    });
});
</script>
<style type="text/css">
	.bootstrap-switch .bootstrap-switch-handle-on.bootstrap-switch-primary, .bootstrap-switch .bootstrap-switch-handle-off.bootstrap-switch-primary{background:#ffa300;}
	.pagination > .active > a, .pagination > .active > span, .pagination > .active > a:hover, .pagination > .active > span:hover, .pagination > .active > a:focus, .pagination > .active > span:focus{background-color:#ffa300;border-color:#ffa300;}
</style>
