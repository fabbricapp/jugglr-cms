<link rel="stylesheet" href="<?php echo $this->request->webroot;?>css/custom-with-material.css">
<!-- Morris chart -->
<link rel="stylesheet" href="<?php echo $this->request->webroot;?>admin_theme/plugins/morris/morris.css">
<section class="content-header margin-bottom-lg padding-left-none">
    <h1>
        Dashboard
        <small>Admin</small>
    </h1>
</section>
<!-- Small boxes (Stat box) -->
<div class="row margin-top-lg">
    <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-green">
            <div class="inner">
                <h3><?php echo $response['active_user'];?></h3>
                <p>Active User</p>
            </div>
            <div class="icon">
                <i class="ion ion-ios-people"></i>
            </div>
            <a href="../users/activeuser" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-yellow">
            <div class="inner">
                <h3><?php echo $response['pending_user'];?></h3>
                <p>Pending Verification</p>
            </div>
            <div class="icon">
                <i class="ion-ios-body-outline"></i>
            </div>
            <a href="../users/pendinguser" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-red">
            <div class="inner">
                <h3><?php echo $response['inactive_user'];?></sup></h3>
                <p>Inactive Users</p>
            </div>
            <div class="icon">
                <i class="ion ion-ios-close"></i>
            </div>
            <a href="../users/inactiveuser" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
    <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-aqua">
            <div class="inner">
                <h3><?php echo $response['current_task'];?></h3>
                <p>Current Tasks</p>
            </div>
            <div class="icon">
                <i class="ion-ios-list"></i>
            </div>
            <a href="../usertasks/tasklist" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="nav-tabs-custom">
          <ul class="nav nav-tabs pull-right">
            <li><a href="#sales-chart" data-toggle="tab">Sales</a></li>
            <li class="active"><a href="#bar-chart" data-toggle="tab">Offers</a></li>
            <li class="pull-left header"><i class="fa fa-inbox"></i> Services</li>
          </ul>
          <div class="tab-content no-padding">
            <div class="chart tab-pane active" id="sales-chart" style="position: relative; height: 300px;"></div>
            <div class="chart tab-pane active" id="bar-chart" style="position: relative; height: 300px;"></div>
          </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Task Chart</h3>
            </div>
            <div class="box-body chart-responsive padding-none">
                <div class="row margin-none">
                    <div class="col-md-9 col-sm-8">
                        <div class="chart" id="donut-chart" style="height: 300px; position: relative;"></div>
                    </div>
                    <div class="col-md-3 col-sm-4 padding-none">
                      <div class="pad box-pane-right bg-green" style="min-height: 280px">
                        <div class="description-block padding-lg">
                          <h5 class="description-header"><?php echo $response['current_task'];?></h5>
                          <span class="description-text">Current</span>
                        </div>
                        <div class="description-block padding-lg">
                          <h5 class="description-header"><?php echo $response['complete_task'];?></h5>
                          <span class="description-text">Completed</span>
                        </div>
                        <div class="description-block padding-lg">
                          <h5 class="description-header"><?php echo $response['cancelled_task'];?></h5>
                          <span class="description-text">Cancelled</span>
                        </div>
                      </div>
                    </div>
                  </div>
            </div>
          </div>
    </div>
    <div class="col-md-6">
        <div class="box box-primary">
          <div class="box-header">
            <i class="ion ion-clipboard"></i>
            <h3 class="box-title">Review Pending Offers</h3>
            <div class="box-tools pull-right">
              
            </div>
          </div>
          <div class="box-body">
            <ul class="todo-list">
              <?php if(!empty($offer_data)){?>
                <?php foreach($offer_data as $key=>$value){?>
                  <li>
                    <span class="handle">
                      <i class="fa fa-ellipsis-v"></i>
                      <i class="fa fa-ellipsis-v"></i>
                    </span>
                    Trans #<?php echo $value['id'];?><small class="label label-warning"> <?php echo $value['status'];?></small>
                    <span class="text"><?php echo $value['language'];?></span>
                  </li>
              <?php } 
                } ?>
            </ul>
          </div>
        </div>
    </div>
</div>

<!-- Morris.js charts -->
<script src="<?php echo $this->request->webroot;?>admin_theme/plugins/morris/raphael-min.js"></script>
<script src="<?php echo $this->request->webroot;?>admin_theme/plugins/morris/morris.min.js"></script>
<script>
    $( document ).ready(function() {
        "use strict";
        //DONUT CHART
        var cancel_task = <?php echo $response['cancelled_task'];?>;
        var current_task = <?php echo $response['current_task'];?>;
        var complete_task = <?php echo $response['complete_task'];?>;
        var donut = new Morris.Donut({
          element: 'donut-chart',
          resize: true,
          colors: ["#3c8dbc", "#f56954", "#00a65a"],
          data: [
            {label: "Current Tasks", value: current_task},
            {label: "Cancelled Tasks", value: cancel_task},
            {label: "Completed Tasks", value: complete_task}
          ],
          hideHover: 'auto'
        });
        // LINE CHART
        var saleJan = <?php echo $response['month_sales']['Jan'];?>;
        var saleFeb = <?php echo $response['month_sales']['Feb'];?>;
        var saleMar = <?php echo $response['month_sales']['Mar'];?>;
        var saleApr = <?php echo $response['month_sales']['Apr'];?>;
        var saleMay = <?php echo $response['month_sales']['May'];?>;
        var saleJun = <?php echo $response['month_sales']['Jun'];?>;
        var saleJul = <?php echo $response['month_sales']['Jul'];?>;
        var saleAug = <?php echo $response['month_sales']['Aug'];?>;
        var saleSep = <?php echo $response['month_sales']['Sep'];?>;
        var saleOct = <?php echo $response['month_sales']['Oct'];?>;
        var saleNov = <?php echo $response['month_sales']['Nov'];?>;
        var saleDec = <?php echo $response['month_sales']['Dec'];?>;
        var bar = new Morris.Bar({
          element: 'sales-chart',
          resize: true,
          data: [
            {y: 'Jan', a: saleJan},
            {y: 'Feb', a: saleFeb},
            {y: 'Mar', a: saleMar},
            {y: 'Apr', a: saleApr},
            {y: 'May', a: saleMay},
            {y: 'Jun', a: saleJun},
            {y: 'Jul', a: saleJul},
            {y: 'Aug', a: saleAug},
            {y: 'Sep', a: saleSep},
            {y: 'Oct', a: saleOct},
            {y: 'Nov', a: saleNov},
            {y: 'Dec', a: saleDec}
          ],
          barColors: ['#fd688b'],
          xkey: 'y',
          ykeys: ['a'],
          labels: ['Sales'],
          hideHover: 'auto'
        });
        //BAR CHART
        var Jan = <?php echo $finalservice[0];?>;
        var Feb = <?php echo $finalservice[1];?>;
        var Mar = <?php echo $finalservice[2];?>;
        var Apr = <?php echo $finalservice[3];?>;
        var May = <?php echo $finalservice[4];?>;
        var Jun = <?php echo $finalservice[5];?>;
        var Jul = <?php echo $finalservice[6];?>;
        var Aug = <?php echo $finalservice[7];?>;
        var Sep = <?php echo $finalservice[8];?>;
        var Oct = <?php echo $finalservice[9];?>;
        var Nov = <?php echo $finalservice[10];?>;
        var Dec = <?php echo $finalservice[11];?>;
        var ykey = <?php echo "[".implode(',',$serviceKey)."]";?>;
        var label = <?php echo "[".implode(',',$serviceValue)."]";?>;
        
        var bar = new Morris.Bar({
          element: 'bar-chart',
          resize: true,
          data: [
            Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec
          ],
          barColors: ['#ff1a53', '#ff4d79','#ff809f', '#fd4e77','#fd688b', '#fe84a0','#fe9ab1', '#feb3c5'],
          xkey: 'y',
          ykeys: ykey,
          labels: label,
          hideHover: 'auto'
        });
        $( "#sales-chart" ).removeClass( "active" );
    });
</script>