<div class="login-box">
    <div class="login-logo">
        <span class="logo-lg">
            <img height="30px" src="<?php echo $this->request->webroot;?>img/logo.png" alt="logo_image">
        </span>
        <b>Jugglr</b>Business Users
    </div>
    <?php if($data['token'] == ''){?>
        <div class="login-box-body">
            Invalid Link. Please request forgot password from mobile application.
        </div>
    <?php }elseif($data['token'] == 1){ ?>
        <div class="login-box-body">
            Congratulation, you password is successfully set.
        </div>
    <?php }else {?>
        <div class="login-box-body">
            <p class="login-box-msg">Set Password</p>
            <form action="../users/setpassword" method="post">
                <input type="hidden" name="id" class="form-control" value="<?php echo $data['id']?>">
                <div class="form-group has-feedback">
                    <input type="password" name="password" class="form-control" placeholder="Password">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="password" name="confirmpassword" class="form-control" placeholder="Confirm Password">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div class="row margin-none">
                    <div class="col-xs-4 padding-right-none pull-right">
                        <button type="submit" class="btn btn-loginpink btn-block btn-flat">Set Password</button>
                    </div>
                </div>
            </form>
        </div>
    <?php }?>
</div>