<link rel="stylesheet" href="<?php echo $this->request->webroot;?>css/custom-with-material.css">

<section class="content-header margin-bottom-lg padding-left-none">
    <h1>
        User
        <small>Summary</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">User Summary</li>
    </ol>
</section>
<div class="row margin-none">
  <div class="col-xs-12 padding-none">
    <section class="content padding-none"> 
        <div class="box">
            <div class="box-header">
                
            </div>
            <div class="box-body" style="overflow-y:auto;">
                <div class="row table-header-element">                        
                    <?php echo $this->element('top_filter'); ?>
                </div>
                <table id="example1" class="table table-hover">
                    <thead>
                        <tr>
                          <th>Sr. #</th>
                           <th><?php echo $this->Paginator->sort('Users.id', 'User ID'); ?></th>
                           <th>OS</th>
                           <th>Name</th>
                           <th>Email</th>
                           <th><?php echo $this->Paginator->sort('Users.postcode', 'Postcode'); ?></th>
                           <th><?php echo $this->Paginator->sort('Users.suburb', 'Suburb'); ?></th>
                           <th>City</th>
                           <th>State</th>
                           <th>Country</th>
                           <th>Registered At</th>
                           <th>Status</th>
                           <th>Verified By</th>
                           <th>Verified At</th>
                           <th>Total Transaction</th>
                           <th>Complete Transaction</th>
                           <th>Cancelled Transaction</th>
                           <th>Current Transaction</th>
                           <th>Paid Transaction</th>
                           <th>Paid Value</th>
                           <th>Total Fee</th>
                           <th>Average Completion Time</th>
                           <th>Average Payment Time</th>
                           <th>Postive Rating</th>
                           <th>Neutral Rating</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $page = $this->Paginator->params()['page'];
                        $limit = $this->Paginator->params()['perPage'];
                        $i = ($page * $limit) - $limit + 1; ?>
                        <?php foreach($users as $key=>$value){ ?>
                        <tr>
                            <td><?php echo $i++;?></td>
                            <td><?php echo $value['id'];?></td>
                            <td><?php echo $value['os'];?></td>
                            <td><?php echo $value['firstname'];?></td>
                            <td><?php echo $value['email'];?></td>
                            <td><?php echo $value['postcode'];?></td>
                            <td><?php echo $value['suburb'];?></td>
                            <td><?php echo $value['city'];?></td>
                            <td><?php echo $value['state'];?></td>
                            <td><?php echo $value['country'];?></td>
 			                      <td><?php echo $value['createdAt'];?></td>
                            <td><?php echo $value['status'];?></td>
                            <td>
                                <?php if($value['verified_by'] != 'admin' && $value['verified_by'] != 0){?>
                                    <a href="<?php echo $this->Url->build(["controller" => "users", "action" => "view", "id" => $value['verified_by']]); ?>"><?php echo $value['verified_by'];?></a>
                                <?php }else { echo $value['verified_by']; }?>
                            </td>
			                      <td><?php echo $value['verified_at'];?></td>
                            <td><?php echo $value['total_transaction'];?></td>
                            <td><?php echo $value['complete_transaction'];?></td>
                            <td><?php echo $value['cancelled_transaction'];?></td>
                            <td><?php echo $value['current_transaction'];?></td>
                            <td><?php echo $value['paid_transaction'];?></td>
                            <td><?php echo $value['paid_value_transaction'];?></td>
                            <td><?php echo $value['total_fee'];?></td>
                            <td><?php echo $value['average_time_completion'];?></td>
                            <td><?php echo $value['average_time_payment'];?></td>
                            <td><?php echo $value['positive_rating'];?></td>
                            <td><?php echo $value['neutral_rating'];?></td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <?php echo $this->element('pagination'); ?>
            </div>
        </div>
    </section>
  </div>
</div>
<script type="text/javascript">
jQuery(document).ready(function() {
    
});
</script>
