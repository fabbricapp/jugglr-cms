<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.8
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

/**
 * Configure paths required to find CakePHP + general filepath
 * constants
 */
require __DIR__ . '/paths.php';

// Use composer to load the autoloader.
require ROOT . DS . 'vendor' . DS . 'autoload.php';

/**
 * Bootstrap CakePHP.
 *
 * Does the various bits of setup that CakePHP needs to do.
 * This includes:
 *
 * - Registering the CakePHP autoloader.
 * - Setting the default application paths.
 */
require CORE_PATH . 'config' . DS . 'bootstrap.php';

// You can remove this if you are confident you have intl installed.
if (!extension_loaded('intl')) {
    trigger_error('You must enable the intl extension to use CakePHP.', E_USER_ERROR);
}

use Cake\Cache\Cache;
use Cake\Console\ConsoleErrorHandler;
use Cake\Core\App;
use Cake\Core\Configure;
use Cake\Core\Configure\Engine\PhpConfig;
use Cake\Core\Plugin;
use Cake\Datasource\ConnectionManager;
use Cake\Error\ErrorHandler;
use Cake\Log\Log;
use Cake\Network\Email\Email;
use Cake\Network\Request;
use Cake\Routing\DispatcherFactory;
use Cake\Utility\Inflector;
use Cake\Utility\Security;

/**
 * Read configuration file and inject configuration into various
 * CakePHP classes.
 *
 * By default there is only one configuration file. It is often a good
 * idea to create multiple configuration files, and separate the configuration
 * that changes from configuration that does not. This makes deployment simpler.
 */
try {
    Configure::config('default', new PhpConfig());
    Configure::load('app', 'default', false);
} catch (\Exception $e) {
    die($e->getMessage() . "\n");
}
Configure::write('Xapi', '89c83fcJKS5514OD59283fgf15150c6hhs444ishDF');
// Load an environment local configuration file.
// You can use a file like app_local.php to provide local overrides to your
// shared configuration.
//Configure::load('app_local', 'default');

// When debug = false the metadata cache should last
// for a very very long time, as we don't want
// to refresh the cache while users are doing requests.
if (!Configure::read('debug')) {
    Configure::write('Cache._cake_model_.duration', '+1 years');
    Configure::write('Cache._cake_core_.duration', '+1 years');
}

/**
 * Set server timezone to UTC. You can change it to another timezone of your
 * choice but using UTC makes time calculations / conversions easier.
 */
date_default_timezone_set('UTC');

/**
 * Configure the mbstring extension to use the correct encoding.
 */
mb_internal_encoding(Configure::read('App.encoding'));

/**
 * Set the default locale. This controls how dates, number and currency is
 * formatted and sets the default language to use for translations.
 */
ini_set('intl.default_locale', 'en_US');

/**
 * Register application error and exception handlers.
 */
$isCli = php_sapi_name() === 'cli';
if ($isCli) {
    (new ConsoleErrorHandler(Configure::read('Error')))->register();
} else {
    (new ErrorHandler(Configure::read('Error')))->register();
}

// Include the CLI bootstrap overrides.
if ($isCli) {
    require __DIR__ . '/bootstrap_cli.php';
}

/**
 * Set the full base URL.
 * This URL is used as the base of all absolute links.
 *
 * If you define fullBaseUrl in your config file you can remove this.
 */
if (!Configure::read('App.fullBaseUrl')) {
    $s = null;
    if (env('HTTPS')) {
        $s = 's';
    }

    $httpHost = env('HTTP_HOST');
    if (isset($httpHost)) {
        Configure::write('App.fullBaseUrl', 'http' . $s . '://' . $httpHost);
    }
    unset($httpHost, $s);
}

Cache::config(Configure::consume('Cache'));
ConnectionManager::config(Configure::consume('Datasources'));
Email::configTransport(Configure::consume('EmailTransport'));
Email::config(Configure::consume('Email'));
Log::config(Configure::consume('Log'));
Security::salt(Configure::consume('Security.salt'));

/**
 * The default crypto extension in 3.0 is OpenSSL.
 * If you are migrating from 2.x uncomment this code to
 * use a more compatible Mcrypt based implementation
 */
// Security::engine(new \Cake\Utility\Crypto\Mcrypt());

/**
 * Setup detectors for mobile and tablet.
 */
Request::addDetector('mobile', function ($request) {
    $detector = new \Detection\MobileDetect();
    return $detector->isMobile();
});
Request::addDetector('tablet', function ($request) {
    $detector = new \Detection\MobileDetect();
    return $detector->isTablet();
});

/**
 * Custom Inflector rules, can be set to correctly pluralize or singularize
 * table, model, controller names or whatever other string is passed to the
 * inflection functions.
 *
 * Inflector::rules('plural', ['/^(inflect)or$/i' => '\1ables']);
 * Inflector::rules('irregular', ['red' => 'redlings']);
 * Inflector::rules('uninflected', ['dontinflectme']);
 * Inflector::rules('transliteration', ['/å/' => 'aa']);
 */

/**
 * Plugins need to be loaded manually, you can either load them one by one or all of them in a single call
 * Uncomment one of the lines below, as you need. make sure you read the documentation on Plugin to use more
 * advanced ways of loading plugins
 *
 * Plugin::loadAll(); // Loads all plugins at once
 * Plugin::load('Migrations'); //Loads a single plugin named Migrations
 *
 */

Plugin::load('Migrations');

// Only try to load DebugKit in development mode
// Debug Kit should not be installed on a production system
if (Configure::read('debug')) {
    Plugin::load('DebugKit', ['bootstrap' => true]);
}

/**
 * Connect middleware/dispatcher filters.
 */
DispatcherFactory::add('Asset');
DispatcherFactory::add('Routing');
DispatcherFactory::add('ControllerFactory');


/*
 * Constant for alert/messages
 */
Configure::write('SUPPORT_EMAILID_TO', 'cs@jugglrapp.com');
Configure::write('SUPPORT_EMAILID_FROM', 'no-reply@jugglrapp.com');

Configure::write('USER_INACTIVE', 'Your account is inactive! Please contact support@jugglrapp.com for more');
Configure::write('SERVICEPROVIDER_NOT_FOUND', 'No service provider found');
Configure::write('SERVICEPROVIDER_SHORTLITSED_SUCCESSFULLY', 'SERVICEPROVIDER_SHORTLITSED_SUCCESSFULLY');
Configure::write('SERVICEPROVIDER_BLOCKED_SUCCESSFULLY', 'Service provider blocked!');
Configure::write('SERVICEPROVIDER_UNBLOCKED_SUCCESSFULLY', 'Service provider unblocked!');
Configure::write('SERVICEPROVIDER_ABUSED_SUCCESSFULLY', 'Service provider reported!');
Configure::write('CONTACT_SUPPORT', 'Your request has been sent to support!');
Configure::write('NO_CONNECTION_FOUND', 'No connections found');
Configure::write('CONNECTION_DELETED_SUCCESSFULLY', 'Service provider removed from your connections');
Configure::write('TRANSACTION_ADDED_SUCCESFULLY', 'Offer sent!');
Configure::write('TRANSACTION_UPDATED_SUCCESFULLY', 'Offer updated!');
Configure::write('TRANSACTION_CANCELLED_SUCCESFULLY', 'Offer cancelled');
Configure::write('TRANSACTION_RATED_SUCCESFULLY', 'TRANSACTION_RATED_SUCCESFULLY');
Configure::write('RATING_NOT_FOUND', 'No ratings available');
Configure::write('USER_SIGNOUT_SUCCESFULLY', 'USER_SIGNOUT_SUCCESFULLY');
Configure::write('MYPROFILE_UPDATED_SUCCESSFULLY', 'Details updated!');
Configure::write('DISCOVER_SETTING_UPATED_SUCCESSFULLY', 'Details updated!');
Configure::write('DISCOVER_SETTING_INACTIVE_UPATED_SUCCESSFULLY', "You're now 'inactive', other mums will not be able to contact you.  Change your setting to 'active' to get help from other mums.");
Configure::write('DISCOVER_SETTING_ACTIVE_UPATED_SUCCESSFULLY', "You're now 'active', other mums will be able to contact you. Search for other mums in the area to get the help you need.");
Configure::write('OFFER_ACCEPTED_SUCCESSFULLY', 'Offer accepted!');
Configure::write('CONFIRM_COMPLETE_SUCCESSFULLY', 'Thank you for confirming completion!');
Configure::write('CARD_ADDED_SUCCESSFULLY', 'Payment details saved!');
Configure::write('PREFERENCE_SETTING_UPATED_SUCCESSFULLY', 'Details updated!');
Configure::write('BANK_ACCOUNT_ADDED_SUCCESSFULLY', 'Payment details saved!');
Configure::write('USER_VERIFIED_SUCCESSFULLY', 'User verified successfully!');
Configure::write('USER_ALREADY_VERIFIED_SUCCESSFULLY', 'User already verified!');
Configure::write('COVERIMAGE_REMOVED_SUCCESSFULLY', 'Cover image removed!');
Configure::write('NEW_VERSION_RELEASE', 'New version is available, please update to new version!');
Configure::write('EMAIL_DELIVERED_SUPPORT', 'Thank you for your feedback, we will follow up to resolve the issue');
Configure::write('NO_SEARCH_FOUND', 'We did not find a match for your search... try again or change keyword!');
//Promotion message
Configure::write('PROMOTION_APPLIED', 'Promotion applied.');
Configure::write('PROMOTION_NOT_APPLIED', 'Promotion could not be applied.');
Configure::write('PROMOTION_ALREADY_USED_BY_USER', 'Promotion is already been used by you.');
Configure::write('AMOUNT_NOT_ENOUGH_TO_APPLY_THIS_PROMOTION', 'Amount is not enough to apply this promotion.');
Configure::write('SERVICE_DATE_NOT_BETWEEN_START_AND_END_DATES', 'Service date exceeds the promotion start and end dates.');
Configure::write('PROMOTION_NOT_APPLICABLE', 'You are not eligible for this promotion.');
//Marketplace message
Configure::write('MARKETPLACE_JOB_ADDED', 'Job posted in marketplace!');
Configure::write('MARKETPLACE_OFFER_ADDED', 'Offer posted in marketplace!');
Configure::write('JOB_UPDATED', 'Job Updated!');
Configure::write('JOB_SAVED', 'Job Saved!');
Configure::write('JOB_UNSAVED', 'Job Unsaved!');
Configure::write('JOB_OFFER_SENT', 'Offer Sent!');
Configure::write('JOB_OFFER_ALREADY_SENT', 'Offer already sent or Invalid request!');
Configure::write('NO_SUCH_JOB_EXISTS', "No such job exists!");

Configure::write('NOT_AUTHORIZED', "You are not authorized for this action!");
Configure::write('SUCCESS', "Success Request!");
Configure::write('ERROR', "Invalid Request!");
Configure::write('REQUEST_FAILURE', "Oops! something went wrong, try again!");

Configure::write('SIGNUP_SUCCESSFULLY', "Signup successfully!");
Configure::write('USER_ALREADY_REGISTERED', "User is already registered!");
Configure::write('USER_NOT_REGISTERED_YET', "User is not registered yet!");
Configure::write('USER_ALREADY_EXISTS', "It seems you have already signup!");
Configure::write('EMAIL_DONT_EXISTS', "We cannot find this mail address in our system!");

Configure::write('DEFAULT_ABOUT_ME', "Hi, I'm <username>.  Would love to connect with other mums in the area and see how we can help each other.  Message me if you think I can help!");
Configure::write('DEFAULT_ABOUT_BUSINESS', "Hi, we are <username>.  We operate in your local area and look forward to helping you out.  Please contact us to know more about our special offer for Jugglr mums!");

Configure::write('PASSWORD_INCORRECT', "Password is incorrect!");

Configure::write('PAYMENT_SERVICE_ID', 8);
Configure::write('PAYMENT_SUBSERVICE_ID', 28);

/* all third party credentails */

/* stripe */
Configure::write('STRIPE_CURRENCY', "usd");
/* US test 1-account */
//Configure::write('STRIPE_API_KEY', "sk_test_12A525FXvHh8T9RkZ6Q1XQiZ");
/* US test 2-account */
Configure::write('STRIPE_API_KEY', "sk_test_phHstmPJCsqAOcRgvb8y63wO");
/* AUS test 1-account */
//Configure::write('STRIPE_API_KEY', "sk_test_RdVeJ8ggztORrpIvMtcbB9YW");
//Configure::write('STRIPE_API_KEY', "sk_live_Z5vIE15HJ7f8OsXLxSAb5KiN");

/* Push Notification */
Configure::write('PUSH_NOTIFICATION_API_KEY', "AAAAneSeR8s:APA91bFZpMxbVDNBqYxyHeX6Gv_Uivdw_fwUwx9ljn3FuiUnhN1pcE0DIYYDCRAdpTOLF_G8xAFRLImXouiP48IyASVtApMBfJdXti5tMyWs7lABnZ6IBJ2Yxpus5FrSKgVMbY6K_zu0");
Configure::write('PUSH_NOTIFICATION_PATH_TO_FIREBASE', "https://fcm.googleapis.com/fcm/send");

/* firebase database */
Configure::write('FIREBASE_DATABASE', "https://jugglrtest.firebaseio.com/");
//Configure::write('FIREBASE_DATABASE', "https://jugglrapp.firebaseio.com/");

/* S3 AWS */
Configure::write('S3_KEY', "AKIAIZ7JYNNJTWYIFKVA");
Configure::write('S3_SECREAT_KEY', "6t/3Ar5RIfPvTB5YfZD1saFmhSqBHLbXzdsDb8oe");
Configure::write('S3_BUCKET', "jugglrimages");

/* Facebook keys */
Configure::write('FB_APP_ID', "1521527624820825");
Configure::write('FB_SECREAT_KEY', "9f37864d7b532a6dda040f869a668ead");






